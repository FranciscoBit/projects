﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessParkE.Global.Constants
{
    public static class Menus_Items
    {
        public static string GLOBAL_TITLE()
        {
            return "ParkE";
        }

        public static string GLOBAL_DESCRIPTION()
        {
            return "ParkE Description";
        }

        public static string GLOBAL_ADMINS()
        {
            return "Admins";
        }

        public static string GLOBAL_MODS()
        {
            return "Mods";
        }

    }

    public static class Menus_Sidebar_Admins
    {
        public static string GLOBAL_DASHBOARD()
        {
            return "Dashboard";
        }

        public static string GLOBAL_COMPANY_PROFILE()
        {
            return "Company Profile";
        }

        public static string GLOBAL_MY_PROFILE()
        {
            return "My Profile";
        }

        public static string GLOBAL_COMPANY_MODS()
        {
            return "Company Mods";
        }

        public static string GLOBAL_COMPANY_SENSORS()
        {
            return "Company Sensors";
        }

        public static string GLOBAL_SENSOR_ANALYTICS()
        {
            return "Sensor Analytics";
        }


    }

    public static class Menus_Sidebar_Mods
    {
        public static string GLOBAL_COMPANY_PROFILE()
        {
            return "Company Profile";
        }

        public static string GLOBAL_MY_PROFILE()
        {
            return "My Profile";
        }

        public static string GLOBAL_COMPANY_SENSORS()
        {
            return "Company Sensors";
        }

        public static string GLOBAL_SENSOR_ANALYTICS()
        {
            return "Sensor Analytics";
        }


    }



    public static class LandingPage
    {
        public static string GLOBAL_TITLE()
        {
            return "ParkE";
        }

        public static string GLOBAL_DESCRIPTION()
        {
            return "ParkE Description";
        }

        public static string OtherPages_About()
        {
            return "About";
        }

        public static string OtherPages_Services()
        {
            return "Services";
        }

        public static string OtherPages_Services_Sensors()
        {
            return "Sensors";
        }

        public static string OtherPages_Services_Management()
        {
            return "Management";
        }

        public static string OtherPages_Services_Spaces()
        {
            return "Spaces";
        }

        public static string OtherPages_Help()
        {
            return "Help";
        }

        public static string OtherPages_Download()
        {
            return "Download";
        }

        public static string OtherPages_Register()
        {
            return "Sign Up";
        }

        public static string OtherPages_Login()
        {
            return "Login";
        }

        public static string OtherPages_Businesses()
        {
            return "Businesses";
        }

        public static string OtherPages_Blog()
        {
            return "Blog";
        }

        public static string OtherPages_Privacy()
        {
            return "Privacy";
        }

        public static string OtherPages_Terms()
        {
            return "Terms";
        }

        public static string OtherPages_Guest()
        {
            return "Guest Admins";
        }




        public static string OtherPages_LearnMore()
        {
            return "Learn More";
        }



        public static string OtherPages_()
        {
            return "Mods";
        }

    }



}