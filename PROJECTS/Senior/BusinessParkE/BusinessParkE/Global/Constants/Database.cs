﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessParkE.Global.Constants
{
    public static class CONSTANT_QUERIES
    {
        // This query inserts data aquired by the sensors to the Azure Cloud (Data use for analytics)
        public static string DB_FromSensor2Cloud_Data()
        {
            return @"INSERT INTO dbo.Table_Device_Data values (@Device_ID, @Device_Key, @Company_Name, @Company_UniqueToken, @Device_State, @Device_Time, @Device_Date)";
        }

        // This query inserts data aquired by the sensors to the Azure Cloud (Data use for real time)
        public static string DB_FromSensor2Cloud_RealTime()
        {
            return @"INSERT INTO dbo.Table_Device_Data_RealTime values (@Device_ID, @Device_Key, @Company_Name, @Company_UniqueToken, @Device_State, @Device_Time, @Device_Date)";
        }

        // This query gets info from sql databae cloud and shows it to the web app
        public static string DB_FromCloud2Web_RealTime ()
        {
            return @"SELECT TOP 10 sensor_company, sensor_name, iot_device, sensor_state, EventProcessedUtcTime FROM dbo.TableParkECE";
        }

    }
}