﻿var markers = [];
var FirstSensor;

function initialize() {

    var beaches = [
        ['Bondi Beach', -33.890542, 151.274856, 1],
        ['Coogee Beach', -33.923036, 151.259052, 1],
        ['Cronulla Beach', -34.028249, 151.157507, 2],
        ['Manly Beach', -33.800101, 151.287478, 2],
        ['Maroubra Beach', -33.950198, 151.259302, 2]
    ];

    // MAIN COORDINATES
    var MainMapCoor = { lat: 25.7700569, lng: -80.36777889999996};


    // SENSOR COORDINATES

    var coordinateA = new google.maps.LatLng(25.769375, -80.367100);
    var coordinateB = new google.maps.LatLng(25.769375, -80.367150);
    var coordinateC = new google.maps.LatLng(25.769400, -80.367150);
    var coordinateD = new google.maps.LatLng(25.769400, -80.367100);

    var SensorsCoor =
        [
            { lat: coordinateA.lat(), lng: coordinateA.lng() },
            { lat: coordinateB.lat(), lng: coordinateB.lng() },
            { lat: coordinateC.lat(), lng: coordinateC.lng() },
            { lat: coordinateD.lat(), lng: coordinateD.lng() }
        ];

    



    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 18,
        center: MainMapCoor,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var MainMarker = new google.maps.Marker({
        position: MainMapCoor,
        map: map
    });






    var infowindow = new google.maps.InfoWindow();

    for (var i = 0; i < beaches.length; i++) {

        var newMarker = new google.maps.Marker({
            position: new google.maps.LatLng(beaches[i][1], beaches[i][2]),
            map: map,
            title: beaches[i][0]
        });

        google.maps.event.addListener(newMarker, 'click', (function (newMarker, i) {
            return function () {
                infowindow.setContent(beaches[i][0]);
                infowindow.open(map, newMarker);
            }
        })(newMarker, i));

        markers.push(newMarker);
    }

    var rectangle = new google.maps.Rectangle({
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#ffffff',
        fillOpacity: 0.35,
        map: map,
        bounds: new google.maps.LatLngBounds(
            new google.maps.LatLng(47.5204, -122.2047),
            new google.maps.LatLng(47.6139, -122.1065))
    });

    FirstSensor = new google.maps.Polygon({
            map: map,
            paths: SensorsCoor,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35
        });


    



}

initialize();









