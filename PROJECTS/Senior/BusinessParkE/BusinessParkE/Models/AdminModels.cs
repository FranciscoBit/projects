﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace BusinessParkE.Models
{
    public class ViewModel
    {
        public IEnumerable<Model_User_Details> UserDetails { get; set; }

        public IEnumerable<Model_Company_Table> CompanyDetails { get; set; }

        public IEnumerable<Model_Sensor_Details_Long> SensorDetails { get; set; }

        public IEnumerable<Model_Device_Details_Long> DeviceDetails { get; set; }

        public IEnumerable<Model_Device_RealTime> RealTimeDetails { get; set; }

        public IEnumerable<Model_ThingSpeak_Details> ThingSpeakDetails { get; set; }

    }

    // SUCCESS
    public class Model_User_Details
    {
        public string Name { get; set; }
        public string ObjectId { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }


        public List<Model_User_Details> Get_User_Details()
        {
            List<Model_User_Details> model = new List<Model_User_Details>();

            Model_User_Details UserDetailsClass = new Model_User_Details();

            UserDetailsClass.Name = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Name).Value.ToString();
            UserDetailsClass.ObjectId = ClaimsPrincipal.Current.FindFirst("http://schemas.microsoft.com/identity/claims/objectidentifier").Value.ToString();
            UserDetailsClass.GivenName = ClaimsPrincipal.Current.FindFirst(ClaimTypes.GivenName).Value.ToString();
            UserDetailsClass.Surname = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Surname).Value.ToString();
            UserDetailsClass.Email = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Email).Value.ToString();
            

            model.Add(UserDetailsClass);

            return model;
        }

        public string Get_User_Email()
        {
            string UserEmail = ClaimsPrincipal.Current.FindFirst(ClaimTypes.Email).Value.ToString();
            return UserEmail;
        }



    }

    // SUCCESS
    public class Model_Company_Table
    {
        public string CompanyName { get; set; }
        public string CompanyEmail { get; set; }
        public string BlobUrl { get; set; }
        public string TableNameDevice { get; set; }
        public string TableNameSensor { get; set; }
        public string ThingSpeakIframe { get; set; }
        public string MapsLat { get; set; }
        public string MapsLo { get; set; }
        public string MapsName { get; set; }

        public string EmailTruth { get; set; }

        public bool Get_Company_Email(string companyEmail)
        {
            int result = 0;

            List<Model_Company_Table> model = new List<Model_Company_Table>();
            Model_Company_Table CompanyDetailsClass = new Model_Company_Table();

            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "myparkeserver.database.windows.net";
            builder.UserID = "AdminParkE";
            builder.Password = "MySqlPass1";
            builder.InitialCatalog = "ParkEDB";

            using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
            {
                connection.Open();

                StringBuilder sb = new StringBuilder();
                sb.Append("SELECT count(*) ");
                sb.Append("FROM [dbo].TableCompany ");
                sb.Append("WHERE company_email = @company_email ");

                String sql = sb.ToString();

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("company_email", companyEmail);
                    result = (int)command.ExecuteScalar();
                    connection.Close();
                }

            }

            if (result > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<Model_Company_Table> Get_Company_Details(bool EmailsMatchTruth, string UserEmail)
        {

            if (EmailsMatchTruth == true)
            {
                List<Model_Company_Table> model = new List<Model_Company_Table>();
                Model_Company_Table CompanyDetailsClass = new Model_Company_Table();

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "parkeserver.database.windows.net";
                builder.UserID = "AdminParkE";
                builder.Password = "MySqlPass1";
                builder.InitialCatalog = "ParkEDB";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT company_name, company_email, blob_url, table_name_device, table_name_sensors, thingspeak_iframes, maps_lat, maps_lo, maps_name ");
                    sb.Append("FROM [dbo].TableCompany ");
                    sb.Append("WHERE company_email = @company_email");

                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("company_name", UserEmail);
                        command.Parameters.AddWithValue("company_email", UserEmail);
                        command.Parameters.AddWithValue("blob_url", UserEmail);
                        command.Parameters.AddWithValue("table_name_device", UserEmail);
                        command.Parameters.AddWithValue("table_name_sensors", UserEmail);
                        command.Parameters.AddWithValue("thingspeak_iframes", UserEmail);
                        command.Parameters.AddWithValue("maps_lat", UserEmail);
                        command.Parameters.AddWithValue("maps_lo", UserEmail);
                        command.Parameters.AddWithValue("maps_name", UserEmail);

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CompanyDetailsClass.CompanyName = reader["company_name"].ToString();
                                CompanyDetailsClass.CompanyEmail = reader["company_email"].ToString();
                                CompanyDetailsClass.BlobUrl = reader["blob_url"].ToString();
                                CompanyDetailsClass.TableNameDevice = reader["table_name_device"].ToString();
                                CompanyDetailsClass.TableNameSensor = reader["table_name_sensors"].ToString();
                                CompanyDetailsClass.ThingSpeakIframe = reader["thingspeak_iframes"].ToString();
                                CompanyDetailsClass.MapsLat = reader["maps_lat"].ToString();
                                CompanyDetailsClass.MapsLo = reader["maps_lo"].ToString();
                                CompanyDetailsClass.MapsName = reader["maps_name"].ToString();

                                model.Add(CompanyDetailsClass);
                            }

                            reader.Close();
                            connection.Close();
                        }
                    }


                }


                return model;
            }
            else
            {

                List<Model_Company_Table> model = new List<Model_Company_Table>();
                Model_Company_Table CompanyDetailsClass = new Model_Company_Table();

                CompanyDetailsClass.CompanyName = "NULL";
                CompanyDetailsClass.CompanyEmail = "NULL";
                CompanyDetailsClass.BlobUrl = "NULL";
                CompanyDetailsClass.TableNameDevice = "NULL";
                CompanyDetailsClass.TableNameSensor = "NULL";
                CompanyDetailsClass.ThingSpeakIframe = "NULL";
                CompanyDetailsClass.MapsLat = "NULL";
                CompanyDetailsClass.MapsLo = "NULL";
                CompanyDetailsClass.MapsName = "NULL";

                model.Add(CompanyDetailsClass);


                return model;
            }


            




                
        }



    }

    // SUCCESS
    public class Model_Sensor_Details_Long
    {
        public string Lat1 { get; set; }
        public string Lat2 { get; set; }
        public string Lat3 { get; set; }
        public string Lat4 { get; set; }
        public string Lo1 { get; set; }
        public string Lo2 { get; set; }
        public string Lo3 { get; set; }
        public string Lo4 { get; set; }
        public string TagName { get; set; }
        public string PlaceName { get; set; }


        public List<Model_Sensor_Details_Long> Get_Sensor_Details_Long(bool EmailsMatchTruth, string CompanyTableSensor)
        {

            if (EmailsMatchTruth == true)
            {
                List<Model_Sensor_Details_Long> model = new List<Model_Sensor_Details_Long>();
                

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "myparkeserver.database.windows.net";
                builder.UserID = "AdminParkE";
                builder.Password = "MySqlPass1";
                builder.InitialCatalog = "ParkEDB";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT TOP 20 Lat1, Lat2, Lat3, Lat4, Lo1, Lo2, Lo3, Lo4, TagName, PlaceName ");
                    sb.Append("FROM " + CompanyTableSensor);

                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Model_Sensor_Details_Long SensorDetailsClass = new Model_Sensor_Details_Long();

                                SensorDetailsClass.Lat1 = reader["Lat1"].ToString();
                                SensorDetailsClass.Lat2 = reader["Lat2"].ToString();
                                SensorDetailsClass.Lat3 = reader["Lat3"].ToString();
                                SensorDetailsClass.Lat4 = reader["Lat4"].ToString();
                                SensorDetailsClass.Lo1 = reader["Lo1"].ToString();
                                SensorDetailsClass.Lo2 = reader["Lo2"].ToString();
                                SensorDetailsClass.Lo3 = reader["Lo3"].ToString();
                                SensorDetailsClass.Lo4 = reader["Lo4"].ToString();
                                SensorDetailsClass.TagName = reader["TagName"].ToString();
                                SensorDetailsClass.PlaceName = reader["PlaceName"].ToString();

                                model.Add(SensorDetailsClass);
                            }

                            reader.Close();
                            connection.Close();
                        }
                    }
                }

                return model;
            }
            else
            {
                List<Model_Sensor_Details_Long> model = new List<Model_Sensor_Details_Long>();
                Model_Sensor_Details_Long SensorDetailsClass = new Model_Sensor_Details_Long();

                SensorDetailsClass.Lat1 = "NULL";
                SensorDetailsClass.Lat2 = "NULL";
                SensorDetailsClass.Lat3 = "NULL";
                SensorDetailsClass.Lat4 = "NULL";
                SensorDetailsClass.Lo1 = "NULL";
                SensorDetailsClass.Lo2 = "NULL";
                SensorDetailsClass.Lo3 = "NULL";
                SensorDetailsClass.Lo4 = "NULL";
                SensorDetailsClass.TagName = "NULL";
                SensorDetailsClass.PlaceName = "NULL";

                model.Add(SensorDetailsClass);


                return model;

            }



            

        }



    }

    // SUCCESS
    public class Model_Device_Details_Long
    {
        public string SensorCompany { get; set; }
        public string SensorName { get; set; }
        public string SensorState { get; set; }
        public string IoTDeviceId { get; set; }
        public string IoTCompanyEmail { get; set; }
        public string EventProcessedUtcTime { get; set; }
        public string IoTHub { get; set; }
        public string PartitionId { get; set; }
        public string EventEnqueuedUtcTime { get; set; }


        public List<Model_Device_Details_Long> Get_Device_Details_Long(bool EmailsMatchTruth, string CompanyTableSensor)
        {

            if (EmailsMatchTruth == true)
            {
                List<Model_Device_Details_Long> model = new List<Model_Device_Details_Long>();
                

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "myparkeserver.database.windows.net";
                builder.UserID = "AdminParkE";
                builder.Password = "MySqlPass1";
                builder.InitialCatalog = "ParkEDB";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT sensor_company, sensor_name, sensor_state, iot_device_id, iot_company_email, EventProcessedUtcTime, IoTHub, PartitionId, EventEnqueuedUtcTime ");
                    sb.Append("FROM " + CompanyTableSensor);

                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Model_Device_Details_Long DeviceDetailsClass = new Model_Device_Details_Long();

                                DeviceDetailsClass.SensorCompany = reader["sensor_company"].ToString();
                                DeviceDetailsClass.SensorName = reader["sensor_name"].ToString();
                                DeviceDetailsClass.SensorState = reader["sensor_state"].ToString();
                                DeviceDetailsClass.IoTDeviceId = reader["iot_device_id"].ToString();
                                DeviceDetailsClass.IoTCompanyEmail = reader["iot_company_email"].ToString();
                                DeviceDetailsClass.EventProcessedUtcTime = reader["EventProcessedUtcTime"].ToString();
                                DeviceDetailsClass.IoTHub = reader["IoTHub"].ToString();
                                DeviceDetailsClass.PartitionId = reader["PartitionId"].ToString();
                                DeviceDetailsClass.EventEnqueuedUtcTime = reader["EventEnqueuedUtcTime"].ToString();

                                model.Add(DeviceDetailsClass);
                            }

                            reader.Close();
                            connection.Close();
                        }
                    }
                }

                return model;
            }
            else
            {
                List<Model_Device_Details_Long> model = new List<Model_Device_Details_Long>();
                Model_Device_Details_Long DeviceDetailsClass = new Model_Device_Details_Long();

                DeviceDetailsClass.SensorCompany = "NULL";
                DeviceDetailsClass.SensorName = "NULL";
                DeviceDetailsClass.SensorState = "NULL";
                DeviceDetailsClass.IoTDeviceId = "NULL";
                DeviceDetailsClass.IoTCompanyEmail = "NULL";
                DeviceDetailsClass.EventProcessedUtcTime = "NULL";
                DeviceDetailsClass.IoTHub = "NULL";
                DeviceDetailsClass.PartitionId = "NULL";
                DeviceDetailsClass.EventEnqueuedUtcTime = "NULL";

                model.Add(DeviceDetailsClass);


                return model;

            }





        }



    }


    // SUCCESS
    public class Model_Device_RealTime
    {
        public string SensorCompany { get; set; }
        public string SensorName { get; set; }
        public string SensorState { get; set; }

        public string SensorDate { get; set; }
        public string SensorTime { get; set; }



        public string GetStringFromBlobAsync(string BlobUrl)
        {

            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(BlobUrl);

                Dictionary<string, object> desiObjResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                string myJsonString = desiObjResult["device_data_sensors_fiu_ece"].ToString();

                return myJsonString;


            }
        }



        public List<Model_Device_RealTime> Get_RealTime_Details_Long(string StrFromBlob, bool EmailsMatchTruth)
        {

            if (EmailsMatchTruth == true)
            {
                List<Model_Device_RealTime> model = new List<Model_Device_RealTime>();


                List<MyDevice_Data_New> bindJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MyDevice_Data_New>>(StrFromBlob);

                for (int i = 0; i < bindJsonData.Count; i++)
                {
                    Model_Device_RealTime RealTimeClass = new Model_Device_RealTime();

                    RealTimeClass.SensorCompany = bindJsonData[i].sensor_company;
                    RealTimeClass.SensorName = bindJsonData[i].sensor_name;
                    RealTimeClass.SensorState = bindJsonData[i].sensor_state;
                    RealTimeClass.SensorDate = DateTime.Now.ToString();


                    model.Add(RealTimeClass);


                }
                return model;
            }
            else
            {
                List<Model_Device_RealTime> model = new List<Model_Device_RealTime>();

                Model_Device_RealTime RealTimeClass = new Model_Device_RealTime();


                RealTimeClass.SensorCompany = "NULL";
                RealTimeClass.SensorName = "NULL";
                RealTimeClass.SensorState = "NULL";
                RealTimeClass.SensorDate = "NULL";
                RealTimeClass.SensorTime = "NULL";

                model.Add(RealTimeClass);
                return model;
            }


            



            
        }



    }


    // SUCCESS
    [DataContract]
    public class MyDevice_Data_New
    {
        public string _code = string.Empty;

        [DataMember]
        public string sensor_company
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_name
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_state
        {
            get
            {
                return _code.Trim();
            }
            set
            {
                _code = value;
            }
        }
    }


    // SUCCESS
    public class Model_ThingSpeak_Details
    {
        public string SensorIframe { get; set; }
        public string SensorName { get; set; }

        public List<Model_ThingSpeak_Details> Get_ThingSpeak_Details(bool EmailsMatchTruth, string CompanyTableThingSpeak)
        {
            if (EmailsMatchTruth == true)
            {
                List<Model_ThingSpeak_Details> model = new List<Model_ThingSpeak_Details>();


                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "myparkeserver.database.windows.net";
                builder.UserID = "AdminParkE";
                builder.Password = "MySqlPass1";
                builder.InitialCatalog = "ParkEDB";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    StringBuilder sb = new StringBuilder();
                    sb.Append("SELECT SensorName, SensorIframe ");
                    sb.Append("FROM " + CompanyTableThingSpeak);

                    String sql = sb.ToString();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Model_ThingSpeak_Details ThingSpeakDetailsClass = new Model_ThingSpeak_Details();

                                ThingSpeakDetailsClass.SensorName = reader["SensorName"].ToString();
                                ThingSpeakDetailsClass.SensorIframe = reader["SensorIframe"].ToString();

                                model.Add(ThingSpeakDetailsClass);
                            }

                            reader.Close();
                            connection.Close();
                        }
                    }
                }

                return model;
            }
            else
            {
                List<Model_ThingSpeak_Details> model = new List<Model_ThingSpeak_Details>();
                Model_ThingSpeak_Details ThingSpeakDetailsClass = new Model_ThingSpeak_Details();

                ThingSpeakDetailsClass.SensorIframe = "NULL";
                ThingSpeakDetailsClass.SensorName = "NULL";

                model.Add(ThingSpeakDetailsClass);


                return model;
            }



        }


    }




}