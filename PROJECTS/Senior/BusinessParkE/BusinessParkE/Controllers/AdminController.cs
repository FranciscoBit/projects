﻿using BusinessParkE.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace BusinessParkE.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [Authorize]
        public ActionResult Dashboard()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_DASHBOARD() + " - ParkE";

            ViewModel mymodel = new ViewModel();

            // SETUP
            Model_User_Details myUserDetails = new Model_User_Details();
            Model_Company_Table myCompanyDetails = new Model_Company_Table();

            mymodel.UserDetails = myUserDetails.Get_User_Details();

            bool emailTruth = myCompanyDetails.Get_Company_Email(myUserDetails.Get_User_Email());
            mymodel.CompanyDetails = myCompanyDetails.Get_Company_Details(emailTruth, myUserDetails.Get_User_Email());


            // GET COMPANY VALUES
            string tableNameDevice, TableNameSensor, mystring;
            string BlobUrl = "";
            Model_Device_Details_Long myDeviceDetails = new Model_Device_Details_Long();
            Model_Device_RealTime myRealTimeDetails = new Model_Device_RealTime();
            Model_Sensor_Details_Long mySensorDetails = new Model_Sensor_Details_Long();


            foreach (var item in mymodel.CompanyDetails)
            {
                
                

                // This is to rule out the exception thrown
                string test = "https://myparkestorage.blob.core.windows.net/myparkecontainer/blob_ece_data.json";
                if (BlobUrl == "")
                {
                    mystring = myRealTimeDetails.GetStringFromBlobAsync(test);
                    TableNameSensor = item.TableNameSensor;

                    mymodel.RealTimeDetails = myRealTimeDetails.Get_RealTime_Details_Long(mystring, emailTruth);
                    mymodel.SensorDetails = mySensorDetails.Get_Sensor_Details_Long(emailTruth, TableNameSensor);
                }
                else
                {
                    BlobUrl = item.BlobUrl;
                    mystring = myRealTimeDetails.GetStringFromBlobAsync(BlobUrl);
                    TableNameSensor = item.TableNameSensor;

                    mymodel.RealTimeDetails = myRealTimeDetails.Get_RealTime_Details_Long(mystring, emailTruth);
                    mymodel.SensorDetails = mySensorDetails.Get_Sensor_Details_Long(emailTruth, TableNameSensor);
                }
                


                

                //mymodel.DeviceDetails = myDeviceDetails.Get_Device_Details_Long(emailTruth, tableNameDevice);
            }

            Response.AddHeader("Refresh", "10");

            return View(mymodel);
        }

        [Authorize]
        public ActionResult Admin()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_MY_PROFILE() + " - ParkE";

            ViewModel mymodel = new ViewModel();

            // SETUP
            Model_User_Details myUserDetails = new Model_User_Details();
            Model_Company_Table myCompanyDetails = new Model_Company_Table();

            mymodel.UserDetails = myUserDetails.Get_User_Details();

            bool emailTruth = myCompanyDetails.Get_Company_Email( myUserDetails.Get_User_Email());
            mymodel.CompanyDetails = myCompanyDetails.Get_Company_Details(emailTruth, myUserDetails.Get_User_Email());


            // GET COMPANY VALUES
            string tableNameDevice, TableNameSensor;
            Model_Sensor_Details_Long mySensorDetails = new Model_Sensor_Details_Long();
            Model_Device_Details_Long myDeviceDetails = new Model_Device_Details_Long();


            foreach (var item in mymodel.CompanyDetails)
            {
                TableNameSensor = item.TableNameSensor;
                tableNameDevice = item.TableNameDevice;

                mymodel.SensorDetails = mySensorDetails.Get_Sensor_Details_Long(emailTruth, TableNameSensor);
                mymodel.DeviceDetails = myDeviceDetails.Get_Device_Details_Long(emailTruth, tableNameDevice);
            }


            //string SensorTableName = mySensorDetails.Get_Sensor_Table_Name(myUserDetails.Get_User_Email());
            //ViewBag.PageTitle = SensorTableName;
            //mymodel.CompanyDetails = myCompanyDetails.Get_Company_Email(myUserDetails.Get_User_Email());

            return View(mymodel);
        }

        [Authorize]
        public ActionResult Analytic()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_SENSOR_ANALYTICS() + " - ParkE";

            ViewModel mymodel = new ViewModel();

            // SETUP
            Model_User_Details myUserDetails = new Model_User_Details();
            Model_Company_Table myCompanyDetails = new Model_Company_Table();

            mymodel.UserDetails = myUserDetails.Get_User_Details();

            bool emailTruth = myCompanyDetails.Get_Company_Email(myUserDetails.Get_User_Email());
            mymodel.CompanyDetails = myCompanyDetails.Get_Company_Details(emailTruth, myUserDetails.Get_User_Email());

            // GET COMPANY VALUES
            string tableThingspeak;
            Model_ThingSpeak_Details myThingSpeakDetails = new Model_ThingSpeak_Details();


            foreach (var item in mymodel.CompanyDetails)
            {
                tableThingspeak = item.ThingSpeakIframe;

                mymodel.ThingSpeakDetails = myThingSpeakDetails.Get_ThingSpeak_Details(emailTruth, tableThingspeak);
            }


            return View(mymodel);
        }



        [Authorize]
        public ActionResult Sensor()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_COMPANY_SENSORS() + " - ParkE";


            ViewModel mymodel = new ViewModel();

            // SETUP
            Model_User_Details myUserDetails = new Model_User_Details();
            Model_Company_Table myCompanyDetails = new Model_Company_Table();

            mymodel.UserDetails = myUserDetails.Get_User_Details();

            bool emailTruth = myCompanyDetails.Get_Company_Email(myUserDetails.Get_User_Email());
            mymodel.CompanyDetails = myCompanyDetails.Get_Company_Details(emailTruth, myUserDetails.Get_User_Email());


            // GET COMPANY VALUES
            string tableNameDevice, TableNameSensor;
            Model_Device_Details_Long myDeviceDetails = new Model_Device_Details_Long();


            foreach (var item in mymodel.CompanyDetails)
            {
                tableNameDevice = item.TableNameDevice;

                mymodel.DeviceDetails = myDeviceDetails.Get_Device_Details_Long(emailTruth, tableNameDevice);
            }




            return View(mymodel);
        }

    }
}