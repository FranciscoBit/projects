﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace UwpParkE.Code
{
    /*
     * {
     *      "device_data_sensors_fiu_ece": 
     *      [
     *          {
     *              "sensor_company": "FIU ECE",
         *          "sensor_name": "my sensor name" ,
         *          "sensor_state": "my sensor state" 
         *          
     *          },
     *          ....
     *      ]
     * }
     */


    /*
     * This is the JSON file data contract that comes from Sensors --> Arduino --> RBP3
     * It has the Sensor Company, Sensor Name, and Sensor State
    */
    [DataContract]
    public class Device_Data
    {
        public string _code = string.Empty;

        [DataMember]
        public string sensor_company
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_name
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_state
        {
            get
            {
                return _code.Trim();
            }
            set
            {
                _code = value;
            }
        }
    }
}
