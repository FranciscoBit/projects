﻿using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows;
using Windows.ApplicationModel.Core;
using Windows.UI.Xaml.Controls;


using Microsoft.WindowsAzure; // Namespace for CloudConfigurationManager
using Microsoft.WindowsAzure.Storage; // Namespace for CloudStorageAccount
using Microsoft.WindowsAzure.Storage.Blob; // Namespace for Blob storage types


namespace UwpParkE.Code
{

    /*
     * This class is used to get json file from local location and sent it to IoT Hub
    */
    public class ParseJsonFile
    {
        /*
         * Azure IoT Hub Information 
         */
        private const string IotHubUri = "IoTParkE.azure-devices.net";
        private const string DeviceKey = "1SIv4J7kpoaQa+xTczVlsJzcjqp0m8Ag1Dn+JhBcsYg=";
        private const string DeviceId = "myFirstDevice";


        private static DeviceClient TheDeviceClient = null;


        /*
         * 1. This method reads the json files found inside specfic location in the rastberry pi
         * 2. Then it parses the data and returns the value in a string
         */
        public static string readJson()
        {
            // Will have to rename file path
            // Location: UWPParkE\UwpParkE\UwpParkE\bin\x86\Debug\AppX\MyData\device_data.json
            string MyFilePath = Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, "MyData/frequent_device_data_sensors_fiu_ece.json");
            using (StreamReader MyFile = File.OpenText(MyFilePath))
            {
                var myJSON = MyFile.ReadToEnd();

                Dictionary<string, object> desiObjResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(myJSON);
                string myJsonString = desiObjResult["device_data_sensors_fiu_ece"].ToString();

                return myJsonString;
            }
        }


        /*
         * 1. This method gets the string from previous method
         * 2. Then it desializes the string and converts it into a List<Device_Data> object
         * 3. We then loop throght all the information found
         * 4. Once we do this information will be send to the IoT Hub (to specifc device) and wait specifc time before doing it again
         * 
         * These steps are not included in the program but it will then do the following:
         * 5. By using Stream Analytics it will be configure the following way
         * 6. Input is from IoT Hub and specifc device
         * 7. Output is to specific SQL Server, Database, and Table
         * 8. The Query Function serves as the middleman between the input and output
         * 9. The query must be made so only specifc info goes to specifc tables that we want to use
         * 10. Once it connects succesfully the JSON goes directly to our Database Tables along with other information
         */
        public static async void SendDeviceToCloudMessagesAsync(string MyJsonString)
        {
                // We bind the data with json
                List<Device_Data> bindJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Device_Data>>(MyJsonString);

                if (TheDeviceClient == null)
                {
                TheDeviceClient = DeviceClient.Create(IotHubUri,
                      new DeviceAuthenticationWithRegistrySymmetricKey(
                       DeviceId, DeviceKey), TransportType.Http1);
                }

                for (int i = 0; i < bindJsonData.Count; i++)
                {
                    var telemetryDataPoint = new
                    {
                        iot_device = DeviceId,
                        sensor_company = bindJsonData[i].sensor_company,
                        sensor_name = bindJsonData[i].sensor_name,
                        sensor_state = bindJsonData[i].sensor_state
                    };

                    var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                    var message = new Message(Encoding.ASCII.GetBytes(messageString));
                    
                    await TheDeviceClient.SendEventAsync(message);

                    // If we want to wait couple of seconds till we do task again
                    //await Task.Delay(10000);
                }

            //CoreApplication.Exit();









        }


    }
}
