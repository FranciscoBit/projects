﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace UwpParkE.Code
{
    class StorageAzureBlob
    {

        /*
         * Azure Storage Blob Information 
         */
        private const string StorageAccountName = "storageparke";
        private const string AccessKey = "9QUDlF1aW/n/uVuVpCBsWJ02iFJS2iGXqlm1gxS4O2e2TLwUgInuF8zBa9nzmuD3ISVg7zlTFxzIM/6pHp18mA==";
        private const string PolicyName = "testpolicy";
        private const string ContainerName = "containerecesensors";


        /*
         * 1. This method sends data from JSON file to the Storage Blob
         * 2. We start by first establishing a connection string
         * 3. We then look at the file path inside the RBP3 where JSON file was stored by the Arduino
         * 4. Once we have this we create a storage client where we want to send the file to
         * 5. We name the specifc container where the faile will be placed
         * 6. We name the file however we want (if another file of same name is found it will be re-written)
         * 7. The file is uploaded to the storage Blob
         */
        public static async void SaveJsonToBlob()
        {
            var connectionString = String.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net",
                StorageAccountName, AccessKey);

            // Location: UWPParkE\UwpParkE\UwpParkE\bin\x86\Debug\AppX\MyData\device_data.json
            string MyFilePath = Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, "MyData/frequent_device_data_sensors_fiu_ece.json");

            // Retrieve storage account from connection string.
            CloudStorageAccount MyStorageAccount = CloudStorageAccount.Parse(connectionString);

            // Create the blob client.
            CloudBlobClient MyBlobClient = MyStorageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer TheContainer = MyBlobClient.GetContainerReference("containerecesensors");

            //await container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            // Retrieve reference to a blob named "myblob".
            CloudBlockBlob TheBlob = TheContainer.GetBlockBlobReference("frequent_device_data_sensors_fiu_ece.json");

            // Create or overwrite the "myblob" blob with contents from a local file.
            using (var fileStream = System.IO.File.OpenRead(MyFilePath))
            {
                await TheBlob.UploadFromStreamAsync(fileStream);
            }


        }


    }
}
