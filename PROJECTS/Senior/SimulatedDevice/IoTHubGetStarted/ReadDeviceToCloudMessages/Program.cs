﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadDeviceToCloudMessages
{
    using System;
    using System.Threading.Tasks;
    using System.Text;
    using Microsoft.ServiceBus.Messaging;
    using System.Threading;
    using System.Linq;

    public class Program
    {
        private const string ConnectionString = "HostName=IoTParkE.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=FXq1UFGWLHSxet6OOc/KKHijNx1MOt3PylzpxGgeyJE=";
        private const string IotHubD2CEndpoint = "messages/events";
        private static EventHubClient EventHubClient;

        private static async Task ReceiveMessagesFromDeviceAsync(string MyPartition, CancellationToken MyCancellationToken)
        {
            var eventHubReceiver = EventHubClient.GetDefaultConsumerGroup().CreateReceiver(MyPartition, DateTime.UtcNow);
            while (true)
            {
                if (MyCancellationToken.IsCancellationRequested) break;
                var eventData = await eventHubReceiver.ReceiveAsync();
                if (eventData == null) continue;

                var data = Encoding.UTF8.GetString(eventData.GetBytes());
                Console.WriteLine("Message received. Partition: {0} Data: '{1}'", MyPartition, data);
            }
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("Receive messages. Ctrl-C to exit.\n");
            EventHubClient = EventHubClient.CreateFromConnectionString(ConnectionString, IotHubD2CEndpoint);
            var partitions = EventHubClient.GetRuntimeInformation().PartitionIds;
            var cts = new CancellationTokenSource();

            Console.CancelKeyPress += (s, e) =>
            {
                e.Cancel = true;
                cts.Cancel();
                Console.WriteLine("Exiting...");
            };

            var tasks = partitions.Select(partition => ReceiveMessagesFromDeviceAsync(partition, cts.Token));
            Task.WaitAll(tasks.ToArray());
        }
    }
}
