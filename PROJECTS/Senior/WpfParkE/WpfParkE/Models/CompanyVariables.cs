﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfParkE.Models
{

    // These string will be change for each different company
    class MyCompanyVariables
    {

        public static string CompanyEmail()
        {
            return "parke.ece@outlook.com";
        }

        public static string CompanyName()
        {
            return "parke.ece";
        }

        public static string BlogUrl()
        {
            return "https://myparkestorage.blob.core.windows.net/myparkecontainer/blob_ece_data.json";
        }

        public static string TableNameSensors()
        {
            return "TableECESensors";
        }

        public static string TableNameDevices()
        {
            return "TableEceDevice";
        }



    }
}
