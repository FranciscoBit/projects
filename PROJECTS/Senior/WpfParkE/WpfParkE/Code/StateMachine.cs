﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WpfParkE.MainWindow;

using WpfParkE.Models;
using System.IO;

namespace WpfParkE.Code
{
    class MyStateMachine
    {
        public static void CopyFile()
        {
            try
            {
                // Use Path class to manipulate file and directory paths.
                string sourceFile = System.IO.Path.Combine(MyGlobalVariables.OriginalFilePath(), MyGlobalVariables.JsonFileName());
                string destFile = System.IO.Path.Combine(MyGlobalVariables.FirstFilePath(), MyGlobalVariables.JsonFileName());

                //System.IO.File.Move(sourceFile, destFile);
                System.IO.File.Copy(sourceFile, destFile, true);
            }
            catch (IOException i)
            {

            }



            

        }

        public static void DeleteFile()
        {
            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(MyGlobalVariables.OriginalFilePath(), MyGlobalVariables.JsonFileName());
            string destFile = System.IO.Path.Combine(MyGlobalVariables.FirstFilePath(), MyGlobalVariables.JsonFileName());

            System.IO.File.Delete(destFile);
            //System.IO.File.Copy(sourceFile, destFile, true);

        }



        public async  static void CopyFileUpdate()
        {
            string StartDirectory = MyGlobalVariables.OriginalFilePath();
            string EndDirectory = MyGlobalVariables.FirstFilePath();

            foreach (string filename in Directory.EnumerateFiles(StartDirectory))
            {
                using (FileStream SourceStream = File.Open(filename, FileMode.Open))
                {
                    using (FileStream DestinationStream = File.Create(EndDirectory + filename.Substring(filename.LastIndexOf('\\'))))
                    {
                        await SourceStream.CopyToAsync(DestinationStream);
                    }
                }
            }
        }




        public static void TickFunction()
        {
            // Create new stopwatch
            Stopwatch stopwatch = new Stopwatch();
            // Begin timing
            stopwatch.Start();


            //Model_Device_RealTime.MyJsonAsync();
            //MyFileInfo.FileTester();
            //Debug.WriteLine(MyFileInfo.ReadJson() );


            // SUCCESS
            // 1. Send json file to storage blob
            MyAzureCloud.SaveJsonToBlob();


            // SUCCESS
            // 2. Parse the json file, and send it to (Azure, ThingSpeak, IBM, Google Cloud, etc)
            //    MyFileInfo.ReadJson() returns the parse string of the json file
            MyAzureCloud.SendDeviceToCloudMessagesAsync(MyFileInfo.ReadJson());


            // Stop timing
            stopwatch.Stop();
            Debug.WriteLine("The Time elapsed in milliseconds: {0}", stopwatch.ElapsedMilliseconds);
            Debug.WriteLine("The Time elapsed: {0}", stopwatch.Elapsed);


        }


    }
}
