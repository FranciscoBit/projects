﻿using Microsoft.Azure.Devices.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

using WpfParkE.Models;

namespace WpfParkE.Code
{
    class MyFileInfo
    {

        /*
         * SUCCESS
         */
        public static void FileTester()
        {
            string MyFilePath = Path.Combine( MyGlobalVariables.FirstFilePath(), MyGlobalVariables.JsonFileName() );

            if (File.Exists(MyFilePath))
                Console.WriteLine("File exists.");
            else
                Console.WriteLine("File does not exist.");
        }


        /*
         * SUCCESS
         * 1. This method reads the json files found inside specfic location in the laptop
         * 2. Then it parses the data and returns the value in a string
         */
        public static string ReadJson()
        {
            // Will have to rename file path
            // Location: C:\\ParkE\\blob_ece_data.json
            string MyFilePath = Path.Combine(MyGlobalVariables.FirstFilePath(), MyGlobalVariables.JsonFileName());

            using (StreamReader MyFile = File.OpenText(MyFilePath))
            {
                var myJSON = MyFile.ReadToEnd();

               Dictionary<string, object> desiObjResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(myJSON);

                string myJsonString = desiObjResult["device_data_sensors_fiu_ece"].ToString();

                return myJsonString;
            }
        }



        






    }



    public class ViewModel
    {
        public IEnumerable<Model_Device_RealTime> RealTimeDetails { get; set; }
    }



    public class Model_Device_RealTime
    {
        public string SensorCompany { get; set; }
        public string SensorName { get; set; }
        public string SensorState { get; set; }

        public string SensorDate { get; set; }
        public string SensorTime { get; set; }



        public string GetStringFromBlobAsync(string BlobUrl)
        {
            


            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(BlobUrl);

                Dictionary<string, object> desiObjResult = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                string myJsonString = desiObjResult["device_data_sensors_fiu_ece"].ToString();

                return myJsonString;

           
            }
        }



        public List<Model_Device_RealTime> Get_Device_Details_Long(string StrFromBlob)
        {
            List<Model_Device_RealTime> model = new List<Model_Device_RealTime>();
            

            List<MyDevice_Data_New> bindJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MyDevice_Data_New>>(StrFromBlob);

            for (int i = 0; i < bindJsonData.Count; i++)
            {
                Model_Device_RealTime RealTimeClass = new Model_Device_RealTime();

                RealTimeClass.SensorCompany = bindJsonData[i].sensor_company;
                RealTimeClass.SensorName = bindJsonData[i].sensor_name;
                RealTimeClass.SensorState = bindJsonData[i].sensor_state;
                RealTimeClass.SensorDate = DateTime.Now.ToString();


                model.Add(RealTimeClass);

                
            } 



            return model;
        }



    }



    [DataContract]
    public class MyDevice_Data_New
    {
        public string _code = string.Empty;

        [DataMember]
        public string sensor_company
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_name
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_state
        {
            get
            {
                return _code.Trim();
            }
            set
            {
                _code = value;
            }
        }
    }


}
