﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
using Microsoft.Azure.Devices.Client;

using WpfParkE.Models;
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace WpfParkE.Code
{
    class MyAzureCloud
    {
        private static DeviceClient TheDeviceClient = null;

        /*
         * 1. This method gets the string from previous method
         * 2. Then it desializes the string and converts it into a List<Device_Data> object
         * 3. We then loop throght all the information found
         * 4. Once we do this information will be send to the IoT Hub (to specifc device) and wait specifc time before doing it again
         * 
         * These steps are not included in the program but it will then do the following:
         * 5. By using Stream Analytics it will be configure the following way
         * 6. Input is from IoT Hub and specifc device
         * 7. Output is to specific SQL Server, Database, and Table
         * 8. The Query Function serves as the middleman between the input and output
         * 9. The query must be made so only specifc info goes to specifc tables that we want to use
         * 10. Once it connects succesfully the JSON goes directly to our Database Tables along with other information
         */
        public static async void SendDeviceToCloudMessagesAsync(string MyJsonString)
        {
            // We bind the data with json
            List<MyDevice_Data> bindJsonData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MyDevice_Data>>(MyJsonString);

            if (TheDeviceClient == null)
            {
                TheDeviceClient = DeviceClient.Create(MyCloudVariables_IoT.IotHubUri(),
                      new DeviceAuthenticationWithRegistrySymmetricKey(
                       MyCloudVariables_IoT.DeviceId(), MyCloudVariables_IoT.DeviceKey() ), TransportType.Http1);
            }

            for (int i = 0; i < bindJsonData.Count; i++)
            {
                var telemetryDataPoint = new
                {
                    sensor_company = bindJsonData[i].sensor_company,
                    sensor_name = bindJsonData[i].sensor_name,
                    sensor_state = bindJsonData[i].sensor_state,
                    iot_device_id = MyCloudVariables_IoT.DeviceId(),
                    iot_company_email = MyCompanyVariables.CompanyEmail()
                };

                var messageString = JsonConvert.SerializeObject(telemetryDataPoint);
                var message = new Message(Encoding.ASCII.GetBytes(messageString));

                await TheDeviceClient.SendEventAsync(message);

                // If we want to wait couple of seconds till we do task again
                //await Task.Delay(10000);
            }

            //CoreApplication.Exit();

        }




        /*
         * 1. This method sends data from JSON file to the Storage Blob
         * 2. We start by first establishing a connection string
         * 3. We then look at the file path inside the RBP3 where JSON file was stored by the Arduino
         * 4. Once we have this we create a storage client where we want to send the file to
         * 5. We name the specifc container where the faile will be placed
         * 6. We name the file however we want (if another file of same name is found it will be re-written)
         * 7. The file is uploaded to the storage Blob
         */
        public static async void SaveJsonToBlob()
        {
            var connectionString = String.Format("DefaultEndpointsProtocol=https;AccountName={0};AccountKey={1};EndpointSuffix=core.windows.net",
                MyCloudVariables_Storage.StorageAccountName(), MyCloudVariables_Storage.AccessKey() );

            // Location: UWPParkE\UwpParkE\UwpParkE\bin\x86\Debug\AppX\MyData\device_data.json
            string MyFilePath = Path.Combine(MyGlobalVariables.FirstFilePath(), MyGlobalVariables.JsonFileName());

            // Retrieve storage account from connection string.
            CloudStorageAccount MyStorageAccount = CloudStorageAccount.Parse(connectionString);

            // Create the blob client.
            CloudBlobClient MyBlobClient = MyStorageAccount.CreateCloudBlobClient();

            // Retrieve reference to a previously created container.
            CloudBlobContainer TheContainer = MyBlobClient.GetContainerReference(MyCloudVariables_Storage.ContainerName() );

            //await container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            // Retrieve reference to a blob named "myblob".
            CloudBlockBlob TheBlob = TheContainer.GetBlockBlobReference(MyGlobalVariables.JsonFileName() );

            // Create or overwrite the "myblob" blob with contents from a local file.
            using (var fileStream = System.IO.File.OpenRead(MyFilePath))
            {
                await TheBlob.UploadFromStreamAsync(fileStream);
            }


        }



    }



    /*
     * {
     *      "device_data_sensors_fiu_ece": 
     *      [
     *          {
     *              "sensor_company": "FIU ECE",
         *          "sensor_name": "my sensor name" ,
         *          "sensor_state": "my sensor state" 
         *          
     *          },
     *          ....
     *      ]
     * }
     */


    /*
     * This is the JSON file data contract that comes from Sensors --> Arduino --> RBP3
     * It has the Sensor Company, Sensor Name, and Sensor State
    */
    [DataContract]
    public class MyDevice_Data
    {
        public string _code = string.Empty;

        [DataMember]
        public string sensor_company
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_name
        {
            get;
            set;
        }

        [DataMember]
        public string sensor_state
        {
            get
            {
                return _code.Trim();
            }
            set
            {
                _code = value;
            }
        }
    }



}
