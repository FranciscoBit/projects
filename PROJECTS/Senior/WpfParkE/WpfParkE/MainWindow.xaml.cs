﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfParkE.Code;

namespace WpfParkE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {






        public MainWindow()
        {
            InitializeComponent();

            /*
             * 
             * while (true)
            {
                MyStateMachine.TickFunction();

                //Here goes the waiting period before doing it again
            }
             */


            //MyStateMachine.TickFunction();

            System.Timers.Timer timer = new System.Timers.Timer();

            // Tell the timer what to do when it elapses
            timer.Elapsed += new ElapsedEventHandler(myEvent);
            // Set it to go off every five seconds
            timer.Interval = 5000;
            // And start it        
            timer.Enabled = true;


            //DispatcherTimer timer = new DispatcherTimer();
            //timer.Tick += new EventHandler(myEvent);
            //timer.Interval = TimeSpan.FromMilliseconds(5000);
            //timer.IsEnabled = true;
            


        }





        public class TodoItem
        {
            public string Title { get; set; }
            public int Completion { get; set; }
        }


        private void myEvent(object source, ElapsedEventArgs e)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            MyStateMachine.CopyFile();
            //MyStateMachine.CopyFileUpdate();
            MyStateMachine.TickFunction();
            
            List<TodoItem> items = new List<TodoItem>();
            items.Add(new TodoItem() { Title = "Proccess Started" });


            System.Windows.Application.Current.Dispatcher.Invoke(
            System.Windows.Threading.DispatcherPriority.Normal, (Action)delegate
            {
                
                stopwatch.Stop();

                items.Add(new TodoItem() { Title = "Data Send Successfully! ----- " });
                items.Add(new TodoItem() { Title = "Time of Execution: ----- " + stopwatch.Elapsed.ToString() });
                items.Add(new TodoItem() { Title = "Send At:  ----- " + DateTime.Now.ToLocalTime() });

                icTodoList.ItemsSource = items;

                Thread.Sleep(2000);
            });

            //MyStateMachine.DeleteFile();
        }



        /*
         * private void myEvent(object source, EventArgs e)
        {


            Stopwatch stopwatch = new Stopwatch();
            List<TodoItem> items = new List<TodoItem>();
            items.Add(new TodoItem() { Title = "Proccess Started" });


            stopwatch.Start();
            MyStateMachine.TickFunction();
            stopwatch.Stop();


            items.Add(new TodoItem() { Title = "Data Send Successfully! ----- " });
            items.Add(new TodoItem() { Title = "Time of Execution: ----- " + stopwatch.Elapsed.ToString() });
            items.Add(new TodoItem() { Title = "Send At:  ----- " + DateTime.Now.ToLocalTime() });

            Debug.WriteLine("COMPLETED");

            icTodoList.ItemsSource = items;
            Thread.Sleep(2000);

        }
         */











    }
}
