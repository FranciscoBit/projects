
import java.util.Random;
import javax.swing.JOptionPane;

/**
 * A class to represent a standard 6-sided die
 */
class Die1 // die is the singular of "dice"
{
    // random number generator object

    private static Random r = new Random();

    /**
     * Roll one die.
     * @return a random integer from 1 to 6
     */
    public int roll() {
        return r.nextInt(6) + 1;
    }
}

class MatchGame {
    // instance var's are Die objects 

    Die1 d1 = new Die1();				// create pair of dice
    Die1 d2 = new Die1();

    // This method just aligns the output...
    private static String format(int number) {
        if (number <= 9) // if single-digit number...
        {
            return " " + number;			// ...pad with one leading space
        } else // 2-digit number...
        {
            return "" + number;			// ...no padding
        }
    }

    public void play() {
        int initialRoll;		// sum of first roll of two dice

        int gameNumber = 0;	// counts number of match games played

        int rollCount;		// counts number of rolls needed to 
        // match the initial roll

        // "priming" read
        String answer = JOptionPane.showInputDialog("Want to play?" //1. lcv is answer must be initialized
                + "\nType Y for Yes or N for No");

        // repeat as long as user wants to play
        while (answer.equalsIgnoreCase("Y")) { // 2. then it must be tested in boolean
            gameNumber++;

            // get the initial roll
            initialRoll = d1.roll() + d2.roll();

            // roll dice again to try to match initial roll
            int currentRoll = d1.roll() + d2.roll();

            // for each initial roll, reset counter for number of rolls
            // needed to match			
            rollCount = 1;

            // repeat as long as initial roll is not matched
            // DO NOT MODIFY THIS LOOP!!!!!
            while (currentRoll != initialRoll) {
                // roll 'em again!
                currentRoll = d1.roll() + d2.roll();
                // increment number of rolls made
                rollCount++;
            }
            // Loop postcondition:  initialRoll has been matched
            // print stats
            System.out.println("Game #" + format(gameNumber)
                    + "   Initial roll = " + format(initialRoll)
                    + "   Matched in " + format(rollCount) + " rolls.");

            answer = JOptionPane.showInputDialog("Want to play?"
                    + "\nType Y for Yes or N for No"); //3. then it must be again changed in loop body


        }
    }
}

public class GallopingDominoes {

    public static void main(String[] args) {
        MatchGame fun = new MatchGame();
        fun.play();
    }
}