package hello;

public class Hello {

    public static void main(String[] args) {
        //Question 1
        System.out.println("Hello World!");
        //Question 2
        System.out.println("Hello Francisco!");
        //Question 3
        System.out.println("How are you?");
        //Question 4
        System.out.print("Hello World!");
        //Question 5
        System.out.println("\nHello World!");
        //Question 6
        System.out.println("\tHello World!");
        //Question 7
        System.out.println("Hello World!");
        //Question 8

        System.out.println("The sum of integers from 1 tp 10 is: "
                + 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10);
        //Question 12
        System.out.println("The sum of integers from 1 tp 10 is: "
                + (1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10));
        //Question 13
        System.out.println("The sum of integers from 1 tp 10 is: "
                + (1 / 10.0 + 2 / 10.0 + 3 / 10.0 + 4 / 10.0 + 5 / 10.0 + 6 / 10.0 + 7 / 10.0 + 8 / 10.0 + 9 / 10.0 + 10 / 10.0));


    }
}
