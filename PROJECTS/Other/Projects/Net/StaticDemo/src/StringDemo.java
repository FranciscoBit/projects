/*	File:	StringDemo.java

    Purpose:	Demonstrates some String processing techniques using the 
                indexOf, substring, charAt, and toUppercase methods.
*/

import java.util.Scanner ;

/**  
 * A class to demonstrate String processing.
 */  
public class StringDemo
{
	// instance var
	private String aString ;		// input String to be processed
	
	/**
	 * Creates a StringDemo object containing the input string
	 * @param aString
	 */
	public StringDemo(String aString)
	{
		this.aString = aString ;
	}
	
	/**
	 * Finds and prints the positions of all occurences of a given character
	 * in a String.  Finds both UPPERCASE and lowercase characters.
	 * @param letter the character to search for
	 */
	public void findAll(String letter)
	{
		System.out.println("\nIn the string \"" + aString + "\" the letter \"" 
		                    + letter + "\" occurs in these positions:\n ") ;
		
		// Convert the letter to uppercase		
		letter = letter.toUpperCase() ;
		
		// Do the same for the string
		String word = aString.toUpperCase() ;
		                   
		int position = 0 ;			// position of letter in name
		do
		{
			position = word.indexOf(letter,position) ;
			
			if (position != -1)		// if letter was found
			{
				System.out.print(position + " ") ;
				position++ ;		// advance so won't find same 
			}						// letter again
		}
		while (position != -1) ;	// exit when no more letters found
		
		System.out.println() ;
	}

	/**
	 * Returns a reversed or "backwards" copy of aString
	 * @return aString, with all characters appearing in reverse order
	 */
	public String reversed()
	{
		String out = "" ;	// init return String to the "null string"
		
		// for each character of the String, starting with the last...
		
		for (int i = aString.length() - 1 ; i >= 0 ; i--)
		{
		   out = out + aString.charAt(i) ;		// concatenate to return string
		}
		return out ;
	}

	/**
	 * Given a name in the form "First Last" rearranges it as "Last, First".
	 * @return a reference to the rearranged String if it contains a space;
	 * otherwise, returns a null reference
	 */
	public String rearrange()
	{
		String first,	// the first name
		       last ;	// the last name
		int pos ;		// used to find the space between the names
		
		// locate the first space in the name
		pos = aString.indexOf(" ") ;

		if (pos == -1)	// no space found
		  return null ;
		  
		// first name is substring from 1st char to the one before the space
		first = aString.substring(0,pos) ;
		
		// last name is all chars after the space
		last = aString.substring(pos + 1) ;
		
		return last + ", " + first ;	// rearrange and return
	}

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in) ;
		
		System.out.print("Enter a word or phrase: ") ;
		
		String aString = input.nextLine() ;

		StringDemo demo = new StringDemo(aString) ;
		
		System.out.print("\nEnter a letter to search for: ") ;

		String letter = input.nextLine() ;

		// find all occurrences of letter in aString
		demo.findAll(letter) ;	  
		
		// print it backwards
		System.out.println("\n" + aString + " backwards is: " + 
		                   demo.reversed() ) ;

		System.out.print("\nEnter a name in the form \"First Last\" : ") ;

		String name = input.nextLine() ;
		
		demo = new StringDemo(name) ;		// create new StringDemo object
		
		// get name in form "Last, First"
		String lastNameFirst = demo.rearrange() ;
		
		if (lastNameFirst != null)
		  System.out.println("\n\"" + name + "\" can also be printed:  " + 
		                     lastNameFirst) ;
		else
		  System.out.println("\nSorry, " + name + " doesn't contain a space.") ;
	}
}

/*  program output:

Enter a word or phrase: Abracadabra!

Enter a letter to search for: a

In the string "Abracadabra!" the letter "a" occurs in these positions:

0 3 5 7 10

Abracadabra! backwards is: !arbadacarbA

Enter a name in the form "First Last" : Ludwig von Beethoven

"Ludwig von Beethoven" can also be printed:  von Beethoven, Ludwig

*/
