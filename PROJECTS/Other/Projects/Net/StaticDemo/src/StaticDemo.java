// File:	StaticDemo.java

// Demonstrates static "class variables" and methods, using a class
// called "Class" which models a class offered at a school  :-)

class Class 
{
	// instance variables
	private String name ;				// name of class offered
	private int numStudents ;			// enrollment in class
	
	// static "class variables"
	private static int count = 0;			// # of objects created
	private static int totalEnrolled = 0 ;	// accumulates total enrollment
	private static final int 
	                   MAX_STUDENTS = 35 ;	// largest allowable class size
	
	/**
	 * Create a Class object.
	 * @param id the course name (e.g., COP 2210)
	 * @param students the number of students in the class
	 */	
	public Class (String id, int students)	
	{ 
		System.out.println("\nConstructor called for class " +
		                    id + " with " + students + " students") ;
		
		// initialize new object
		name = id ;
		if ( students >= 0 && students <= MAX_STUDENTS)
		  numStudents = students ;
		else
		{
		  System.out.println("Illegal number of students: " + students +
		                     "  (using 0 instead)" ) ;
		  numStudents = 0 ;
		}

		// increment count of objects & accumulate total students enrolled
		count++ ;
		totalEnrolled += numStudents ;
	}
	
	/**
	 * Get the number of classes that exist.
	 * @return count: the number of classes that exist.
	 */
	public static int getCount()
	{		
		return count ;
	}
	
	/**
	 * Get the total number of students in all existing classes.
	 * @return totalEnrolled: the total number of students in all classes
	 */
	public static int getEnrollment()
	{	
		return totalEnrolled ;
	}
	
	/**
	 * Cancel a class.
	 */
	public void cancel() 
	{
		System.out.println("\nCancelling class " + name
		                    + " with " + numStudents + " students") ;
		                    
		// decrement count of objects and reduce total enrollment
		count-- ;
		totalEnrolled -= numStudents ;
	}
} // end of Class class declaration

class StaticDemo 
{
	public static void main(String args[]) 
	{
		// no objects yet, but class var's still exist!
		System.out.println("Initially " + Class.getCount() + " classes with "
			                + Class.getEnrollment() + " total students") ;
			   
		Class c1 = new Class("ANT 2222", 35) ; // Ancient Egyptian Plumbing
		
		// print updated class var's
		System.out.println("\nNumber of classes = " + Class.getCount() +
		                   "\tTotal students = " + Class.getEnrollment()) ;
		      
		Class c2 = new Class("PHI 0000", 12) ; // Existentialism in the Films
											   // of Carrot Top

		System.out.println("\nNumber of classes = " + Class.getCount() +
		                   "\tTotal students = " + Class.getEnrollment()) ;
		      
		Class c3 = new Class("COP 1861", 64) ; // Computer Languages of the 
											   // Antebellum South
											   
		// may call static fn via object name, but poor programming practice!								   
		System.out.println("\nNumber of classes = " + c1.getCount() +
		                   "\tTotal students = " + c1.getEnrollment()) ;
		
		// cancel all classes
		      
		c3.cancel() ;
		c3 = null ;		// schedule object for garbage collection
		System.out.println("Number of classes = " + Class.getCount() +
		                   "\tTotal students = " + Class.getEnrollment()) ;
		c2.cancel() ;
		c2 = null ;
		System.out.println("Number of classes = " + Class.getCount() +
		                   "\tTotal students = " + Class.getEnrollment()) ;
		c1.cancel() ;
		c1 = null ;
		System.out.println("Number of classes = " + Class.getCount() + 
		                   "\tTotal students = " + Class.getEnrollment()) ;
	}
} // end of StaticDemo class declaration

/* program output:

Initially 0 classes with 0 total students

Constructor called for class ANT 2222 with 35 students

Number of classes = 1   Total students = 35

Constructor called for class PHI 0000 with 12 students

Number of classes = 2   Total students = 47

Constructor called for class COP 1861 with 64 students
Illegal number of students: 64  (using 0 instead)

Number of classes = 3   Total students = 47

Cancelling class COP 1861 with 0 students
Number of classes = 2   Total students = 47

Cancelling class PHI 0000 with 12 students
Number of classes = 1   Total students = 35

Cancelling class ANT 2222 with 35 students
Number of classes = 0   Total students = 0

*/		