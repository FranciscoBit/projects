/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class BottleWaterTester {

    public static void main(String[] args) {
        //Imput info of program...
        //Question 1:
        BottleWater myBottleWater = new BottleWater("USA", 320000000, 190, 8.5,
                12);

        //Assign variables
        String Name;
        int Population;
        int AmountCircles;
        double Lenght;
        int Volume;

        //Set them equal to....
        //Question 2:
        Name = myBottleWater.GetName();
        Population = myBottleWater.GetPopulation();
        AmountCircles = myBottleWater.GetEarthCircles();
        Lenght = myBottleWater.GetLenght();
        Volume = myBottleWater.GetVolume();

        //Print the imput
        //Question 2:
        System.out.println("Country Name: " + Name);
        System.out.println("Country Population: " + Population);
        System.out.println("Number of times around earth: " + AmountCircles);
        System.out.println("Bottle Lenght: " + Lenght);
        System.out.println("Bottle Volume: " + Volume);

        //Question 3:

        double TotalBottles;
        double AmountWater;

        TotalBottles = myBottleWater.TotalBottles();
        AmountWater = myBottleWater.AmountWater();

        System.out.println("Total Bottles: " + TotalBottles);
        System.out.println("Amount of Water: " + AmountWater);

        //Question 4:
        myBottleWater.Resize(9, 16);

        //Question 5:
        double NewLenght;
        int NewVolume;

        NewLenght = myBottleWater.GetLenght();
        NewVolume = myBottleWater.GetVolume();
        System.out.println("New Bottle Lenght: " + NewLenght);
        System.out.println("New Bottle Volume: " + NewVolume);

        //Question 6:
        double MyNewAmountWater = myBottleWater.AmountWater();
        double MyNewTotalBottle = myBottleWater.TotalBottles();

        System.out.println("New Total Number of Botttles: " + MyNewTotalBottle);
        System.out.println("New Amount of Water Per Person: " + MyNewAmountWater);

    }
}
