/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class BottleWater {

    private String CountryName;
    private int CountryPopulation;
    private int EarthCircles;
    private double LenghtBottle;
    private int VolumeBottle;

    public BottleWater(String CountryName, int CountryPopulation,
            int EarthCircles, double LenghtBottle, int VolumeBottle) {
        this.CountryName = CountryName;
        this.CountryPopulation = CountryPopulation;
        this.EarthCircles = EarthCircles;
        this.LenghtBottle = LenghtBottle;
        this.VolumeBottle = VolumeBottle;

    }

    public String GetName() {
        return CountryName;
    }

    public int GetPopulation() {
        return CountryPopulation;
    }

    public int GetEarthCircles() {
        return EarthCircles;
    }

    public double GetLenght() {
        return LenghtBottle;
    }

    public int GetVolume() {
        return VolumeBottle;
    }

    public double TotalBottles() {
        double Circumference;
        double BottleTotal;

        Circumference = 1.58e9; //circumference in inches

        BottleTotal = (EarthCircles * Circumference)
                / (LenghtBottle);

        return BottleTotal;
    }

    public double AmountWater() {
        double circumference = 1.58e9; //circumference in inches
        double bottleTotal;
        double WaterPerPerson;

        bottleTotal = ((EarthCircles * circumference)
                / (LenghtBottle));


        WaterPerPerson = (bottleTotal * VolumeBottle) / (CountryPopulation);
        WaterPerPerson = WaterPerPerson / 128.0;

        return WaterPerPerson;
    }

    public double Resize(int newLenght, int newVolume) {
        this.LenghtBottle = newLenght;
        this.VolumeBottle = newVolume;
        return 0;
    }
}
