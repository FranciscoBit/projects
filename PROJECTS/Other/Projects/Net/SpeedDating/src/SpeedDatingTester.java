/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
import javax.swing.JOptionPane;

public class SpeedDatingTester {

    public static void main(String[] args) {
        SpeedDating speedDating = new SpeedDating();

        String input;
        //Part 1
        input = JOptionPane.showInputDialog("Enter Year(to determine July 4): "
                + "(or press Cancel to quit)");
        int year = Integer.parseInt(input);
        speedDating.print4thOfJulys(year);
        //Part 2
        JOptionPane.showInputDialog("Enter Year(to determine Thanksgiving): "
                + "(or press Cancel to quit)");
        int year1 = Integer.parseInt(input);
        speedDating.getThanksgiving(year1);
        
        
    }
}
