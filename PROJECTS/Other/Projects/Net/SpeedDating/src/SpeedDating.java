
/**
 * A class to give students experience using loops.  This class
 * creates and manipulates objects of Greg's Date class.
 */
public class SpeedDating {
    // Note: this class has no instance variables!

    /**
     * Create an empty SpeedDating object
     * (this is known as a "default" constructor)
     */
    public SpeedDating() {
        // Constructor has empty body
    }

    /**
     * Prints the day of the week (e.g. "Thursday") on which the 4th of July
     * will fall in 10 consecutive years
     * @param startYear the first of the 10 consecutive years
     */
    public void print4thOfJulys(int startYear) {
        // TO DO: write body of this method here

        for (int i = startYear; i < (startYear + 10); i++) {
            Date date = new Date(7, 4, i);
            
            System.out.print("Year is: " + i);
            System.out.print(" Day is: " + date.getDayOfWeek());
            System.out.println("");
        }

    }

    /**
     * Computes and returns the Date on which Thanksgiving will fall 
     * in a given year.
     *
     * NOTE: By law, Thanksgiving is the 4th Thursday in November
     *
     * @param year the year for which to compute the date of Thanksgiving
     * @return the Date of Thanksgiving for the specified year
     */
    public Date getThanksgiving(int year) {
        
        Date date = new Date(11, 1, year);
        
        int thu = 0;   
         while (!(thu == 4))
         {
             if ("Thursday" == date.getDayOfWeek())
             {
                 thu ++ ;
                 if (thu == 4)
                 {
                     System.out.println(date.getMediumDate());
                     break;
                 }
                 else date.next() ;  
             }
             else
                 date.next() ;
         }
         return date ; 
     }

    /**
     * Computes and returns the number of days between two dates,
     * counting the end date but not the start date.  E.g., the
     * number of days between 11/2/2010 and 11/5/2010 is 3, not 4.
     *
     * Precondition: The start date must occur on or before the end date.
     * 
     * @param start the earlier of the two dates
     * @param end the later of the two dates
     * 
     * @return the number of days elapsed between the start date and the 
     * end date 
     */
    public int getDaysBetween(Date start, Date end) {
        // NOTE: Do NOT modify the parameters "start" or "end" in this method!
        // Doing so will modify the arguments in the calling method!
        // If you need to do this (most likely you will need to modify one),
        // use one of the copies provided here instead:

        Date d1 = new Date(start.getMonth(), start.getDay(), start.getYear());
        Date d2 = new Date(end.getMonth(), end.getDay(), end.getYear());
        // TO DO: write remainder of body of this method here
        
        


    }
}
