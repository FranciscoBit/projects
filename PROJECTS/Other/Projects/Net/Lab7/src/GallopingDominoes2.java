import java.util.Random ;

/**
 * A class to represent a standard 6-sided die
 */
class Die2	// die is the singular of "dice"
{
	// random number generator object
	private static Random r = new Random() ;
	
	/**
	 * Roll one die.
	 * @return a random integer from 1 to 6
	 */
	public int roll()		
	{
		return r.nextInt(6) + 1 ;
	}
}

class MatchGame2
{
	// instance var's are Die objects 
	Die d1 = new Die() ;				// create pair of dice
	Die d2 = new Die() ;

	// This method just aligns the output...
	private static String format (int number)
	{
		if (number <= 9)				// if single-digit number...
		  return " " + number ;			// ...pad with one leading space
		else							// 2-digit number...
		  return "" + number ;			// ...no padding
	}
		
	public void play()
	{
		int initialRoll ;		// sum of first roll of two dice

		int gameNumber = 0 ;	// counts number of match games played
		
		int rollCount ;		// counts number of rolls needed to 
		                     // match the initial roll

		System.out.println() ;
		
		// repeat as long as ...
		while ( true )					// loop forever!
		{
			gameNumber++ ;				// increment # of games played

			// get the initial roll
			initialRoll = d1.roll() + d2.roll() ;

			// roll dice again to try to match initial roll
			int currentRoll = d1.roll() + d2.roll() ;
						
			// for each initialRoll, reset roll counter
			rollCount = 1 ;	
			
			// repeat as long as the initial roll is not matched
			// DO NOT MODIFY THIS LOOP!  ONLY THE OUTER ONE!
			while ( currentRoll != initialRoll )	// while NOT matched...
			{
				// ...roll 'em again!
				currentRoll = d1.roll() + d2.roll() ;
				
				// ...increment number of rolls made
				rollCount++ ;			
			}
			// Loop postcondition:  initialRoll has been matched
			
			// print stats
			System.out.println("Trial #" + format(gameNumber) + 
			                 "   Initial roll = " + format(initialRoll) + 
			                 "   Matched in " + format(rollCount) + " rolls.") ;
			                 
		} 
	}
}

public class GallopingDominoes2
{
	public static void main(String[] args)
	{
		MatchGame2 fun = new MatchGame2() ;
		fun.play() ;
	}
}