
/**
 * A test class for the Craps class.  Plays a game and prints results.
 */
import javax.swing.JOptionPane;

public class CrapsTest {

    public static void main(String[] args) {
        // create a Craps game object, with trace on
        Craps shooter = new Craps(true);

        // play a game
        

        do {
            shooter.play();
            if (shooter.isWinner()) {
                System.out.println("Player wins!\n");
            } 
            else {
                System.out.println("Sorry, player loses.\n");
            }
            
        } while (P != null);
      
    }
}