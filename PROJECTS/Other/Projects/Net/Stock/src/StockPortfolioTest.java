
/**
 *A class to test the Stock Portfolio class
 */
/**
 *
 * @author Francisco Munoz
 */
import javax.swing.JOptionPane;

public class StockPortfolioTest {

    public static void main(String args[]) {
        // "local" variable declarations
        String getInput;
        String countryName;
        int shareNumber;
        int dollar;
        int eightDollar;



        countryName = JOptionPane.showInputDialog("Enter Company Name: ");  //Lets users input the company name


        getInput = JOptionPane.showInputDialog("Enter Number of Shares: "); // Lets user input the share value

        shareNumber = Integer.parseInt(getInput);

        getInput = JOptionPane.showInputDialog("Enter Dollars: "); //Lets user enter the dollar value of the share
        dollar = Integer.parseInt(getInput);

        getInput = JOptionPane.showInputDialog("Enter the Eights: "); //lets the user input the eights part of the dollar
        eightDollar = Integer.parseInt(getInput);

        //Creates a stock portfolio object
        StockPorfolio stockPorfolio = new StockPorfolio(countryName,
                shareNumber, dollar, eightDollar);


        String valueName;   //Name of the company
        int valueShare;     //Number of shares held by company
        int valueDollar;    //Value of each share in dollars
        int valueDollarEights;  //Value of eights part of dollar

        //Call accessor methods to get portfolio data
        valueName = stockPorfolio.GetCompany();
        valueShare = stockPorfolio.GetShareNumber();
        valueDollar = stockPorfolio.GetDollar();
        valueDollarEights = stockPorfolio.GetEightDollar();

        System.out.println("Company Name: " + valueName);   // Prints company name
        System.out.println("Shares Held: " + valueShare);   //Prints number of shares held
        System.out.println("Opening Price per Share: " + valueDollar + " "
                + valueDollarEights + "/8"); // Prints the value of the dollar and eights of dollar


        double totalPorfolioValue;  //

        //Calls acessor method to get total portfolio value
        totalPorfolioValue = stockPorfolio.GetPorfolioValue();
        System.out.println("Opening Price per Share: " + totalPorfolioValue); //Prints total portfolio value

        //Calls mutator method to reset to new values
        stockPorfolio.ModifyStock(44, 7);

        int newDollarValue; //New dollar values
        int newEightsValue; //New eights of the dollar value
        double newPorfolioValue; //New portfolio value

        //Calls acessor method to get the new values of the portfolio object
        newDollarValue = stockPorfolio.GetDollar();
        newEightsValue = stockPorfolio.GetEightDollar();
        newPorfolioValue = stockPorfolio.GetPorfolioValue();

        System.out.println("Closing Price per Share: " + newDollarValue + " "
                + newEightsValue + "/8"); // Prints new dollar and eights of dollar values
        System.out.println("Closing Portfolio Value: " + newPorfolioValue); //Prints new portfolio value


    }
}
