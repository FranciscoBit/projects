
/**
 *
 * @author Francisco Munoz
 */
public class StockPorfolio {
    //Instance variables

    private String companyName;
    private int shareNumber;
    private int dollar;
    private int eightDollar;

    /**
     * Create a stock portfolio with the name of the company, share value,
     * and the amount in dollars and eights of dollar.
     * @param companyName the name of company input by user
     * @param shareNumber the share value number of the company
     * @param dollar the amount of dollar the share is worth
     * @param eightDollar section of the dollar
     */
    public StockPorfolio(String companyName, int shareNumber, int dollar,
            int eightDollar) {
        this.companyName = companyName;
        this.dollar = dollar;
        this.eightDollar = eightDollar;
        this.shareNumber = shareNumber;
    }

    /**
     * Return the company name of the user
     * @return the company name of the user
     */
    public String GetCompany() {
        return companyName;
    }

    /**
     * Return the share value of the company
     * @return the share value of the company
     */
    public int GetShareNumber() {
        return shareNumber;
    }

    /**
     * Return the the dollar value the share is worth
     * @return the the dollar value the share is worth
     */
    public int GetDollar() {
        return dollar;
    }

    /**
     * Return the the eights of the dollar
     * @return the the eights of the dollar
     */
    public int GetEightDollar() {
        return eightDollar;
    }

    /**
     * Reset the dollar and eights of the dollar of an existing portfolio 
     * object to a specified value.
     * @param newDollar the new dollar the share is worth
     * @param newEightDollar the new eights of the dollar
     */
    public void ModifyStock(int newDollar, int newEightDollar) {
        this.dollar = newDollar;
        this.eightDollar = newEightDollar;
    }

    /**
     * Calculates the value of the portfolio
     * @return totalPorfolioValue the total value of the portfolio
     */
    public double GetPorfolioValue() {

        int conversion;

        double conversionDeci;
        double constant = 8.0;

        double totalConversion;
        double shareXDollarDeci;

        double totalPorfolioValue;

        conversion = ((dollar * 8) + eightDollar);  //Calculates the top part of the improper fraction 

        conversionDeci = (double) conversion; //Converts an int to a double

        totalConversion = conversionDeci / constant; //Calculates the improper part in a "double" form

        shareXDollarDeci = (double) shareNumber; //Share number is transformed into a double 

        totalPorfolioValue = totalConversion * shareXDollarDeci; //Calculates total portfolio value in "double" form

        return totalPorfolioValue;
    }
}
