/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Pyramid {

    private int Lenght;
    private int Height;

    public Pyramid(int Lenght, int Height) {
        this.Lenght = Lenght;
        this.Height = Height;
    }

    public int GetLenght() {
        return Lenght;
    }

    public int GetHeight() {
        return Height;
    }

    public void resize(int newHeight, int newLenght) {
        this.Height = newHeight;
        this.Lenght = newLenght;
    }

    public double GetVolume() {
        double Volume = ((1 / 3.0) * (Lenght * Lenght) * Height);
        return Volume;
    }
}
