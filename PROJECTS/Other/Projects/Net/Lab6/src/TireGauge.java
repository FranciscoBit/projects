//  Class:  TireGauge.java

//  Purpose:  Evaluates the air pressure in a tire and prints an appropriate
//            message
import javax.swing.JOptionPane;

class TireGauge {

    private static final int MIN = 30;
    private static final int MAX = 35;

    public static void main(String[] args) {
        double pressure;					// tire pressure, in psi

        String input = JOptionPane.showInputDialog("Enter tire pressure: ");

        pressure = Double.parseDouble(input);

        // Pressure in the range 30 - 35 psi (inclusive) is OK

        if ((pressure >= MIN) && (pressure <= MAX)) {
            input = "OK";
        } else if (pressure < MIN) {
            input = "UNDERinflated";
        } else if (pressure > MAX) {
            input = "OVERinflated";
        }

        System.out.println("Your Value is " + pressure + " and it is "
                + input);

        // Write code below to display the appropriate message chosen from

        // "OVERinflated"    "UNDERinflated"    "Pressure OK"






    }
}
