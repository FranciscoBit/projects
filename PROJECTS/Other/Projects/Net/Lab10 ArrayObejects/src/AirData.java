/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
/*
 * A class to store data for an airline
 */
class AirData {
    // instance vars

    private String name;		// airline name
    private int revenueMiles;	// annual revenue miles (in 1000's)
    private int passengerMiles;	// annual passenger miles (in 1000's)

    /**
     * Creates an AirData object.
     * @param name the airline name
     * @param revenueMiles the number of revenue miles flown
     * @param passengerMiles the number of passenger miles flown
     */
    public AirData(String name, int revenueMiles, int passengerMiles) {
        this.name = name;
        this.revenueMiles = revenueMiles;
        this.passengerMiles = passengerMiles;
    }

    /**
     * Returns the airline name.
     * @return the airline name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the airline's revenue miles flown.
     * @return the revenue miles
     */
    public int getRevMiles() {
        return revenueMiles;
    }

    /**
     * Returns the airline's passenger miles flown.
     * @return the passenger miles
     */
    public int getPassMiles() {
        return passengerMiles;
    }
} // end of AirData class definition ========================================