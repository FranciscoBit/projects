/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class AirDataTester {

    public static void main(String[] args) throws IOException {
        AirDataList list = new AirDataList();

        // create Scanner object to read each line of file until eof
        Scanner infile = new Scanner(new File("AirData.txt"));

        System.out.println("Data entered:\n");

        while (infile.hasNext()) // while not eof...
        {
            // read next line
            String line = infile.nextLine();

            // "echo print" data entered
            System.out.println(line);

            // 1. create a Scanner object associated with String "line"
            Scanner lineScan = new Scanner(line);

            // 2. extract tokens from current line
            while (lineScan.hasNext()) // while line has more tokens...
            {
                String token = lineScan.next();    // ...get next token
                // 3. create AirData object passing the tokens to the constructor
                int revenueMiles = lineScan.nextInt();
                int passengerMiles = lineScan.nextInt();

                AirData airData = new AirData(token, revenueMiles, passengerMiles);
                // 4. add object to list
                list.addToList(airData);

                double convert1 = (double) airData.getRevMiles();
                double convert2 = (double) list.ComputeRevMiles();
                double convert3 = (double) airData.getPassMiles();
                double convert4 = (double) list.ComputePassMiles();

                double totalShareRev = ((convert1 / convert2) * 100);
                double totalSharePass = ((convert3 / convert4) * 100);

                System.out.println("AirLine Share of Total Revenue Miles: " + totalShareRev + "%");
                System.out.println("AirLine Share of Total Passenger Miles: " + totalSharePass + "%");
            }
        }
        System.out.println();

        System.out.println(list.toString());		// print the list

        System.out.println("Total Revenue Miles: " + list.ComputeRevMiles());
        System.out.println("Total Passenger Miles: " + list.ComputePassMiles());
    }
} // end of AirDataListTester class definition