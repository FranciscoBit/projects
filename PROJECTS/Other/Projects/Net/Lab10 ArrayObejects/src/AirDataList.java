/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
/**
 * A class to implement a list of AirData objects
 */
import GregBank.Align;
import java.util.ArrayList;

class AirDataList {
    // instance var

    private ArrayList<AirData> list;	// list of AirData objects

    /**
     * Creates an empty list
     */
    public AirDataList() {
        list = new ArrayList<AirData>();
    }

    /**
     * Appends an AirData object to the list.
     * @param current the object to be appended to the list
     */
    public void addToList(AirData current) {
        list.add(current);
    }

    public int ComputePassMiles() {
        int totPassMiles = 0;
        int totRevMiles = 0;
        for (int i = 0; i < list.size(); i++) {
            AirData current = list.get(i);	// get the account
            totPassMiles = totPassMiles + current.getPassMiles();
        }
        return totPassMiles;
    }

    public int ComputeRevMiles() {
        int totRevMiles = 0;
        for (int i = 0; i < list.size(); i++) {
            AirData current = list.get(i);
            totRevMiles = totRevMiles + current.getRevMiles();
        }
        return totRevMiles;
    }

    /**
     * Converts the list to a multi-line string, with each line containing the
     * data for one airline.
     * @return the String containing all the data on the list
     */
    public String toString() {
        // headings
        String out = "               Revenue Miles   Passenger Miles\n"
                + "     Airline    (in 1000's)      (in 1000's)\n"
                + "     =======   =============   ===============" + "\n";

        // for each AirData object on the list...
        for (int i = 0; i < list.size(); i++) {
            AirData air = list.get(i);             // get next AirData obj
            String name = air.getName();           // get airline name
            int revMiles = air.getRevMiles();      // get revenue miles
            int passMiles = air.getPassMiles();    // get passenger miles
            // concatenate data to output string
            out = out + Align.right(name, 12) + Align.right(revMiles, 16)
                    + Align.right(passMiles, 18) + "\n";
        }
        return out + "\n";
    }
} // end of AirDataList class definition =====================================