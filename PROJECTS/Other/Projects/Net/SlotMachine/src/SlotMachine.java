/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class SlotMachine {
    //Instance variables

    private int disk1;
    private int disk2;
    private int disk3;
    private int wage;

    /**
     * Create a slot machine portfolio with the name of the disk 1, disk 2, disk 3
     * and the amount in dollars that was waged.
     * @param disk1 the name of company input by user
     * @param disk2 the share value number of the company
     * @param disk3 the amount of dollar the share is worth
     * @param wage section of the dollar
     */
    public SlotMachine(int disk1, int disk2, int disk3, int wage) {
        this.disk1 = disk1;
        this.disk2 = disk2;
        this.disk3 = disk3;
        this.wage = wage;
    }

    /**
     * Return disk 1 of the slot machine
     * @return disk 1 of slot machine
     */
    public int GetDisk1() {
        return disk1;
    }

    /**
     * Return disk 2 of the slot machine
     * @return disk 2 of slot machine
     */
    public int GetDisk2() {
        return disk2;
    }

    /**
     * Return disk 3 of the slot machine
     * @return disk 3 of slot machine
     */
    public int GetDisk3() {
        return disk3;
    }

    /**
     * Return the wage played
     * @return the wage payed
     */
    public int GetWage() {
        return wage;
    }

    /**
     * Calculates the value won in the slot machine
     * @return answer the string value containing the payoff
     */
    public String Play() {

        double payoff;
        String answer;


        if ((disk1 == disk2) && (disk2 == disk3) && (disk1 == disk3)) // 100:1
        {
            payoff = wage * 100;
            answer = "3 Numbers The Same, You Win " + payoff;
        } else if (((disk1 == disk2) && (disk1 != disk3)) || ((disk2 == disk3)
                && (disk1 != disk2)) || ((disk1 == disk3)) && (disk2 != disk3)) // 4:1
        {
            payoff = wage * 4;
            answer = "2 Numbers The Same, You Win " + payoff;
        } else if ((((disk1 + 1 == disk2)) && ((disk2 + 1 == disk3)))
                || ((disk1 - 1 == disk2) && (disk2 - 1 == disk3))) {
            payoff = wage * 60;
            answer = "3 Consecutive Numbers, You Win " + payoff;
        } else if (((disk1 + 1 == disk3) && (disk2 - 1 == disk3))
                || ((disk1 - 1 == disk3) && (disk2 + 1 == disk3))
                || ((disk3 + 1 == disk1) && (disk1 - 1 == disk2))
                || ((disk1 + 1 == disk3) && (disk1 + 1 == disk3)
                || ((disk1 + 1 == disk3) && (disk1 + 1 == disk3)
                || ((disk1 + 1 == disk2) && (disk3 + 1 == disk1))))) {
            payoff = wage * 20;
            answer = "3 Consecutive Numbers, You Win " + payoff;
        } else {
            payoff = 0;
            answer = "Nothing, You Win " + payoff;
        }

        return answer;
    }

    public boolean isThreeNumbers() {
        if ((disk1 == disk2) && (disk2 == disk3)) // 100:1
        {
            return true;
        } else {
            return false;
        }
    }

    public boolean isTwoNumbers() {
        if (((disk1 == disk2) && (disk1 != disk3)) || ((disk2 == disk3)
                && (disk1 != disk2)) || ((disk1 == disk3)) && (disk2 != disk3)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConNumbers() {
        if ((((disk1 + 1 == disk2)) && ((disk2 + 1 == disk3)))
                || ((disk1 - 1 == disk2) && (disk2 - 1 == disk3))) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isConNumberAgain() {
        if ((((disk1 + 1 == disk2)) && ((disk2 + 1 == disk3)))
                || ((disk1 - 1 == disk2) && (disk2 - 1 == disk3))
                || ((disk1 + 1 == disk3) && (disk2 - 1 == disk3))
                || ((disk1 - 1 == disk3) && (disk2 + 1 == disk3))
                || ((disk3 + 1 == disk1) && (disk1 - 1 == disk2))
                || ((disk1 + 1 == disk3) && (disk1 + 1 == disk3)
                || ((disk1 + 1 == disk2) && (disk3 + 1 == disk1)))) {

            return true;
        } else {
            return false;
        }
    }
}
