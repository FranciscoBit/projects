/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
import javax.swing.JOptionPane;
public class SlotMachineTest 
{
    public static void main(String args[]) 
    {
        
        String getInput;
        int disk1;
        int disk2;
        int disk3;
        int wage;
        
        getInput = JOptionPane.showInputDialog("Enter Disk 1 Value:  ");
        disk1 = Integer.parseInt(getInput);
        
        getInput = JOptionPane.showInputDialog("Enter Disk 2 Value:  ");
        disk2 = Integer.parseInt(getInput);
        
        getInput = JOptionPane.showInputDialog("Enter Disk 3 Value:  ");
        disk3 = Integer.parseInt(getInput);
        
        getInput = JOptionPane.showInputDialog("Enter Wage Value:  ");
        wage = Integer.parseInt(getInput);
        
        SlotMachine slotMachine = new SlotMachine(disk1, disk2, disk3, wage);


        int valueDisk1;    
        int valueDisk2;    
        int valueDisk3;  
        int valueWage;
        String play;

      
        valueDisk1 = slotMachine.GetDisk1();
        valueDisk2 = slotMachine.GetDisk2();
        valueDisk3 = slotMachine.GetDisk3();
        valueWage = slotMachine.GetWage();
        play = slotMachine.Play();
        
        System.out.println("Disk 1: " + valueDisk1);  
        System.out.println("Disk 2: " + valueDisk2);  
        System.out.println("Disk 3: " + valueDisk3);
        System.out.println("Wage: " + valueWage);
        System.out.println(play + " Dollars");
        
        boolean valueThreeNumbers;
        boolean valueTwoNumbers;
        boolean valueConNumbers;
        
        valueThreeNumbers = slotMachine.isThreeNumbers();
        valueTwoNumbers = slotMachine.isTwoNumbers();
        valueConNumbers = slotMachine.isConNumbers();
        
        System.out.println("Same Three Number: " + valueThreeNumbers); 
        System.out.println("Same Two Number: " + valueTwoNumbers); 
        System.out.println("Consecutive Numbers: " + valueConNumbers); 
        
        boolean valueConNumberAgain;
        valueConNumberAgain = slotMachine.isConNumberAgain();
        System.out.println("Consecutive Numbers Again: " + valueConNumberAgain);
        
        
    }
    
}
