
/**
 * A class that uses the Circle class.
 */
import java.awt.Shape;

public class CircleTest {

    public static void main(String args[]) {
        Circle myCircle = new Circle(10, 20, 5.5);

        double xValue;
        double yValue;
        double radius;

        xValue = myCircle.getX();
        yValue = myCircle.getY();
        radius = myCircle.getRadius();

        System.out.println("The center of myCircle is at " + xValue + ","
                + yValue + " and its radius is " + radius);

        //Question 2

        double XValue = 30.0;
        double YValue = 15.0;
        double Radius = 10.5;
        System.out.println("The center of FMCircle is at " + XValue + ","
                + YValue + " and its radius is " + Radius);

        //Question 3
        Circle BigCircle = new Circle(5, 10, 20);
        BigCircle.move(5, -10);
        System.out.println("\nAfter \"BigCircle.move(5,-10)\", "
                + "BigCircle is now at ("
                + BigCircle.getX() + "," + BigCircle.getY() + ")\n");

        //Question 4

        double NewRadius = Radius + 5;

        System.out.println("New Radius is: " + NewRadius);

        //Question 5

        double Area = (Math.PI * ((Radius * Radius)));

        System.out.println("Area of FMCircle is : " + Area);

        //Question 7

        double SuperRadius = Radius * 3;

        System.out.println("Radius of orginal FMCircle is: " + Radius
                + "\nRadius of FMCircle times 3 is: " + SuperRadius);
    }
}
