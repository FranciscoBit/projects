/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Army {

    private ArrayList<Regiment> regiment;

    public Army() {
        regiment = new ArrayList<Regiment>();
    }

    public void AddRegiment(Regiment name) {
        regiment.add(name);
    }

    public void printInitialList() throws IOException {
        PrintWriter toFile = new PrintWriter("stuff.out");

        System.out.println("Week: " + 1);
        System.out.println(Align.right("Regiment Number", 15)
                + Align.right("Regiment Name", 20)
                + Align.right("Men Number", 20)
                + Align.right("Battlefield", 20));
        System.out.println(Align.right("==============", 15)
                + Align.right("===============", 20)
                + Align.right("===============", 20)
                + Align.right("===============", 20));

        toFile.println("Week: " + 1);
        toFile.println(Align.right("Regiment Number", 15)
                + Align.right("Regiment Name", 20)
                + Align.right("Men Number", 20)
                + Align.right("Battlefield", 20));
        toFile.println(Align.right("==============", 15)
                + Align.right("===============", 20)
                + Align.right("===============", 20)
                + Align.right("===============", 20));






        Regiment largestYet = regiment.get(0);
        int men = largestYet.getMenNumber();
        for (int i = 0; i < regiment.size(); i++) {
            Regiment more = regiment.get(i);
            men = men - 50;

            String max = "";

            if (men >= largestYet.RealValue1()) {
                largestYet = more;
                max = largestYet.GetStar();
            } else {
                max = largestYet.GetNoStar();
            }


            
            
            
            System.out.println(Align.right(more.getRegimentNumber(), 15)
                    + "     "
                    + Align.right(more.getName(), 15)
                    + "     "
                    + Align.right(men, 15)
                    + "     "
                    + Align.right(max, 15));

            toFile.println(Align.right(more.getRegimentNumber(), 15)
                    + "     "
                    + Align.right(more.getName(), 15)
                    + "     "
                    + Align.right(men, 15)
                    + "     "
                    + Align.right(max, 15));
        }
        toFile.close();	// always remember to close file!
    }

    public void PrintUpdateList() throws IOException {
        JOptionPane.showMessageDialog(null, "Next Report?");

        for (int i = 1; i <= 20; i++) {
            PrintWriter toFile = new PrintWriter("stuff.out");

            System.out.println("Week: " + (i + 1));
            System.out.println(Align.right("Regiment Number", 15)
                    + Align.right("Regiment Name", 20)
                    + Align.right("Men Number", 20)
                    + Align.right("Battlefield", 20));
            System.out.println(Align.right("==============", 15)
                    + Align.right("===============", 20)
                    + Align.right("===============", 20)
                    + Align.right("===============", 20));

            toFile.println("Week: " + (i + 1));
            toFile.println(Align.right("Regiment Number", 15)
                    + Align.right("Regiment Name", 20)
                    + Align.right("Men Number", 20)
                    + Align.right("Battlefield", 20));
            toFile.println(Align.right("==============", 15)
                    + Align.right("===============", 20)
                    + Align.right("===============", 20)
                    + Align.right("===============", 20));


            Regiment largestYet = regiment.get(0);
            int men = (largestYet.getMenNumber() - 50) + (100 * i);

            for (int j = i; j < regiment.size(); j++) {
                Regiment more = regiment.get(j);
                if (5 == more.getRegimentNumber()) {
                    men = men - 170;
                } else if ((more.getRegimentNumber() < 5)) {
                    men = men - 50;
                } else if (more.getRegimentNumber() == 6) {
                    men = men + 20;
                } else if (more.getRegimentNumber() > 6) {
                    men = men - 50;
                }


                String max = "";

                if (men >= (more.RealValue2())) {
                    largestYet = more;
                    max = largestYet.GetStar();
                } else {
                    max = largestYet.GetNoStar();
                }








                System.out.println(Align.right(more.getRegimentNumber(), 15)
                        + "     "
                        + Align.right(more.getName(), 15)
                        + "     "
                        + Align.right(men, 15)
                        + "     "
                        + Align.right(max, 15));

                toFile.println(Align.right(more.getRegimentNumber(), 15)
                        + "     "
                        + Align.right(more.getName(), 15)
                        + "     "
                        + Align.right(men, 15)
                        + "     "
                        + Align.right(max, 15));
            }
            toFile.close();	// always remember to close file!
            JOptionPane.showMessageDialog(null, "Next Report?");
        }
    }
}
