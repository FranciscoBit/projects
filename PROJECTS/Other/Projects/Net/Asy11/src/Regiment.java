/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Regiment {

    private int regimentNumber;
    private String name;
    private int numberOfMen = 1050;

    public Regiment(int regimentNumber, String name) {
        this.name = name;
        this.regimentNumber = regimentNumber;
    }

    public int getRegimentNumber() {
        return regimentNumber;
    }

    public String getName() {
        return name;
    }
    
    public int getMenNumber()
    {
        return numberOfMen;
    }
    
    public String GetStar()
    {
        return "YES!";
    }
    public String GetNoStar()
    {
        return "NO!";
    }
    public int RealValue1()
    {
            return 1000;
    }
    public int RealValue2()
    {
        return 1050;
    }
}