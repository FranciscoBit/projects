/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class BalloonTester {

    public static void main(String args[]) {
        //Question 1:
        Balloon myBalloon = new Balloon("myBalloon", 100);

        //Question 2:
        Balloon mySuperBalloon = new Balloon("mySuperBalloon", -100);

        //Question 3:
        int AltmyBaloon;
        int AltmySuperBalloon;

        AltmyBaloon = myBalloon.getAltitude();
        AltmySuperBalloon = mySuperBalloon.getAltitude();

        System.out.println("////###########Question 3 #############////");
        System.out.println("The height of myBalloon is " + AltmyBaloon + " meters");
        System.out.println("The height of mySuperBalloon is " + AltmySuperBalloon
                + " meters");
        //Question 4: 
        myBalloon.ascendTo(300);

        //Question 5:
        mySuperBalloon.adjustAltitude(150);

        //Question 6:

        String NamemyBalloon;
        String NamemySuperBalloon;

        AltmyBaloon = myBalloon.getAltitude();
        AltmySuperBalloon = mySuperBalloon.getAltitude();

        NamemyBalloon = myBalloon.getName();
        NamemySuperBalloon = mySuperBalloon.getName();

        System.out.println("////###########Question 6 #############////");
        System.out.println("The new height of " + NamemyBalloon + " is "
                + AltmyBaloon + " meters");
        System.out.println("The new height of " + NamemySuperBalloon + " is "
                + AltmySuperBalloon + " meters");
        System.out.println();

        //Question 7:
        myBalloon.adjustAltitude(-50);
        AltmyBaloon = myBalloon.getAltitude();

        System.out.println("////###########Question 7 #############////");
        System.out.println("If we decrease the altitude of our original balloon"
                + " AKA myBaloon, our new altitude will be " + AltmyBaloon + " meters");
        System.out.println();

        //Question 8: Not sure if I'm doing it right...but it seems to be 
        //giving me the right output I'm also not sure how this might affect the
        //rest of the questions
        int HeightSuper = mySuperBalloon.getAltitude();
        int HeightNormal = myBalloon.getAltitude();


        System.out.println("////###########Question 8 #############////");
        System.out.println("The height of mySuperBalloon is " + HeightSuper);

        HeightSuper = HeightNormal;

        System.out.println("But if we add the above, the height of mySuperBalloon"
                + " is the same as myBalloon " + HeightSuper);
        System.out.println();

        //Question 9:
        NamemyBalloon = myBalloon.getName();
        NamemySuperBalloon = mySuperBalloon.getName();

        mySuperBalloon.getAltitude();
        myBalloon.getAltitude();

        System.out.println("////###########Question 9 #############////");
        System.out.println("Name of Balloon in Question 1: " + NamemyBalloon);
        System.out.println("Height of myBalloon: " + HeightNormal);
        System.out.println("Name of Balloon in Question 2: " + NamemySuperBalloon);
        System.out.println("Height of mySuperBalloon: " + HeightSuper);
        System.out.println();

        //Question 10:
        int LastAlt = myBalloon.getAltitude();
        LastAlt = LastAlt * 3;

        //Question 11:
        int LastAltSuper = mySuperBalloon.getAltitude();
        LastAltSuper = LastAltSuper - 300;

        //Question 12:
        NamemyBalloon = myBalloon.getName();
        NamemySuperBalloon = mySuperBalloon.getName();

        System.out.println("////###########Question 12 #############////");
        System.out.println("Name of Balloon in Question 1: " + NamemyBalloon);
        System.out.println("Height of myBalloon: " + LastAlt);
        System.out.println("Name of Balloon in Question 2: " + NamemySuperBalloon);
        System.out.println("Height of mySuperBalloon: " + LastAltSuper);
        System.out.println();
    }
}
