/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
import java.text.NumberFormat;        //to format o/p as currency

/**
 * Computes change due - using the minimum coinage possible - when
 * the user enters the amount of a purchase and the amount tendered 
 */
public class ChangeMaker {
    //instance var's

    private double amountDue;
    private double amountTendered;
    private int dollarsDue;
    private int quartersDue;
    private int dimesDue;
    private int nickelsDue;
    private int penniesDue;
    private NumberFormat formatter = NumberFormat.getCurrencyInstance();

    /**
     * Construct a ChangeMaker object
     * @param totalDue the amount of the purchase
     * @param amountRecieved the amount tendered (paid)
     */
    public ChangeMaker(double totalDue, double amountReceived) {
        //set instance variables to parameters passed
        amountDue = totalDue;
        amountTendered = amountReceived;
        //other instances var's automatically initialize to 0

        System.out.println("\nAmount Due: " + formatter.format(amountDue));
        System.out.println("Amount Received: " + formatter.format(amountTendered));
    }

    /**
     * Computes the number of coins of each denomination to be given as a change
     */
    public void computeChange() {
        int changeDue;  //total change due, in cents

        if (amountDue == amountTendered) {
            System.out.println("\nExact change! Thanks and have a nice day!");
        } else if (amountDue > amountTendered) //owes morem oney!
        {
            System.out.println("\nLo siento, you still owe me another "
                    + formatter.format(amountDue - amountTendered));
        } else //has change coming
        {
            //computes change due, in cents for easier claculations
            changeDue = (int) Math.round((amountTendered - amountDue) * 100);

            //computes number of dollars due, and reduce change due accordindly
            dollarsDue = changeDue / 100;
            changeDue = changeDue % 100;

            //now do the same for all other coins

            quartersDue = changeDue / 25;
            changeDue = changeDue % 25;

            dimesDue = changeDue / 10;
            changeDue = changeDue % 10;

            nickelsDue = changeDue / 5;
            changeDue = changeDue % 5;

            penniesDue = changeDue; //change due is <= 4 cents

            //activate change dispenser
            this.giveChange();
        }
    }

    //dispenses (i.e.,prints) the coins comprising the change
    private void giveChange() {
        System.out.println("\nYour change: ");
        if (dollarsDue != 0) {
            System.out.println("\t" + dollarsDue + " dollar(s)");
        }
        if (quartersDue != 0) {
            System.out.println("\t" + quartersDue + " quarter(s)");
        }
        if (dimesDue != 0) {
            System.out.println("\t" + dimesDue + " nickel(s)");
        }
        if (nickelsDue != 0) {
            System.out.println("\t" + nickelsDue + " nickel(s)");
        }
        if (penniesDue != 0) {
            System.out.print("\t" + penniesDue);
            if (penniesDue == 1) {
                System.out.println(" penny");
            } else {
                System.out.println(" pennies");
            }
        }
        System.out.println("\nThanks and have a nice day!");
    }
} //end of ChangeMaker class declaration
