/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class Regiment {

    private int regimentNumber;
    private String name;
    private int numberOfMen;

    public Regiment(int regimentNumber, String name) {
        this.name = name;
        this.regimentNumber = regimentNumber;
    }

    public int getRegimentNumber() {
        return regimentNumber;
    }

    public String getName() {
        return name;
    }

    public int getNumberOfMen() { 
        return numberOfMen;
    }
}



