/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.IOException;
import javax.swing.JOptionPane;
import java.io.PrintWriter;

import java.util.ArrayList;

public class Army {

    private ArrayList<Regiment> regiment;

    public Army() {
        regiment = new ArrayList<Regiment>();
    }

    public void AddRegiment(Regiment name) {
        regiment.add(name);
    }

    public void PrintList() throws FileNotFoundException {

        int miniWeek = 1;
        String input;
        input = JOptionPane.showInputDialog("Enter Week"
                + "(Cancel to quit)");
        int week = Integer.parseInt(input);

        // create output file in default folder
        PrintWriter toFile = new PrintWriter("stuff.out");

        // print headings to screen
        System.out.println("Week: " + week);
        System.out.println(Align.right("Regiment Number", 15)
                + Align.right("Regiment Name", 20)
                + Align.right("Men Number", 20));
        System.out.println(Align.right("==============", 15)
                + Align.right("===============", 20)
                + Align.right("===============", 20));
        // print headings to file
        toFile.println("Week: " + week);
        toFile.println(Align.right("Regiment Number", 15)
                + Align.right("Regiment Name", 20)
                + Align.right("Men Number", 20));
        toFile.println(Align.right("==============", 15)
                + Align.right("===============", 20)
                + Align.right("===============", 20));

        int men = 1050;
        for (int i = 0; i < regiment.size(); i++) {
            Regiment more = regiment.get(i);
            men = men - 50;
            if (week == 1) {
                System.out.println(Align.right(more.getRegimentNumber(), 15)
                        + "     "
                        + Align.right(more.getName(), 15)
                        + "     "
                        + Align.right(men, 15));

                toFile.println(Align.right(more.getRegimentNumber(), 15)
                        + "     "
                        + Align.right(more.getName(), 15)
                        + "     "
                        + Align.right(men, 15));
                
            } else if (week > 1) {
                while (miniWeek < week) { 
                    miniWeek++;
                    if(more.toString().compareTo("Elephants") == 0)
                    {
                        men = men + 30;
                    }
                    else
                        men = men + 100;
                        
                    }
                }
                System.out.println(Align.right(more.getRegimentNumber(), 15)
                        + "     "
                        + Align.right(more.getName(), 15)
                        + "     "
                        + Align.right(men, 15));

                toFile.println(Align.right(more.getRegimentNumber(), 15)
                        + "     "
                        + Align.right(more.getName(), 15)
                        + "     "
                        + Align.right(men, 15));
        }
    }

    public String getMaximum() {
        if (regiment.size() == 0) // if no accounts, return null
        {
            return null;
        }

        // assume first account is the largest 
        Regiment largestYet = regiment.get(0);
        String name = largestYet.getName();
        // for all other accounts

        for (int i = 1; i < regiment.size(); i++) {
            Regiment current = regiment.get(i);		// get next account
            // is current account > highest so far?
            if (current.getNumberOfMen() > largestYet.getNumberOfMen()) {
                largestYet = current;
            }
            largestYet.getName();
        }
        return name;
    }

    public Regiment find(String target) {
        for (int i = 0; i < regiment.size(); i++) {
            Regiment current = regiment.get(i);		   // get the account
            String accountNum = current.getName();  // get account number
            if (accountNum.equals(target)) // found a match
            {
                return current;
            }
        }
        return null; // No match in the entire array list
    }
}
