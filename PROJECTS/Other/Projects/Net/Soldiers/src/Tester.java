/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import javax.swing.JOptionPane;

public class Tester {

    public static void main(String[] args) throws IOException {
        Army superArmy = new Army();

        Scanner fileScan = new Scanner(new File("regiments.txt"));

        while (fileScan.hasNext()) // while not eof2
        {
            int regimentNumber = fileScan.nextInt();
            String name = fileScan.next();
            Regiment next = new Regiment(regimentNumber, name);
            superArmy.AddRegiment(next);
            
        }
        superArmy.PrintList();
        System.out.println("Send To The Battlefield: " + superArmy.getMaximum());
    }
}
