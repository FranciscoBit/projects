
import javax.swing.JOptionPane;

public class HeronTest {

    public static void main(String args[]) {
        double side1;
        double side2;
        double side3;
        String input;
        
        // insert statements here to get the lengths of the sides
        // from the user and store them in side1, side2, and side2
        input = JOptionPane.showInputDialog("Enter Side 1");
        side1 = Double.parseDouble(input);
        input = JOptionPane.showInputDialog("Enter Side 2");
        side2 = Double.parseDouble(input);
        input = JOptionPane.showInputDialog("Enter Side 3");
        side3 = Double.parseDouble(input);
        
        Heron heron = new Heron(side1, side2, side3);

        System.out.println("A triangle with sides of " + heron.toString()
                + "...\n...has area of " + heron.getArea());
    }
}
