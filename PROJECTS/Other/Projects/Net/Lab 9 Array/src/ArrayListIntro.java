//	File:  ArrayListIntro.java   

// Uses an "ArrayList of Integer" object to show list processing basics: 
// using a for statement to traverse a list, and ArrayList methods add(), 
// size(), and get(). 
import java.util.ArrayList;
import java.util.Random;

/**
 * A class to demonstrate the basics of list processing
 */
public class ArrayListIntro {

    public static void main(String[] args) {
        Random r = new Random();

        ArrayList<Integer> list = new ArrayList<Integer>();
        
        // ***** POPULATE the list *****
        
        // add 10 random ints between 10 and 99 to the list
        for (int i = 1; i <= 10; i++) {
            list.add(r.nextInt(90) + 10);
        }
        
        // ***** PROCESS the list *****
        
        // print the list
        System.out.print("The list:  ");
        for (int i = 0; i < list.size(); i++) 
        {
            int num = list.get(i) ;
            System.out.print( num + "  " );
        }

        // print the list backwards
        System.out.print("\n\nThe list backwards:  ");
        for (int i = list.size() - 1; i >= 0; i--) 
        {
            int number = list.get(i) ;
            System.out.print( number + "  " );
        }

        // compute the sum of all ints on the list
        int sum = 0;
        for (int i = 0; i < list.size(); i++) 
        {
            int num = list.get(i);	// get next int from the list...
            sum = sum + num;		// ...and add to sum
        }
        System.out.println("\n\nThe sum of the numbers on the list is " + sum);

        // find the largest value on the list and its position
        int max = list.get(0);  	// assume max is in first element
        int indexOfMax = 0;     	// first element has index 0

        // for all other elements...
        for (int i = 1; i < list.size(); i++) 
        {
            int current = list.get(i);	// get next int from list
            if (current > max)          // if greater than current max...
            {
                max = current;		// ...we have a new max
                indexOfMax = i;         // ... at this index (position)
            }
        }
        System.out.println("\nThe largest value on the list is " + max
                + " (in element #" + indexOfMax + ")");
    }
}

/*  sample output

The list:  48  34  88  73  15  12  10  25  62  26  

The list backwards:  26  62  25  10  12  15  73  88  34  48  

The sum of the numbers on the list is 393

The largest value on the list is 88 (in element #2)

*/