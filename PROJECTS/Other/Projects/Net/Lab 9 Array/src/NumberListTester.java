
import java.util.*;		// for ArrayList and Random classes

/**
 * A class to provide practice using ArrayLists
 */
import javax.swing.JOptionPane;
import java.util.ArrayList;

class NumberList {
    // instance var's

    private ArrayList<Integer> aList;		// a list of Integer objects
    private ArrayList<Integer> aBigList;

    /**
     * Creates a NumberList object.
     */
    public NumberList() {
        aList = new ArrayList<Integer>();	// creates an empty list
        aBigList = new ArrayList<Integer>();
    }

    /**
     * Adds a number to the list.
     * @param number the number to be added to the list
     */
    public void add(int number) {
        aList.add(number);		// calls add method of ArrayList class
    }

    /**
     * Prints the numbers stored in aList.
     */
    public void printList() {
        System.out.println("The numbers on the list: ");

        for (int i = 0; i < aList.size(); i++) {
            System.out.print(aList.get(i) + " ");
        }
        System.out.println("\n");


    }

    public void printBigList() {
        System.out.println("The numbers on the list: ");

        for (int i = 0; i < aBigList.size(); i++) {
            System.out.print(aBigList.get(i) + " ");
        }
        System.out.println("\n");
    }

    /**
     * Prints the numbers stored in aList, in reverse order.
     */
    public void printReversed() {
        System.out.println("The numbers on the list, in reverse order: ");
        // write your code here

        for (int i = aList.size() - 1; i >= 0; i--) {
            System.out.print(aList.get(i) + " ");
        }
        System.out.println("\n");
    }

    /**
     * Prints every other number stored in aList, starting with the first one.
     */
    public void printEveryOther() {
        System.out.println("Starting with the first, every other number: ");
        // write your code here
        for (int i = 0; i < aList.size(); i++) {
            if ((i % 2) == 0) {
                System.out.print(aList.get(i) + " ");
            }
        }
        System.out.println("\n");
    }

    /**
     * Prints all the even-numbered ints stored in aList.
     */
    public void printEvens() {
        System.out.println("The even numbers on the list: ");
        // write your code here
        for (int i = 0; i < aList.size(); i++) {
            if ((aList.get(i) % 2) == 0) {
                System.out.print(aList.get(i) + " ");
            } else {
                System.out.print("");
            }
        }
        System.out.println("\n");
    }

    /**
     * Copies all ints that are 50 or greater from aList to bigList, and 
     * removes them from aList.
     */
    public void splitList() {
        System.out.println("The BigList numbers are(50>): ");
        // write your code here
        for (int i = 0; i < aList.size(); i++) {
            if (aList.get(i) >= 50) {
                aBigList = aList;
                System.out.print(aBigList.get(i) + " ");
            }
        }

        System.out.println("\n");
    }

    public void Insert(int number2, int number3) {
        aList.add(number2 - 1, number3);
        if ((number2 < 0) || (number2 > 9)) {
            System.out.println("ERROR!");
        } else {
            System.out.println("");
        }
    }
}

public class NumberListTester {

    public static void main(String[] args) {
        Random r = new Random();
        NumberList list = new NumberList();

        // fill list with 10 random 2-digit ints (i.e., 10 to 99)
        for (int i = 1; i <= 10; i++) {
            int next = r.nextInt(90) + 10;
            // call the "add" method of the NumberList class
            list.add(next);
        }

        // print the aList
        list.printList();
        list.printReversed();
        list.printEveryOther();
        list.printEvens();
        list.splitList();

        String input = JOptionPane.showInputDialog("Enter index\n(or click Cancel to quit)");
        int number2 = Integer.parseInt(input);
        String input1 = JOptionPane.showInputDialog("Enter integer\n(or click Cancel to quit)");
        int number3 = Integer.parseInt(input1);
        list.Insert(number2, number3);
        list.printList();
    }
}