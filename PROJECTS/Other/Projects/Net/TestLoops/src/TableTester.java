//  File: TableTester.java

//  Class MultiplicationTable prints a multiplication table for 1's through 
//  10's.  The table printed may or may not contain duplicate values,
//  depending on a boolean value passed to the constructor.
//  Demonstrates "nested" for statements
//  Note: Depends on Align class for formatted output
class MultiplicationTable {
    // instance var

    boolean duplicates;

    // constructor
    public MultiplicationTable(boolean duplicates) {
        this.duplicates = duplicates;
    }

    // print the column headings for a table
    private void printColHeadings() {
        System.out.print("         ");			// indents 9 spaces

        // for each number from 1 to 10...
        for (int i = 1; i <= 10; i++) {
            // print number right-aligned in a field 5 spaces wide
            System.out.print(Align.right(i, 5));
        }
        System.out.print("\n         ");		// new line and indent

        for (int i = 1; i <= 50; i++) // underline col headings
        {
            System.out.print('-');
        }
        System.out.println();
    }

    public void printTable() {
        this.printColHeadings();	// print column headings

        int innerLoopLimit;		// controls # of iterations of inner loop

        // for each row of the table...
        for (int mult1 = 1; mult1 <= 10; mult1++) {
            // print row headings
            System.out.print(Align.right(mult1, 8) + "|");

            if (duplicates) {
                innerLoopLimit = 10;		// each row has 10 products
            } else {
                innerLoopLimit = mult1;	// each row has only "mult1" products
            }
            // for each column of the table
            for (int mult2 = 1; mult2 <= innerLoopLimit; mult2++) {
                int product = mult1 * mult2;
                System.out.print(Align.right(product, 5));
            }
            System.out.println();
        }
        System.out.println("\n");
    }
}

public class TableTester {

    public static void main(String[] args) {
        MultiplicationTable table = new MultiplicationTable(true);

        System.out.println("       "
                + "Multiplication table with duplicate values:\n");
        table.printTable();

        table = new MultiplicationTable(false);

        System.out.println("       "
                + "Multiplication table with no duplicate values:\n");
        table.printTable();
    }
}

/*  program output:
       
       Multiplication table with duplicate values:

             1    2    3    4    5    6    7    8    9   10
         --------------------------------------------------
       1|    1    2    3    4    5    6    7    8    9   10
       2|    2    4    6    8   10   12   14   16   18   20
       3|    3    6    9   12   15   18   21   24   27   30
       4|    4    8   12   16   20   24   28   32   36   40
       5|    5   10   15   20   25   30   35   40   45   50
       6|    6   12   18   24   30   36   42   48   54   60
       7|    7   14   21   28   35   42   49   56   63   70
       8|    8   16   24   32   40   48   56   64   72   80
       9|    9   18   27   36   45   54   63   72   81   90
      10|   10   20   30   40   50   60   70   80   90  100

       
       Multiplication table with no duplicate values:

             1    2    3    4    5    6    7    8    9   10
         --------------------------------------------------
       1|    1
       2|    2    4
       3|    3    6    9
       4|    4    8   12   16
       5|    5   10   15   20   25
       6|    6   12   18   24   30   36
       7|    7   14   21   28   35   42   49
       8|    8   16   24   32   40   48   56   64
       9|    9   18   27   36   45   54   63   72   81
      10|   10   20   30   40   50   60   70   80   90  100

*/