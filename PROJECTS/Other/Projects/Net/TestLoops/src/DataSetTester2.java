//	File:	DataSetTester2.java

//	Shows how an input statement can be used as a loop condition!
//  Note: DataSet class is the same as in previous version.
import javax.swing.JOptionPane;

/**
 * Computes statistics for a set of data.
 */
class DataSet2 {

    private double sum;			// sum of the values in the set
    private int count;				// number of values in the set
    private double maximum;		// largest value

    /**
     * Create an empty data set.
     */
    public DataSet2() {
        count = 0;
        sum = 0;
        maximum = Double.NEGATIVE_INFINITY;	// Java constant
    }

    /**
     * add a value to the data set.
     * @param currentValue the number to be added to the set
     */
    public void add(double currentValue) {
        sum = sum + currentValue;		// add new number to the sum
        count++;						// keep count of numbers in the set

        if (currentValue > maximum) // if current value is largest so far...	
        {								// (note: must be true first time)
            maximum = currentValue;	// ...current value becomes new largest
        }
    }

    /**
     * Return the largest value in the set.
     * Note: if set is empty, will return NEGATIVE INFINITY
     * @return the largest value
     */
    public double getMaximum() {
        return maximum;
    }

    /**
     * Return the average of the values in the set.
     * Note: if set is empty, will return INFINITY
     * @return average
     */
    public double getAverage() {
        return sum / count;
    }

    /** 
     * Is the set empty?
     * @return true if the set is empty, false if nonempty
     */
    public boolean isEmpty() {
        return count == 0;
    }
}

public class DataSetTester2 {

    public static void main(String[] args) {
        DataSet set = new DataSet();	// create an empty data set
        String input;
        String prompt = "Enter a value\n(press Cancel when done)";

        System.out.print("Data entered:  ");

        //while input does not eqaul to cancel
        //parse(make) the input(string) into a value(int)
        //print this value and add it
        while ((input = JOptionPane.showInputDialog(prompt)) != null) {
            double value = Double.parseDouble(input);
            System.out.print(value + "  ");

            set.add(value);			// add value to set
        }

        //if cancel print error message
        if (set.isEmpty()) // if set is empty, print error message
        {
            System.out.println("ERROR! No data entered!");
        } else // set is non-empty, print stats
        {
            System.out.println("\n\nThe average is:\t" + set.getAverage()
                    + "\nThe maximum is:\t" + set.getMaximum());
        }
    }
}

/*  sample output from two runs:

Data entered:  1.2  3.4  1.75  4.44  2.23  

The average is: 2.604
The maximum is: 4.44


Data entered:  ERROR! No data entered!

 */
