// File: InvestmentTester.java

// Shows a while loop, for loop, accumulators, and counters
// NOTE: The InvestmentTester class "depends on" the Align class to
//       format the output
import javax.swing.JOptionPane;

/**
A class to monitor the growth of an investment that accumulates interest 
at a fixed annual rate.
 */
class Investment {
    // instance variables

    private double balance;	// initial investment + accumulted interest
    private double rate;	// annual interest rate, as a per cent
    private int years;		// number of years until an investment reaches
    // a certain "target" balance

    /**
    Creates an Investment object from a starting balance and interest rate.
    @param aBalance the starting balance
    @param aRate the interest rate in percent
     */
    public Investment(double aBalance, double aRate) {
        balance = aBalance;
        rate = aRate;
        years = 0;
    }

    /**
    Keeps accumulating interest until a target balance has
    been reached.
    @param targetBalance the desired balance
     */
    public void waitForBalance(double targetBalance) {
        while (balance < targetBalance) {
            years++;
            double interest = balance * rate / 100;
            balance = balance + interest;
        }
    }

    /**
    Keeps accumulating interest for a given number of years.
    @param numberOfYears the number of years to wait
     */
    public void waitYears(int numberOfYears) {
        for (int year = 1; year <= numberOfYears; year++) {
            double interest = balance * rate / 100;
            balance = balance + interest;
        }
    }

    /**
    Gets the current investment balance.
    @return the current balance
     */
    public double getBalance() {
        return balance;
    }

    /**
    Gets the number of years this investment has accumulated
    interest.
    @return the number of years since the start of the investment
     */
    public int getYears() {
        return years;
    }
}

public class InvestmentTester {

    public static void main(String[] args) {
        double amountInvested;	// initial amount invested
        double rate;		// annual percentage rate of interest
        double target;         // desired value for investment to reach
        int years;		// number of years for investment to double

        String input = JOptionPane.showInputDialog(
                "What is the initial amount invested?");

        amountInvested = Double.parseDouble(input);

        input = JOptionPane.showInputDialog("What is the annual interest rate"
                + " (as a per cent)?" + "(ex: for 6 1/2%, enter 6.5)");

        rate = Double.parseDouble(input);

        input = JOptionPane.showInputDialog("What is the desired \"target\""
                + " amount for the investment to reach?");

        target = Double.parseDouble(input);
        // "echo print" inputs
        System.out.println("Principal: " + Align.currency(amountInvested, 0)
                + "\nAnnual interest rate: " + rate + "%");

        // first, test the method with the "while"

        // create Investment object using inputs
        Investment invest = new Investment(amountInvested, rate);

        invest.waitForBalance(target);

        years = invest.getYears();

        System.out.println("\nIt took " + years + " years for "
                + Align.currency(amountInvested, 0)
                + " to reach a target value of "
                + Align.currency(invest.getBalance(), 0));

        // now test the method with the "for"

        input = JOptionPane.showInputDialog("What is the term of the investment in years?");
        years = Integer.parseInt(input);

        // create new object with previous inputs (since original balance
        // was changed in the object)

        invest = new Investment(amountInvested, rate);
        invest.waitYears(years);
        double balance = invest.getBalance();
        System.out.println("\nAfter " + years + " years, our investment of "
                + Align.currency(amountInvested, 0)
                + " is now worth " + Align.currency(balance, 0));

    }
}

/* Sample output:

Principal: $1,000.00
Annual interest rate: 10.0%

It took 8 years for $1,000.00 to double in value to $2,143.59




 */
