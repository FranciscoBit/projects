//	File:  PrimeMinister.java 

//	Class PrimeFinder finds and prints the first "n" prime numbers, 10 per line.
//	Demonstrates the conditional loops ("while" and "do-while"), a boolean 
//  method, and private "utility" methods.  
//  Utility methods do behind-the-scenes chores (aka: "housekeeping" chores).
//  They are declared private because they are called only by other methods of
//  the class, and not by client code. 
//  NOTE: The PrimeFinder class "depends on" the Align class to format the 
//  output in columns 
import javax.swing.JOptionPane;

/**
 * Computes and print the first "n" prime numbers.
 */
class PrimeFinder {

    private int howMany;		// number of primes to be found

    /**
     *  Construct a PrimeFinder object
     *  @param howMany the number of primes to be found
     */
    public PrimeFinder(int howMany) {
        this.howMany = howMany;
    }

    /**
     * Prints the first N prime numbers
     */
    public void printPrimes() {
        int count = 0;			// counts actual number of primes found
        int number = 1;   		// each of a series of ints to be tested for
        // "primehood," beginning with 1

        System.out.println("\nThe First " + howMany + " Prime Numbers:\n");

        // find and print primes
        do // repeat...
        {
            if (isPrime(number)) // ...if number is prime ...
            {
                // ...print it right-aligned in columns 6 spaces wide
                System.out.print(Align.right(number, 6));
                count++;                   // .......and count it
                if (count % 10 == 0) // ...every ten primes ...
                {
                    System.out.println();	// ...... start new line
                }
            }

            number++;						// ...get next test number
        } while (count < howMany);			// until required # of primes found

        System.out.println();
    }

    // See if a positive int is a prime number by trying to divide it by a
    // series of trial divisors
    private boolean isPrime(int num) {
        // Handle special cases of 1, 2, and all even numbers first ...

        if (num == 1) // 1 is NOT prime, by def., so return false 
        {
            return false;
        }

        if (num == 2) // 2 is the only EVEN prime, so return true
        {
            return true;
        }

        if (num % 2 == 0) // multiple of 2?  not prime
        {
            return false;
        }

        // here only if num is an odd number >= 3 ...

        // compute largest trial divisor we need to test
        int limit = (int) Math.round(Math.sqrt(num));

        int testDiv = 3;			   // 3 will be first trial divisor

        while (testDiv <= limit) // while more trial divisors to test
        {
            if (num % testDiv == 0) // current test divisor works...
            {
                return false;           // ...exit and return false
            } else // current testDiv not a divisor ...
            {
                testDiv += 2;           // ...get next trial divisor
            }
        }

        // loop postcondition:  testDiv > limit (i.e., all trial divisors
        //                      were tested and none worked) ...

        return true;           // ...so number is prime
    }
}   // end of PrimeFinder class definition ====================================

/**
 * A test class for the PrimeFinder class.
 */
public class PrimeMinister {

    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "I will find and print the first N prime numbers!");

        String input = JOptionPane.showInputDialog("Enter N (number of primes to find)");

        int howMany = Integer.parseInt(input);

        PrimeFinder primes = new PrimeFinder(howMany);

        primes.printPrimes();
    }
}

/*  sample output:

The First 100 Prime Numbers:

2     3     5     7    11    13    17    19    23    29
31    37    41    43    47    53    59    61    67    71
73    79    83    89    97   101   103   107   109   113
127   131   137   139   149   151   157   163   167   173
179   181   191   193   197   199   211   223   227   229
233   239   241   251   257   263   269   271   277   281
283   293   307   311   313   317   331   337   347   349
353   359   367   373   379   383   389   397   401   409
419   421   431   433   439   443   449   457   461   463
467   479   487   491   499   503   509   521   523   541

 */
