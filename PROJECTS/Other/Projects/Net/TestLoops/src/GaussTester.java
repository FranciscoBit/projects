//	File:  GaussTester.java

//	The Gauss class computes the sum of the integers from 1 to n, using a 
//  "brute force" approach.  N is entered by the user.
//	Demonstrates a "for" loop and an accumulator
import javax.swing.JOptionPane;

/**
 * A class to demonstrate a for statement and an accumulator.
 */
class Gauss {

    private int sum;				// sum of int's from 1 to n
    private int highInt;			// n

    /**
     * Create a Gauss object.
     * @param limit the highest int to be added
     */
    public Gauss(int limit) {
        sum = 0;
        highInt = limit;
    }

    /**
     * Compute the sum of the ints from 1 to highInt.
     * @return the sum
     */
    public int getSum() {
        // for each int from 1 to highInt...

        for (int number = 1; number <= highInt; number++) {
            sum = sum + number;	// ...add current number to sum
        }
        return sum;
    }
}

public class GaussTester {

    public static void main(String[] args) {
        String input = JOptionPane.showInputDialog("I will compute the sum of "
                + "the integers from 1 to N.\n\nPlease enter N");

        int limit = Integer.parseInt(input);

        Gauss karl = new Gauss(limit);

        System.out.println("\nThe sum of the integers from 1 to " + limit
                + " is " + karl.getSum());
        System.exit(0);
    }
}

/*  sample output:

The sum of the integers from 1 to 100 is 5050

*/