//	File:	DataSetTester.java

//	The DataSet class computes the average and maximum for a set of data consisting 
//  of any number of values.
//  The test class shows how to use JOptionPane to process any number of input values
//  until end-of-file.
//  Note: To indicate end-of-file, just click the Cancel button.
import javax.swing.JOptionPane;

/**
 * Computes average and maximum of a set of data values.
 */
class DataSet {

    private double sum;			// sum of the values in the set
    private int count;				// number of values in the set
    private double maximum;		// largest value

    /**
     * Create an empty data set.
     */
    public void DataSet() {
        count = 0;
        sum = 0;
        maximum = Double.NEGATIVE_INFINITY;	// Java constant
    }

    /**
     * add a value to the data set.
     * @param currentValue the number to be added to the set
     */
    public void add(double currentValue) {
        sum = sum + currentValue;		// add new number to the sum
        count++;						// keep count of numbers in the set

        if (currentValue > maximum) // if current value is largest so far...	
        {								// (note: must be true first time)
            maximum = currentValue;	// ...current value becomes new largest
        }
    }

    /**
     * Return the largest value in the set.
     * Note: if set is empty, will return NEGATIVE INFINITY
     * @return the largest value
     */
    public double getMaximum() {
        return maximum;
    }

    /**
     * Return the average of the values in the set.
     * Note: if set is empty, will return INFINITY
     * @return average
     */
    public double getAverage() {
        return sum / count;
    }

    /** 
     * Is the set empty?
     * @return true if the set is empty, false if nonempty
     */
    public boolean isEmpty() {
        return count == 0;
    }
}

public class DataSetTester {

    public static void main(String[] args) {
        DataSet set = new DataSet();	// create empty data set

        String input = JOptionPane.showInputDialog("Enter first data value "
                + "(or press Cancel to quit)");

        System.out.println("Data entered:");

        while (input != null) // while more data values...
        {
            double value = Double.parseDouble(input);

            // "echo print" input data
            System.out.println(value);

            set.add(value);			// add data value to set

            input = JOptionPane.showInputDialog("Enter next data value "
                    + "(or press Cancel to quit)");
        }

        if (set.isEmpty()) // if set is empty, print error message
        {
            System.out.println("ERROR! No data entered!");
        } else // set is non-empty, print stats
        {
            System.out.println("\nThe average is:\t" + set.getAverage()
                    + "\nThe maximum is:\t" + set.getMaximum());
        }
    }
}

/*  sample output from two runs:

Data entered:
1.0
3.0
2.0
4.0

The average is: 2.5
The maximum is: 4.0


Data entered:
ERROR! No data entered!

 */
