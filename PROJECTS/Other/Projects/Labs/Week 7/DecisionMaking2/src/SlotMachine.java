/**
 * A class to represent a slot machine with three rotating 
 * disks.  Each disk contains the int's from 0 to 9.  If 
 * all three numbers are the same or any two numbers are 
 * the same, the player wins.  Otherwise the player loses.
 */
public class SlotMachine 
{
	// instance var's
	private int disk1 ;			// the first disk
	private int disk2 ;			// the second disk
	private int disk3 ;			// the third disk
	
	/**
	 * Create a SlotMachine object
	 * @param disk1 the number on the first disk
	 * @param disk2 the number on the second disk
	 * @param disk3 the number on the third disk
	 */
    public SlotMachine(int disk1, int disk2, int disk3) 
    {
    	this.disk1 = disk1 ;
    	this.disk2 = disk2 ;
    	this.disk3 = disk3 ;
    }
    
   /**
    * Convert a SlotMachine object to a String
    * @return a string containging the numbers that
    * came up on the 3 disks
    */    
    public String toString()
    {
    	return disk1 + "  " + disk2 + "  " + disk3 ;
    }
    
   /**
    * Returns true or false indicating whether all three
    * numbers are the same
    * @return true if all three numbers are the same,
    * otherwise false
    */
    public boolean allThreeEqual()
    {
        if((disk1 == disk2) && (disk2 == disk3))
        {
            return true;
        }
        else
        {
            return false ;
        }
    }
    	
   /**
    * Returns a message indicating the outcome of a spin
    * and, if the spin is a winner, the amount won
    * @return a String indicating the amount won or 
    * "Sorry, you lose" if the spin is not a winner
    */
    public String getResult()
    {
        if(this.allThreeEqual() == true )
        {
            return "Part 4: All three numbers are the same" ;
        }
        else if (this.TwoEqauls() == true)
        {
            return "Part 4: Only two are equal!";
        }
        else
        {
            return "Sorry, /you lose!" ;
        }
    }
    
    
    public boolean TwoEqauls()
    {
        if( (disk1 == disk2) || (disk1 == disk3) || ( disk2 == disk3))
        {
            return true;
        }
        else
        {
            return false;
        }
    
    }
    
    
    
}