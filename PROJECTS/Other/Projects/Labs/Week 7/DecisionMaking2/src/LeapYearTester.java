//  File:  LeapYearTester.java

//  Purpose:  Indicates whether or not a year - entered by the user - 
//            is a leap year.  A year is a leap year if either one of
//            the following is true:
//			  1.	the year is divisible by 4 and not by 100
//			  2.	the year is divisible by 400

//  Demonstrates boolean type (variables, assignments, operators, and methods)

import javax.swing.JOptionPane ;

/**
 * A class thats tests whether a given year is a leap year
 */
public class LeapYearTester
{
	// instance var
	private int year ;				// 4-digit year being tested
	
	/**
	 * Create a LeapYearTester object.
	 * @param inputYear the year being tested
	 * Precondition: inputYear >= 1583
	 */
	public LeapYearTester (int inputYear)
	{
		year = inputYear ;
	}
		
	/**
	 * Is a year a leap year?
	 * @return true if year is a leap year, false if not
	 */
	public boolean isLeapYear()
	{
   		boolean ordLeapYr ;   	// true if "ordinary" leap year (every 4 years
   		                        // except for years ending in 00)
  		boolean centLeapYr ;  	// true if "century" leap year (every 400 years)

		ordLeapYr  = (year % 4 == 0) && !(year % 100 == 0) ;
   		centLeapYr = (year % 400 == 0) ;

   		return ordLeapYr || centLeapYr ;   // true if either is true
	}

	public static void main(String[] args)
	{
   		String input = JOptionPane.showInputDialog("Enter a 4-digit year") ;
   		
   		int inputYear = Integer.parseInt(input) ;
   		
   		LeapYearTester testYear = new LeapYearTester(inputYear) ;

   		// Is it a leap year?
	
   		if ( testYear.isLeapYear() )		// calls boolean method
   		{
			System.out.println(inputYear + " is a leap year.") ;
   		}
   		else
   		{
			System.out.println(inputYear + " is not a leap year.") ;
   		}
   	}
}

/* sample output from 4 runs:

2010 is not a leap year.

2012 is a leap year.

2100 is not a leap year.

2000 is a leap year.

*/