import javax.swing.JOptionPane ;

/**
 * A test class (aka: "driver" class, aka: "client code")
 * for the SlotMachine class
 */
public class SlotMachineTest 
{
   public static void main(String[] args) 
   {
        String input = JOptionPane.showInputDialog
        	              ("What is the number on the first disk?") ;
        int disk1 = Integer.parseInt(input) ;
        	              
        input = JOptionPane.showInputDialog("The second disk?") ;
        int disk2 = Integer.parseInt(input) ;
        
        input = JOptionPane.showInputDialog("The third disk?") ;
        int disk3 = Integer.parseInt(input) ;
        
        SlotMachine bandit = new SlotMachine(disk1, disk2, disk3) ;
        
        System.out.println("\n\tYour numbers: " + bandit.toString()) ;
        
        //Question 2
        if(bandit.allThreeEqual() == true)
        {
            System.out.println("Part 2: You win $100!") ;
        }
        else
        {
            System.out.println("Part 2: Sorry, you lose!") ;
        }
        
        //Part 3
        
        if(bandit.allThreeEqual() == true)
        {
            System.out.println("Part 3: All three are equal!") ;
        }
        else if(bandit.allThreeEqual() == true)
        {
            System.out.println("Part 3: Only two are equal!") ;
        }
        else
        {
            System.out.println("Part 3: Sorry, you lose!") ;
        }
        
        //Part 4
        System.out.println(bandit.getResult()) ;
        
        
        
        
   }
}