import javax.swing.JOptionPane ;

public class HeronTest
{
    public static void main(String args[])
    {
        // insert statements here to get the lengths of the sides
        // from the user and store them in side1, side2, and side2
        
        
        String input1 = JOptionPane.showInputDialog(
                "Enter Side 1: ");
        
        String input2 = JOptionPane.showInputDialog(
                "Enter Side 2: ");
        
        String input3 = JOptionPane.showInputDialog(
                "Enter Side 3: ");
        
        
        double side1 = Double.parseDouble(input1);   // note: initialiazations necessary only
        double side2 = Double.parseDouble(input2);   // to get "skeleton" program to run
        double side3 = Double.parseDouble(input3);
        

        Heron heron = new Heron(side1, side2, side3);

        System.out.println("A triangle with sides of " + heron.toString()
                + "...\n...has area of " + heron.getArea());
    }
}
		
		
	