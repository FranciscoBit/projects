
import javax.swing.JOptionPane;


public class PaymentCalculatorTest
{
    public static void main(String[] args)
    {
        // insert statements to do input here
        // ----------------------------------
        
        String input1 = JOptionPane.showInputDialog(
                "Enter Amount Borrowed: ");
        
        String input2 = JOptionPane.showInputDialog(
                "Enter Interest Rate: ");
        
        String input3 = JOptionPane.showInputDialog(
                "Enter Number of Years: ");
        
        
        int amountBorrowed = Integer.parseInt(input1);
        double interestRate = Double.parseDouble(input2);
        int years = Integer.parseInt(input3);

        
        String input11 = JOptionPane.showInputDialog(
                "Enter Amount Borrowed: ");
        
        String input22 = JOptionPane.showInputDialog(
                "Enter Interest Rate: ");
        
        String input33 = JOptionPane.showInputDialog(
                "Enter Number of Years: ");
        
        
        int amountBorrowed1 = Integer.parseInt(input11);
        double interestRate1 = Double.parseDouble(input22);
        int years1 = Integer.parseInt(input33);
        
        



        // create PaymentCalculator object using input values
        PaymentCalculator myCalc = new PaymentCalculator
                                   (amountBorrowed, interestRate, years);
        
        PaymentCalculator myCalc1 = new PaymentCalculator
                                   (amountBorrowed1, interestRate1, years1);

        // print loan data
        System.out.println(myCalc.getData());
        System.out.println(myCalc1.getData());

        // get payment amount
        double payment = myCalc.computePayment();
        double payment1 = myCalc1.computePayment();

        // print payment amount
        System.out.println("Monthly Payment: $" + payment + "\n");
        System.out.println("===================================");
         System.out.println("Monthly Payment: $" + payment1 + "\n");
         
         
         double total = payment * years * 12;
         double total2 = payment1 * years1 * 12;
                 
         System.out.println("Total Payment: $" + total + "\n");
         System.out.println("Total Payment1: $" + total2 + "\n");
         
      
         
    }
}	// 	end of PaymentCalculatorTest class definition






