/**
 * A class that uses the Circle class.
 */
public class CircleTest
{
	public static void main (String args[])
	{
		Circle myCircle = new Circle(10,20,5.5) ;
		
		double xValue ;
		double yValue ;
		double radius ;
		
		xValue = myCircle.getX() ;
		yValue = myCircle.getY() ;
		radius = myCircle.getRadius() ;
		
		System.out.println("Question #1 : The center of myCircle is at " + xValue + "," + 
							yValue + " and its radius is " + radius) ;
                
                //Question #2 - create a new circle object and print the info
               
                Circle myNewCircle = new Circle(50,60,100) ;
                
                double xxValue = myNewCircle.getX();
                double yyValue = myNewCircle.getY();
                double rradius = myNewCircle.getRadius();
                
                
                
                System.out.println("Question #2 : The center of NEW myCircle is at " + xxValue + "," + 
							yyValue + " and its radius is " + rradius) ;
                
                //Question #3 - we now add statement to move the object
                
                Circle myBigCircle = new Circle(10,20,5.5) ;
                myBigCircle.move(8, 10);
                
                System.out.println("Question #3 : The center of  myBigCircle is at " + myBigCircle.getX() + "," + 
							myBigCircle.getY() + " and its radius is " + myBigCircle.getRadius()) ;
                
                //Question 4 - we reset radius and print new radius
                
                Circle myResetCircle = new Circle(30,290,540) ;
                myResetCircle.setRadius(50);
                
                System.out.println("Question #4 : The center of  myResetCircle is at " + myResetCircle.getX() + "," + 
							myResetCircle.getY() + " and its radius is " + myResetCircle.getRadius()) ;
                
                 //Question 5 - we compute area of the circle
                
                double superRadius = 99;
                Circle myAreaCircle = new Circle(70,399,superRadius) ;
                double area = Math.PI * (superRadius * superRadius);
                
                System.out.println("Question #5 : The center of  myAreaCircle is at " + myAreaCircle.getX() + "," + 
							myAreaCircle.getY() + " and its radius is " + myAreaCircle.getRadius() +
                        " and the Area is " + area) ;
                
                //Question 6 - move original circle to a new point as your new circle
                
                double myX = 21;
                double myY = 31;
                double myRadius = 41;
                        
                Circle myOriginalMovementCircle = new Circle(myX, myY, myRadius);
                
                System.out.println("Question #6 : The center of  myOriginalMovementCircle is at " + myOriginalMovementCircle.getX() + "," + 
							myOriginalMovementCircle.getY() + " and its radius is " + 
                        myOriginalMovementCircle.getRadius()) ;
                
                
                double myNewX = myOriginalMovementCircle.getX();
                double myNewY = myOriginalMovementCircle.getY();
                double myNewRadius = myOriginalMovementCircle.getRadius();
                
                Circle myNewMovementCircle = new Circle(myNewX, myNewY,myNewRadius  );
                
                System.out.println("Question #6 : The center of  myNewMovementCircle is at " + myNewMovementCircle.getX() + "," + 
							myNewMovementCircle.getY() + " and its radius is " + myNewMovementCircle.getRadius()) ;
                
                
                
                //Question 7 - resize circle so its radius is excatly 3 times the value
                
                double bigRadius = radius * 3;
                System.out.println("Question #7 : The center of NEW myCircle is at " + xValue + ",radius" + 
							yValue + " and its radius is " + radius + " and triple radius is " + bigRadius) ;
                
                
                
	}
}
		
		
	