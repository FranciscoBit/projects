
/**
 * A test class for the Craps class. Plays a game and prints results.
 */
import javax.swing.JOptionPane;

public class CrapsTest {

    public static void main(String[] args) {
        // create a Craps game object, with trace on
        Craps shooter = new Craps(true);


        String input;

        do {
            // play a game
            shooter.play();

            if (shooter.isWinner()) {
                System.out.println("Player wins!\n");
            } else {
                System.out.println("Sorry, player loses.\n");
            }
            input = JOptionPane.showInputDialog("Would you like to play again? Y for Yes or N for N");

        } 
        while (input.equals("Y"));
        

        
        double numberOfTrials = 1000;
        double times = 1;
        Craps shooter1 = new Craps(true);
        double numberOfWins = 0;
        double numberOfLoses = 0;
        
        while(times <= numberOfTrials)
        {
            times++;
            shooter1.play();
            
            if(shooter1.isWinner())
            {
                System.out.println("Player wins!\n");
                numberOfWins++;
            }
            else
            {
                System.out.println("Sorry, player loses.\n");
                numberOfLoses++;
            }
            
        }
        
        double percentWins = (numberOfWins / numberOfTrials) * 100;
        double percentLose = (numberOfLoses / numberOfTrials) * 100;
        

        System.out.println("% of Wins: " + percentWins + "%");
        System.out.println("% of Loses: " + percentLose + "%");

    }
}