import java.util.Random ;
import javax.swing.JOptionPane ;

/**
 * A class to represent a standard 6-sided die
 */
class Die	// die is the singular of "dice"
{
	// random number generator object
	private static Random r = new Random() ;
	
	/**
	 * Roll one die.
	 * @return a random integer from 1 to 6
	 */
	public int roll()		
	{
		return r.nextInt(6) + 1 ;
	}
}

/**
 * A class to play a matching game using two dice.  The dice are rolled and the
 * total is recorded.  Then, the dice are continually rolled again until the
 * initial roll is matched, counting the number of rolls required
 */
class MatchGame
{
	// instance var's are Die objects 
	Die d1 = new Die() ;				// create pair of dice
	Die d2 = new Die() ;

	// This method just aligns the output...
	private static String format (int number)
	{
		if (number <= 9)               // if single-digit number...
		  return " " + number ;         // ...pad with one leading space
        else                          // 2-digit number...
		  return "" + number ;         // ...no padding
	}
		
	/**
     * Plays the game.  I.e., rolls the dice to get the initial roll and counts
     * number of additional rolls required to match it.  Repeats as long as the
     * user wants to play another game
     */
    public void play()
	{
		int initialRoll ;		// total of first roll of two dice
		
		int rollCount ;         // counts number of rolls it takes to
		                      // match the initial roll

        int gameNumber = 0 ;	// counts number of games played
		
        // "priming" read
		String answer = JOptionPane.showInputDialog("Want to play?" +
		                 "\nType Y for Yes or N for No") ;
		
		// repeat as long as user wants to play
                //Part 3 :  
		while ( answer.equalsIgnoreCase("Y") )
		{
			gameNumber++ ;

			// get the initial roll
			initialRoll = d1.roll() + d2.roll() ;

			// roll dice again to try to match initial roll
			int currentRoll = d1.roll() + d2.roll() ;
			
			// initialize count of number of rolls needed to match
			rollCount = 1 ;		
			
			// repeat as long as initial roll is not matched
			// DO NOT MODIFY THIS LOOP!!!!!
			while ( currentRoll != initialRoll )
			{
				// roll 'em again!
				currentRoll = d1.roll() + d2.roll() ;
				// increment number of rolls made
				rollCount++ ;
			}
			// Loop postcondition:  initialRoll has been matched
			
			// print stats
			System.out.println("Game #" + format(gameNumber) + 
			                 "   Initial roll = " + format(initialRoll) + 
			                 "   Matched in " + format(rollCount) +
                             " rolls.") ;
                        
                        //Part2 - A
                        answer = JOptionPane.showInputDialog("Want to play?" +
		                 "\nType Y for Yes or N for No") ; 
		} 
	}
}

public class GallopingDominoes
{
	public static void main(String[] args)
	{
		MatchGame fun = new MatchGame() ;
		fun.play() ;
	}
}