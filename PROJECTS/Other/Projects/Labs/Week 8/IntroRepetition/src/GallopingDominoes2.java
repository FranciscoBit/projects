
import java.util.Random;

/**
 * A class to represent a standard 6-sided die
 */
class Die2 // die is the singular of "dice"
{
    // random number generator object

    private static Random r = new Random();

    /**
     * Roll one die.
     *
     * @return a random integer from 1 to 6
     */
    public int roll() {
        return r.nextInt(6) + 1;
    }
}

/**
 * Plays the game. I.e., rolls the dice to get the initial roll and counts
 * number of additional rolls required to match it. Repeats as long as the user
 * wants to play another game
 */
class MatchGame2 {
    // instance var's are Die objects 

    Die2 d1 = new Die2();				// create pair of dice
    Die2 d2 = new Die2();

    // This method just aligns the output...
    private static String format(int number) {
        if (number <= 9) // if single-digit number...
        {
            return " " + number;			// ...pad with one leading space
        } else // 2-digit number...
        {
            return "" + number;			// ...no padding
        }
    }

    /**
     * Plays the game. I.e., rolls the dice to get the initial roll and counts
     * number of additional rolls required to match it. Repeats as long as the
     * user wants to play another game
     */
    public void play() {
        int initialRoll = 0;		// total of first roll of two dice

        int rollCount = 0;         // counts number of rolls it takes to


        int gameNumber = 0;	// counts number of games played
        int maximum = Integer.MIN_VALUE;	
        int minimum = Integer.MAX_VALUE;
        

        System.out.println();

        // Part 3 - B - C
        // initialRoll not equal to 2 =  (initialRoll != 2)
        // (rollCount AKA matched in roll) does not equal to 1  = (rollCount != 1)
        while (rollCount != 1) 
        {
            gameNumber++;				// increment # of games played

            if (rollCount > maximum) // if current score is largest so far...	
            {
                maximum = rollCount;	// ...save it as new maximum
            }
            if (rollCount < minimum) {
                minimum = rollCount;
            }
            
            
            
            
            
            // get the initial roll
            initialRoll = d1.roll() + d2.roll();

            // roll dice again to try to match initial roll
            int currentRoll = d1.roll() + d2.roll();

            // initialize count of number of rolls needed to match
            rollCount = 1;
            
            
            
            
            
            
            
            
            

            // repeat as long as the initial roll is not matched
            // DO NOT MODIFY THIS LOOP!  ONLY THE OUTER ONE!
            while (currentRoll != initialRoll) // while NOT matched...
            {
                // ...roll 'em again!
                currentRoll = d1.roll() + d2.roll();

                // ...increment number of rolls made
                rollCount++;
            }
            // Loop postcondition:  initialRoll has been matched

            // print stats
            System.out.println("Trial #" + format(gameNumber)
                    + "   Initial roll = " + format(initialRoll)
                    + "   Matched in " + format(rollCount)+ " rolls."
                    );
        }
        
        System.out.println("   Most attempts: " + format(maximum) + " to matched " + format(initialRoll)
                    + "   Least attempts: " + format(minimum + 1) + " to matched " + format(initialRoll));
        
    }
    
    
    
    
}

public class GallopingDominoes2 {

    public static void main(String[] args) {
        MatchGame2 fun = new MatchGame2();
        fun.play();
    }
}