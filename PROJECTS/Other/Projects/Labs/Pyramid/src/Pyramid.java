/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class Pyramid {
    
    //Question 1 - two instance variables
    private int pyramidBase;
    private int pyramidHeight;
    
    //Question 2 - create constructors
    public Pyramid (int pyramidBase, int pyramidHeight)
    {
        this.pyramidBase = pyramidBase;
        this.pyramidHeight = pyramidHeight;
    }
    
    //Question 4 - get the base and height
    public int getBase()
    {
        return pyramidBase;
    }
    
    public int getHeight()
    {
        return pyramidHeight;
    }
    
    //Question 5 - we add mutator methods for resizing
    
    public void resize(int newBase, int newHeight)
    {
        this.pyramidBase = newBase;
        this.pyramidHeight = newHeight;
    }
    
    //Question 6 - we add method for getiing volume
    
    public double volume()
    {
        double newVolume; 
        newVolume = (1/3.0) * (this.pyramidBase * this.pyramidBase) * this.pyramidHeight;
        
        return newVolume;
        
    }
    
    
    
    
    
}
