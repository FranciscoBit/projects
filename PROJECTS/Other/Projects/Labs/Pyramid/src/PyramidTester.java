/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class PyramidTester {
    
    public static void main(String args[])
    {
        //Question 3 - call the constructor
        Pyramid myPyramid = new Pyramid (5,10);
        
        
        //Question 4 - call the getbase and get height
        
        System.out.println("Lenght: " + myPyramid.getBase() + " ----" + " Height: " + myPyramid.getHeight());
        
        //Question 5 - we resize and print the info
        myPyramid.resize(20, 40);
        System.out.println("Lenght: " + myPyramid.getBase() + " ----" + " Height: " + myPyramid.getHeight());
        
        //Question 6 - we now get the volume
        System.out.println("Valume: " + myPyramid.volume());
        
        
        
        
    }
    
    
}
