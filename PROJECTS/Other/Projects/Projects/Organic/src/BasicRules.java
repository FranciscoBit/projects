/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dragon
 */
public class BasicRules {
    
    private String completeName;
    
    /**
     * 
     * @param completeName 
     */
    public BasicRules(String completeName)
    {
        this.completeName = completeName;
    }
    
    
    /**
     * 
     * @return 
     */
    public String getCompleteName()
    {
        return completeName;
    }
    
    
    /**
     * 
     * @return 
     */
    public boolean findAlkane()
    {
        boolean isAlkane = false;
        
        if(this.completeName.endsWith("ane"))
        {
            isAlkane = true;
        }
        else
        {
            isAlkane = false;
        }

        return isAlkane;
    }
    /**
     * 
     * @return 
     */
    public boolean findAlkene()
    {
        boolean isAlkene = false;
        
        if(this.completeName.endsWith("ene"))
        {
            isAlkene = true;
        }
        else
        {
            isAlkene = false;
        }
    
        return isAlkene;
    }
    
    /**
     * 
     * @return 
     */
    public boolean findAlkyne()
    {
        boolean isAlkyne = false;
        
        if(this.completeName.endsWith("yne"))
        {
            isAlkyne = true;
        }
        else
        {
            isAlkyne = false;
        }
         
        return isAlkyne;
    }
    
     

    
    
    
    
    
}
