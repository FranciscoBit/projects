
import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class StockPortfolioTest {
    
    public static void main (String args[] )
    {
        //Question 1 - Have the user enter the data 
        //(company name, number of shares, dollars, eighths)
        String input1 = JOptionPane.showInputDialog(
                "Enter Company Name: ");
        
        String input2 = JOptionPane.showInputDialog(
                "Enter Shares Held: ");
        
        String input3 = JOptionPane.showInputDialog(
                "Enter Dollar Portion of Share Price: ");
        
        String input4 = JOptionPane.showInputDialog(
                "Enter Eight Portion of Share Price: ");
        
        
        String CompanyName = input1;
        int SharesHeld = Integer.parseInt(input2);
        int DollarPortionSharePrice = Integer.parseInt(input3);
        String EightPortionSharePrice = input4;
        
        
        //Question 2 - Create a StockPortfolio object
        StockPortfolio myStockPortfolio = new StockPortfolio
                (CompanyName, SharesHeld, DollarPortionSharePrice, EightPortionSharePrice);
        
        //Question 3 - Call the “get” methods to access the object data, and then print it
        System.out.println("Company Name: " + myStockPortfolio.GetCompanyName());
        System.out.println("Shares Held: " + myStockPortfolio.GetSharesHeld());
        System.out.println("Opening Price per Share: " 
                + myStockPortfolio.GetDollarPortionSharePrice() +" | " + myStockPortfolio.GetEightPortionSharePrice(input4));
        
        
        
        //Question 4 - Get and print the portfolio value as a decimal
        System.out.println("Opening " + myStockPortfolio.Parse(input4));
        
        
        //===========================================================================================
        //===========================================================================================
        //===========================================================================================
        //===========================================================================================
        
        
        //Question 5 - Have the user enter the change in the share price 
        //- in dollars and eighths - and print it
        String input33 = JOptionPane.showInputDialog(
                "Enter MODIFY Dollar Portion of Share Price: ");
        
        String input44 = JOptionPane.showInputDialog(
                "Enter MODIFY Eight Portion of Share Price: ");
        
        int DollarPortionSharePrice33 = Integer.parseInt(input33);
        String EightPortionSharePrice44 = myStockPortfolio.Parse(input44);
        
        myStockPortfolio.ModifiedStockPrice(DollarPortionSharePrice33, EightPortionSharePrice44);
        System.out.println("========================================");
        System.out.println("========================================");
        System.out.println("Change in Share Price: " + input33 + " " +myStockPortfolio.GetEightPortionSharePrice(input44));
        
        
        //Question 6 - Get and print the updated share price
        System.out.println("Closing Price per Share: " 
                + myStockPortfolio.GetDollarPortionSharePrice() +" | " + input44);
        
        
        
        //Question 7 - Get and print the updated portfolio value
        
        System.out.println("Closing " 
                + myStockPortfolio.Parse(myStockPortfolio.doubleParse(input3, input4, input33, input44)));
        
        
        //===========================================================================================
        //===========================================================================================
        //===========================================================================================
        //===========================================================================================
        
        
        //Question 8 - Have the user enter another change in the share price 
        //- in dollars and eighths - and print it
        String input333 = JOptionPane.showInputDialog(
                "Enter MODIFY Dollar Portion of Share Price: ");
        
        String input444 = JOptionPane.showInputDialog(
                "Enter MODIFY Eight Portion of Share Price: ");
        
        int DollarPortionSharePrice333 = Integer.parseInt(input333);
        String EightPortionSharePrice444 = myStockPortfolio.Parse(input444);
        
        myStockPortfolio.ModifiedStockPrice(DollarPortionSharePrice333, EightPortionSharePrice444);
        System.out.println("========================================");
        System.out.println("========================================");
        System.out.println("Change in Share Price: " + input333 + " " + input444);
        
        
        //Question 9 - Get and print the updated share price
        System.out.println("Closing Price per Share: " 
                + myStockPortfolio.GetDollarPortionSharePrice() +" | " + input444);
        
        //Question 10 - Get and print the updated portfolio value
        System.out.println("Closing " 
                + myStockPortfolio.Parse(myStockPortfolio.doubleParse(input33, input44, input333, input444)));
        
        System.exit(0) ;
    }
    
}
