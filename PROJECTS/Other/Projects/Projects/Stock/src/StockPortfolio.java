/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class StockPortfolio {
    
    /**
     * State the instance variables
     * @param CompanyName the company name in string
     * @param SharesHeld the shares held by the company as an integer
     * @param DollarPortionSharePrice the dollar portion of the share price as an integer
     * @param EightPortionSharePrice the eight portion of the share price as an integer
     */
      
    private String CompanyName;
    private int SharesHeld;
    private int DollarPortionSharePrice;
    private String EightPortionSharePrice;
    
    /**
     * The StockPortfolio object
     * @param CompanyName the company name in string
     * @param SharesHeld the shares held by the company as an integer
     * @param DollarPortionSharePrice the dollar portion of the share price as an integer
     * @param EightPortionSharePrice the eight portion of the share price as an integer
     */
    
    public StockPortfolio(String CompanyName, int SharesHeld, int DollarPortionSharePrice, String EightPortionSharePrice)
    {
        this.CompanyName = CompanyName;
        this.SharesHeld = SharesHeld;
        this.DollarPortionSharePrice = DollarPortionSharePrice;
        this.EightPortionSharePrice = EightPortionSharePrice;
    }
    
    /**
     * Get the company name
     * @return the company name
     */
    public String GetCompanyName()
    {
        return CompanyName;
    }
    
    /**
     * Get the shares held
     * @return the shares held
     */
    public int GetSharesHeld()
    {
        return SharesHeld;
    }
    
    /**
     * Get the dollar portion of the share price
     * @return the dollar portion of share price
     */
    public int GetDollarPortionSharePrice()
    {
        return DollarPortionSharePrice;
    }
    
    /**
     * Get the eight portion of share price
     * @return eight portion of share price
     */
    public String GetEightPortionSharePrice(String input)
    { 
        String[] splitter = input.split("/");
        String top = splitter[0];
        String bottom = splitter[1];
        
        String output = top + "/" + bottom;
        
        return output;
    }
    
    
    
    /**
     * Modify the stock price
     * @param NewDollarPortionSharePrice modify the dollar portion of share price
     * @param NewEightPortionSharePrice modify the eight portion of share price
     */
    public void ModifiedStockPrice( int NewDollarPortionSharePrice, String NewEightPortionSharePrice)
    {
        this.DollarPortionSharePrice = NewDollarPortionSharePrice + this.DollarPortionSharePrice;
        
    }
    
    
    /**
     * We now get the decimal value
     * @return the decimal value of the portfolio
     */
 
    public String Parse(String theParser)
    {
        String[] splitter = theParser.split("/");
        String top = splitter[0];
        String bottom = splitter[1];
        
        int topInt = Integer.parseInt(top);
        int bottomInt = Integer.parseInt(bottom);

        int Output = (DollarPortionSharePrice * bottomInt) + (topInt);
        
        double total = ((Output) / ((double)bottomInt)) * SharesHeld ;
        
        String realOutput = "Porfolio Value: $" + total;
        
        return realOutput;
        
    }
    
    
    public String doubleParse(String dollarX, String x, String dollarY, String Y)
    {
        //X string
        String[] splitterX = x.split("/");
        String topX = splitterX[0];
        String bottomX = splitterX[1];
        
        int topIntX = Integer.parseInt(topX);
        int bottomIntX = Integer.parseInt(bottomX);
        int DollarX = Integer.parseInt(dollarX);
        

        

        

        
        //Y string
        String[] splitterY = Y.split("/");
        String topY = splitterY[0];
        String bottomY = splitterY[1];
        
        int topIntY = Integer.parseInt(topY);
        int bottomIntY = Integer.parseInt(bottomY);
        int DollarY = Integer.parseInt(dollarY);
        
        
        

        //the addtion of both strings
        int totalTop1 = (bottomIntY * topIntX) + (topIntY * bottomIntX);
        int totalBottom1 = (bottomIntY * bottomIntX);
        
        //Now we convert them to strings
        
        String output = totalTop1 + "/" + totalBottom1;
        
        return output;
        
        
        
    }
    
    
    
    
    
    
    
    
    
}
