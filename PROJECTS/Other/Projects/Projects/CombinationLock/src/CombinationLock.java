/**
 * A class to implement a combination lock with 26 dial positions
 * and a three-letter combination
 * 
 * @author Francisco Munoz
 */
public class CombinationLock
{
   // instance variable declarations go here
    private String string1;
    private String string2;
    private String string3;
    private boolean open;
    
    //these instaces is to be used with LockTester method to test for strings equality
    private String testString1;
    private String testString2;
    private String testString3;
 
  /**
   * Creates a lock with a given combination consisting of three upper-case characters.
   * @param first the first letter of the combination
   * @param second the second letter of the combination
   * @param third the third letter of the combination
   */
   public CombinationLock(String first, String second, String third, boolean open)
   {
      this.string1 = first;
      this.string2 = second;
      this.string3 = third;
      this.open = open;
   }

  /**
   * Set the dial to a position, and use if/else statement for uppercase letter test
   * @param aPosition a String consisting of a single uppercase letter (A..Z)
   */
   public void setPosition(String aPosition)
   {
      if(
              aPosition.equals("A") || aPosition.equals("B") || aPosition.equals("C") || aPosition.equals("D") ||
              aPosition.equals("E") || aPosition.equals("F") || aPosition.equals("G") || aPosition.equals("H") ||
              aPosition.equals("I") || aPosition.equals("J") || aPosition.equals("K") || aPosition.equals("L") ||
              aPosition.equals("M") || aPosition.equals("N") || aPosition.equals("O") || aPosition.equals("P") ||
              aPosition.equals("Q") || aPosition.equals("R") || aPosition.equals("S") || aPosition.equals("T") ||
              aPosition.equals("U") || aPosition.equals("V") || aPosition.equals("W") || aPosition.equals("Y") ||
              aPosition.equals("Y") || aPosition.equals("Z") 

        )
      {
          System.out.println("#### Letter(s) can be used. You can keep going! ####"
                  + "\n--------------------------------------------");
          System.out.println();
      }
      else
      {
          System.out.println("#### Sorry, we only accept letter A - Z, some values are wrong! ####");
          System.out.println("#### We have Exit the CombinationLock App ####");
          System.exit(0);
      }
   }
   


  /**
   * Method tryToOpen does not return a value and does no output. 
   * It checks to see whether the combination entered is the right one and, 
   * if so, sets instance variable open to true
   * 
   * Also it supports the testString1, testString2, testString3
   * When users enter the retype keys
   */
   public void tryToOpen()
   {
       
      if( 
              (this.string1.equals(this.testString1)) && 
              (this.string2.equals(this.testString2)) && 
              (this.string3.equals(this.testString3)) )
      {
          this.open = true;
      }
      else
      {
          this.open = false;
      }
       
   }

  /**
   * Check whether the lock is open
   * @return true or false indicating whether the lock is open
   */
   public boolean isOpen()
   {
       if(this.open == true)
       {
           return true;
       }
       else
       {
           return false;
       }
   }

  /**
   * Close the lock and print a message indicating that the lock is now closed
   */
   public void lock()
   {
       this.open = false;
       System.out.println("The lock in now CLOSED!");
   }
   
   
   /**
    * This method was created mainly to be used in the main class
    * What it basically does is it will act as a support for the method tryToOpen even thoght they are not related
    * I will be used for the retype keys in main method
    * @param testString1 it will be equal to the testString1 value instance
    * @param testString2 it will be equal to the testString2 value instance
    * @param testString3 it will be equal to the testString3 value instance
    */
   public void LockTester(String testString1, String testString2, String testString3)
   {
       this.testString1 = testString1;
       this.testString2 = testString2;
       this.testString3 = testString3;
   }
   
   /**
    * This return the strings with the letter values and states if the lock is open or not
    * @return the string with the letters input by user and wheather or not
    * the lock is open or not
    */
   public String GetString()
   {
       String answer;
       if(this.isOpen() == true)
       {
           answer = "       Combination: " + this.string1 + "|" + this.string2 + "|" + this.string3 + "\n"  
                  + "Retype Combination: " + this.testString1 + "|" + this.testString2 + "|" +this.testString3 + "\n" 
                  + "Combination is RIGHT! Lock is now Open!"
                  + "\n--------------------------------------------";
           return answer;
       }
       else
       {
           answer = "       Combination: " + this.string1 + "|" + this.string2 + "|" + this.string3 + "\n"  
                  + "Retype Combination: " + this.testString1 + "|" + this.testString2 + "|" +this.testString3 + "\n" 
                  + "Combination is WRONG! Locked is still Closed"
                   + "\n--------------------------------------------";
           return answer;
       }
       
       
   }
   
}