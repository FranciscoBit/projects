/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
import javax.swing.JOptionPane;

public class CombinationLockTester {

    public static void main(String[] args) {

        //Initial imputs Keys
        String input1 = JOptionPane.showInputDialog("Type First Letter: ");

        String input2 = JOptionPane.showInputDialog("Type Second Letter: ");

        String input3 = JOptionPane.showInputDialog("Type Third Letter: ");

        boolean OpenOrClose = false;

        //We start out with a closed lock
        CombinationLock myCombinationLock = new CombinationLock(input1, input2, input3, OpenOrClose);

        //Now we see if A - Z are used
        myCombinationLock.setPosition(input1);
        myCombinationLock.setPosition(input2);
        myCombinationLock.setPosition(input3);


        //Question 2 - Have the user enter an invalid key
        String input11 = JOptionPane.showInputDialog("Retype First Letter: ");

        String input22 = JOptionPane.showInputDialog("Retype Second Letter: ");

        String input33 = JOptionPane.showInputDialog("Retype Third Letter: ");

        //LockTester is to comapre both string...or test if the keys match
        myCombinationLock.LockTester(input11, input22, input33);

        //Question 3 - Try to open the lock
        myCombinationLock.tryToOpen();

        //Question 4 - Print a message indicating whether the lock is open or 
        //closed (NOTE: you must use the value returned by method isOpen to 
        //determine what to print)
        //The GetString method is used to get the info out of the isOpen mathod
        //and it see wheather or not isOpen is true or false
        System.out.println(myCombinationLock.GetString());

        //#####################################################################
        //#####################################################################
        //#####################################################################
        //#####################################################################

        //Question 5 - Have the user enter the correct combination
        
        String input111 = JOptionPane.showInputDialog("Try Again! First Letter: ");

        String input222 = JOptionPane.showInputDialog("Try Again! Second Letter: ");

        String input333 = JOptionPane.showInputDialog("Try Again! Third Letter: ");
        
        
        //Try to open the lock
        myCombinationLock.LockTester(input111, input222, input333);
        
        //Print a message indicating whether the lock is open or closed (it should be open now)
        myCombinationLock.tryToOpen();
        System.out.println(myCombinationLock.GetString());
        
        //Question 6 - Closed the lock, this will reset the lock back to false
        myCombinationLock.lock();
        System.out.println("---------------------------------------------");
        
        //#####################################################################
        //#####################################################################
        //#####################################################################
        //#####################################################################
        
        //Question 7 - Have the user enter the invalid combination
        
        String input1111 = JOptionPane.showInputDialog("Type First Letter: ");

        String input2222 = JOptionPane.showInputDialog("Type Second Letter: ");

        String input3333 = JOptionPane.showInputDialog("Type Third Letter: ");
        
        //Try to open the lock
        myCombinationLock.LockTester(input1111, input2222, input3333);
        
        //Print a message indicating whether the lock is open or closed (it should still be close now)
        myCombinationLock.tryToOpen();
        System.out.println(myCombinationLock.GetString());

    }
}
