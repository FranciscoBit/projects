/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class BalloonTester {
    
    public static void main (String args[])
    {
        //Question 1 - we create baloon object with name and altitude 100
        String nameBalloon = "1st Cool Balloon";
        int superBalloonAltitude = 100;
        
        Balloon myBalloon = new Balloon(nameBalloon, superBalloonAltitude);
        
        
        //Question 2 - we create a second baloon object with name and altitude -100
        String nameSecondBalloon = "2nd Cool Balloon";
        int superSecondBalloonAltitude = -100;
        Balloon mySecondBalloon = new Balloon(nameSecondBalloon, superSecondBalloonAltitude);
        
        //Question 3 - we print the data by using the acessor methods
        System.out.println("Name of 1st Balloon: " + myBalloon.getName() + " --" + " Altitude of 1st Balloon: " + myBalloon.getAltitude() );
        System.out.println("Name of 2nd Balloon: " + mySecondBalloon.getName() + " --" + " Altitude of 2nd Balloon: " + mySecondBalloon.getAltitude() );
        
        //Question 4 - we now ascend to 250 om first baloon
        int myBalloonAscendTo = 250;
        myBalloon.ascendTo(myBalloonAscendTo);
        
        //Question 5 - we now adjust altitude to 200 om second baloon
        int mySecondBalloonAdjustAltitude = 200;
        mySecondBalloon.adjustAltitude(mySecondBalloonAdjustAltitude);
        
        //Question 6 - we now use acessor to print new data
        System.out.println("===========================NEW=======================================");
        System.out.println("Name of 1st Balloon: " + myBalloon.getName() + " --" + " Altitude of 1st Balloon: " + myBalloon.getAltitude() );
        System.out.println("Name of 2nd Balloon: " + mySecondBalloon.getName() + " --" + " Altitude of 2nd Balloon: " + mySecondBalloon.getAltitude() );
        
        //uestion 7 - we now adjust altitude of first balloon again to 100
        int myBalloonAdjustAltitudeSecond = -100;
        myBalloon.adjustAltitude(myBalloonAdjustAltitudeSecond);
        
        //Question 8 - we now descend the second baloon to same altitude as the first 
        mySecondBalloon.descendTo(myBalloon.getAltitude());
        
        //Question 9 - we now print the new data
        System.out.println("===========================NEW=======================================");
        System.out.println("Name of 1st Balloon: " + myBalloon.getName() + " --" + " Altitude of 1st Balloon: " + myBalloon.getAltitude() );
        System.out.println("Name of 2nd Balloon: " + mySecondBalloon.getName() + " --" + " Altitude of 2nd Balloon: " + mySecondBalloon.getAltitude() );
        
        
        //Question 10 - we now move the first baloon to an altitude 3x the current altitude
        myBalloon.adjustAltitude(myBalloon.getAltitude() * 3);
        
        //Question 11 - we now adjust the altitude to 200 meters below the current altitude
        mySecondBalloon.adjustAltitude(-200);
        
        //Question 12 - we now print the new data
        System.out.println("===========================NEW=======================================");
        System.out.println("Name of 1st Balloon: " + myBalloon.getName() + " --" + " Altitude of 1st Balloon: " + myBalloon.getAltitude() );
        System.out.println("Name of 2nd Balloon: " + mySecondBalloon.getName() + " --" + " Altitude of 2nd Balloon: " + mySecondBalloon.getAltitude() );
        
        
    }
    
    
    
    
}
