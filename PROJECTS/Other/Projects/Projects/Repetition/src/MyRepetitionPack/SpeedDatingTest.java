package MyRepetitionPack;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * A class to test the SpeedDating class
 * @author Francisco Munoz
 */
import MyRepetitionPack.SpeedDating;
import MyRepetitionPack.Date;
import javax.swing.JOptionPane;

public class SpeedDatingTest {
    
    public static void main(String[] args) 
    {
        //Question 1 - Create SpeedDating object
        SpeedDating mySpeedDating = new SpeedDating();
        
        //Question 2 - Have mySpeedDatinge user enter year
        //Then call the auldLangSync method 
        //Then Print the year and the day of the week
        
        System.out.println("================ New Year's Day ===================");
        
        String input = JOptionPane.showInputDialog("Enter New Year's Day: ") ; 
        int theYear = Integer.parseInt(input);
        

        System.out.println("Original New Year's Day: " + input + "\n" +
                           "Next New Year's Day: " + mySpeedDating.auldLangSynch(theYear));
        
        System.out.println(mySpeedDating.auldLangSynchPrint(theYear));
        
        
        //Question 3 - Have mySpeedDatinge user another enter year
        //Then call the pollDancer method 
        //Then print the Date returned, properly labeled
        
        String input1 = JOptionPane.showInputDialog("Enter Year: ") ; 
        int theYear1 = Integer.parseInt(input1);
        
        
        System.out.println("================= Election Day ===============");
        System.out.println("Year of Election Day: " + input1 + "\n" +
                           "Full Date of Election Day: " + mySpeedDating.pollDancer(theYear1).getLongDate()); //we added estra to get info out
        
        //Question 4 - Have the user enter a date (month, day, and year) "mm/dd/yyyy"
        //and call the iExcel method to get the Excel Date for the input date. 
        //Print it in your main method, not in iExcel.
        
        String inputDay = JOptionPane.showInputDialog("Enter Excel Day: ") ;
        String inputMonth = JOptionPane.showInputDialog("Enter Excel Month: ") ; 
        String inputYear = JOptionPane.showInputDialog("Enter Excel Year: ") ; 

        int intInputDay = Integer.parseInt(inputDay);
        int intInputMonth = Integer.parseInt(inputMonth);
        int intInputYear = Integer.parseInt(inputYear);
        
        Date myDate = new Date(intInputMonth,intInputDay,intInputYear);
        
        System.out.println("=================== Excel Dates ================");
        System.out.println("Input Excel Date: " + myDate.getShortDate() + " OR " + myDate.getLongDate());
        

        System.out.println("Excel Date Count: " + mySpeedDating.iExcel(myDate));
        
        
        System.out.println();
        
        
    }
    
    
}
