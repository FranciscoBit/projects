package MyRepetitionPack;


import MyRepetitionPack.Date;


/**
 * A class to give students experience using loops.  This class
 * creates and manipulates objects of Greg's Date class.
 */
 public class SpeedDating
 {
    // Note: this class has no instance variables!
 	
    /**
     * Create an empty SpeedDating object
     */
     public SpeedDating()
     {}	   // Constructor has empty body
           // (this is known as a "default" constructor)
 	
    /**
     * Computes and returns the next year in which New Year's Day will
     * fall on the same day of the week as in a given year
     * @param theYear the given year
     * @return the next year in which New Year's day is the same day
     * of the week as in parameter theYear
     */
     public int auldLangSynch(int theYear)
     {

         int myMonth = 12;      // This is december
         int myDay = 31;        // This is 31
         
         // We create a new Date object
         //We use the constant month(myMonth), and day(myDay), with the imput of user year(theYear)
         Date myDate = new Date(myMonth, myDay, theYear);
         
         //This part is to test the original year
         String originalYear = myDate.getLongDate();
         
         //Now we call the day of the week (e.g. "Saturday") that is from year user imput
         String myDayOfWeek = myDate.getDayOfWeek();
         
         int theYearOfSameDay;  // This is the year after all is calculated
         
         do
         {
             myDate.next();
         }
         while( !( (myDate.getDay() == 31) && (myDate.getMonth() == 12) && ( myDate.getDayOfWeek().equals(myDayOfWeek) ) ));
         

         //Now we get the year
         theYearOfSameDay = myDate.getYear();
         
         
         return theYearOfSameDay;
        
     }
 	
    /**
     * Computes and returns the Date on which Election Day will fall 
     * in the USA for a given year.
     *
     * NOTE: By law, Election  Day is the first Tuesday after the first
     * Monday in November.
     *
     * @param year the year for which to compute the date of Election Day
     * @return the Date of Election Day for the specified year
     */
     public Date pollDancer(int year)
     {
        
         int myPollMonth = 11;      // This is november
         int myPollDay = 1;        // This is 1st
         
         //Create new object for the poll
         Date mySecondDate = new Date(myPollMonth, myPollDay, year);
         

         int count = 0;
         do
         {
             mySecondDate.next();
             count = count + 1;
  
         }
         while(!(  ((mySecondDate.getDayOfWeek().equals("Tuesday")) && (count <= 8))    ));
         
         
         return mySecondDate;
         
     }

 	 	
     /**
      * Computes and returns the corrected "Excel Date" for a given Date.
      * I.e., the number of the given Date where 1/1/1900 is date #1
      * "mm/dd/yyyy"
      * 
      * @param aDate the Date for which to return the Excel Date
      * @return the Excel Date of parameter aDate 
      */
      public int iExcel(Date aDate) 
      {
         int realExcelValue = 1;
         
         //These are the constant beginning from Jan, 1, 1900
         int CONS_DAY = 1;
         int CONS_MONTH = 1;
         int CONS_YEAR = 1900;
         
         //Create a second Date class to store the cons
         Date consDate = new Date(CONS_MONTH, CONS_DAY, CONS_YEAR);
         
         
         int count = 1;
         //while the consDate equals aDate is FALSE, we keep looping
         while(!consDate.equals(aDate))
         {
             count++;
             consDate.next();
             
             //if the date is a day after 
             if(   (consDate.getMonth() == 3)  && (consDate.getDay() == 1)   && (consDate.getYear() == 1900 )      )
             {
                 count = count - 1;
             }
             
         }

         return  count ;
          
      }
      
      
     /**
     * Computes and returns the complete date (Long Date) in which New Year's Day will
     * fall on the same day of the week as in a given year
     * @param theYear the given year
     * @return the next year in which New Year's day is the same day
     * of the week as in parameter theYear
     */
     public String auldLangSynchPrint(int theYear)
     {

         int myMonth = 12;      // This is december
         int myDay = 31;        // This is 31
         
         // We create a new Date object
         //We use the constant month(myMonth), and day(myDay), with the imput of user year(theYear)
         Date myDate = new Date(myMonth, myDay, theYear);
         
         //This part is to test the original year
         String originalYear = myDate.getLongDate();
         
         //Now we call the day of the week (e.g. "Saturday") that is from year user imput
         String myDayOfWeek = myDate.getDayOfWeek();
         
        
         do
         {
             myDate.next();
         }
         while( !( (myDate.getDay() == 31) && (myDate.getMonth() == 12) && ( myDate.getDayOfWeek().equals(myDayOfWeek) ) ));
         

         //This is to test the date
         String completeDate = myDate.getLongDate();
         
         
         return  "*********************************************" + "\n" +
                 "In specific, the Long Dates are: \n" +
                 "\tOriginal New Year's Day: " + originalYear + "\n" + 
                 "\tNext New Year's Day: " + completeDate;

     }
      
          
 }