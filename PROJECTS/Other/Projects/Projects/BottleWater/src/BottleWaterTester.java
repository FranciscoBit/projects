
import WaterBottlePack.BottleWater;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Francisco Munoz
 */
public class BottleWaterTester {
    
    public static void main (String[] args)
    {
        
        //Question 1 - create a bottlewater object
        BottleWater myBottleWater = new BottleWater("USA", 350000000, 190, 8.5, 12);
        
        //Question 2 - call the acessor to print all the data
        
        System.out.println("Country: " + myBottleWater.getCountryName());
        System.out.println("Population: " + myBottleWater.getCountryPopulation());
        System.out.println("Number of Circumferences: " + myBottleWater.getAverageEarth() );
        System.out.println("Length of an Average Bottle: " + myBottleWater.getAverageLenght() );
        System.out.println("Volume of an Average Bottle: " + myBottleWater.getAverageVolume() );
        
        //Question 3 - call the methods that compute output
        
        System.out.println("Total Number of Bottle Used: " + myBottleWater.totalBottlesUsed());
        System.out.println("Average amount of water consumed per person, in gallons: " + myBottleWater.averageWaterConsumed());
        
        
        //Question 4 - call the mutator method to reset the bottle
        myBottleWater.reset(9.5, 16);
        
        //Question 5 - we call the accesor methods to print the new values
        System.out.println("------------------------------------------------");
        System.out.println("Modified Bottle Length: " + myBottleWater.getAverageLenght());
        System.out.println("Modified Bottle Volume: " + myBottleWater.getAverageVolume());
        
        //Question 6 - call the methods that compute the new output
        
        System.out.println("Modified: Total Number of Bottle Used: " + myBottleWater.totalBottlesUsed());
        System.out.println("Modified: Average amount of water consumed per person, in gallons: " + myBottleWater.averageWaterConsumed());
    }
    
}
