package WaterBottlePack;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * File Name: BottleWater
 * @author Francisco Munoz 
 * Purpose: A class to represent a water bottle. 
 * I affirm that this program is entirely my own work and none of it is the work
 * of any other person.
 */
public class BottleWater {

    /**
     *
     */
    private String countryName;     // Country Name
    private int countryPopulation;  // Country Population
    private double averageEarth;    // Avarage circles around the earth
    private double averageLenght;   // Bottle Length
    private double averageVolume;   // Bottle Volume

    /**
     * Create a bottle water object.
     *
     * @param countryName the country name
     * @param countryPopulation the country population
     * @param averageEarth the number of times the bottle will circle earth
     * @param averageLenght the average length the bottle
     * @param averageVolume the average volume
     */
    public BottleWater(String countryName, int countryPopulation, double averageEarth, double averageLenght, double averageVolume) {
        this.countryName = countryName;
        this.countryPopulation = countryPopulation;
        this.averageEarth = averageEarth;
        this.averageLenght = averageLenght;
        this.averageVolume = averageVolume;
    }

    /**
     * Get the country name.
     *
     * @return country name
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * Get the country population.
     *
     * @return country population
     */
    public int getCountryPopulation() {
        return countryPopulation;
    }

    /**
     * Get the number of times the bottle will circle earth.
     *
     * @return number of times the bottle will circle earth
     */
    public double getAverageEarth() {
        return averageEarth;
    }

    /**
     * Get the average length the bottle.
     *
     * @return average length the bottle
     */
    public double getAverageLenght() {
        return averageLenght;
    }

    /**
     * Get the average volume.
     *
     * @return average volume
     */
    public double getAverageVolume() {
        return averageVolume;
    }

    /**
     * Make the calculations to get total number of bottles use
     *
     * @return total number of bottles use
     */
    public double totalBottlesUsed() {
        double CIRCUMFERENCE = 24902;
        double totalMilesOfBottles = CIRCUMFERENCE * averageEarth;

        double convertMiToInch = (totalMilesOfBottles) * (5280) * (12);

        double totalBottles = convertMiToInch / averageLenght;

        return totalBottles;

    }

    /**
     * Make the calculations to get average water consumed
     *
     * @return average water consumed
     */
    public double averageWaterConsumed() {
        double CIRCUMFERENCE = 24902.0;
        double totalMilesOfBottles = CIRCUMFERENCE * averageEarth;

        double convertMiToInch = (totalMilesOfBottles) * (5280.0) * (12);

        double totalBottles = convertMiToInch / averageLenght;

        double totalVolumeOz = averageVolume * totalBottles;

        double totalVolumeOzToGallons = (totalVolumeOz) * (1 / 128.0);

        double avarageWaterConsumedPerPerson;
        avarageWaterConsumedPerPerson = totalVolumeOzToGallons / countryPopulation;

        return avarageWaterConsumedPerPerson;

    }

    /**
     * Reset the length and volume of bottle water
     *
     * @param NewAverageLenght the new average length of the bottle
     * @param NewAverageVolume the new average volume of the bottle
     */
    public void reset(double newAverageLenght, double newAverageVolume) {
        this.averageLenght = newAverageLenght;
        this.averageVolume = newAverageVolume;
    }
}
