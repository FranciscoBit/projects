package poly;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Test class for the polynomial class
 * @author Francisco Munoz
 */

import poly.Polynomial;
import java.util.Scanner;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;

public class Test {
    
    public static void main(String[] args) throws IOException
    {
        //Create Polynomial object
        Polynomial myPolynomial = new Polynomial();
        
        //Scan the file and everything in it
        Scanner myFileScanner = new Scanner(new File("operations.txt") );
        
        
        //While there is stuff in the file keep going
        //This will print the original data in the file
        while(myFileScanner.hasNext())
        {
                  
            // b. extract the 3 tokens from the current line
            String operationType = myFileScanner.next();
            int theCoefficient;
            int theExponent;

            //If INSERT is found on the file
            if( operationType.equals("INSERT") )
            {
                //Scan and get the values
                theCoefficient = myFileScanner.nextInt();
                theExponent = myFileScanner.nextInt();
                
                System.out.println( theCoefficient + "  " + theExponent );
                
                //We print the original values
                System.out.println(operationType + " " + 
                        theCoefficient + " " + theExponent);
                
                myPolynomial.insert(theCoefficient, theExponent);   //Call the method INSERT
                System.out.println(myPolynomial.toString());
                System.out.println("=============================================================================");
                
            }
            
            //If DELETE is found on the file
            else if(operationType.equals("DELETE"))
            {
                //Scan and get the values
                theCoefficient = myFileScanner.nextInt();
                theExponent = myFileScanner.nextInt();
                
                //We print the original values
                System.out.println(operationType + " " + 
                        theCoefficient + " " + theExponent);
                
                myPolynomial.delete(theCoefficient, theExponent);   //Call the method DELETE
                System.out.println(myPolynomial.toString());
                System.out.println("=============================================================================");
            }
            
            //If REVERSE is found on the file
            else if(operationType.equals("REVERSE"))
            {
                //We print the original values
                System.out.println(operationType);
                myPolynomial.reverse();                             //Call the method REVERSE
                System.out.println("=============================================================================");
                
            }

            //If PRODUCT is found on the file
            else if(operationType.equals("PRODUCT"))
            {
                 //We print the original values
                System.out.println(operationType);
                System.out.println(myPolynomial.product());         //Call the method PRODUCT
                System.out.println("=============================================================================");
            }

        }
        
        System.out.println("=============== END ==================");

    }
    
}
