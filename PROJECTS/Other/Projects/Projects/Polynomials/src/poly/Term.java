package poly;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Term class for the polynomial programs with two parameters
 * @author Francisco Munoz
 */
public class Term {
    
    private int theCoefficient;     //Instance variable for coefficient
    private int theExponent;        //Instance variable for Exponent
    
    /**
     * Create a Arraylist object with the following parameters
     * @param theCoefficient Coefficient of the polynomial
     * @param theExponent Exponent of the polynomial
     */
    public Term(int theCoefficient, int theExponent)
    {
        this.theCoefficient = theCoefficient;
        this.theExponent = theExponent;
    }
    
    /**
     * Gets the coefficient of the polynomial
     * @return the coefficient of the polynomial
     */
    public int getCoefficient()
    {
        return theCoefficient;
    }
    
    /**
     * Gets the exponent of the polynomial
     * @return the exponent of the polynomial
     */
    public int getExponent()
    {
        return theExponent;
    }
    
    
}
