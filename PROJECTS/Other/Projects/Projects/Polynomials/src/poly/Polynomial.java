package poly;


/**
 * A class to implement a Polynomial as a list of terms, where each term has
 * an integer coefficient and a nonnegative integer exponent
 * @author Francisco Munoz
 */
import java.util.ArrayList;



public class Polynomial
{
    private ArrayList<Term> polynomialList;

    
//-----------------------------------------------------------------------------
   /**
    * Creates a new Polynomial object with no terms
    */
   public Polynomial()
   {
       this.polynomialList = new ArrayList<Term>();
   }

//-----------------------------------------------------------------------------
   
   /**
    * Inserts a new term into its proper place in a Polynomial
    * @param coeff the coeffiecent of the new term
    * @param expo the exponent of the new term
    */
    public void insert(int coeff, int expo) {

        //We create Term object to store the values found when calling INSERT
        Term myTerm = new Term(coeff, expo);

        //We add the myTerm to our polynomialList
        polynomialList.add(myTerm);
        
        int polynomialSize = polynomialList.size() ;
        //This serves as a counter for the last part
        int varibleTester = 1;


        for (int i = 0; i < polynomialSize; i++) 
        {
            Term myListOfItems = polynomialList.get(i);

            int eachCoefficient = myListOfItems.getCoefficient();
            int eachExponent = myListOfItems.getExponent();
            
            
            if(i == 0)              //We do nothing
            {
                
            }
            else if(i == 1)         //We remove the orginal and imput balue to right position
            {
                int testExponent = polynomialList.get(i - 1).getExponent();
                if(eachExponent >= testExponent)
                {
                    polynomialList.remove(i);
                    polynomialList.add(i - 1, myTerm);
                    
                }
                
            }
            else if(i >= 2)
            {
                varibleTester = varibleTester + 1;
                int testExponent = polynomialList.get(i - varibleTester).getExponent();
                if(eachExponent >= testExponent)
                {
                    polynomialList.remove(i);
                    polynomialList.add(i - varibleTester, myTerm);
                    
                }
                
            }
            
            
        }
        

    }

//-----------------------------------------------------------------------------    
   /**
    * Deletes the first occurrence of a specified term from a Polynomial, or
    * prints an appropriate message if the term does not appear in the 
    * Polynomial
    * @param coeff the coeffiecent of the term to be deleted
    * @param expo the exponent of the term to be deleted
    */
   public void delete (int coeff, int expo)
   {

        System.out.println("#### We will DELETE: (" + coeff + ", " + expo + ") ####");

        int polynomialSize = polynomialList.size() - 1;

        for (int i = 0; i < polynomialSize; i++) 
        {
            Term myListOfItems = polynomialList.get(i );

            //These are items values 1 by 1
            int eachCoefficient = myListOfItems.getCoefficient();
            int eachExponent = myListOfItems.getExponent();
                      
            System.out.println( "\"" + i +"\" " + eachCoefficient + " " + eachExponent ); 
            
            //if the orginal delete items are the same as the ones in the list
            if( ((eachCoefficient == coeff) && (eachExponent == expo))  )
            {
                polynomialList.remove(i);
            }
            else
            {
                System.out.println("#### We will INSERT: (" + eachCoefficient + ", " + eachExponent + ") ####");
            }
        }

   }
//-----------------------------------------------------------------------------
   /**
    * Returns the product of all the terms of a Polynomial, as a String
    * E.g. for the polynomial 3x^2 + 7x^3 + 2x^5, will return 42x^10
    * @return the polynomial product, as a String
    */
   public String product()
   {
        System.out.println("#### We will print the PRODUCT ####");

        int eachCoefficient = 1;
        int eachExponent = 0;
        String out = "";

        int polynomialSize = polynomialList.size()  ;

        for (int i = 0; i < polynomialSize; i++) 
        {
            Term myListOfItems = polynomialList.get(i);

            //This are the equations to get the product by adding all exponents and multiplying coefficients
            eachCoefficient = eachCoefficient * myListOfItems.getCoefficient();
            eachExponent = eachExponent + myListOfItems.getExponent();
            out = eachCoefficient + "X^" + eachExponent;
        }        
       
       return out;
   }

//-----------------------------------------------------------------------------
   
   /**
    * Returns a polynomial as a String in this form: 3x^2 + 7x^3 + 2x^5
    * @return the polynomial as a String
    */
   public String toString()
   {
       String out = "UPDATED Polynomial: ";
       int polynomialSize = polynomialList.size();
       
       for (int i = 0; i < polynomialSize; i++) 
       {
           Term myListOfItems = polynomialList.get(i);
           int eachCoefficient = myListOfItems.getCoefficient();
           int eachExponent = myListOfItems.getExponent();
           

           // If the index is 0 then we print normnally
           // If index is bigger than 1 then we print an extra "+" at the beginning
           if(i >= 1)
           {
               out = out + " + (" + eachCoefficient + "X^" + eachExponent + ")";
           }
           else
           {
               out = out + "(" + eachCoefficient + "X^" + eachExponent + ")";
           }
              
       }

      return out ;
   }

//-----------------------------------------------------------------------------   
   /**
    * Reverses the order of the terms of a Polynomial.
    * E.g. the polynomial 3x^2 + 7x^3 + 2x^5 would be 2x^5 + 7x^3 + 3x^2 after
    * reversal
    */
   public void reverse()
   {
      
       String out = "UPDATED REVERSE Polynomial: ";
       int polynomialSize = polynomialList.size() -1;
       
       for (int i = polynomialSize; i >= 0; i--) 
       {
           Term myListOfItems = polynomialList.get(i);
           int eachCoefficient = myListOfItems.getCoefficient();
           int eachExponent = myListOfItems.getExponent();
           
           // If the index is 0 then we print normnally
           // If index is bigger than 1 then we print an extra "+" at the beginning
           if(i == 1)
           {
               out = out + " + " + eachCoefficient + "X^" + eachExponent + " + " ;    
           }
           else if(i >= 2)
           {
               out = out + eachCoefficient + "X^" + eachExponent + " + " ;
           }
           else
           {
               out = out + eachCoefficient + "X^" + eachExponent;
           }  
           
       }
       

       //This is to correct the output...was giving me "+ +" instead of ""+"
       if(out.contains("+  +"))
       {
           out = out.replace("+  +", "+");
       }
       
       System.out.println(out);
   }
}