﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="VideoClipsFinal.Login" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="CSS/Style_Master.css">

    <title>Lemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>


<body>
    <form id="form1" runat="server">



       
        <div class="frame">

            <!-- COLUMN 1 -->
            <div class="bit-3">
                <div class="box">


                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">1</div>
                        </div>
                    </div>




                </div>
            </div>


            <!-- START OF CONTENT -->
            <!-- COLUMN 2 -->
            <div class="bit-3">
                <div class="box">

                    <!-- LOGIN - USERNAME -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:TextBox ID="login_username" runat="server" CssClass="Css_Page_Login_Textbox" placeholder="Username" ValidationGroup="Group_Login"></asp:TextBox>

                            </div>
                        </div>
                    </div>

                    <!-- LOGIN - PASSWORD -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:TextBox ID="login_password" runat="server" CssClass="Css_Page_Login_Textbox" placeholder="Password" ValidationGroup="Group_Login" TextMode="Password"></asp:TextBox>

                            </div>
                        </div>
                    </div>

                    <!-- LOGIN - SUBMIT -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:Button ID="login_submit" runat="server" CssClass="Css_Page_Footer" Text="Sign In" ValidationGroup="Group_Login" OnClick="login_submit_Click" />

                            </div>
                        </div>
                    </div>

                    <!-- LOGIN - FORGOT PASSWORD -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:HyperLink ID="recover_hyperlink" runat="server">Forgot Password? Click Here!</asp:HyperLink>

                            </div>
                        </div>
                    </div>




                </div>
            </div>


            <!-- COLUMN 3 -->
            <div class="bit-3">
                <div class="box">


                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">1</div>
                        </div>
                    </div>


                </div>
            </div>



        </div>
        <!-- END OF CONTENT -->


        <!-- START OF REQUIRED -->
        <br />

        <div class="Css_Page_Login_Required">

            <asp:RequiredFieldValidator ID="required_username" runat="server" ErrorMessage="Check Username" ValidationGroup="Group_Login" ControlToValidate="login_username" BackColor="Red" ForeColor="White" Font-Italic="true"></asp:RequiredFieldValidator>
            |
            <asp:Label ID="login_failed" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            |
            <asp:RequiredFieldValidator ID="required_password" runat="server" ErrorMessage="Check Password" ValidationGroup="Group_Login" ControlToValidate="login_password" BackColor="Red" ForeColor="White" Font-Italic="true"></asp:RequiredFieldValidator>


        </div>


        <br />
        <!-- END OF REQUIRED -->


        <!-- START OF FOOTER -->
        <div class="frame">

            <!-- ABOUT US -->
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_About" runat="server" CssClass="Css_Page_Footer" Text="About" />

                </div>
            </div>

            <!-- TERMS -->
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Terms" runat="server" CssClass="Css_Page_Footer" Text="Terms" />

                </div>
            </div>

            <!-- PRIVACY -->
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Privacy" runat="server" CssClass="Css_Page_Footer" Text="Privacy" />

                </div>
            </div>

            <!-- HELP -->
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Help" runat="server" CssClass="Css_Page_Footer" Text="Help" />

                </div>
            </div>

            <!-- COPYRIGHT -->
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Copyright" runat="server" CssClass="Css_Page_Footer" Text="Copyright" />

                </div>
            </div>


        </div>
        <!-- END OF FOOTER -->



    </form>
</body>
</html>
