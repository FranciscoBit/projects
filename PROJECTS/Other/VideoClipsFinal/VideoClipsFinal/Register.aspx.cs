﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using VideoClipsFinal;


namespace VideoClipsFinal
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

        }


        //====================================================================================================================
        //====================================================================================================================
        //=========================================== REGISTER BUTTOM ========================================================
        //====================================================================================================================
        //====================================================================================================================

        protected void register_submit_Click(object sender, EventArgs e)
        {

  



            //Established a connection with the database
            using (SqlConnection myRegisterConnection = new SqlConnection(
                    @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
            {
                //Open the connection
                myRegisterConnection.Open();

                // First create a new Guid for the user. This will be unique for each user
                Guid userGuid = System.Guid.NewGuid();

                string myUniqueGuid = userGuid.ToString();
                // Hash the password together with our unique userGuid
                string hashedPassword = VideoClipsFinal.App_Code.SuperSecurity.CreateHash(register_password.Text);

                string activatedOrNot = "False";


                //Get time to utc
                DateTime saveUtcNow = DateTime.UtcNow;

                string userIP;
                userIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (userIP == "" || userIP == null)
                {
                    userIP = Request.ServerVariables["REMOTE_ADDR"];
                }


                //Create a command where each control is equal to the values in the table
                using (SqlCommand myRegisterCommand = new SqlCommand(
                    "INSERT INTO [Table_Membership] values (@UserName, @UserPassword, @UserGuid, @UserEmail, @CreationDate, @UserIP, @ActivatedOrNot)", myRegisterConnection))
                {
                    myRegisterCommand.Parameters.AddWithValue("UserName", register_username.Text);
                    myRegisterCommand.Parameters.AddWithValue("UserPassword", hashedPassword);
                    myRegisterCommand.Parameters.AddWithValue("UserGuid", myUniqueGuid);
                    myRegisterCommand.Parameters.AddWithValue("UserEmail", register_email.Text);
                    myRegisterCommand.Parameters.AddWithValue("CreationDate", saveUtcNow);
                    myRegisterCommand.Parameters.AddWithValue("UserIP", userIP);
                    myRegisterCommand.Parameters.AddWithValue("ActivatedOrNot", activatedOrNot);


                    myRegisterCommand.ExecuteNonQuery();
                }
                //Close connection 
                myRegisterConnection.Close();
            }
            Response.Redirect("Intro/Welcome.aspx");



        }






    }
}