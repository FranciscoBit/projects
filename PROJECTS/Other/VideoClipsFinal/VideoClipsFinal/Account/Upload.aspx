﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="VideoClipsFinal.Account.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../CSS/Style_Master.css">

    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../JS/JS_Category.js" charset="utf-8"></script>

    <title>Lemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">

        <!-- MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" OnClick="navigation_home_Click" ToolTip="Home" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png" OnClick="navigation_categories_Click" ToolTip="Search" />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png" OnClick="navigation_profile_Click" ToolTip="My Profile" />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png" OnClick="navigation_notifications_Click" ToolTip="Notifications" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png" OnClick="navigation_upload_Click" ToolTip="Upload" />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png" OnClick="navigation_settings_Click" ToolTip="Settings" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" OnClick="navigation_logout_Click" ToolTip="Logout" />
                </ul>
            </div>
        </div>


        <br />
        <br />
        <br />
        <br />



        <div class="frame">
            <div class="bit-2">
                <div class="box">

                    <asp:Button ID="create_series" runat="server" CssClass="Css_Page_Upload_Buttoms" Text="Create Series" OnClick="create_series_Click" />


                </div>
            </div>
            <div class="bit-2">
                <div class="box">

                    <asp:Button ID="create_episodes" runat="server" CssClass="Css_Page_Upload_Buttoms" Text="Upload Episodes" OnClick="create_episodes_Click" />


                </div>
            </div>
        </div>



        <div class="frame">
            <div class="bit-2">
                <div class="box">

                    <asp:Button ID="create_movie" runat="server" CssClass="Css_Page_Upload_Buttoms" Text="Create Movie" />


                </div>
            </div>
            <div class="bit-2">
                <div class="box">

                    <asp:Button ID="create_commercial" runat="server" CssClass="Css_Page_Upload_Buttoms" Text="Create Commercial" />


                </div>
            </div>
        </div>





    </form>
</body>
</html>
