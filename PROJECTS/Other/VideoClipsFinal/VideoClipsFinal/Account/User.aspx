﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="VideoClipsFinal.Account.User" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_User.css">

    <link rel="stylesheet" type="text/css"
        href="//releases.flowplayer.org/5.4.3/skin/playful.css">

    <!-- flowplayer depends on jQuery 1.7.1+ -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <!-- flowplayer javascript component -->
    <script src="//releases.flowplayer.org/5.4.3/flowplayer.min.js"></script>


    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../JS/JS_Category.js" charset="utf-8"></script>
    <script type="text/javascript" src="../JS/JS_User.js" charset="utf-8"></script>




    <title>Lemonade Example</titLemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->






</head>
<body>
    <form id="form1" runat="server">

        <!-- MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" ToolTip="Home" OnClick="navigation_home_Click" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png" ToolTip="Search" OnClick="navigation_categories_Click" />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png" ToolTip="My Profile" OnClick="navigation_profile_Click" />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png" ToolTip="Notifications" OnClick="navigation_notifications_Click" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png" ToolTip="Upload" OnClick="navigation_upload_Click" />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png" ToolTip="Settings" OnClick="navigation_settings_Click" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" ToolTip="Logout" OnClick="navigation_logout_Click" />
                </ul>
            </div>
            <!-- close menu -->
        </div>
        <!-- close navi -->

        <br />
        <br />




        <div class="frame">
            <div class="bit-1">
                <div class="box">

                    <asp:Label ID="user_my_profile_name" runat="server"></asp:Label>

                </div>
            </div>
        </div>





        <div class="frame">
            <div class="bit-3">
                <div class="box">


                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <!-- VIDEO - SOURCE ITEMS -->
                                <div data-swf="//releases.flowplayer.org/5.4.3/flowplayer.swf"
                                    class="flowplayer play-button"
                                    data-ratio="0.416">
                                    <video poster="http://farm4.staticflickr.com/3169/2972817861_73ae53c2e5_b.jpg">
                                        <source type="video/webm" src="http://stream.flowplayer.org/bauhaus/624x260.webm" />
                                        <source type="video/mp4" src="http://stream.flowplayer.org/bauhaus/624x260.mp4" />
                                        <source type="video/ogv" src="http://stream.flowplayer.org/bauhaus/624x260.ogv" />
                                    </video>
                                </div>
                                <!-- END VIDEO - SOURCE ITEMS -->




                            </div>
                        </div>
                    </div>



                </div>
            </div>
            <div class="bit-3">
                <div class="box">



                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:Button ID="buttom_timeline" runat="server" Text="Timeline" OnClick="buttom_timeline_Click" />

                            </div>
                        </div>
                    </div>




                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:Button ID="buttom_about" runat="server" Text="About" OnClick="buttom_about_Click" />

                            </div>
                        </div>
                    </div>


                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:Button ID="buttom_series" runat="server" Text="Video Series" OnClick="buttom_series_Click" />

                            </div>
                        </div>
                    </div>



                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:Button ID="buttom_followers" runat="server" Text="Followers" OnClick="buttom_followers_Click" />

                            </div>
                        </div>
                    </div>





                </div>
            </div>
            <div class="bit-3">
                <div class="box">

                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <!-- VIDEO - SOURCE ITEMS -->
                                <div data-swf="//releases.flowplayer.org/5.4.3/flowplayer.swf"
                                    class="flowplayer play-button"
                                    data-ratio="0.416">
                                    <video poster="http://farm4.staticflickr.com/3169/2972817861_73ae53c2e5_b.jpg">
                                        <source type="video/webm" src="http://stream.flowplayer.org/bauhaus/624x260.webm" />
                                        <source type="video/mp4" src="http://stream.flowplayer.org/bauhaus/624x260.mp4" />
                                        <source type="video/ogv" src="http://stream.flowplayer.org/bauhaus/624x260.ogv" />
                                    </video>
                                </div>
                                <!-- END VIDEO - SOURCE ITEMS -->




                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>



        <div class="frame">
            <div class="bit-1">
                <div class="box">



                    <!-- TIMELINE -->
                    <div id="load_div_timeline" runat="server">
                        Timeline
                    </div>
                    <!-- ABOUT -->
                    <div id="load_div_about" runat="server">


                        <!-- START OF FRAME -->
                        <div class="frame">

                            <!-- COLUMNS 1 -->
                            <div class="bit-2">
                                <div class="box">


                                    <asp:Image ID="Page_User_Image" runat="server" />





                                </div>
                            </div>


                            <!-- COLUMNS 2 -->
                            <div class="bit-2">
                                <div class="box">


                                    <div class="frame">

                                        <div class="bit-1">
                                            <div class="box">

                                                <asp:Label ID="Page_Profile_Label_FullName" runat="server" Text="Label"></asp:Label>

                                            </div>
                                        </div>
                                        <div class="bit-1">
                                            <div class="box">

                                                <asp:Label ID="Page_Profile_Label_ShortBio" runat="server" Text="Label"></asp:Label>

                                            </div>
                                        </div>
                                        <div class="bit-1">
                                            <div class="box">

                                                <asp:Label ID="Page_Profile_Label_City" runat="server" Text="Label"></asp:Label>

                                            </div>
                                        </div>
                                        <div class="bit-1">
                                            <div class="box">

                                                <asp:Label ID="Page_Profile_Label_Country" runat="server" Text="Label"></asp:Label>

                                            </div>
                                        </div>
                                        <div class="bit-1">
                                            <div class="box">

                                                <asp:Label ID="Page_Profile_Label_Gender" runat="server" Text="Label"></asp:Label>

                                            </div>
                                        </div>
                                        <div class="bit-1">
                                            <div class="box">

                                                <asp:Label ID="Page_Profile_Label_Website" runat="server" Text="Label"></asp:Label>

                                            </div>
                                        </div>
                                    </div>










                                </div>
                            </div>


                        </div>
                        <!-- END OF FRAME -->














                    </div>
                    <!-- VIDEO SERIES -->
                    <div id="load_div_series" runat="server">
                        Series
                    </div>

                    <!-- FOLLOWERS -->
                    <div id="load_div_followers" runat="server">
                        Followers
                    </div>



                </div>
            </div>
        </div>






    </form>
</body>
</html>
