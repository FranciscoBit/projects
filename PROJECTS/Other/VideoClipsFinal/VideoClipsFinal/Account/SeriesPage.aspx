﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SeriesPage.aspx.cs" Inherits="VideoClipsFinal.Account.SeriesPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../CSS/Style_Master.css">


    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../JS/JS_Category.js" charset="utf-8"></script>


    <title>Lemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    
         <!-- MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" ToolTip="Home" OnClick="navigation_home_Click" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png" ToolTip="Search" OnClick="navigation_categories_Click"  />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png" ToolTip="My Profile" OnClick="navigation_profile_Click"  />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png" ToolTip="Notifications" OnClick="navigation_notifications_Click" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png" ToolTip="Upload" OnClick="navigation_upload_Click"  />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png" ToolTip="Settings" OnClick="navigation_settings_Click" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" ToolTip="Logout" OnClick="navigation_logout_Click" />


                </ul>
            </div>
            <!-- close menu -->
        </div>
        <!-- close navi -->

        <br />
        <br />


        <!-- START OF FRAME -->
        <div class="frame">

            <!-- COLUMNS 1 -->
            <div class="bit-2">
                <div class="box">
                    

                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:Image ID="Page_SeriesPage_SeriesImage" CssClass="Css_Page_Images" runat="server" />

                            </div>
                        </div>
                    </div>






                </div>
            </div>


            <!-- COLUMNS 2 -->
            <div class="bit-2">
                <div class="box">
                    

                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">
                                
                                
                                <asp:Label ID="Page_SeriesPage_SeriesName" runat="server" Text="Label"></asp:Label>


                            </div>
                        </div>
                    </div>

                     
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">
                                
                                <asp:Label ID="Page_SeriesPage_SeriesAuthor" runat="server" Text="Label"></asp:Label>
                               


                            </div>
                        </div>
                    </div>


                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">
                                
                                <asp:Label ID="Page_SeriesPage_DateCreated" runat="server" Text="Label"></asp:Label>
                               


                            </div>
                        </div>
                    </div>


                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">
                                
                                <asp:Label ID="Page_SeriesPage_SeriesDescrib" runat="server" Text="Label"></asp:Label>
                               


                            </div>
                        </div>
                    </div>










                </div>
            </div>


        </div>
        <!-- END OF FRAME -->



        <!-- START OF EPISODES -->
        <div class="frame">


            <asp:ListView ID="Repeater_Column_1" runat="server" GroupItemCount="1">

                <ItemTemplate>

                    <div class="bit-3">
                        <div class="box">


                            <ul class="bottom_wrapper_category">
                                <asp:HyperLink ID="category_1_part_1" Text='<%#Eval("SeriesTitle") %>' NavigateUrl='<%#Eval("UniqueSeriesID", "~/Account/SeriesPage/{0}") %>' runat="server" ForeColor="Lime" Font-Underline="false"  Font-Bold="True">HyperLink</asp:HyperLink>
                            </ul>

                            

                            <ul class="bottom_wrapper_image">
                                <asp:ImageButton ID="image_1_part_1" PostBackUrl='<%#Eval("UniqueEpisodeID", "~/Account/Watch/{0}") %>' ImageUrl='<%#Eval("EpisodeImageShortPath") %>' CssClass="Css_Page_Images" runat="server" />
                            </ul>

                            <ul class="bottom_wrapper_author">
                                <asp:HyperLink ID="author_1_part_1" Text='<%#Eval("UserName", "Created By: {0}") %>' NavigateUrl='<%#Eval("UserName", "~/Account/User/{0}") %>' runat="server" ForeColor="Red" Font-Underline="false" ></asp:HyperLink>

                            </ul>

                            <ul class="bottom_wrapper_title">
                                <asp:HyperLink ID="title_1_part_1" Text='<%#Eval("EpisodeTitle") %>' runat="server" ForeColor="Yellow">HyperLink</asp:HyperLink>
                            </ul>



                        </div>
                    </div>

                </ItemTemplate>




            </asp:ListView>

        </div>
        <!-- END OF EPISODES -->






    </form>
</body>
</html>
