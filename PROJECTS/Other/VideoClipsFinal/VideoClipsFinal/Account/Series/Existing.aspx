﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Existing.aspx.cs" Inherits="VideoClipsFinal.Account.Series.Existing" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../../CSS/Style_Master.css">


    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../../JS/JS_Category.js" charset="utf-8"></script>


    <title>Lemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->



</head>
<body>
    <form id="form1" runat="server">

        <!-- MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" ToolTip="Home" OnClick="navigation_home_Click" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png" ToolTip="Search" OnClick="navigation_categories_Click" />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png" ToolTip="My Profile" OnClick="navigation_profile_Click" />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png" ToolTip="Notifications" OnClick="navigation_notifications_Click" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png" ToolTip="Upload" OnClick="navigation_upload_Click" />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png" ToolTip="Settings" OnClick="navigation_settings_Click" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" ToolTip="Logout" OnClick="navigation_logout_Click" />

                </ul>
            </div>
            <!-- close menu -->
        </div>
        <!-- close navi -->

        <br />
        <br />
        <br />
        <br />


        <div class="frame">
            <div class="bit-1">
                <div class="box">

                    <asp:Label ID="existing_episode_label" runat="server" Text="Upload Episode Now!"></asp:Label>

                </div>
            </div>
        </div>



        <div class="frame">
            <div class="bit-2">
                <div class="box">

                    <asp:TextBox ID="existing_episode_title" runat="server" CssClass="Css_Page_Existing_Textbox" placeholder="Episode Title" ValidationGroup="Existing_Episode_Upload"></asp:TextBox>

                </div>
            </div>

            <div class="bit-2">
                <div class="box">

                    <asp:TextBox ID="existing_episode_description" runat="server" CssClass="Css_Page_Existing_Textbox" placeholder="Episode Description" TextMode="MultiLine" ValidationGroup="Existing_Episode_Upload"></asp:TextBox>

                </div>
            </div>

        </div>






        <div class="frame">
            <div class="bit-2">
                <div class="box">

                    <asp:DropDownList ID="existing_episode_season_number" runat="server" CssClass="Css_Page_Existing_Textbox" ValidationGroup="Existing_Episode_Upload"></asp:DropDownList>

                </div>
            </div>


            <div class="bit-2">
                <div class="box">

                    <asp:DropDownList ID="existing_episode_number" runat="server" CssClass="Css_Page_Existing_Textbox" ValidationGroup="Existing_Episode_Upload"></asp:DropDownList>

                </div>
            </div>


        </div>




        <div class="frame">
            <div class="bit-2">
                <div class="box">

                    <asp:FileUpload ID="existing_episode_upload_image" runat="server" CssClass="Css_Page_Existing_Textbox" ToolTip="Upload Episode Image" />

                </div>
            </div>

            <div class="bit-2">
                <div class="box">

                    <asp:FileUpload ID="existing_episode_upload_video" runat="server" CssClass="Css_Page_Existing_Textbox" ToolTip="Upload Episode Video" />

                </div>
            </div>




        </div>





        <div class="frame">
            <div class="bit-1">
                <div class="box">
                    progresss bar


                </div>
            </div>
        </div>




        <div class="frame">
            <div class="bit-1">
                <div class="box">

                    <asp:Button ID="existing_upload_buttom" runat="server" Text="Upload Now!" ValidationGroup="Existing_Episode_Upload" />


                </div>
            </div>
        </div>




        <div class="frame">
            <div class="bit-1">
                <div class="box">

                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required Episode Title" ControlToValidate="existing_episode_title" ValidationGroup="Existing_Episode_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required Episode Description" ControlToValidate="existing_episode_description" ValidationGroup="Existing_Episode_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required Season Number" ControlToValidate="existing_episode_season_number" ValidationGroup="Existing_Episode_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required Episode Number" ControlToValidate="existing_episode_number" ValidationGroup="Existing_Episode_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required Image Upload" ControlToValidate="existing_episode_upload_image" ValidationGroup="Existing_Episode_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Required Video Upload" ControlToValidate="existing_episode_upload_image" ValidationGroup="Existing_Episode_Upload"></asp:RequiredFieldValidator>

                </div>
            </div>
        </div>



    </form>
</body>
</html>
