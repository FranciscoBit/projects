﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="New.aspx.cs" Inherits="VideoClipsFinal.Account.Series.New" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../../CSS/Style_Master.css">


    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../../JS/JS_Category.js" charset="utf-8"></script>


    <title>Lemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">

        <!-- MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" ToolTip="Home" OnClick="navigation_home_Click" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png" ToolTip="Search" OnClick="navigation_categories_Click" />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png" ToolTip="My Profile" OnClick="navigation_profile_Click" />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png" ToolTip="Notifications" OnClick="navigation_notifications_Click" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png" ToolTip="Upload" OnClick="navigation_upload_Click" />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png" ToolTip="Settings" OnClick="navigation_settings_Click" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" ToolTip="Logout" OnClick="navigation_logout_Click" />

                </ul>
            </div>
        </div>


        <br />
        <br />


        <!-- TITLE TOP -->
        <div class="frame">
            <div class="bit-1">
                <div class="box">

                    <asp:Label ID="new_series_label_top" runat="server" Text="Create Series Now!"></asp:Label>

                </div>
            </div>
        </div>




        <!-- THE SERIES STUFF-->
        <div class="frame">
            <div class="bit-2">
                <div class="box">

                    <!-- SERIES TITLE -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:TextBox ID="series_title" runat="server" CssClass="Css_Page_Upload_New_Textbox" placeholder="Series Title" ValidationGroup="Group_New_Upload"></asp:TextBox>

                            </div>
                        </div>
                    </div>

                    <!-- THE SERIES DESCRIB -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:TextBox ID="series_describ" runat="server" CssClass="Css_Page_Upload_New_Textbox" placeholder="Series Description" TextMode="MultiLine" ValidationGroup="Group_New_Upload"></asp:TextBox>

                            </div>
                        </div>
                    </div>

                    <!-- SERIES CATEGORY -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:DropDownList ID="series_category" runat="server" CssClass="Css_Page_Upload_New_Textbox" ValidationGroup="Group_New_Upload" ToolTip="Series Category"></asp:DropDownList>

                            </div>
                        </div>
                    </div>

                    <!-- SERIES IMAGE -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:FileUpload ID="series_image" runat="server" CssClass="Css_Page_Upload_New_Textbox" ToolTip="Upload Series Image" />

                            </div>
                        </div>
                    </div>





                </div>
            </div>

            <!-- THE EPISODES STUFF-->
            <div class="bit-2">
                <div class="box">

                    <!-- EPISODE TITLE -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:TextBox ID="episode_title" runat="server" CssClass="Css_Page_Upload_New_Textbox" placeholder="Episode Title" ValidationGroup="Group_New_Upload"></asp:TextBox>

                            </div>
                        </div>
                    </div>

                    <!-- EPISODE DESCRIB -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:TextBox ID="episode_describ" runat="server" CssClass="Css_Page_Upload_New_Textbox" placeholder="Episode Description" TextMode="MultiLine" ValidationGroup="Group_New_Upload"></asp:TextBox>

                            </div>
                        </div>
                    </div>


                    <!-- EPISODE SEASON -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:DropDownList ID="season_number" runat="server" CssClass="Css_Page_Upload_New_Textbox" ValidationGroup="Group_New_Upload"></asp:DropDownList>


                            </div>
                        </div>
                    </div>

                    <!-- EPISODE NUMBER -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:DropDownList ID="episode_number" runat="server" CssClass="Css_Page_Upload_New_Textbox" placeholder="yo" ValidationGroup="Group_New_Upload"></asp:DropDownList>


                            </div>
                        </div>
                    </div>

                    <!-- EPISODE IMAGE -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:FileUpload ID="episode_image" runat="server" CssClass="Css_Page_Upload_New_Textbox" ToolTip="Upload Episode Image" />


                            </div>
                        </div>
                    </div>

                    <!-- EPISODE VIDEO -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <asp:FileUpload ID="episode_video_upload" CssClass="Css_Page_Upload_New_Textbox" runat="server" ToolTip="Upload Video" />

                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <div class="frame">
            <div class="bit-1">
                <div class="box">

                    <asp:Button ID="buttom_submit" runat="server" Text="Upload Now!" ValidationGroup="Group_New_Upload" OnClick="buttom_submit_Click" />

                </div>
            </div>
        </div>




       

                    <asp:RequiredFieldValidator ID="new_RequiredFieldValidator1" runat="server" ErrorMessage="Required Series Title" ControlToValidate="series_title" ValidationGroup="Group_New_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="new_RequiredFieldValidator2" runat="server" ErrorMessage="Required Series Description" ControlToValidate="series_describ" ValidationGroup="Group_New_Upload"></asp:RequiredFieldValidator>
                   
                    |<asp:RequiredFieldValidator ID="new_RequiredFieldValidator4" runat="server" ErrorMessage="Required Series Image" ControlToValidate="series_image" ValidationGroup="Group_New_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="new_RequiredFieldValidator5" runat="server" ErrorMessage="Required Episode Title" ControlToValidate="episode_title" ValidationGroup="Group_New_Upload"></asp:RequiredFieldValidator>



                    <asp:RequiredFieldValidator ID="new_RequiredFieldValidator6" runat="server" ErrorMessage="Required Episode Description" ControlToValidate="episode_describ" ValidationGroup="Group_New_Upload"></asp:RequiredFieldValidator>
                    
                    |<asp:RequiredFieldValidator ID="new_RequiredFieldValidator9" runat="server" ErrorMessage="Required Episode Image" ControlToValidate="episode_image" ValidationGroup="Group_New_Upload"></asp:RequiredFieldValidator>
                    |<asp:RequiredFieldValidator ID="new_RequiredFieldValidator10" runat="server" ErrorMessage="Required Episode Video" ControlToValidate="episode_video_upload" ValidationGroup="Group_New_Upload"></asp:RequiredFieldValidator>

                





    </form>
</body>
</html>
