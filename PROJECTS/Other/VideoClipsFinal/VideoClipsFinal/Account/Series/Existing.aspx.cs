﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VideoClipsFinal.Account.Series
{
    public partial class Existing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {

            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }

        protected void navigation_home_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../MyWall.aspx");
        }

        protected void navigation_categories_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Categories.aspx");
        }

        protected void navigation_profile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../User.aspx");
        }

        protected void navigation_notifications_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void navigation_upload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Upload.aspx");
        }

        protected void navigation_settings_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Settings/Basic.aspx");
        }

        protected void navigation_logout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }
    }
}