﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Media;


namespace VideoClipsFinal.Account.Series
{
    public partial class New : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {

                //Now we try to get the info out to populate the dropdown list
                try
                {
                    using (SqlConnection myConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                    {

                        using (SqlCommand myCommand = new SqlCommand(
                            "SELECT CategoryName FROM Table_Categories ORDER BY CategoryName ASC", myConnection))
                        {
 
                            myConnection.Open();
                             

                            using (SqlDataAdapter myAdapter =new SqlDataAdapter(myCommand))
                            {

                                DataSet myDataSet = new DataSet();

                                myAdapter.Fill(myDataSet);

                                series_category.DataSource = myDataSet.Tables[0];
                                series_category.DataTextField = "CategoryName";
                                series_category.DataValueField = "CategoryName";
                                series_category.DataBind();

                                series_category.SelectedIndex = series_category.Items.IndexOf(series_category.Items.FindByText("CategoryName"));
                            }

                        }

                        using (SqlCommand mySecondCommand = new SqlCommand(
                            "SELECT SeasonNumber FROM Table_Season_Number ORDER BY SeasonNumber ASC", myConnection))
                        {

                            using (SqlDataAdapter myAdapter = new SqlDataAdapter(mySecondCommand))
                            {

                                DataSet myDataSet = new DataSet();

                                myAdapter.Fill(myDataSet);

                                season_number.DataSource = myDataSet.Tables[0];
                                season_number.DataTextField = "SeasonNumber";
                                season_number.DataValueField = "SeasonNumber";
                                season_number.DataBind();

                            }

                        }


                        using (SqlCommand myThirdCommand = new SqlCommand(
                            "SELECT EpisodeNumber FROM Table_Episode_Number", myConnection))
                        {

                            using (SqlDataAdapter myAdapter = new SqlDataAdapter(myThirdCommand))
                            {

                                DataSet myDataSet = new DataSet();

                                myAdapter.Fill(myDataSet);

                                episode_number.DataSource = myDataSet.Tables[0];
                                episode_number.DataTextField = "EpisodeNumber";
                                episode_number.DataValueField = "EpisodeNumber";
                                episode_number.DataBind();

                            }

                        }




                        myConnection.Close();



                    }


                }
                catch
                {
                }







            }
            else
            {
                Response.Redirect("../../Login.aspx");
            }
        }

        protected void navigation_home_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../MyWall.aspx");
        }

        protected void navigation_categories_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Categories.aspx");
        }

        protected void navigation_profile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../User.aspx");
        }

        protected void navigation_notifications_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void navigation_upload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Upload.aspx");
        }

        protected void navigation_settings_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Settings/Basic.aspx");
        }

        protected void navigation_logout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }

        protected void buttom_submit_Click(object sender, EventArgs e)
        {

            //get file time of posting - It will give us a unique EPISODE ID
            string strUniqueEpisodeID;
            strUniqueEpisodeID = DateTime.Now.Ticks.ToString() + System.Guid.NewGuid().ToString();

            //get file time of posting - It will give us a unique SERIES ID
            string strUniqueSeriesID;
            strUniqueSeriesID = System.Guid.NewGuid().ToString() + DateTime.Now.Ticks.ToString();

            //Get username
            string myUserName = Session["New"].ToString();

            //-------------------------------------------- VIDEO FILE -----------------------------------------------
            //THE VIDEO FILE NAME
            string strFileName;
            strFileName = Session["New"].ToString() + "-"
                            + Guid.NewGuid().ToString().Substring(0, 8) +"-" + episode_video_upload.PostedFile.FileName;

            //Get VIDEO FILE EXTENSION 
            string strFileExtension;
            strFileExtension = System.IO.Path.GetExtension(episode_video_upload.FileName);

            //ABSOLUTE PATH OF VIDEO FILE
            string filepathVideo;
            filepathVideo = @"C:\Users\Dragon\Documents\Visual Studio 2012\Web Apps\VideoClipsFinal\VideoClipsFinal\Storage\Videos\" + strFileName;

            string shortFileVideoPath = @"~\Storage\Videos\" + strFileName;


            //-------------------------------------------- SERIES IMAGE ----------------------------------------------
            //THE SERIES PICTURE FILE NAME
            string strPicFileName;
            strPicFileName = Session["New"].ToString() + "-"
                            + Guid.NewGuid().ToString().Substring(0, 8) +"-" + series_image.PostedFile.FileName;

            //Get SERIES PICTURE FILE  EXTENSION
            string strFilePicExtension;
            strFilePicExtension = System.IO.Path.GetExtension(series_image.FileName);

            //ABSOLUTE PATH OF SERIES IMAGE FILE
            string filepathSeriesPicture;
            filepathSeriesPicture = @"C:\Users\Dragon\Documents\Visual Studio 2012\Web Apps\VideoClipsFinal\VideoClipsFinal\Storage\Series\" + strPicFileName;

            //SHORT SERIES IMAGE PATH
            string shortImageSeriesPath = @"~\Storage\Series\" + strPicFileName;

            
            //---------------------------------------------- EPISODE IMAGE ----------------------------------------------

            //THE EPISODE PICTURE FILE NAME
            string strEpisodePicFileName;
            strEpisodePicFileName = Session["New"].ToString() + "-"
                            + Guid.NewGuid().ToString().Substring(0, 8) +"-" + episode_image.PostedFile.FileName;

            //Get EPISODE PICTURE FILE  EXTENSION
            string strEpisodeFilePicExtension;
            strEpisodeFilePicExtension = System.IO.Path.GetExtension(episode_image.FileName);

            //ABSOLUTE PATH OF EPISODE IMAGE FILE
            string filepathEpisodePicture;
            filepathEpisodePicture = @"C:\Users\Dragon\Documents\Visual Studio 2012\Web Apps\VideoClipsFinal\VideoClipsFinal\Storage\Episode\" + strEpisodePicFileName;

            //SHORT SERIES IMAGE PATH
            string shortImageEpisodePath = @"~\Storage\Episode\" + strEpisodePicFileName;

            


            //---------------------------------------------- VIDEO INFORMATION ----------------------------------------------

            

            if (strFileExtension == ".wmv")
            {


                try
                {


                    //Established a connection with the database
                    using (SqlConnection myUploadConnection = new SqlConnection(
                            @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                    {
                        //Open the connection
                        myUploadConnection.Open();

                        //Get time to utc
                        DateTime timeOfCreation = DateTime.UtcNow;


                        using (SqlCommand myFirstUploadCommand = new SqlCommand(
                            "INSERT INTO [Table_Episode] values (@EpisodeTitle, @EpisodeDescrib, @EpisodeNumber, @UserName, @SeriesTitle, @UniqueSeriesID, @UniqueEpisodeID, @UploadDate, @SeriesSeason, @EpisodeImageFilePath, @EpisodeImageShortPath, @EpisodeVideoFilePath, @EpisodeVideoShortPath)", myUploadConnection))
                        {
                            myFirstUploadCommand.Parameters.AddWithValue("EpisodeTitle", episode_title.Text);
                            myFirstUploadCommand.Parameters.AddWithValue("EpisodeDescrib", episode_describ.Text);
                            myFirstUploadCommand.Parameters.AddWithValue("EpisodeNumber", episode_number.Text);
                            myFirstUploadCommand.Parameters.AddWithValue("UserName", myUserName);
                            myFirstUploadCommand.Parameters.AddWithValue("SeriesTitle", series_title.Text);
                            myFirstUploadCommand.Parameters.AddWithValue("UniqueSeriesID", strUniqueSeriesID);
                            myFirstUploadCommand.Parameters.AddWithValue("UniqueEpisodeID", strUniqueEpisodeID);
                            myFirstUploadCommand.Parameters.AddWithValue("UploadDate", timeOfCreation);
                            myFirstUploadCommand.Parameters.AddWithValue("SeriesSeason", season_number.Text);
                            myFirstUploadCommand.Parameters.AddWithValue("EpisodeImageFilePath", filepathEpisodePicture);
                            myFirstUploadCommand.Parameters.AddWithValue("EpisodeImageShortPath", shortImageEpisodePath);
                            myFirstUploadCommand.Parameters.AddWithValue("EpisodeVideoFilePath", filepathVideo);
                            myFirstUploadCommand.Parameters.AddWithValue("EpisodeVideoShortPath", shortFileVideoPath);



                            myFirstUploadCommand.ExecuteNonQuery();
                        }


                        using (SqlCommand mySecondUploadCommand = new SqlCommand(
                            "INSERT INTO [Table_Series] values (@SeriesTitle, @SeriesDescrib, @SeriesCategory, @UserName, @UniqueSeriesID, @CreationDate, @SeriesImageLongPath, @SeriesImageShortPath)", myUploadConnection))
                        {
                            mySecondUploadCommand.Parameters.AddWithValue("SeriesTitle", series_title.Text);
                            mySecondUploadCommand.Parameters.AddWithValue("SeriesDescrib", series_describ.Text);
                            mySecondUploadCommand.Parameters.AddWithValue("SeriesCategory", series_category.Text);
                            mySecondUploadCommand.Parameters.AddWithValue("UserName", myUserName);
                            mySecondUploadCommand.Parameters.AddWithValue("UniqueSeriesID", strUniqueSeriesID);
                            mySecondUploadCommand.Parameters.AddWithValue("CreationDate", timeOfCreation);
                            mySecondUploadCommand.Parameters.AddWithValue("SeriesImageLongPath", filepathSeriesPicture);
                            mySecondUploadCommand.Parameters.AddWithValue("SeriesImageShortPath", shortImageSeriesPath);

                            mySecondUploadCommand.ExecuteNonQuery();
                        }


                        using (SqlCommand myThirdUploadCommand = new SqlCommand(
                            "INSERT INTO [Table_Episode_Image] values (@FileName, @FileExtension, @UserName, @UniqueEpisodeID, @EpisodeFilePath, @UniqueSeriesID, @EpisodeShortPath)", myUploadConnection))
                        {
                            myThirdUploadCommand.Parameters.AddWithValue("FileName", strEpisodePicFileName);
                            myThirdUploadCommand.Parameters.AddWithValue("FileExtension", strEpisodeFilePicExtension);
                            myThirdUploadCommand.Parameters.AddWithValue("UserName", myUserName);
                            myThirdUploadCommand.Parameters.AddWithValue("UniqueEpisodeID", strUniqueEpisodeID);
                            myThirdUploadCommand.Parameters.AddWithValue("EpisodeFilePath", filepathEpisodePicture);
                            myThirdUploadCommand.Parameters.AddWithValue("UniqueSeriesID", strUniqueSeriesID);
                            myThirdUploadCommand.Parameters.AddWithValue("EpisodeShortPath", shortImageEpisodePath);

                            myThirdUploadCommand.ExecuteNonQuery();
                        }


                        using (SqlCommand myFourthUploadCommand = new SqlCommand(
                            "INSERT INTO [Table_Series_Image] values (@FileName, @FileExtension, @UserName, @UniqueSeriesID, @SeriesFilePath, @SeriesImageShortPath)", myUploadConnection))
                        {
                            myFourthUploadCommand.Parameters.AddWithValue("FileName", strPicFileName);
                            myFourthUploadCommand.Parameters.AddWithValue("FileExtension", strFilePicExtension);
                            myFourthUploadCommand.Parameters.AddWithValue("UserName", myUserName);
                            myFourthUploadCommand.Parameters.AddWithValue("UniqueSeriesID", strUniqueSeriesID);
                            myFourthUploadCommand.Parameters.AddWithValue("SeriesFilePath", filepathSeriesPicture);
                            myFourthUploadCommand.Parameters.AddWithValue("SeriesImageShortPath", shortImageSeriesPath);


                            myFourthUploadCommand.ExecuteNonQuery();
                        }


                        using (SqlCommand myFifthUploadCommand = new SqlCommand(
                            "INSERT INTO [Table_Video] values (@VideoPath, @UniqueEpisodeID, @UniqueSeriesID, @VideoShortPath)", myUploadConnection))
                        {
                            myFifthUploadCommand.Parameters.AddWithValue("VideoPath", filepathVideo);
                            myFifthUploadCommand.Parameters.AddWithValue("UniqueEpisodeID", strUniqueEpisodeID);
                            myFifthUploadCommand.Parameters.AddWithValue("UniqueSeriesID", strUniqueSeriesID);
                            myFifthUploadCommand.Parameters.AddWithValue("VideoShortPath", shortFileVideoPath);


                            myFifthUploadCommand.ExecuteNonQuery();
                        }


                        using (SqlCommand mySixUploadCommand = new SqlCommand(
                            "INSERT INTO [Table_Series_Title] values (@SeriesName, @UniqueSeriesID, @SeriesImageShortPath)", myUploadConnection))
                        {
                            mySixUploadCommand.Parameters.AddWithValue("SeriesName", series_title.Text);
                            mySixUploadCommand.Parameters.AddWithValue("UniqueSeriesID", strUniqueSeriesID);
                            mySixUploadCommand.Parameters.AddWithValue("SeriesImageShortPath", shortImageSeriesPath);


                            mySixUploadCommand.ExecuteNonQuery();
                        }



                        //Close connection 
                        myUploadConnection.Close();


                        //Save SERIES IMAGE
                        series_image.PostedFile.SaveAs(filepathSeriesPicture);

                        //Save EPISODE IMAGE
                        episode_image.PostedFile.SaveAs(filepathEpisodePicture);

                        //Save the VIDEO file in location
                        episode_video_upload.PostedFile.SaveAs(filepathVideo); 


                    }


                    Response.Redirect("../MyWall.aspx");

                }//end of try

                //If try is unsuccessful then we catch the exception
                catch (Exception exp)
                {
                    //error messeage
                }


            }//End of if stament

            else
            {
                //error message
            }



        }



















        
    }
}