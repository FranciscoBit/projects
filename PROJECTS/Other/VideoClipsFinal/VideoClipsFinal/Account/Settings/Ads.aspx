﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ads.aspx.cs" Inherits="VideoClipsFinal.Account.Settings.Ads" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../../CSS/Style_Master.css">
    <link rel="stylesheet" type="text/css" href="../../CSS/Styles_Basic.css">

    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../../JS/JS_Category.js" charset="utf-8"></script>




    <title>Lemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
<body>
    <form id="form1" runat="server">
    <!-- MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" ToolTip="Home" OnClick="navigation_home_Click" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png"  ToolTip="Search" OnClick="navigation_categories_Click" />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png"  ToolTip="My Profile" OnClick="navigation_profile_Click" />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png"  ToolTip="Notifications" OnClick="navigation_notifications_Click" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png"  ToolTip="Upload" OnClick="navigation_upload_Click" />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png"  ToolTip="Settings" OnClick="navigation_settings_Click" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" ToolTip="Logout" OnClick="navigation_logout_Click" />



                </ul>
            </div>
        </div>

        <br />
        <br />
        <br />
        <br />


        <!-- START OF OPTIONS -->
        <div class="frame">

            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Basic_Options_Normal" runat="server" CssClass="Css_Page_Basic_Options" Text="Basic" OnClick="Page_Basic_Options_Normal_Click" />


                </div>
            </div>
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Basic_Options_Profile" runat="server" CssClass="Css_Page_Basic_Options" Text="Profile" OnClick="Page_Basic_Options_Profile_Click" />

                </div>
            </div>
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Basic_Options_Videos" runat="server" CssClass="Css_Page_Basic_Options" Text="Videos" OnClick="Page_Basic_Options_Videos_Click" />

                </div>
            </div>
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Basic_Options_Ads" runat="server" CssClass="Css_Page_Basic_Options" Text="Ads" OnClick="Page_Basic_Options_Ads_Click" />

                </div>
            </div>
            <div class="bit-5">
                <div class="box">

                    <asp:Button ID="Page_Basic_Options_Donate" runat="server" CssClass="Css_Page_Basic_Options" Text="Donate" OnClick="Page_Basic_Options_Donate_Click" />

                </div>
            </div>

        </div>
        <!-- END OF OPTIONS -->

        <!-- START OF FRAME -->
        <div class="frame">

            <!-- COLUMNS 1 -->
            <div class="bit-2">
                <div class="box">
                    

                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">
                                1
                            </div>
                        </div>
                    </div>





                </div>
            </div>


            <!-- COLUMNS 2 -->
            <div class="bit-2">
                <div class="box">
                    

                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">
                                1
                            </div>
                        </div>
                    </div>








                </div>
            </div>


        </div>
        <!-- END OF FRAME -->






    </form>
</body>
</html>
