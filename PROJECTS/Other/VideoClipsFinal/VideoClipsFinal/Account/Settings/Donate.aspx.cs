﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace VideoClipsFinal.Account.Settings
{
    public partial class Donate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void navigation_home_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../MyWall.aspx");
        }

        protected void navigation_categories_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Categories.aspx");
        }

        protected void navigation_profile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../User.aspx");
        }

        protected void navigation_notifications_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void navigation_upload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Upload.aspx");
        }

        protected void navigation_settings_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Settings/Basic.aspx");
        }

        protected void navigation_logout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }

        protected void Page_Basic_Options_Normal_Click(object sender, EventArgs e)
        {
            Response.Redirect("Basic.aspx");
        }

        protected void Page_Basic_Options_Profile_Click(object sender, EventArgs e)
        {
            Response.Redirect("Profile.aspx");
        }

        protected void Page_Basic_Options_Videos_Click(object sender, EventArgs e)
        {
            Response.Redirect("Videos.aspx");
        }

        protected void Page_Basic_Options_Ads_Click(object sender, EventArgs e)
        {
            Response.Redirect("Ads.aspx");
        }

        protected void Page_Basic_Options_Donate_Click(object sender, EventArgs e)
        {
            Response.Redirect("Donate.aspx");
        }
    }
}