﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Categories.aspx.cs" Inherits="VideoClipsFinal.Account.Categories" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../CSS/Style_Master.css">

    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../JS/JS_Category.js" charset="utf-8"></script>




    <title>Lemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">


        <!-- START MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" ToolTip="Home" OnClick="navigation_home_Click" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png" OnClick="navigation_categories_Click" ToolTip="Search" />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png" ToolTip="My Profile" OnClick="navigation_profile_Click" />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png" ToolTip="Notifications" OnClick="navigation_notifications_Click" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png" ToolTip="Upload" OnClick="navigation_upload_Click" />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png" ToolTip="Settings" OnClick="navigation_settings_Click" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" ToolTip="Logout" OnClick="navigation_logout_Click" />
                </ul>
            </div>
        </div>
        <!-- END MENU BAR AND NAVIGATION -->


        <br />
        <br />



        <!-- START OF OPTIONS -->
        <div class="box">
            <div class="frame">

                <!-- BEST RATED -->
                <div class="bit-7">
                    <div class="box">
                        <asp:Button ID="load_most_rated" CssClass="Css_Page_Categories_Options" runat="server" Text="Rated" />
                    </div>
                </div>

                <!-- MOST LIKED -->
                <div class="bit-7">
                    <div class="box">
                        <asp:Button ID="load_most_like" runat="server" CssClass="Css_Page_Categories_Options" Text="Liked" />
                    </div>
                </div>

                <!-- MOST VIEWED -->
                <div class="bit-7">
                    <div class="box">
                        <asp:Button ID="load_most_view" runat="server" CssClass="Css_Page_Categories_Options" Text="Viewed" />
                    </div>
                </div>

                <!-- SHOW RANDOM-->
                <div class="bit-7">
                    <div class="box">
                        <asp:Button ID="load_random" runat="server" CssClass="Css_Page_Categories_Options" Text="Random" />
                    </div>
                </div>

                <!-- SHOW NEWEST -->
                <div class="bit-7">
                    <div class="box">
                        <asp:Button ID="load_newest" runat="server" CssClass="Css_Page_Categories_Options" Text="Newest" />
                    </div> 
                </div>

                <!-- SHOW OLDEST -->
                <div class="bit-7">
                    <div class="box">
                        <asp:Button ID="load_oldest" runat="server" CssClass="Css_Page_Categories_Options" Text="Oldest" />
                    </div>
                </div>

                <!-- SURPRISE ME -->
                <div class="bit-7">
                    <div class="box">
                        <asp:Button ID="load_surprise" runat="server" CssClass="Css_Page_Categories_Options" Text="Surprise" />
                    </div>
                </div>

            </div>

        </div>
        <!-- END OF OPTIONS -->


        <!-- START OF SEARCH -->
        <div class="frame">

            <!-- SEARCH BAR -->
            <div class="bit-2">
                <div class="box">

                    <asp:TextBox ID="buttom_search" runat="server" placeholder="Search"></asp:TextBox>

                </div>
            </div>

            <!-- SEARCH BUTTOM -->
            <div class="bit-2">
                <div class="box">

                    <asp:Button ID="buttom_submit_search" runat="server" Text="Search Now!" />

                </div>
            </div>
        </div>
        <!-- END OF SEARCH -->



        <!-- START OF RELATED VIDEOS -->
        <div class="frame">


            <!-- ================ COLUMN 1 OF 3 ================ -->
            <div class="bit-3">
                <div class="box">

                    <!-- ================ LEFT ============ -->
                    <!-- ================ VIDEO 1 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_LEFT_category_1" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_LEFT_image_1" runat="server" CssClass="Css_Page_Images" />
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_LEFT_author_1" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_LEFT_title_1" runat="server">HyperLink</asp:HyperLink>
                                </ul>



                            </div>
                        </div>
                    </div>

                    <!-- ================ LEFT ============ -->
                    <!-- ================ VIDEO 2 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_LEFT_category_2" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_LEFT_image_2" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_LEFT_author_2" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_LEFT_title_2" runat="server">HyperLink</asp:HyperLink>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <!-- ================ LEFT ============ -->
                    <!-- ================ VIDEO 3 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_LEFT_category_3" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_LEFT_image_3" runat="server" CssClass="Css_Page_Images" />
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_LEFT_author_3" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_LEFT_title_3" runat="server">HyperLink</asp:HyperLink>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <!-- ================ LEFT ============ -->
                    <!-- ================ VIDEO 4 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_LEFT_category_4" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_LEFT_image_4" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_LEFT_author_4" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_LEFT_title_4" runat="server">HyperLink</asp:HyperLink>
                                </ul>

                            </div>
                        </div>
                    </div>


                </div>
            </div>


            <!-- ================COLUMN 2 OF 3 ================ -->
            <div class="bit-3">
                <div class="box">

                    <!-- ================ CENTER ============ -->
                    <!-- ================ VIDEO 1 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_CENTER_category_1" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_CENTER_image_1" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_CENTER_author_1" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_CENTER_title_1" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                            </div>
                        </div>
                    </div>

                    <!-- ================ CENTER ============ -->
                    <!-- ================ VIDEO 2 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_CENTER_category_2" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_CENTER_image_2" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_CENTER_author_2" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_CENTER_title_2" runat="server">HyperLink</asp:HyperLink>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <!-- ================ CENTER ============ -->
                    <!-- ================ VIDEO 3 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                <!-- ================WRAPPER 3 ================ -->
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_CENTER_category_3" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_CENTER_image_3" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_CENTER_author_3" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_CENTER_title_3" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                            </div>
                        </div>
                    </div>

                    <!-- ================ CENTER ============ -->
                    <!-- ================ VIDEO 4 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_CENTER_category_4" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_CENTER_image_4" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_CENTER_author_4" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_CENTER_title_4" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                            </div>
                        </div>
                    </div>



                </div>
            </div>


            <!-- ================ COLUMN 3 OF 3 ================ -->
            <div class="bit-3">
                <div class="box">

                    <!-- ================ RIGHT ============ -->
                    <!-- ================ VIDEO 1 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">


                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_category_1" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_RIGHT_image_1" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_RIGHT_author_1" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_title_1" runat="server">HyperLink</asp:HyperLink>
                                </ul>




                            </div>
                        </div>
                    </div>

                    <!-- ================ RIGHT ============ -->
                    <!-- ================ VIDEO 2 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_category_2" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_RIGHT_image_2" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_RIGHT_author_2" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_title_2" runat="server">HyperLink</asp:HyperLink>
                                </ul>



                            </div>
                        </div>
                    </div>

                    <!-- ================ RIGHT ============ -->
                    <!-- ================ VIDEO 3 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_category_3" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_RIGHT_image_3" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_RIGHT_author_3" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_title_3" runat="server">HyperLink</asp:HyperLink>
                                </ul>

                            </div>
                        </div>
                    </div>

                    <!-- ================ RIGHT ============ -->
                    <!-- ================ VIDEO 4 ================ -->
                    <div class="frame">
                        <div class="bit-1">
                            <div class="box">

                                
                                <ul class="bottom_wrapper_category">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_category_4" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                                <ul class="bottom_wrapper_image">
                                    <asp:ImageButton ID="BOTTOM_RIGHT_image_4" runat="server" CssClass="Css_Page_Images"/>
                                </ul>

                                <ul class="bottom_wrapper_author">
                                    <asp:Label ID="BOTTOM_RIGHT_author_4" runat="server" Text="Label"></asp:Label>

                                </ul>

                                <ul class="bottom_wrapper_title">
                                    <asp:HyperLink ID="BOTTOM_RIGHT_title_4" runat="server">HyperLink</asp:HyperLink>
                                </ul>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!-- END OF RELATED VIDEOS -->




        
















        

    </form>
</body>
</html>
