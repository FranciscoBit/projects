﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Text;

namespace VideoClipsFinal.Account
{
    public partial class SeriesPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["New"] != null)
            {

                string publicSeriesName = Page.RouteData.Values["publicSeriesName"] as string;
                int result = 0;


                using (SqlConnection myPublicUserConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                {

                    using (SqlCommand myPublicUserCommand = new SqlCommand("SELECT count(*) FROM Table_Series WHERE UniqueSeriesID = @UniqueSeriesID", myPublicUserConnection))
                    {
                        myPublicUserCommand.Parameters.AddWithValue("UniqueSeriesID", publicSeriesName);

                        myPublicUserConnection.Open();

                        result = (int)myPublicUserCommand.ExecuteScalar();
                    }


                    myPublicUserConnection.Close();

                }


                if (result > 0)
                {
                    try
                    {
                        using (SqlConnection myWatchSecondVideoConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                        {
                            myWatchSecondVideoConnection.Open();
                            using (SqlCommand mySecondWatchVideoCommand = new SqlCommand(
                                "SELECT SeriesImageShortPath, SeriesTitle, UserName, CreationDate, SeriesDescrib FROM Table_Series WHERE UniqueSeriesID = @UniqueSeriesID; "
                              + "SELECT EpisodeTitle, UserName, EpisodeImageShortPath, SeriesTitle, UniqueEpisodeID, UniqueSeriesID FROM Table_Episode WHERE UniqueSeriesID = @UniqueSeriesID", myWatchSecondVideoConnection))
                            {
                                mySecondWatchVideoCommand.Parameters.AddWithValue("UniqueSeriesID", publicSeriesName);

                                using (SqlDataReader reader = mySecondWatchVideoCommand.ExecuteReader())
                                {

                                    while (reader.Read())
                                    {
                                        Page_SeriesPage_SeriesImage.ImageUrl = reader["SeriesImageShortPath"].ToString();
                                        Page_SeriesPage_SeriesName.Text = "Series Title: " + reader["SeriesTitle"].ToString() ;
                                        Page_SeriesPage_SeriesAuthor.Text = "Created By: " + reader["UserName"].ToString();
                                        Page_SeriesPage_DateCreated.Text = "Creation Date: " + reader["CreationDate"].ToString();
                                        Page_SeriesPage_SeriesDescrib.Text = "Description: " + reader["SeriesDescrib"].ToString();
                                    }

                                    reader.Close();

                                }//END of data reader

                            }//END of first command


                            using (SqlCommand myThirdWatchVideoCommand = new SqlCommand(
                                "SELECT EpisodeTitle, UserName, EpisodeImageShortPath, SeriesTitle, UniqueEpisodeID, UniqueSeriesID FROM Table_Episode WHERE UniqueSeriesID = @UniqueSeriesID", myWatchSecondVideoConnection))
                            {
                                myThirdWatchVideoCommand.Parameters.AddWithValue("UniqueSeriesID", publicSeriesName);

                                DataSet myDataSet = new DataSet();

                                SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter(myThirdWatchVideoCommand);

                                mySqlDataAdapter.Fill(myDataSet);

                                Repeater_Column_1.DataSource = myDataSet;

                                Repeater_Column_1.DataBind();

                            }









                            //Close Connection 
                            myWatchSecondVideoConnection.Close();

                        }//END of using connection

                    }//END of try statement

                    catch (Exception except)
                    {

                    }

                }




            }
            else
            {
                Response.Redirect("../Login.aspx");
            }





        }

        protected void navigation_home_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../MyWall.aspx");
        }

        protected void navigation_categories_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Categories.aspx");
        }

        protected void navigation_profile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../User.aspx");
        }

        protected void navigation_notifications_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void navigation_upload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Upload.aspx");
        }

        protected void navigation_settings_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Settings/Basic.aspx");
        }

        protected void navigation_logout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }
    }
}