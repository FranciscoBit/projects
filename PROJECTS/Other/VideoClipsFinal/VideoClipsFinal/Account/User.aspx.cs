﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace VideoClipsFinal.Account
{
    public partial class User : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                load_div_timeline.Visible = true;
                load_div_about.Visible = false;
                load_div_series.Visible = false;
                load_div_followers.Visible = false;



                string publicUserName = Page.RouteData.Values["publicUserName"] as string;
                int result = 0;

                using (SqlConnection myPublicUserConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                {

                    using (SqlCommand myPublicUserCommand = new SqlCommand("SELECT count(*) FROM Table_Members WHERE UserName = @UserName", myPublicUserConnection))
                    {
                        myPublicUserCommand.Parameters.AddWithValue("UserName", publicUserName);

                        myPublicUserConnection.Open();

                        result = (int)myPublicUserCommand.ExecuteScalar();
                    }


                    myPublicUserConnection.Close();

                }


                if (result > 0)
                {
                    try
                    {
                        using (SqlConnection myWatchSecondVideoConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                        {
                            using (SqlCommand mySecondWatchVideoCommand = new SqlCommand(
                                "SELECT UserName FROM Table_Members WHERE UserName = @UserName; ", myWatchSecondVideoConnection))
                            {
                                mySecondWatchVideoCommand.Parameters.AddWithValue("UserName", publicUserName);

                                myWatchSecondVideoConnection.Open();

                                using (SqlDataReader reader = mySecondWatchVideoCommand.ExecuteReader())
                                {

                                    while (reader.Read())
                                    {
                                        user_my_profile_name.Text = "Welcome to " + reader["UserName"].ToString() + " profile page!";

                                    }

                                    reader.Close();

                                }//END of data reader

                            }//END of first command

                            //Close Connection 
                            myWatchSecondVideoConnection.Close();

                        }//END of using connection

                    }//END of try statement

                    catch (Exception except)
                    {

                    }

                }













            }
            else
            {
                Response.Redirect("../Login.aspx");
            }

            

        }
        protected void buttom_timeline_Click(object sender, EventArgs e)
        {
            load_div_timeline.Visible = true;
            load_div_about.Visible = false;
            load_div_series.Visible = false;
            load_div_followers.Visible = false;
        }


        protected void buttom_about_Click(object sender, EventArgs e)
        {
            load_div_timeline.Visible = false;
            load_div_about.Visible = true;
            load_div_series.Visible = false;
            load_div_followers.Visible = false;
        }
        protected void buttom_series_Click(object sender, EventArgs e)
        {
            load_div_timeline.Visible = false;
            load_div_about.Visible = false;
            load_div_series.Visible = true;
            load_div_followers.Visible = false;
        }
        protected void buttom_followers_Click(object sender, EventArgs e)
        {
            load_div_timeline.Visible = false;
            load_div_about.Visible = false;
            load_div_series.Visible = false;
            load_div_followers.Visible = true;
        }

        protected void navigation_home_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../MyWall.aspx");
        }

        protected void navigation_categories_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Categories.aspx");
        }

        protected void navigation_profile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../User.aspx");
        }

        protected void navigation_notifications_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void navigation_upload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Upload.aspx");
        }

        protected void navigation_settings_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Settings/Basic.aspx");
        }

        protected void navigation_logout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }

        
    }
}