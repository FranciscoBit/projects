﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace VideoClipsFinal.Account
{
    public partial class Watch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                Load_Watch_On_Comments.Visible = false;

                string publicVideoID = Page.RouteData.Values["publicVideoID"] as string;
                int result = 0;
                int resultVideoOptions = 0;

                using (SqlConnection myPublicUserConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                {

                    using (SqlCommand myPublicUserCommand = new SqlCommand("SELECT count(*) FROM Table_Episode WHERE UniqueEpisodeID = @UniqueEpisodeID", myPublicUserConnection))
                    {
                        myPublicUserCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);

                        myPublicUserConnection.Open();

                        result = (int)myPublicUserCommand.ExecuteScalar();
                    }


                    myPublicUserConnection.Close();

                }

                if (result > 0)
                {
                    try
                    {
                        using (SqlConnection myWatchSecondVideoConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                        {
                            myWatchSecondVideoConnection.Open();

                            using (SqlCommand mySecondWatchVideoCommand = new SqlCommand(
                                "SELECT EpisodeTitle, SeriesTitle, EpisodeNumber, UserName FROM Table_Episode WHERE UniqueEpisodeID = @UniqueEpisodeID", myWatchSecondVideoConnection))
                            {
                                mySecondWatchVideoCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);

                                

                                using ( SqlDataReader reader = mySecondWatchVideoCommand.ExecuteReader() )
                                {
                                    while (reader.Read())
                                    {
                                        watch_episode_title_label.Text = reader["SeriesTitle"].ToString() + " - " + reader["EpisodeNumber"].ToString() + " - " + reader["EpisodeTitle"].ToString();
                                        details_username.Text = reader["UserName"].ToString();
                                    }

                                    reader.Close();

                                }//END of data reader

                            }//END of first command


                            using (SqlCommand mythirddWatchVideoCommand = new SqlCommand(
                                "SELECT count(*) FROM Table_Video_Options WHERE UniqueEpisodeID = @UniqueEpisodeID AND UserName = @UserName", myWatchSecondVideoConnection))
                            {
                                mythirddWatchVideoCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                                mythirddWatchVideoCommand.Parameters.AddWithValue("UserName", Session["New"].ToString());

                                resultVideoOptions = (int)mythirddWatchVideoCommand.ExecuteScalar();
                                
                            }

                            if (resultVideoOptions > 0)
                            {
                                try
                                {
                                    using (SqlConnection myVideoOptionsConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                                    {
                                        myVideoOptionsConnection.Open();

                                        using (SqlCommand myVideoOptionsCommand = new SqlCommand(
                                        "SELECT VideoDislikes, VideoLikes, VideoFavorites FROM Table_Video_Options WHERE UniqueEpisodeID = @UniqueEpisodeID AND UserName = @UserName", myWatchSecondVideoConnection))
                                        {
                                            myVideoOptionsCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                                            myVideoOptionsCommand.Parameters.AddWithValue("UserName", Session["New"].ToString());

                                            using ( SqlDataReader reader = myVideoOptionsCommand.ExecuteReader() )
                                            {
                                                while (reader.Read())
                                                {
                                                    //BOTTOM_LEFT_title_2.Text = reader["VideoDislikes"].ToString();
                                                    //BOTTOM_LEFT_title_3.Text = reader["UserName"].ToString();


                                                    if ( (reader["VideoDislikes"].ToString() == "True") && (reader["VideoLikes"].ToString() == "False") )
                                                    {
                                                        details_downvote.BackColor = System.Drawing.Color.Red;
                                                    }
                                                    else if ((reader["VideoDislikes"].ToString() == "False") && (reader["VideoLikes"].ToString() == "True"))
                                                    {
                                                        details_upvote.BackColor = System.Drawing.Color.Lime;
                                                    }


                                                    if (reader["VideoFavorites"].ToString() == "True")
                                                    {
                                                        details_favorite.BackColor = System.Drawing.Color.Lime;
                                                    }



                                                }

                                                reader.Close();

                                            }//END of data reader


                                        }





                                    }
                                }
                                catch
                                {
 
                                }


                            }
                            else
                            {
                                //BOTTOM_LEFT_title_1.Text = "You have not clicked nothing yet~!";
                            }






                            //Close Connection 
                            myWatchSecondVideoConnection.Close();

                        }//END of using connection

                    }//END of try statement

                    catch (Exception except)
                    {

                    }

                }




            }//end of first if statement
            else
            {
                Response.Redirect("../Login.aspx");
            }
        }

        protected void navigation_home_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../MyWall.aspx");
        }

        protected void navigation_categories_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Categories.aspx");
        }

        protected void navigation_profile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../User.aspx");
        }

        protected void navigation_notifications_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void navigation_upload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Upload.aspx");
        }

        protected void navigation_settings_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Settings/Basic.aspx");
        }

        protected void navigation_logout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }

        protected void details_downvote_Click(object sender, ImageClickEventArgs e)
        {

            string theUserName = Session["New"].ToString();
            string publicVideoID = Page.RouteData.Values["publicVideoID"] as string;


            int result = 0;

            using (SqlConnection myDownConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
            {
                using (SqlCommand myDownCommand = new SqlCommand(
                       "SELECT count(*) FROM Table_Video_Options WHERE UserName = @UserName AND UniqueEpisodeID = @UniqueEpisodeID", myDownConnection))
                {

                    myDownCommand.Parameters.AddWithValue("UserName", theUserName);
                    myDownCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);

                    myDownConnection.Open();

                    result = (int)myDownCommand.ExecuteScalar();

                }

                myDownConnection.Close();
            }

            if (result > 0)
            { 
                //if username and uniqueepisodeID EXIST in video options we will UPDATE

                using (SqlConnection myVideoOptionUpdateConnection = new SqlConnection(
                    @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))

                {
                    myVideoOptionUpdateConnection.Open();

                    using (SqlCommand myVideoUpdateCommand = new SqlCommand(
                       "UPDATE Table_Video_Options SET VideoLikes = @VideoLikes, VideoDislikes = @VideoDislikes WHERE UserName = @UserName AND UniqueEpisodeID = @UniqueEpisodeID", myVideoOptionUpdateConnection))
                    {
                        myVideoUpdateCommand.Parameters.AddWithValue("UserName", theUserName);
                        myVideoUpdateCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                        myVideoUpdateCommand.Parameters.AddWithValue("VideoLikes", "False");
                        myVideoUpdateCommand.Parameters.AddWithValue("VideoDislikes", "True");

                        myVideoUpdateCommand.ExecuteNonQuery();
                    }
                    myVideoOptionUpdateConnection.Close();
                }
                



            }
            else if (result == 0)
            {
                //if username and uniqueepisodeID DO NOT EXIST in video options we will INSERT THE VALUES

                using (SqlConnection myVideoOptionInsertConnection = new SqlConnection(
                    @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                
                {
                    myVideoOptionInsertConnection.Open();

                    using (SqlCommand myVideoInsertCommand = new SqlCommand(
                       "INSERT INTO [Table_Video_Options] values (@UserName, @UniqueEpisodeID, @VideoLikes, @VideoDislikes, @VideoFavorites)", myVideoOptionInsertConnection))
                    {
                        myVideoInsertCommand.Parameters.AddWithValue("UserName", theUserName);
                        myVideoInsertCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                        myVideoInsertCommand.Parameters.AddWithValue("VideoLikes", "False");
                        myVideoInsertCommand.Parameters.AddWithValue("VideoDislikes", "True");
                        myVideoInsertCommand.Parameters.AddWithValue("VideoFavorites", "False");


                        myVideoInsertCommand.ExecuteNonQuery();
                    }
                    myVideoOptionInsertConnection.Close();



                }
                
            }

            Response.Redirect(Request.RawUrl);
        }

        protected void details_upvote_Click(object sender, ImageClickEventArgs e)
        {

            string theUserName = Session["New"].ToString();
            string publicVideoID = Page.RouteData.Values["publicVideoID"] as string;


            int result = 0;

            using (SqlConnection myDownConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
            {
                using (SqlCommand myDownCommand = new SqlCommand(
                       "SELECT count(*) FROM Table_Video_Options WHERE UserName = @UserName AND UniqueEpisodeID = @UniqueEpisodeID", myDownConnection))
                {

                    myDownCommand.Parameters.AddWithValue("UserName", theUserName);
                    myDownCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);

                    myDownConnection.Open();

                    result = (int)myDownCommand.ExecuteScalar();

                }

                myDownConnection.Close();
            }

            if (result > 0)
            {
                //if username and uniqueepisodeID EXIST in video options we will UPDATE

                using (SqlConnection myVideoOptionUpdateConnection = new SqlConnection(
                    @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                {
                    myVideoOptionUpdateConnection.Open();

                    using (SqlCommand myVideoUpdateCommand = new SqlCommand(
                       "UPDATE Table_Video_Options SET VideoLikes = @VideoLikes, VideoDislikes = @VideoDislikes WHERE UserName = @UserName AND UniqueEpisodeID = @UniqueEpisodeID", myVideoOptionUpdateConnection))
                    {
                        myVideoUpdateCommand.Parameters.AddWithValue("UserName", theUserName);
                        myVideoUpdateCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                        myVideoUpdateCommand.Parameters.AddWithValue("VideoLikes", "True");
                        myVideoUpdateCommand.Parameters.AddWithValue("VideoDislikes", "False");

                        myVideoUpdateCommand.ExecuteNonQuery();
                    }
                    myVideoOptionUpdateConnection.Close();
                }




            }
            else if (result == 0)
            {
                //if username and uniqueepisodeID DO NOT EXIST in video options we will INSERT THE VALUES

                using (SqlConnection myVideoOptionInsertConnection = new SqlConnection(
                    @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                {
                    myVideoOptionInsertConnection.Open();

                    using (SqlCommand myVideoInsertCommand = new SqlCommand(
                       "INSERT INTO [Table_Video_Options] values (@UserName, @UniqueEpisodeID, @VideoLikes, @VideoDislikes, @VideoFavorites)", myVideoOptionInsertConnection))
                    {
                        myVideoInsertCommand.Parameters.AddWithValue("UserName", theUserName);
                        myVideoInsertCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                        myVideoInsertCommand.Parameters.AddWithValue("VideoLikes", "True");
                        myVideoInsertCommand.Parameters.AddWithValue("VideoDislikes", "False");
                        myVideoInsertCommand.Parameters.AddWithValue("VideoFavorites", "False");


                        myVideoInsertCommand.ExecuteNonQuery();
                    }
                    myVideoOptionInsertConnection.Close();



                }

            }





            Response.Redirect(Request.RawUrl);


        }

        protected void details_favorite_Click(object sender, ImageClickEventArgs e)
        {

            string theUserName = Session["New"].ToString();
            string publicVideoID = Page.RouteData.Values["publicVideoID"] as string;


            int result = 0;

            using (SqlConnection myDownConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
            {
                using (SqlCommand myDownCommand = new SqlCommand(
                       "SELECT count(*) FROM Table_Video_Options WHERE UserName = @UserName AND UniqueEpisodeID = @UniqueEpisodeID", myDownConnection))
                {

                    myDownCommand.Parameters.AddWithValue("UserName", theUserName);
                    myDownCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);

                    myDownConnection.Open();

                    result = (int)myDownCommand.ExecuteScalar();

                }

                myDownConnection.Close();
            }

            if (result > 0)
            {
                //if username and uniqueepisodeID EXIST in video options we will UPDATE

                using (SqlConnection myVideoOptionUpdateConnection = new SqlConnection(
                    @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                {
                    myVideoOptionUpdateConnection.Open();

                    using (SqlCommand myVideoUpdateCommand = new SqlCommand(
                       "UPDATE Table_Video_Options SET VideoFavorites = @VideoFavorites WHERE UserName = @UserName AND UniqueEpisodeID = @UniqueEpisodeID", myVideoOptionUpdateConnection))
                    {
                        myVideoUpdateCommand.Parameters.AddWithValue("UserName", theUserName);
                        myVideoUpdateCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                        myVideoUpdateCommand.Parameters.AddWithValue("VideoFavorites", "True");

                        myVideoUpdateCommand.ExecuteNonQuery();
                    }
                    myVideoOptionUpdateConnection.Close();
                }




            }
            else if (result == 0)
            {
                //if username and uniqueepisodeID DO NOT EXIST in video options we will INSERT THE VALUES

                using (SqlConnection myVideoOptionInsertConnection = new SqlConnection(
                    @"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                {
                    myVideoOptionInsertConnection.Open();

                    using (SqlCommand myVideoInsertCommand = new SqlCommand(
                       "INSERT INTO [Table_Video_Options] values (@UserName, @UniqueEpisodeID, @VideoLikes, @VideoDislikes, @VideoFavorites)", myVideoOptionInsertConnection))
                    {
                        myVideoInsertCommand.Parameters.AddWithValue("UserName", theUserName);
                        myVideoInsertCommand.Parameters.AddWithValue("UniqueEpisodeID", publicVideoID);
                        myVideoInsertCommand.Parameters.AddWithValue("VideoLikes", "False");
                        myVideoInsertCommand.Parameters.AddWithValue("VideoDislikes", "False");
                        myVideoInsertCommand.Parameters.AddWithValue("VideoFavorites", "True");


                        myVideoInsertCommand.ExecuteNonQuery();
                    }
                    myVideoOptionInsertConnection.Close();



                }

            }





            Response.Redirect(Request.RawUrl);




        }

        protected void details_comments_Click(object sender, ImageClickEventArgs e)
        {
            Load_Watch_On_Comments.Visible = true;
        }
    }
}