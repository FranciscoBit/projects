﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Watch.aspx.cs" Inherits="VideoClipsFinal.Account.Watch" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- This is the main grid stylesheet -->
    <link rel="stylesheet" type="text/css" href="../CSS/Lemonade.css">

    <!-- This is styles for the grid ie. h1, p, .content -->
    <link rel="stylesheet" type="text/css" href="../CSS/Style_Master.css">

    <link rel="stylesheet" type="text/css"
        href="//releases.flowplayer.org/5.4.3/skin/playful.css">

    <!-- flowplayer depends on jQuery 1.7.1+ -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <!-- flowplayer javascript component -->
    <script src="//releases.flowplayer.org/5.4.3/flowplayer.min.js"></script>


    <!-- This is for the JS and library -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="../JS/JS_Category.js" charset="utf-8"></script>



    <title>Lemonade Example</titLemonade Example</titLemonade Example</titLemonade Example</titLemonade Example</titLemonade Example</titLemonade Example</titLemonade Example</title>

    <!--[if IE]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">

        <!-- MENU BAR AND NAVIGATION -->
        <div id="navi">
            <div id="menu" class="default">
                <ul>
                    <asp:ImageButton ID="navigation_home" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.home.png" ToolTip="Home" OnClick="navigation_home_Click" />
                    <asp:ImageButton ID="navigation_categories" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.column.one.png" ToolTip="Search" OnClick="navigation_categories_Click" />
                    <asp:ImageButton ID="navigation_profile" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.user.png" ToolTip="My Profile" OnClick="navigation_profile_Click" />
                    <asp:ImageButton ID="navigation_notifications" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.notification.above.png" ToolTip="Notifications" OnClick="navigation_notifications_Click" />
                    <asp:ImageButton ID="navigation_upload" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.inbox.out.png" ToolTip="Upload" OnClick="navigation_upload_Click" />
                    <asp:ImageButton ID="navigation_settings" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.lock.png" ToolTip="Settings" OnClick="navigation_settings_Click" />
                    <asp:ImageButton ID="navigation_logout" runat="server" CssClass="Css_Page_Navigation_All" ImageUrl="~/Images/appbar.power.png" ToolTip="Logout" OnClick="navigation_logout_Click" />
                </ul>
            </div>
        </div>

        <br />
        <br />


        <div class="frame">
            <div class="bit-1">
                <div class="box">
                    <asp:Label ID="watch_episode_title_label" runat="server"></asp:Label>
                </div>
            </div>
        </div>



        <!-- VIDEO -->
        <div class="frame">
            <div class="bit-1">
                <div class="box">

                    <!-- VIDEO - SOURCE ITEMS -->
                    <div data-swf="//releases.flowplayer.org/5.4.3/flowplayer.swf"
                        class="flowplayer play-button"
                        data-ratio="0.416">
                        <video poster="http://farm4.staticflickr.com/3169/2972817861_73ae53c2e5_b.jpg">
                            <source type="video/webm" src="http://stream.flowplayer.org/bauhaus/624x260.webm" />
                            <source type="video/mp4" src="http://stream.flowplayer.org/bauhaus/624x260.mp4" />
                            <source type="video/ogv" src="http://stream.flowplayer.org/bauhaus/624x260.ogv" />
                        </video>
                    </div>
                    <!-- END VIDEO - SOURCE ITEMS -->

                </div>
            </div>
        </div>
        <!-- END VIDEO -->



        <!-- DETAILS FRAME -->



        <!-- USER IMAGE -->
        <div class="frame">
            <div class="bit-8">
                <div class="box">

                    <asp:ImageButton ID="details_userimage" runat="server" CssClass="Css_Page_Watch_Deatails" ImageUrl="~/Images/appbar.futurama.bender.png" />

                </div>
            </div>

            <!-- USER LINK -->
            <div class="bit-8">
                <div class="box">

                    <asp:HyperLink ID="details_username" runat="server" CssClass="Css_Page_Watch_Deatails"></asp:HyperLink>

                </div>
            </div>

            <!-- DOWNVOTE -->
            <div class="bit-8">
                <div class="box">

                    <asp:ImageButton ID="details_downvote" runat="server" CssClass="Css_Page_Watch_Deatails" ImageUrl="~/Images/appbar.smiley.frown.png" ToolTip="Down" OnClick="details_downvote_Click" />

                </div>
            </div>

            <!-- UPVOTE -->
            <div class="bit-8">
                <div class="box">

                    <asp:ImageButton ID="details_upvote" runat="server" CssClass="Css_Page_Watch_Deatails" ImageUrl="~/Images/appbar.smiley.happy.png" ToolTip="Up" OnClick="details_upvote_Click" />

                </div>
            </div>

            <!-- FAVORITE -->
            <div class="bit-8">
                <div class="box">

                    <asp:ImageButton ID="details_favorite" runat="server" CssClass="Css_Page_Watch_Deatails" ImageUrl="~/Images/appbar.crown.png" ToolTip="Favorite" OnClick="details_favorite_Click" />

                </div>
            </div>


            <!-- SUPPORT -->
            <div class="bit-8">
                <div class="box">

                    <asp:ImageButton ID="details_support" runat="server" CssClass="Css_Page_Watch_Deatails" ImageUrl="~/Images/appbar.cards.heart.png" ToolTip="Support" />

                </div>
            </div>

            <!-- COMMENTS -->
            <div class="bit-8">
                <div class="box">

                    <asp:ImageButton ID="details_comments" runat="server" CssClass="Css_Page_Watch_Deatails" ImageUrl="~/Images/appbar.book.open.text.image.png" ToolTip="Comments" OnClick="details_comments_Click" />

                </div>
            </div>



            <!-- VIEWCOUNT -->
            <div class="bit-8">
                <div class="box">

                    <asp:Label ID="details_viewcount" runat="server" CssClass="Css_Page_Watch_Deatails" Text="View count"></asp:Label>

                </div>
            </div>
        </div>





        <!-- END DETAILS -->


        <!-- START OF COMMENTS -->

        <div id="Load_Watch_On_Comments" runat="server">





            <div class="frame">
                <div class="bit-2">
                    <div class="box">

                        <asp:TextBox ID="Page_Watch_Comment_Textbox" runat="server" TextMode="MultiLine"></asp:TextBox>

                        <br />
                        <br />

                        <asp:Button ID="Page_Watch_Comment_Buttom" runat="server" Text="Comment Now!" />

                    </div>
                </div>



                <div class="bit-2">
                    <div class="box">


                        <div class="frame">

                            <div class="bit-1">
                                <div class="box">


                                    <div class="CSS_theLine">
                                        <asp:ImageButton ID="Page_Watch_Comment_User_Img" runat="server" ImageUrl="~/Images/appbar.user.png" />

                                        <div class="bubble">

                                            <div class="CSS_Page_Watch_Comment_Name">

                                                <asp:Label ID="Page_Watch_Comment_UserName_Date" runat="server" Text="Label" ForeColor="Black"></asp:Label>

                                            </div>


                                            

                                            <asp:Label ID="Page_Watch_Comment_Describ" runat="server" Text="Label" ForeColor="Black"></asp:Label>
                                        </div>
                                    </div>

                                    
                                </div>
                            </div>



                        </div>




                    </div>
                </div>

            </div>




        </div>




    </form>
</body>
</html>
