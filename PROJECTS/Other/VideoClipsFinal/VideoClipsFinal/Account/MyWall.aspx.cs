﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using System.IO;
using System.Text;
using System.Data;

namespace VideoClipsFinal.Account
{
    public partial class MyWall : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {


                try
                {
                    using (SqlConnection myWallConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
                    {
                        using (SqlCommand mySecondWatchVideoCommand = new SqlCommand(
                            "SELECT TOP 10 EpisodeTitle, UserName, EpisodeImageShortPath, SeriesTitle, UniqueEpisodeID, UniqueSeriesID FROM Table_Episode ORDER BY NEWID()", myWallConnection))
                        {
                            //"SELECT TOP 1 EpisodeTitle, UserName, EpisodeImageShortPath, SeriesTitle, UniqueEpisodeID, UniqueSeriesID FROM Table_Episode ORDER BY NEWID()", myWallConnection))
                            myWallConnection.Open();

                            //using (SqlDataReader reader = mySecondWatchVideoCommand.ExecuteReader())
                            //{


                            //while (reader.Read())
                            // {
                            //title_1_part_1.Text = reader["EpisodeTitle"].ToString();
                            //author_1_part_1.Text =  "By "+ reader["UserName"].ToString();
                            //category_1_part_1.Text = reader["SeriesTitle"].ToString();
                            //image_1_part_1.ImageUrl = reader["EpisodeImageShortPath"].ToString();
                            //image_1_part_1.PostBackUrl = "~/Account/Watch/" + reader["UniqueEpisodeID"].ToString();
                            //category_1_part_1.NavigateUrl = "~/Account/SeriesPage/" + reader["UniqueSeriesID"].ToString();
                            //}

                            //reader.NextResult();


                            //reader.Close();

                            //}//END of data reader

                            //}//END of first command

                            //Close Connection 

                            DataSet myDataSet = new DataSet();

                            SqlDataAdapter mySqlDataAdapter = new SqlDataAdapter(mySecondWatchVideoCommand);

                            mySqlDataAdapter.Fill(myDataSet);

                            Repeater_Column_1.DataSource = myDataSet;

                            Repeater_Column_1.DataBind();
                            


                            myWallConnection.Close();

                        }//END of using connection

                    }//END of try statement
                }
                catch (Exception except)
                {

                }


















            }
            else
            {
                Response.Redirect("../Login.aspx");
            }
        }


        protected void navigation_home_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("MyWall.aspx");
        }

        protected void navigation_categories_Click1(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Categories.aspx");
        }

        protected void navigation_profile_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("User/" + Session["New"].ToString() );
        }

        protected void navigation_notifications_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void navigation_upload_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Upload.aspx");
        }

        protected void navigation_settings_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Settings/Basic.aspx");
        }

        protected void navigation_logout_Click(object sender, ImageClickEventArgs e)
        {
            Session.Abandon();
            Session.RemoveAll();
            System.Web.Security.FormsAuthentication.SignOut();
            System.Web.Security.FormsAuthentication.RedirectToLoginPage();
        }

        





        


        
    }
}