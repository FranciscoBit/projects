﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;

namespace VideoClipsFinal
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
        }


        //====================================================================================================================
        //====================================================================================================================
        //=========================================== LOGIN BUTTOM ===========================================================
        //====================================================================================================================
        //====================================================================================================================
        protected void login_submit_Click(object sender, EventArgs e)
        {
            int result = 0;
            //Established a connection with the database
            using (SqlConnection myLoginConnection = new SqlConnection(@"Data Source=DRAGON-PC\SQLEXPRESS;Initial Catalog=VideoSiteDB;Integrated Security=True"))
            {

                //Create a command where you select the info from the Database
                using (SqlCommand myLoginCommand = new SqlCommand("SELECT UserID, UserPassword FROM Table_Membership WHERE UserName = @UserName", myLoginConnection))
                {
                    myLoginCommand.Parameters.AddWithValue("UserName", login_username.Text);
                    

                    //Open the connection
                    myLoginConnection.Open();

                    SqlDataReader myReader = myLoginCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        // myReader.Read() = we found user(s) with matching username!

                        int dbUserId = Convert.ToInt32(myReader["UserID"]);
                        string dbPassword = Convert.ToString(myReader["UserPassword"]);


                        bool isPasswordCorrect = VideoClipsFinal.App_Code.SuperSecurity.ValidatePassword(login_password.Text, dbPassword);

                        // if its correct password the result of the hash is the same as in the database
                        if (isPasswordCorrect == true)
                        {
                            // The password is correct
                            Session["New"] = login_username.Text;
                            result = dbUserId;
                        }
                    }
                    myLoginConnection.Close();



                }
                //Close Connection 
                myLoginConnection.Close();
            }

            if (result > 0)
            {
                Response.Redirect("Account/MyWall.aspx");
            }
            else
            {
                login_failed.Text = "Invalid";
            }






        }//End of login buttom






    }
}