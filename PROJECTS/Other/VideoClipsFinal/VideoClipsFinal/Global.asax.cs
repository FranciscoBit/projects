﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;

namespace VideoClipsFinal
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            //Code that runs on application startup
            RegisterRoutes(RouteTable.Routes);
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("RoutePublicUsers",
                "Account/User/{publicUserName}",
                "~/Account/User.aspx");

            routes.MapPageRoute("RouteWatchVideos",
               "Account/Watch/{publicVideoID}",
               "~/Account/Watch.aspx");


            routes.MapPageRoute("RouteSeriesMain",
               "Account/SeriesPage/{publicSeriesName}",
               "~/Account/SeriesPage.aspx");


        }





        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}