public class Instructor extends Person {

    private double salary;

    public Instructor(String n, int age, double s) {
        super(n, age);
        salary = s;
    }

    public String toString() {
        return "Instructor[super=" + super.toString() + " salary = " + salary + "]";
    }
}
