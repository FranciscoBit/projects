public class Undergraduate extends Person {

    private String major;

    public Undergraduate(String n, int age, String m) {
        super(n, age);
        major = m;
    }

    public String toString() {
        return "Undergraduate[super= " + super.toString() + "\nmajor=" + major + "]";
    }
}
