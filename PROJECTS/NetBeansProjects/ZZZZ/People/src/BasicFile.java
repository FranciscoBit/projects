
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.io.*;

public class BasicFile
{
    File f;
    File outputfile;
    File backupfile;
    JFileChooser choose;

    BasicFile
            ()
    {
      choose = new JFileChooser(".");
      //outputfile = new File("");
      backupfile = new File("");
    }

    public void selectFile() throws IOException
    {
        int select = choose.showOpenDialog(null);
        try
        {
            if (select != JFileChooser.APPROVE_OPTION) throw new IOException();
            f = choose.getSelectedFile();
            if (!f.exists()) throw new FileNotFoundException();
        }
        catch(FileNotFoundException e)
        {
            displayError(e.toString());
        }
        catch(IOException e)
        {
            displayError(e.toString());
        }
        fileWriter("\n"+"\nOpening file " + f.getName() +"\n" + f.toString() + "\nFile Size: " + getFileSize() +" Bytes" +"\n\n"
                    + getFiles() + "\nContents in the file " +getName() +":\n\n");
    }

    public void wordCount()
    {
     try
     {
       FileReader fileread = new FileReader(f);
       StreamTokenizer streamToken = new StreamTokenizer(fileread);
       streamToken.resetSyntax();
       streamToken.whitespaceChars(0, ' ');
       streamToken.wordChars('!','~');
       streamToken.eolIsSignificant(true);
       int word = 0;
       int line = 1;
       int character = 0;
       int total = 0;

       while(streamToken.nextToken()!=StreamTokenizer.TT_EOF)
       {
           switch(streamToken.ttype)
           {
           case StreamTokenizer.TT_NUMBER:
                word++;
           break;

           case StreamTokenizer.TT_WORD:
                character += streamToken.sval.length();
                word++;
           break;

           case StreamTokenizer.TT_EOL:
                line++;
           break;

           case StreamTokenizer.TT_EOF:
           break;
           }
        }

        fileread.close();
        total = word;

        fileWriter("\n\n" + getName() + " has " +  line + " lines " + total + " words " + character  + " characters");
        displayMessage("File Contents:\n" + getName() + " has " + line + " lines " + total + " words " + character  + " characters");
        }
        catch(IOException e)
        {
            displayError(e.toString());
        }
   }

    public void backUp(File thisFile) throws IOException
    {
        DataInputStream in = null;
        DataOutputStream out = null;
        DataInputStream inOutput = null;
        DataOutputStream outOutput = null;
    try
    {
        in = new DataInputStream(new FileInputStream(f));
        out = new DataOutputStream(new FileOutputStream(thisFile));
        inOutput = new DataInputStream(new FileInputStream(f));
        outOutput = new DataOutputStream(new FileOutputStream(outputfile));

        try
        {
            while (true)
            {
                byte data = in.readByte();
                out.writeByte(data);
                byte dataOutput = inOutput.readByte();
                outOutput.writeByte(dataOutput);
            }
        }
        catch(EOFException e)
        {
        }
    }
    finally
        {
            in.close();
            out.close();
            inOutput.close();
            outOutput.close();
            displayMessage("Backup created for " +getName());
        }
    }

    public void grep(String s) throws IOException
    {
        String str = s.toUpperCase();
        String currentLine = "";
        LineNumberReader lnRead;
        String text = "";

        lnRead = new LineNumberReader(new FileReader(f));

        while((currentLine=lnRead.readLine())!=null)
        {
            if(currentLine.toUpperCase().indexOf(str)>=0)
            {
                text=text+lnRead.getLineNumber()+": "+currentLine+"\n";
            }
        }

        fileWriter("\n\n" + "Word searched for: " + s +"\n" + text);
        scrollPaneMethod(text);
    }

    public void fileWriter(String outputText) throws IOException
    {    
       JOptionPane.showMessageDialog(null, "Select save destination for output file");
       int savefile = choose.showSaveDialog(null);
       outputfile = choose.getSelectedFile();
       FileWriter fileWriter = new FileWriter(outputText);
       fileWriter.write(outputText);
       fileWriter.close();
    }

    public void scrollPaneMethod(String searchedLines)
    {
        JTextArea textArea;
        textArea = new JTextArea(searchedLines, 15, 40);
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane = new JScrollPane(textArea);

        JOptionPane.showMessageDialog(null, scrollPane);
    }

    void displayError(String msg)
    {
        JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }

    void displayMessage(String msg)
    {
        JOptionPane.showMessageDialog(null, msg, "Information", JOptionPane.INFORMATION_MESSAGE);
    }
    String getName()
    {
        return f.getName();
    }

    public File getBackupfile(File backupfile) {
        return backupfile;
    }

    String getPath()
    {
        return f.getAbsolutePath();
    }

    long getFileSize()
    {
        return f.length();
    }

    boolean exists()
    {
        return f.exists();
    }

    File getFile()
    {
      return f;
    }

    String getFiles() throws IOException
    {
        String dirPath = "\n List of all directories in the path\n\n";
        String filesPath = "\n List of all files in the path\n\n";
        File[] fileList = f.getParentFile().listFiles();
        int counter = 0;
        while (counter < fileList.length)
        {
           if (fileList[counter].isDirectory() == true )
           {
                dirPath = dirPath + fileList[counter].getAbsolutePath() + "\n" ;
           }
           else if(fileList[counter].isFile() == true)
           {
                filesPath = filesPath + fileList[counter].getAbsolutePath() + "\n";
           }
           counter++;
        }

        return dirPath + filesPath;
    }

    public String toString()
    {
        return f.getName() +"\n" + f.getAbsolutePath() +"\n" + f.length() + " bytes";
    }

}




















import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.lang.NullPointerException;
import javax.swing.JFileChooser;



public class TestFile {

    public static void main(String[] arg) throws IOException
    {
        testMethod();
    }

    static void testMethod() throws IOException
    {
        JOptionPane.showMessageDialog(null, "Select your input file");
        BasicFile f = new BasicFile();
        f.selectFile();
        JOptionPane.showMessageDialog(null, "Select save destination for Backup file");
        
        
        
        
        try
        {   boolean x = true;
            File backUpFile = new File("[Backup]_"+ f.getName());
            f.backUp(backUpFile);
            while(x == true){
            String s = JOptionPane.showInputDialog("Make Selection \n 1.File Contents \n 2.Word Search \n 3. File Info \n 4. Exit");
            int i =  Integer.parseInt(s);
        
            switch(i)
            {
                case 1:            
                    f.wordCount();
                   break;
                    
                case 2:
                              String menu = displayQuestion("Do you want to do a word search?\n1. Yes\n2. No","Word Search menu");

                              if (menu.equals("1"))
                                 {
                                     f.grep(displayQuestion("Enter a word to search for: ", "Word Search"));

                                      displayQuestion("Done","Thank you");
                                  }
                              else if(menu.equals("2"))
                                  {
                                       displayQuestion("No word search","No search");
                                  }
                               else
                                    {
                                        displayQuestion("Sorry incorrect answer","incorrect answer");
                                    } 
                    break;
                    
                case 3:
                            displayInfo(f.toString()+ "\n" + f.getFiles(), "File");
                     break;
                    
                case 4:
                    x = false;
                    break;
            }

          }

 
        }
        catch(IOException e)
        {
            displayError(e.toString(),"Error IO Exception Occured");
        }
      //  catch(NullPointerException e)
        //{
          //  displayError(e.toString(),"Error Null Pointer Exception Occured");
       // }
    }

    static void displayError(String s, String title)
    {
        JTextArea jta = new JTextArea(s,20,50);
        JScrollPane jsp = new JScrollPane(jta);
        JOptionPane.showMessageDialog(null, jsp, title, JOptionPane.ERROR_MESSAGE);
    }
    static void displayInfo(String s, String title)
    {
        JTextArea jta = new JTextArea(s,20,50);
        JScrollPane jsp = new JScrollPane(jta);
        JOptionPane.showMessageDialog(null, jsp, title, JOptionPane.INFORMATION_MESSAGE);
    }
    static String displayQuestion(String s, String title)
    {
        JTextArea jta = new JTextArea(s,10,5);
        JScrollPane jsp = new JScrollPane(jta);
        return JOptionPane.showInputDialog(null, jsp, title, JOptionPane.QUESTION_MESSAGE);
    }
}

