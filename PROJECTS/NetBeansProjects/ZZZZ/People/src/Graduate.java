public class Graduate extends Undergraduate {

    private String thesis;

    public Graduate(String n, int age, String m, String thesis) {
        super(n, age, m);
        this.thesis = thesis;
    }

    public String toString() {
        return "Graduate[super = " + super.toString() + "\nThesis = " + thesis + "]";
    }
}
