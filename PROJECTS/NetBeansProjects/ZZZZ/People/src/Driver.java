
import java.util.ArrayList;

/**
 * This class tests primarily the Person, Undergraduate, Graduate, and
 * Instructor classes
 */
public class Driver {

    public static void main(String[] args) {
        ArrayList<Object> list = new ArrayList<Object>();
        list.add(new Person("Perry", 35));
        list.add(new Undergraduate("Sylvia", 24, "Computer Science"));
        list.add(new Graduate("James", 30, "Computer Science", "Graphical User Interface"));
        list.add(new Instructor("Edgar", 45, 65000));
        list.add(new Graduate("Barry", 32, "Computer Science", "Compiler Construction"));
        System.out.println(print(list));
    }

    static void print(ArrayList list) {
        String s = "Individual is a(n):\n";
        int length = list.size();
        for (int i = 0; i < length; i++) {
            Object o = list.get(i); // 5th Graduate
            
            if (o instanceof Graduate) {
                System.out.println(s + (Graduate) o.toString() + "\n");
            } else if (o instanceof Undergraduate) {
                System.out.println(s + (Undergraduate) o + "\n");
            } else if (o instanceof Instructor) {
                System.out.println(s + (Instructor) o + "\n");
            } else if (o instanceof Person) {
                System.out.println(s + (Person) o + "\n");
            } else {
                System.out.println((Object) o + " ...Unknown individual\n");
            }
        }
    }
}
}
