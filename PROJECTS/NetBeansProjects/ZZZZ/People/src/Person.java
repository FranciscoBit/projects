public class Person {

    private String name;
    private int age;

    public Person(String n, int a) {
        name = n;
        age = a;
    }

    public String toString() {
        return "Person[name = " + name + "\nage = " + age + "]";
    }
}
