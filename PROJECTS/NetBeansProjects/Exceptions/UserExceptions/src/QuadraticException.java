class QuadraticException extends Throwable
{
	double a, b, discriminant;

	QuadraticException(String message)
	{
		super(message);
	}
	QuadraticException(String message, double a, double b, double d)
	{
		super(message);
		this.a = a;
		this.b = b;
		discriminant = d;
	}
	public String imaginaryRoots()
	{
		double x1 = (-b + Math.sqrt(-discriminant))/(2 * a);
		double x2 = (-b - Math.sqrt(-discriminant))/(2 * a);
		return "Imaginary roots: " + x1 + "i" + " and " + x2 + "i";
	}
}