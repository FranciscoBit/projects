class TestQuadratic
{
	public static void main(String[] arg)
	{
		double arr[][] = {{1, 0, -9}, {1, 0, 9}, {2, -8, 8}, {0, 4, 12}, {1, -1, -6}};

		for (int i = 0; i < arr.length; i++)
		{
			String s = "a = " + arr[i][0] + "  b = " + arr[i][1] + "  c = " + arr[i][2] + "\n";
			Quadratic q = new Quadratic(arr[i]);

			if (!q.isZero())
			{
				q.calculateDiscriminant();
				if (q.singleRoot())
					q.calculateSingleRoot();
			}
			System.out.println( s + q.toString() + "\n");
		}
	}
}