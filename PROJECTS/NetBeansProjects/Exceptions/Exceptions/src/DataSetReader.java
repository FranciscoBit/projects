import java.io.File ;
import java.io.IOException;
import java.util.Scanner;

/**
   Reads a data set from a file. The first line of the file must contain an 
   int, which is the number of additional lines in the file.  Each additional 
   line must contain a double value.
*/
public class DataSetReader
{
   /**
      Reads a data set.
      @param filename the name of the file holding the data
      @return the data in the file
   */
   public double[] readFile(String filename) 
         throws IOException, BadDataException
   {
      Scanner in = null ;
      try 
      {
         in = new Scanner(new File(filename)) ;
         readData(in);
      }
      finally
      {
         if (in != null)	// if file was opened...
           in.close();		// ...close it
      }
      return data;
   }

   /**
      Reads all data.
      @param in the scanner that scans the data
   */
   private void readData(Scanner in) throws BadDataException
   {
      if (!in.hasNextInt()) 
         throw new BadDataException("Length expected");
      int numberOfValues = in.nextInt();
      data = new double[numberOfValues];

      for (int i = 0; i < numberOfValues; i++)
         readValue(in, i);

      if (in.hasNext()) 
         throw new BadDataException("End of file expected");
   }

   /**
      Reads one data value.
      @param in the scanner that scans the data
      @param i the position of the value to read
   */
   private void readValue(Scanner in, int i) throws BadDataException
   {
      if (!in.hasNextDouble()) 
         throw new BadDataException("Data value expected");
      data[i] = in.nextDouble();      
   }

   private double[] data ;		// array instance variable
}