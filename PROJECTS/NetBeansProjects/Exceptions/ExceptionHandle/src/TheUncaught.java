// File: TheUncaught.java

// If a RuntimeException gets all the way out to main() without
// being caught, method printStackTrace() is called for the exception
// and the program terminates.
  
// RuntimeException is the superclass of standard Java exceptions
// such as NullPointerException, ArithmeticException, and 
// IndexOutOfBoundsException.  

// Since the standard Java exceptions are "unchecked" exceptions, no 
// exception specification (i.e., "throw list") is required.

public class TheUncaught 
{
  public static void method3() 
  {
    throw new RuntimeException("Thrown in method3()") ;
  }
  
  public static void method2() 
  {
    method3() ;
  }
  public static void method1() 
  {
    method2() ;
  }
  
  public static void main(String[] args) 
  {
    method1() ;
  }
} 

/*  program output

Exception in thread "main" java.lang.RuntimeException: Thrown in method3()
    at TheUncaught.method3(TheUncaught.java:18)
    at TheUncaught.method2(TheUncaught.java:23)
    at TheUncaught.method1(TheUncaught.java:27)
    at TheUncaught.main(TheUncaught.java:32)

*/