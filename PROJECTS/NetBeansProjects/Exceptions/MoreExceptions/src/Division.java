public class Division
{
	int arr[];

	Division(int a[])
	{
		arr = a;
	}
  
  public void divide(int i)
  {
    try
    {
        //int j = i + 1;
        System.out.println("  i + 1 = " + (i+1) + "  result = " + arr[i]/arr[i+1]);
        //int x = arr[i]/arr[j];
        System.out.println("No exception occured\n"); //, result = " + (i+1) + "\n" );
    }
    catch(ArithmeticException e)
    {
		System.out.print("index i+ 1 = " + (i+1) + " arr[" + (i+1) + "] = " + arr[i+1] + "\n");
		e.printStackTrace();
		System.out.println();
    }
    catch(ArrayIndexOutOfBoundsException e)
    {
		System.out.print("  i + 1 = " + (i+1) + "\n");
		e.printStackTrace();
		System.out.println();
    }
  }
}