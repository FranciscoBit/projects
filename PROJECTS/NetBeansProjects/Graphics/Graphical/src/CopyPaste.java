import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
class Testing
{
  public void buildGUI()
  {
    final JTextArea ta = new JTextArea(10,20);
    JButton btn = new JButton("Copy to clipboard");
    JButton pastebtn = new JButton("paste to clipboard");
    JFrame f = new JFrame();
    f.getContentPane().add(new JScrollPane(ta),BorderLayout.CENTER);
    f.getContentPane().add(btn,BorderLayout.SOUTH);
    f.getContentPane().add(pastebtn,BorderLayout.NORTH);
    f.pack();
    f.setLocationRelativeTo(null);
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.setVisible(true);
    
    
    btn.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent ae){
        int pos = ta.getCaretPosition();//<---optional
        ta.getSelectedText();
        ta.copy();
        ta.setCaretPosition(pos);//<---optional
        ta.requestFocusInWindow();//<---optional
      }
    });
    
     pastebtn.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent ae){
        int pos = ta.getCaretPosition();//<---optional
        //ta.getSelectedText();
        ta.paste();
        ta.setCaretPosition(pos);//<---optional
        ta.requestFocusInWindow();//<---optional
      }
    });
    
    
    
    
  }
  public static void main(String[] args)
  {
    SwingUtilities.invokeLater(new Runnable(){
      public void run(){
        new Testing().buildGUI();
      }
    });
  }
}