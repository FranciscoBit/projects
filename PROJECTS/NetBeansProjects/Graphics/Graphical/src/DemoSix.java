/*
Adding action event to menuitems.
*/

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JToggleButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
public class DemoSix extends JFrame
{
    JMenuBar menubar;
    JMenu f;
    JMenuItem mi;
    JCheckBoxMenuItem cb;
    JToggleButton rb;

    public DemoSix(String title)
    {
            super(title);

            // The menubar is invisible
            menubar = new JMenuBar();
            setJMenuBar(menubar);

            buildMenu();
    }

    void buildMenu()
    {
        for (int i = 0; i < Constants.MENU.length; i++)
            {	//Build and add each menu choice onto the menubar
                f = new JMenu(Constants.MENU[i]);
                switch(i)
                {
                    case 0:
                        for (int j = 0; j < Constants.ITEMS.length; j++)
                            if (Constants.ITEMS[j].equals("-"))
                                    f.addSeparator();
                            else
                            {
                                f.add(mi = new JMenuItem(Constants.ITEMS[j]));

                               /* Attach action listener object to menuitem
                                *1. Register the menuitem using the method "addActionListener"
                                *2. Create the actionlistener object using the construct "new ActionListener"
                                *3. Implement the single abstract method "actionPerformed" in the interface "ActionListener".
                                *   Notice that its argument is an "ActionEvent" object. At this point we are interested in identifying
                                *   the menuitem that was clicked. The method "getActionCommand" tells us.
                                * The entire implementation is shown below
                                */

                                mi.addActionListener(new Actions());
                             }
                    break;
                    case 1:
                            // Same pattern
                    break;
                }
                menubar.add(f);
            }
    }
    
    
    
    /**
         * // Add File Option
        myJMenu = new JMenu("File");
        
        myJMenuItem = new JMenuItem("New"); 
        myJMenu.add(myJMenuItem);
        
        myJMenuItem = new JMenuItem("List Files");
        myJMenu.add(myJMenuItem);
        
        myJMenuItem = new JMenuItem("Save as");
        myJMenu.add(myJMenuItem);
        
        myJMenuItem = new JMenuItem("Close");   
        myJMenu.add(myJMenuItem);
        
        myJMenuBar.add(myJMenu);        //End of File Option

        
        
        
        
        // Add Tool Option
        myJMenu = new JMenu("Tool");
        myJMenuBar.add(myJMenu);

        // Add Help Option
        myJMenu = new JMenu("Help");
        myJMenuBar.add(myJMenu);
         */
    
    
}