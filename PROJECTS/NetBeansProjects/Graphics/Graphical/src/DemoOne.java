/*
 Reserving/setting up the menubar.
*/

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

public class DemoOne extends JFrame
{
	JMenuBar menubar;
	JMenu mi;

	public DemoOne(String title)
	{
		super(title);

		menubar = new JMenuBar();
		setJMenuBar(menubar);

		buildMenu();
   	}
   	void buildMenu()
   	{
   		// Add one menu choice
   		mi = new JMenu("File");
      	menubar.add(mi);

       // Add a second menu choice
        mi = new JMenu("Tool");
      	menubar.add(mi);


       // Add a third menu choice
        mi = new JMenu("ToolX");
      	menubar.add(mi);
   	}
}