/*
 Reserving/setting up the menubar.
*/

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class DemoFive extends JFrame
{
	JMenuBar menubar;
	JMenu f;
	JMenuItem mi;

	public DemoFive(String title)
	{
		super(title);

		// The set up the menubar. R
		menubar = new JMenuBar();
		setJMenuBar(menubar);

		buildMenu();
   	}
   	void buildMenu()
   	{
   		f = new JMenu("File");

   		mi = new JMenuItem("Open .........It .......");


   	   	mi.addActionListener(new ActionListener()
	   		{
	   			public void actionPerformed(ActionEvent e)
	   			{
	   				 System.out.println("The string is --->: " + e.getActionCommand());

	   			}
	   		}
   		);

   		f.add(mi);

   		menubar.add(f);
   	}
}