/* 
Adding action event to menuitems.
*/

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JToggleButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
public class DemoFour extends JFrame
{
    JMenuBar menubar;
    JMenu f;
    JMenuItem mi;
    JCheckBoxMenuItem cb;
    JToggleButton rb;

    public DemoFour(String title)  
    {
            super(title);

            // The menubar is invisible
            menubar = new JMenuBar();
            setJMenuBar(menubar);

            buildMenu();
    }

    void buildMenu()
    {
        for (int i = 0; i < Constants.MENU.length; i++) 
            {	//Build and add each menu choice onto the menubar
                f = new JMenu(Constants.MENU[i]);
                switch(i)
                {
                    case 0:
                        for (int j = 0; j < Constants.ITEMS.length; j++)
                            if (Constants.ITEMS[j].equals("-"))
                                    f.addSeparator();
                            else
                            {
                                f.add(mi = new JMenuItem(Constants.ITEMS[j]));
                               
                                mi.addActionListener(new Action());
                                
                                /*Listener()
                                {
                                    public void actionPerformed(ActionEvent e)
                                    {
                                        System.out.println("The string is " + e.getActionCommand());
                                    }
                                });
                            */
                            }
                    break;
                    case 1:
                            // Same pattern
                    break;
                }
                menubar.add(f);
            }
    }
}