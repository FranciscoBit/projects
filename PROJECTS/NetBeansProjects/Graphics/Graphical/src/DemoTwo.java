/*
Demo 6 This section actually builds the menu choices onto the menubar.
*/

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;

public class DemoTwo extends JFrame
{
	JMenuBar menubar;
	JMenu f;

	public DemoTwo(String title)
	{
		super(title);

		// The menubar is invisible
		menubar = new JMenuBar();
		setJMenuBar(menubar);

		buildMenu();
   	}

   	void buildMenu()
   	{
   		for (int i = 0; i < Constants.MENU.length; i++)
   		{
   			f = new JMenu( Constants.MENU[i] );
   			menubar.add(f);
   		}
   	}
 }