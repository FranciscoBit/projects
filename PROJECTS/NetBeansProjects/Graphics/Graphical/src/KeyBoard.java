import java.awt.event.*;
import javax.swing.*;

public class KeyBoard extends JFrame implements KeyListener
{
	JTextArea txt;

	public KeyBoard()
	{
		super("My Text Editor");
		txt = new JTextArea("Key Code:\n", 20, 25);
		txt.addKeyListener(this);//Listens for key inputs in the text field
		txt.setEditable(true);
		add(txt);
		setSize(300,300);//set the screen size
		setVisible(true);//show the window on screen.
	}
	//Called when the key is pressed down.

	public void keyPressed(KeyEvent e)
	{
		System.out.println("Key Pressed!!!");
	}
	//Called when the key is released

	public void keyReleased(KeyEvent e)
	{
	//	System.out.println("Key Released!!!");
//		txt.setText("Key Code:" + e.getKeyCode());//displays the key code in the text box
	}
	//Called when a key is typed

	public void keyTyped(KeyEvent e)
	{
//		txt.setTex
//		System.out.println("Key Code:" + (int)e.getKeyChar() + "           " + e.getKeyChar());

	}
}