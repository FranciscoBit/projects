import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Container;

public class DisplayText
{
   private static JTextArea text;

   public DisplayText( String title, String info)
   {
		DemoSix f = new DemoSix(title);
		Container c = f.getContentPane();

		text = new JTextArea(info);

		JScrollPane sp = new JScrollPane(text);
		c.add( sp );

      	f.setBounds(100,200, 500, 400 );
	   	f.setVisible(true);
   }
}