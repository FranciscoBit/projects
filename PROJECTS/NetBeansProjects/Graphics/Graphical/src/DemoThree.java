/*
Demo 4 This section actually builds the menu choices onto the menubar.
*/

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class DemoThree extends JFrame
{
	JMenuBar menubar;
	JMenu f;
	JMenuItem mi;
	public DemoThree(String title)
	{
		super(title);

		// The menubar is invisible
		menubar = new JMenuBar();
		setJMenuBar(menubar);

		buildMenu();
   	}

   	void buildMenu()
   	{
            f = new JMenu("File");//Constants.MENU[i] );

            mi = new JMenuItem("Open");
            f.add(mi);
            menubar.add(f);
            mi = new JMenuItem("Save");
            f.add(mi);
            mi = new JMenuItem("Close");
            f.add(mi);
            menubar.add(f);

   	}
 }