/**
 * @author Francisco Munoz
 * @ID: 2419358
 * @Class: COP3337
 * Professor: Joslyn Smith
 * Term: Spring 2015 
 */
public class Transpose extends Cipher {

    Transpose(String s) {
        super(s);
    }

    public String encode(String word) {
        StringBuffer result = new StringBuffer(word);
        result.reverse();
        return result.toString();
    }

    public String decode(String word) {
        // Complete this method so that it reverses the encoded string;
        // DONE
        StringBuffer result = new StringBuffer(word);
        result.reverse();
        return result.toString();
        
    }
}
