/**
 * @author Francisco Munoz
 * @ID: 2419358
 * @Class: COP3337
 * Professor: Joslyn Smith
 * Term: Spring 2015 
 */
public class Reverser extends Transpose {

    public Reverser(String s) {
        // Complete the constructor
        // DONE
        super(s);
    }

    public String reverseText(String word) {
        // Complete this method so that it reverses the original string
        // DONE
        
        
        String[] splitString = word.split(" ");
        
        int wordLenght = splitString.length;
        String newWord = "";
        
        // We use the for loop to get the the last word first, and first word last
        for (int i = (wordLenght - 1); i > (-1); i--)
        {
            newWord = newWord + " " + splitString[i];
        }
        
        return newWord;
    }

    public String decode(String word) {
        // Complete this method so that it reverses the reversed string
        // DONE
        StringBuffer result = new StringBuffer(word);
        result.reverse();
        return result.toString();
    }
}
