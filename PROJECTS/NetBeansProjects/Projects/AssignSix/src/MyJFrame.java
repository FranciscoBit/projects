/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class MyJFrame extends JFrame {
    
    JMenuBar myJMenuBar;
    JMenu myJMenu;
    JMenuItem myJMenuItem;
    JMenu mySubJMenu;
    
    JButton myJButton;
    Container myButtomContainer;
    Container myRegularContainer;

    DisplayText myDisplayText;
    
    boolean isOn;
    
    MyJFrame(String title)
    {
        super(title);
        
        
        myJMenuBar = new JMenuBar();
        setJMenuBar(myJMenuBar);
        //setLayout(null);
        setSize(800,600);
        getContentPane().setBackground(Color.ORANGE);
        //getContentPane().setForeground(Color.RED);

        buildMenu();
        buildButtom();
 
        
    }
    
    void buildMenu() 
    {
        

        // ADD MENU OPTIONS: FILE, TOOL, HELP
        for(int i = 0; i < Constants.MAIN_MENU_OPTIONS.length; i++)
        {
            
            myJMenu = new JMenu(Constants.MAIN_MENU_OPTIONS[i]);
            
            //------------------------------------------------------------------
            // IF MAIN MENU - FILE
            if(i == 0)
            {
                // ADD: FILE ITEMS (NEW, LIST FILES, SAVE AS, CLOSE)
                for (int j = 0; j < Constants.MAIN_MENU_ITEMS_FILE.length; j++)
                {
                    
                    // IF MAIN MENU - FILE - NEW
                    if( (j == 0)
                            || (j == (Constants.MAIN_MENU_ITEMS_FILE.length - 1)) )
                    {
                        myJMenuItem = new JMenuItem(Constants.MAIN_MENU_ITEMS_FILE[j]);
                        myJMenu.add(myJMenuItem);

                        // IF MAIN MENU - FILE - NEW IS CLICK
                        if(j == 0)
                        {
                            myJMenuItem.addActionListener(new ActionListener() 
                            {
                                public void actionPerformed(ActionEvent e) {
                                    DisplayText myDisplayText = new DisplayText(
                                            Constants.MAIN_MENU_ITEMS_FILE_NEW_FILENAME, 
                                            Constants.MAIN_MENU_ITEMS_FILE_NEW_FILECONTENTS);

                                }
                            });

                        }
                        
                        // IF MANIN MENU - FILE - CLOSE IS CLICK
                        else if (j == 3)
                        {
                            myJMenuItem.addActionListener(new ActionListener() 
                            {
                                public void actionPerformed(ActionEvent e) 
                                {
                                    JOptionPane.showMessageDialog(null, "The window is closing.", "Information", JOptionPane.INFORMATION_MESSAGE);
                                    System.exit(0);
                                }
                            });

                        }
                        
                        
                    }
                    
                    else
                    {
                        myJMenuItem = new JMenuItem(Constants.MAIN_MENU_ITEMS_FILE[j]);
                        myJMenu.add(myJMenuItem);
                        myJMenu.addSeparator();
                        
                        // IF MAIN MENU - FILE - LIST FILES IS CLICK
                        if(j == 1)
                        {
                            
                            myJMenuItem.addActionListener(new ActionListener() 
                            {
                                public void actionPerformed(ActionEvent e) {
                                    
                                    try
                                    {
                                        BasicFile f = new BasicFile();
                                        new DisplayText(f.getName(), f.readFile());
                                        
                                    }
                                    catch(IOException err)
                                    {
                                        JOptionPane.showMessageDialog(null, "ERROR", "Information", JOptionPane.ERROR_MESSAGE);
                                    }
                                    

                                }
                            });

                            
                        }
                          
                        
                    }
                    
                    
                    
                } // END OF LOOP: ADD: FILE (NEW, LIST FILES, SAVE AS, CLOSE)
                 
            } // END OF IF: MAIN MENU - FILE
            
            // IF MAIN MENU - TOOL
            else if (i == 1)
            {
                // ADD: TOOL ITEMS (SORT, SEARCH, EDIT)
                for (int j = 0; j < Constants.MAIN_MENU_ITEMS_TOOL.length; j++)
                {
                    myJMenuItem = new JMenuItem(Constants.MAIN_MENU_ITEMS_TOOL[j]);
                    
                    // IF MAIN MENU - TOOL - EDIT
                    if (Constants.MAIN_MENU_ITEMS_TOOL[j].equals("Edit")) 
                    {
                        
                        mySubJMenu = new JMenu(Constants.MAIN_MENU_ITEMS_TOOL[j].toString());   // This gives "Edit"
                        
                        // ADD: TOOL - EDIT ITEMS (COPY PASTE)
                        for (int k = 0; k < Constants.MAIN_MENU_ITEMS_TOOL_EDIT_ITEMS.length; k++)
                        {
                            
                            myJMenuItem = new JMenuItem(Constants.MAIN_MENU_ITEMS_TOOL_EDIT_ITEMS[k]);  // Sub menu items
                            mySubJMenu.add(myJMenuItem);
                            myJMenu.add(mySubJMenu);
                            
                            // IF TOOL - EDIT - COPY IS CLICK
                            if(Constants.MAIN_MENU_ITEMS_TOOL_EDIT_ITEMS[k].equals("Copy"))
                            {
                                
                                myJMenuItem.addActionListener(new ActionListener() 
                                {
                                    public void actionPerformed(ActionEvent e) 
                                    {
                                       myDisplayText.selectText();
                                        
                                    }
                                });
                                
                                
                                
                                
                            }
                            // IF TOOL - EDIT -PASTE IS CLICK
                            else if ( Constants.MAIN_MENU_ITEMS_TOOL_EDIT_ITEMS[k].equals("Paste"))
                            {
                                myJMenuItem.addActionListener(new ActionListener() 
                                {
                                    public void actionPerformed(ActionEvent e) 
                                    {
                                        myDisplayText.insertText();
                                    }
                                });
                            }
                            
                            
                        }
                        
                    } 
                    // ELSE KEEP ADDING ITEMS
                    else 
                    {
                        myJMenu.add(myJMenuItem);
                    }
                      
                }
            } // END OF IF:  MAIN MENU - TOOL

            
            //------------------------------------------------------------------
            
            myJMenuBar.add(myJMenu);        //Main Menu Options
        } // END OF: ADD MENU OPTIONS: FILE, TOOL, HELP
        

    } // END OF BUILD MENU
    
    
    
    void buildButtom() 
    {
        
        myButtomContainer = getContentPane();
        
        myJButton = new JButton();
        myButtomContainer.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        
        for(int i = 0; i < Constants.MAIN_BUTTON_ITEMS.length; i++)
        {
            // Drawing Option
            if (i == 0)
            {
                
                myJButton = new JButton(Constants.MAIN_BUTTON_ITEMS[i].toString());
                c.gridx = 0;
                c.gridy = 0;
                c.insets = new Insets(0, 40, 0, 150);
                myButtomContainer.add(myJButton, c);

                
                myJButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        MyGraphics f = new MyGraphics();
                        
                    }
                });
                

                
            }
            
            // Close Option
            else if(i == 1)
            {
                
                myJButton = new JButton(Constants.MAIN_BUTTON_ITEMS[i].toString());
                c.gridx = 2;
                c.gridy = 0;
                myButtomContainer.add(myJButton, c);
                
                myJButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                       
                        //------------------------------------------------------
                        JOptionPane.showMessageDialog(null, "The window is closing.", "Information", JOptionPane.INFORMATION_MESSAGE);
                        System.exit(0);
                        
                        //------------------------------------------------------
                    }
                });
                
            }
            
            // Image Option
            else if(i == 2)
            {
                
                ImageIcon icon = new ImageIcon("iron.png");
                myJButton = new JButton(icon);
                c.gridx = 0;
                c.gridy = 1;
                c.insets = new Insets(50, 225, 50, 150);
                myButtomContainer.add(myJButton, c);
                
            }
            
            // Browser Option
            else if (i == 3)
            {
                
                myJButton = new JButton(Constants.MAIN_BUTTON_ITEMS[i].toString());
                c.fill = GridBagConstraints.HORIZONTAL;
                c.gridx = 0;
                c.gridy = 2;
                c.gridwidth = 3;
                c.insets = new Insets(0, 125, 0, 125);
                myButtomContainer.add(myJButton, c);
                
                
                myJButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Browser f = new Browser();
                        
                    }
                });
                
            }

            
        } // END of for loop
        
    }
       
    
    
}
