
/**
 * @author Francisco Munoz
 * ID: 2419358 
 * Programming II
 */

import javax.swing.JFileChooser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.swing.JOptionPane;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class BasicFile {

    File f;
    File CreateFile;


    public BasicFile() {

        JFileChooser choose = new JFileChooser(".");
        int status = choose.showOpenDialog(null);

        try {
            if (status != JFileChooser.APPROVE_OPTION) {
                throw new IOException();
            }
            f = choose.getSelectedFile();
            if (!f.exists()) {
                throw new FileNotFoundException();
            }

        } catch (FileNotFoundException e) {
            display(e.toString(), "File not found ....");
        } catch (IOException e) {
            display(e.toString(), "Approve option was not selected");
        }
    }

    void display(String msg, String s) {
        JOptionPane.showMessageDialog(null, msg, s, JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     * Gets size of file
     * @return File Size
     */
    public long getFileSize() {
        return this.f.length();
    }

    /**
     * Gets path of file
     * @return File Path
     */
    public String getPath() {
        return this.f.getAbsolutePath();
    }
    
    /**
     * Gets name of file
     * @return File Name
     */
    public String getName() {
        return this.f.getName();
    }

    /**
     * Gets the following:
     *  - Number of Lines
     *  - Number of Words
     *  - Number of Numbers
     *  - Number of Characters (Approx)
     * @return File Contents
     */
    public String getContents() {
        String myContents = "";

        int numberLines = 1, numberWords = 0, numberNumbers = 0;

        int numberCharTotal = 0, numberCharWords = 0;

        String strCharNumbers = "";
        int intCharNumbers = 0;

        int numberCharWeird = 0;

        //=====================================================================
        try {

            StreamTokenizer myToken = new StreamTokenizer(new FileReader(f));
            myToken.eolIsSignificant(true);
            myToken.wordChars('0', '9');

            int tokenValue;
            while ((tokenValue = myToken.nextToken()) != StreamTokenizer.TT_EOF) {

                switch (tokenValue) {
                    case StreamTokenizer.TT_WORD:
                        numberCharWords = numberCharWords + myToken.sval.length();
                        numberWords++;
                        break;
                    case StreamTokenizer.TT_NUMBER:
                        // So we get the regular int value (we cant use - length)
                        strCharNumbers = strCharNumbers + (int) myToken.nval;
                        // Then we get the lenght of the numbers
                        intCharNumbers = intCharNumbers + strCharNumbers.length();
                        // We count the number of instances a token of number appears
                        numberNumbers++;
                        break;
                    case StreamTokenizer.TT_EOL:
                        numberLines++;
                        break;
                    default:
                        numberCharWeird++;
                        break;
                }

            }

            // Now we add the total number of characters found
            numberCharTotal = intCharNumbers + numberCharWords + numberCharWeird;

            myContents = getName() + " has "
                    + numberLines + " lines, "
                    + (numberWords + numberNumbers) + " words, " // total words = numbers + strings
                    + numberNumbers + " number(s), "
                    + numberCharTotal + " characters (approximately)";

        } catch (IOException e) {
            display(e.toString(), "Approve option was not selected");
        }

        return myContents;
    }

    /**
     * Backs up the file by using DataInputStream, DataOutputStream
     * Reads the bytes of the file
     * @return Successful File Backup
     */
    public String Backup() {

        String stringS = "";
        
        DataInputStream inData = null;
        DataOutputStream outData = null;
        
        try {
            
            String s = JOptionPane.showInputDialog("Please enter a name for the backup file: "); 
            inData = new DataInputStream(new FileInputStream(f));
            outData = new DataOutputStream(new FileOutputStream(s + ".txt"));
            
            byte counter;
            while ((counter = inData.readByte()) != -1)
            {
                outData.writeByte(counter);
            }
            
            inData.close();
            outData.close();

            stringS = "Sucessfull Backup!";

        } catch (IOException e) {
            display(e.toString(), "Approve option was not selected");
        }

        return stringS;

    }

    /**
     * Gets the file search output by doing the following:
     *  - 1.) Input dialog is shown and tells user to enter word
     *  - 2.) The we read the line until recursion finishes
     *  - 3.) If search found something, then show output. 
     *  - 4.) Else nothing was found
     * @return File Search Output
     */
    public String FileSearch() {

        String stringS = "";
        LineNumberReader lineRead = null;
        
        try {
            String s = JOptionPane.showInputDialog("Please enter a word to search for: ");
            String str = s.toUpperCase();
            String currentLine = "";

            String output = "";
            lineRead = new LineNumberReader(new FileReader(f));

            while ((currentLine = lineRead.readLine()) != null) {
                if (currentLine.toUpperCase().indexOf(str) >= 0) {
                    output = output + "Line " + lineRead.getLineNumber() + ": " + currentLine + "\r\n";
                }
                else
                {
                    output = "Nothing was found";
                }
            }

            stringS = "Word searched for: " + s + "\r\n" +
                      "===============================\r\n"+ output;

        } catch (IOException e) {
            display(e.toString(), "Approve option was not selected");
        }

        return stringS;

    }

    /**
     * Get the confirmation that file creation was successful
     * We will convert the string to InputStream 
     * We saved all information of previous steps:
     *  - File Path
     *  - File Size
     *  - File Contents(Words, Numbers, Lines, Characters)
     *  - Search Info
     * @return Successful File Creation
     */
    public String CreateFile() {

        String filename = "Output.txt";
        String s = "";
        String str = "";

        //FileOutputStream fos = null;
        //DataOutputStream dos = null;
        
        DataInputStream inData = null;
        DataOutputStream outData = null;
        InputStream inputStream = null;

        try {
            s = "File Name: " + getName() + "\r\n"
                    + "File Path: " + getPath() + "\r\n"
                    + "File Size: " + getFileSize() + "\r\n"
                    + "File Contents: " + getContents() + "\r\n"
                    + "=================================" + "\r\n"
                    + "Search Info: " + "\r\n"
                    + FileSearch();

            // Here we convert string to inputStream
            inputStream = new ByteArrayInputStream(s.getBytes(Charset.forName("UTF-8")));
            inData = new DataInputStream(inputStream);
            outData = new DataOutputStream(new FileOutputStream(filename + ".txt"));
            
            byte counter;
            while ((counter = inData.readByte()) != -1)
            {
                outData.writeByte(counter);
            }
            
            inData.close();
            outData.close();
            
            

        } catch (IOException e) {
            display(e.toString(), "Approve option was not selected");
        }

        str = "Successfully Created (A, B, C)";

        return str;

    }
    
    /**
     * NOT WORKING(Not sure how to make it work) Option(#8)
     * Gets files and folders in the path of the file
     * @return Folders and Directories
     */
    public String getDirectories(String path) {
        String s = "";
        
        try
        {
            
            File root = new File( path );
            File[] list = root.listFiles();

            for ( File f : list ) {
                if ( f.isDirectory() ) {
                    getDirectories( f.getAbsolutePath() );
                    s = s + "Dir:" + f.getAbsoluteFile() + "\r\n";
                }
                else {
                    s = s + "File:" + f.getAbsoluteFile() + "\r\n";
                }
            }
            
        }
        catch (NullPointerException e) {
            display(e.toString(), "ERROR");
        }
        

        return s;
    }
    
    
    String readFile() throws IOException {
        String s = "";
        String tmp = "";
        BufferedReader bf = new BufferedReader(new FileReader(f));

        while ((tmp = bf.readLine()) != null) {
            s = s + tmp + "\n";
        }

        return s;
    }

}
