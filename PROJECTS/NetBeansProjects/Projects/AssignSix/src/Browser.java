/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import java.awt.Container;
import javax.swing.JScrollPane;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

public class Browser implements HyperlinkListener {

    private JFrame myJFrame;
    private JTextField myURL;
    private JEditorPane myPageContents;

    public Browser() 
    {
        myJFrame = new JFrame("My Browser");
        Container c = myJFrame.getContentPane();

        //--------------------- TEXT FILED URL ---------------------------------
        myURL = new JTextField("http://users.cis.fiu.edu/~smithjo/");      
        myURL.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
                getThePage(e.getActionCommand());
            }
        });
        c.add(myURL, BorderLayout.NORTH);
        
        //--------------------- EDITOR PANE CONTENTS ---------------------------
        myPageContents = new JEditorPane();
        myPageContents.setEditable(false);
        myPageContents.addHyperlinkListener(this);
        c.add(new JScrollPane(myPageContents), BorderLayout.CENTER);
        
        //----------------------------------------------------------------------
        myJFrame.setBounds(100, 200, 500, 400);        
        myJFrame.setVisible(true);
        
    }
    
    
    
    public void getThePage (String pageUrl)
    {
        myJFrame.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        
         try 
         {  
            // Load and display specified page.
            myPageContents.setPage(pageUrl);
            myURL.setText(pageUrl);

         }
         catch(Exception e)
         {
             JOptionPane.showMessageDialog(null, "Error Cannot Access Specfied URL", "Bad URL", JOptionPane.ERROR_MESSAGE);
         }
        
         myJFrame.setCursor(Cursor.getDefaultCursor());
        
    }
    
    
    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) 
    {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            getThePage(e.getURL().toString());
        }
  
    }
    


}
