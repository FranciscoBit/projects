/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyGraphics extends JFrame implements MouseListener, MouseMotionListener {
    
    
    //Define variables
    private Polygon myPolygonStar;	//Polygon class Star
    private Polygon myPolygonHouse;	//Polygon class House
    private Rectangle myRectangleDoor;
    
    int x1, y1;
    
    public MyGraphics() {
        
        //Set JFrame title
        setTitle("My Cool Graphics!");
        
        addMouseListener(this);
        addMouseMotionListener(this);
        //JFrame's size
        setSize(500, 500);
        
        createStar();
        createHouse();
        createDoor();
        
        //Make the JFrame visible on screen
        setVisible(true);
    }
    
    
    
    public void createStar()
    {
        //x coordinates for star shape polygon
        int[] X_COOR_STAR = { 55, 67, 109, 73, 83, 55, 27, 37, 1, 43 };

        //y coordinates for star shape polygon
        int[] Y_COOR_STAR = { 0, 36, 36, 54, 96, 72, 96, 54, 36, 36 };
        
        //Create a star shape polygon
        myPolygonStar = new Polygon(X_COOR_STAR, Y_COOR_STAR, X_COOR_STAR.length);
        myPolygonStar.translate(20, 100);
        
    }
    
    
    public void createHouse()
    {
        //x coordinates for a blue polygon
        int[] X_COOR_HOUSE = { 56, 109, 109, 109, 109, 56, 3, 3, 3, 3 };

        //y coordinates for a blue polygon
        int[] Y_COOR_HOUSE = { 0, 36, 36, 72, 108, 108, 108, 72, 18, 36 };
        
        //Create a star shape polygon
        myPolygonHouse = new Polygon(X_COOR_HOUSE, Y_COOR_HOUSE, X_COOR_HOUSE.length);
        myPolygonHouse.translate(50, 350);

    }
    
    
    public void createDoor ()
    {
        int xPos = 50, yPos = 108, width = 30 , height = 30;
        myRectangleDoor = new Rectangle(xPos,yPos,width,height);
        myRectangleDoor.translate(42, 320);
    }

    
    public void paint(Graphics g) {

        g.setColor(Color.BLACK);    //White color
        
        //Fill the background in white color
        g.fillRect(0, 0, getWidth(), getHeight());
        
        //--------------------------- STAR -------------------------------------
        g.setColor(Color.YELLOW);   //Yellow color
        //Draw polygon star
        g.fillPolygon(myPolygonStar);
        
        //--------------------------- HOUSE ------------------------------------
        g.setColor(Color.GREEN);    //Green color
        //Draw a green polygon
        g.fillPolygon(myPolygonHouse);
        //---------------------------- DOOR ------------------------------------
        g.setColor(Color.ORANGE);
        g.fillRect((int)myRectangleDoor.getX(), (int)myRectangleDoor.getY(), 
                (int)myRectangleDoor.getWidth(), (int)myRectangleDoor.getHeight());  
        //---------------------------- LABEL -----------------------------------
        g.setColor(Color.MAGENTA);
	g.setFont(new Font("Comic Sans", Font.BOLD, 24));
        g.drawString("My house", 180, 420);
        
        
        g.setColor(Color.WHITE);
g.drawLine(100, 100, 100, 500);
g.setColor(Color.RED);
g.drawRect(100, 100, 150, 150);

    }

    
    
    //==========================================================================
    @Override
    public void mouseDragged(MouseEvent e) {
        int x = e.getX();
        int y = e.getY();
        Graphics g = getGraphics();
        g.setColor(Color.CYAN);
        //g.fillRect(lastX, lastY, 5, 5);
        g.drawLine(x, y, x1, y1);
  
    }
    @Override
    public void mousePressed(MouseEvent e) {
        x1 = e.getX();
        y1 = e.getY();   
    }
    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        x1 = e.getX();
        y1 = e.getY();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
    

    
}
