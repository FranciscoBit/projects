/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class DisplayText {
    
    JTextArea myText;
    
    String copyText;
    int textPosition;
    
    DisplayText(String fileName, String fileContents)
    {
        
        MyJFrame f = new MyJFrame(fileName);
        //f.myButtomContainer.setVisible(false);
        Container c = f.getContentPane();
       
        c.setLayout( new BorderLayout());// VERY IMPORTANT FOR LAYOUT TO LOOK NICE

        myText = new JTextArea(fileContents);
        myText.setEditable(true);
        
        c.setBackground(Color.CYAN);
        JScrollPane sp = new JScrollPane(myText);
        sp.setSize(400, 400);
        c.add(sp, BorderLayout.CENTER);


        f.setBounds(400, 50, 600, 600);
        f.setVisible(true);
    }
    
    public void selectText ()
    { 
        int pos = myText.getCaretPosition();
        myText.getSelectedText();
        myText.copy();
        myText.setCaretPosition(pos);
        myText.requestFocusInWindow();
    }
    
    
    public void insertText ()
    {
        int pos = myText.getCaretPosition();
        //ta.getSelectedText();
        myText.paste();
        myText.setCaretPosition(pos);
        myText.requestFocusInWindow();
    }
    

}
