/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

public interface Constants {
    
    String TITLE = "";
    String[] MAIN_MENU_OPTIONS = {"File", "Tool", "Help"};
    String[] MAIN_MENU_ITEMS_FILE = {"New", "Lists Files", "Save as", "Close"};
    String[] MAIN_MENU_ITEMS_TOOL = {"Sort", "Search", "Edit"};
    
    String[] MAIN_MENU_ITEMS_TOOL_EDIT_ITEMS = {"Copy", "Paste"};
    
    String MAIN_MENU_ITEMS_FILE_NEW_FILENAME = "New File";
    String MAIN_MENU_ITEMS_FILE_NEW_FILECONTENTS = "File Contents....Write something here";
    
    String[] MAIN_BUTTON_ITEMS = {"Drawing", "Close", "Image", "Browser"};

    
}
