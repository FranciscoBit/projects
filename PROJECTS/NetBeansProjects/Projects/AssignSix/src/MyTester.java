/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import javax.swing.JFrame;

public class MyTester {
    
    public static void main(String[] arg) 
    {
        MyJFrame myJFrame = new MyJFrame("My First GUI");

        myJFrame.setVisible(true);
        myJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    
}
