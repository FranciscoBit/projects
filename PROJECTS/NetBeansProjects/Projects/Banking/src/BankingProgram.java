/**
 *
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class BankingProgram {

    public static void main(String[] args) {

        // MAIN OPTIONS
        String introInputStr = JOptionPane.showInputDialog(
                " Press(1) to create new Bank Account "
            + "\n Press(2) to access your Bank Account and its option(s)"
            + "\n Press(3) to access Admin panel and its option(s)"
            + "\n Press(Cancel) to Exit");
        
        Database myDatabase = new Database();
        
        // WHILE MAIN OPTIONS IS NOT NULL
        while (introInputStr != null)
        {
            
            int introInputInt = Integer.parseInt(introInputStr);
            
            // OPTION 1: Create New Bank Account
            if (introInputInt == 1) 
            {
                String customerAccountNumber = JOptionPane.showInputDialog("Pick Account Number");
                String customerAccountBalance = JOptionPane.showInputDialog("Enter Initial Account Balance");
                double customerAccountBalanceDouble = Double.parseDouble(customerAccountBalance);

                String customerNameFirst = JOptionPane.showInputDialog("Enter First Name");
                String customerNameLast = JOptionPane.showInputDialog("Enter Last Name");

                String customerAddressStreet = JOptionPane.showInputDialog("Enter Street Address");
                String customerAddressCity = JOptionPane.showInputDialog("Enter City");
                String customerAddressState = JOptionPane.showInputDialog("Enter State");
                String customerAddressZipCode = JOptionPane.showInputDialog("Enter Zip Code");

                // INPUT INFORMATION INTO THE METHODS
                BankAccount myBankAccount = new BankAccount(customerAccountNumber, customerAccountBalanceDouble);
                myBankAccount.InformationName(customerNameFirst, customerNameLast);
                myBankAccount.InformationAddress(customerAddressStreet, customerAddressCity, customerAddressState, customerAddressZipCode);

                // ADDS INFORMATION TO THE DATABASE AND PRINTS
                myDatabase.addBankAccount(myBankAccount);
                myDatabase.printInformation(introInputInt);

            }
            // OPTION 2: Access your Bank Account and its option(s)
            else if (introInputInt == 2) 
            {
                // BEGINS WITH USER INPUTTING THE ACCOUNT NUMBER
                String customerAccountNumber = JOptionPane.showInputDialog("Search Account Number");
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
                
                // WHILE SEARCHING ACCCOUNT NUMBER IS NOT NULL
                while ( customerAccountNumber != null )
                {
                   String myInformation = "";
                   
                   // CALL METHOD TO FIND BANK ACCOUNTS
                   BankAccount target = myDatabase.findBankAccount(customerAccountNumber) ;
                   
                   // SEARCH NOT FOUND 
                   if (target == null)
                   {
                      myInformation = "SORRY, NO BANK ACCOUNT WAS FOUND."
                              + "\nDate: " + ft.format(dNow);
                      JTextArea text= new JTextArea(myInformation, 20, 25);
                      JScrollPane pane= new JScrollPane(text);
                      JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
                   }
                   // SEARCH FOUND
                   else
                   {
                      myInformation = "#### Bank Account Information ####"
                        + "\n"
                        + "Bank Account found."
                        + "\nDate: " + ft.format(dNow)
                        + target.getCustomerName()
                        + "\n Account Number : " + target.getAccountNumber()
                        + target.getCustomerAddress()
                        + "\n Balance: " + target.getAccountBalance()
                        + "\n -----------------------------------------------------";
                      
                      JTextArea text= new JTextArea(myInformation, 20, 25);
                      JScrollPane pane= new JScrollPane(text);
                      JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
                      
                      // IF SEARCH FOR BANK ACCOUNT IS FOUND THEN NEW OPTIONS WILL APPEAR
                      String myOptionsStr = JOptionPane.showInputDialog(
                            " Press(1) to Deposit Money "
                        + "\n Press(2) to Withdraw Money"
                        + "\n Press(Cancel) to Exit");
                      
                      int myOptionsInt = Integer.parseInt(myOptionsStr);
                      
                      // OPTION: DEPOSIT MONEY
                      if(myOptionsInt == 1)
                      {
                          String customerDeposit = JOptionPane.showInputDialog("Enter Deposit Amount (Ex: 500.0)");
                          double customerDepositDouble = Double.parseDouble(customerDeposit);
                          target.AccountDeposit(customerDepositDouble);
                          
                          myInformation = "#### Bank Account Information ####"
                        + "\n"                       
                        + "Bank Account found."
                        + "\nDate: " + ft.format(dNow)
                        + target.getCustomerName()
                        + "\n Account Number : " + target.getAccountNumber()
                        + target.getCustomerAddress()
                        + "\n Balance: " + target.getAccountBalance()
                        + "\n -----------------------------------------------------";
                          
                          text= new JTextArea(myInformation, 20, 25);
                          pane= new JScrollPane(text);
                          JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
                      }
                      // OPTION: WITHDRAW MONEY
                      else if (myOptionsInt == 2)
                      {
                          String customerWithdraw = JOptionPane.showInputDialog("Enter Withdrawal Amount (Ex: 500.0)");
                          double customerWithdrawDouble = Double.parseDouble(customerWithdraw);
                          target.AccountWithdraw(customerWithdrawDouble);
                          
                          myInformation = "#### Bank Account Information ####"
                        + "\n"                       
                        + "Bank Account found."
                        + "\nDate: " + ft.format(dNow)
                        + target.getCustomerName()
                        + "\n Account Number : " + target.getAccountNumber()
                        + target.getCustomerAddress()
                        + "\n Balance: " + target.getAccountBalance()
                        + "\n -----------------------------------------------------";
                          
                          text= new JTextArea(myInformation, 20, 25);
                          pane= new JScrollPane(text);
                          JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
                      }
                      
    
                   }
                   
                   customerAccountNumber = JOptionPane.showInputDialog("Search Account Number");
                } // END of WHILE

                
            }// END of MAIN OPTION 2
            
            // OPTION 3: Access Admin panel and its option(s)
            else if (introInputInt == 3)
            {
                String myInformation = "";
                
                // NEW OPTIONS WILL APPEAR
                String myOptionsStr = JOptionPane.showInputDialog(
                            " Press(1) to view ALL ACTIVE Bank Account(s) "
                        + "\n Press(2) to DELETE a Bank Account"
                        + "\n Press(3) to view ALL DELETED Bank Account(s)"
                        + "\n Press(Cancel) to Exit");
                
                // WHILE THE NEW OPTIONS ARE NOT NULL
                while (myOptionsStr != null)
                {
                    int myOptionsInt = Integer.parseInt(myOptionsStr);
                    
                    // OPTION 1: View ALL ACTIVE Bank Account(s)
                    if (myOptionsInt == 1)
                    {
                        myDatabase.printInformation(introInputInt);
                    }
                    // OPTION 2: DELETE a Bank Account
                    else if (myOptionsInt == 2)
                    {
                        String customerAccountNumber = JOptionPane.showInputDialog("Account Number to be DELEATED");
                        int myIndex = myDatabase.completelyRemoveBankAccount(customerAccountNumber);
                        
                        // IF BANK ACCOUNT IS NOT FOUND
                        if (myIndex == -1)
                        {
                            myInformation = "SORRY, NO BANK ACCOUNT WAS FOUND.";
                            JTextArea text= new JTextArea(myInformation, 20, 25);
                            JScrollPane pane= new JScrollPane(text);
                            JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
                        }
                        // IF BANK ACCOUNT IS FOUND, THEN WE CAN DELETE IT
                        else
                        {
                            BankAccount myDeleatedBankAccount = myDatabase.findBankAccount(customerAccountNumber);
                            myDatabase.addToDeletedList(myDeleatedBankAccount);
                            
                            myDatabase.deleteBankAccount(myIndex);
                            myInformation = "BANK ACCOUNT WAS FOUND. \nSUCCESSFULLY DELEATED";
                            JTextArea text= new JTextArea(myInformation, 20, 25);
                            JScrollPane pane= new JScrollPane(text);
                            JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
                        }
  
                        //+ "\nDate: " + ft.format(dNow);
                        
                        
                    }
                    // OPTION 3: View ALL DELETED Bank Account(s)
                    else if (myOptionsInt == 3)
                    {
                        myDatabase.printDeleteInformation();        
                    }
                    
                    myOptionsStr = JOptionPane.showInputDialog(
                            " Press(1) to view ALL ACTIVE Bank Account(s) "
                        + "\n Press(2) to DELETE a Bank Account"
                        + "\n Press(3) to view ALL DELETED Bank Account(s)"
                        + "\n Press(Cancel) to Exit");

                } // END OF WHILE LOOPS FOR OPTIONS 

            
            }
            introInputStr = JOptionPane.showInputDialog(
                        " Press(1) to create new Bank Account "
                    + "\n Press(2) to access your Bank Account and its option(s)"
                    + "\n Press(3) to access Admin panel and its option(s)"
                    + "\n Press(Cancel) to Exit");
        }
        

    }

}
