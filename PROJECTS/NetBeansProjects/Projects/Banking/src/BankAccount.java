/**
 *
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */
public class BankAccount {
    
    private String accountNumber;
    private double accountBalance;
    
    
    private String addressStreet;
    private String addressCity;
    private String addressState;
    private String addressZipCode;
    
    private String nameFirst;
    private String nameLast;
    
    /**
     * Creates a bank account with account number and balance 
     * @param accountNumber
     * @param accountBalance 
     */
    public BankAccount (String accountNumber, double accountBalance)
    {
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }
    /**
     * Input the first and last name of user
     * @param nameFirst
     * @param nameLast 
     */
    public void InformationName (String nameFirst, String nameLast)
    {
        this.nameFirst = nameFirst;
        this.nameLast = nameLast;
    }
    /**
     * Input the full address of user
     * @param addressStreet
     * @param addressCity
     * @param addressState
     * @param addressZipCode 
     */
    public void InformationAddress (String addressStreet,String addressCity, String addressState, String addressZipCode )
    {
        this.addressStreet = addressStreet;
        this.addressCity = addressCity;
        this.addressState = addressState;
        this.addressZipCode = addressZipCode;
    }
    /**
     * Deposits money into the Bank Account
     * @param add 
     */
    public void AccountDeposit (double add)
    {
        double newAccountBalance = this.accountBalance + add;
        this.accountBalance = newAccountBalance;   
    }
    /**
     * Withdraws money from the Bank Account
     * @param sub 
     */
    public void AccountWithdraw (double sub)
    {
        double newAccountBalance = this.accountBalance - sub;
        this.accountBalance = newAccountBalance;
    }
    /**
     * Gets the account number of the Bank Account.
     * @return Account Number of Customer
     */
    public String getAccountNumber ()
    {
        return this.accountNumber;
    }
    /**
     * Gets the account balance of the Bank Account.
     * @return Account Balance of Customer
     */
    public double getAccountBalance()
    {
        return this.accountBalance;
    }
    /**
     * Gets the full name of the user.
     * @return Full Name of Customer
     */
    public String getCustomerName ()
    {
        return "\n First Name: " + this.nameFirst + "\n Last Name: " + this.nameLast;
    }
    
    /**
     * Gets the complete address of user.
     * @return Complete Customer Address
     */
    public String getCustomerAddress ()
    {
        return "\n Street Address : " + this.addressStreet
                + "\n City: " + this.addressCity
                + "\n State: " + this.addressState
                + "\n Zip Code: " + this.addressZipCode;
    }
    
    /**
     * Gets the street address of user.
     * @return Street of Customer
     */
    public String getAddressStreet ()
    {
        return this.addressStreet;
    }
    /**
     * Gets the city of user.
     * @return City of Customer
     */
    public String getAddressCity ()
    {
        return this.addressCity;
    }
    /**
     * Gets the state of user.
     * @return State of Customer
     */
    public String getAddressState ()
    {
        return this.addressState;
    }
    /**
     * Gets the zip code of user.
     * @return Zip Code of Customer
     */
    public String getAddressZipCode ()
    {
        return this.addressZipCode;
    }
    /**
     * Gets the first name of user.
     * @return First Name of Customer
     */
    public String getNameFirst ()
    {
        return this.nameFirst;
    }
    /**
     * Gets the last name of user.
     * @return Last Name of Customer
     */
    public String getNameLast ()
    {
        return this.nameLast;
    }
    
    
    
    
    
}

