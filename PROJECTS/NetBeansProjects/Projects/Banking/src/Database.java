/**
 *
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.util.Date;
import java.text.*;

public class Database {
    
    private ArrayList<BankAccount> BankAccountList;
    private ArrayList<BankAccount> RemoveBankAccountList;
    
    /**
     * Constructs a Database with no bank accounts
     * Contains two types of Bank Accounts
     */
    public Database ()
    {
        this.BankAccountList = new ArrayList<BankAccount>();
        this.RemoveBankAccountList = new ArrayList<BankAccount>();
    }
    
    /**
     * Adds a bank account to the Database
     * @param newAccount 
     */
    public void addBankAccount (BankAccount newAccount)
    {
        this.BankAccountList.add(newAccount);
    }
    
    /**
     * Deletes a bank account
     * @param index 
     */
    public void deleteBankAccount (int index)
    {
        this.BankAccountList.remove(index);
    }
    
    /**
     * Adds a bank account to the deleted Database
     * @param newAccount 
     */
    public void addToDeletedList (BankAccount newAccount)
    {
        this.RemoveBankAccountList.add(newAccount);
    }
    
    /**
     * Gets the index where the deleted account is found based on account number
     * @param AccountNumber
     * @return index of future deleted bank account
     */
    public int completelyRemoveBankAccount (String AccountNumber)
    {
        int count = 0;
        
        while(count < this.BankAccountList.size())
        {
            BankAccount currentBankAccount = this.BankAccountList.get(count);
            String myAccountNumber = currentBankAccount.getAccountNumber();
            
            if (myAccountNumber.equals(AccountNumber))
            {
                return count;
            }
            count = count + 1;
        }
        return -1;
    }
    
  
    /**
     * Finds a bank account based on account number
     * @param AccountNumber
     * @return bank account that was found
     */
    public BankAccount findBankAccount (String AccountNumber)
    {
        int count = 0;
        
        while(count < this.BankAccountList.size())
        {
            BankAccount currentBankAccount = this.BankAccountList.get(count);
            String myAccountNumber = currentBankAccount.getAccountNumber();
            
            if (myAccountNumber.equals(AccountNumber))
            {
                return currentBankAccount;
            }
            count = count + 1;
        }
        
        return null;
        
    }
    
    /**
     * Prints information of user based on option(s)
     * @param optionNumber 
     */
    public void printInformation (int optionNumber)
    {
        String myInformation = "";
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        
        if (optionNumber == 1)
        {
            int count = 0;
            while (count < this.BankAccountList.size())
            {
                BankAccount currentBankAccount = this.BankAccountList.get(count);
                
                myInformation = "#### Bank Account Information ####"
                        + "\n"
                        + "Date: " + ft.format(dNow)
                        + currentBankAccount.getCustomerName()
                        + "\n Account Number : " + currentBankAccount.getAccountNumber()
                        + currentBankAccount.getCustomerAddress()
                        + "\n Balance: " + currentBankAccount.getAccountBalance()
                        + "\n -----------------------------------------------------";
                
                
                count = count + 1;
            }
            
        }
        else if (optionNumber == 2)
        { 
            
        }
        
        else if (optionNumber == 3)
        {
            int count = 0;
            while (count < this.BankAccountList.size())
            {
                BankAccount currentBankAccount = this.BankAccountList.get(count);
                
                myInformation = myInformation + "\n#### Bank Account Information ####"
                        + "\n"
                        + "Date: " + ft.format(dNow)
                        + currentBankAccount.getCustomerName()
                        + "\n Account Number : " + currentBankAccount.getAccountNumber()
                        + currentBankAccount.getCustomerAddress()
                        + "\n Balance: " + currentBankAccount.getAccountBalance()
                        + "\n -----------------------------------------------------";
                
                
                count = count + 1;
            }
        
        
        }
        
        JTextArea text= new JTextArea(myInformation, 20, 25);
        JScrollPane pane= new JScrollPane(text);
        JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
        
    }
    
    /**
     * Prints information of user that was deleted information
     */
    public void printDeleteInformation ()
    {
        String myInformation = "";
        Date dNow = new Date();
        SimpleDateFormat ft = new SimpleDateFormat ("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        
   
            int count = 0;
        while (count < this.RemoveBankAccountList.size()) 
        {
            BankAccount currentBankAccount = this.RemoveBankAccountList.get(count);

            myInformation = myInformation + "\n#### Bank Account Information ####"
                    + "\n"
                    + "\nDELETED"
                    + "Date: " + ft.format(dNow)
                    + currentBankAccount.getCustomerName()
                    + "\n Account Number : " + currentBankAccount.getAccountNumber()
                    + currentBankAccount.getCustomerAddress()
                    + "\n Balance: " + currentBankAccount.getAccountBalance()
                    + "\n -----------------------------------------------------";

            count = count + 1;
        }
        
        
        JTextArea text= new JTextArea(myInformation, 20, 25);
        JScrollPane pane= new JScrollPane(text);
        JOptionPane.showMessageDialog(null, pane, "Banking Information",JOptionPane.INFORMATION_MESSAGE);
        
        
    }
    
    
}
