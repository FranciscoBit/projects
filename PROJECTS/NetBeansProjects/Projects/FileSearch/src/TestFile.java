/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.io.IOException;

class TestFile {

    public static void main(String[] arg) {
        boolean done = false;
        BasicFile f = new BasicFile();

        String menu = "Enter option"
                + "\n1. Open File"
                + "\n2. File Path"
                + "\n3. File Size"
                + "\n4. Backup"
                + "\n5. File Search"
                + "\n6. Create Files (A, B, C)"
                + "\n7. Quit"
                + "\n8. File Directories(Not Working)";

        while (!done) {
            
            String s = JOptionPane.showInputDialog(menu);

            // If user click on "Cancel" on JOptionPane
            if (s == null) {
                done = true;
            } 
            // Else when user clicks "OK" on JOptionPane
            else {
                try {
                    
                    int i = Integer.parseInt(s);
                    switch (i) {
                        case 1:
                            display(f.getContents());   // Display File Directory
                            break;
                        case 2:
                            displayFilePath(f);
                            break;
                            
                        case 3:
                            displayFileSize(f);
                            break;
                            
                        case 4:
                            display(f.Backup());
                            break;
                            
                        case 5:
                            display(f.FileSearch());
                            break;
                    
                        case 6:
                            display(f.CreateFile());
                            break;
                        case 7:
                            done = true;
                            break;
                        case 8:
                            display(f.getDirectories(f.getPath()));
                            break;

                        default:
                            display("This option is underfined", "Error");
                            break;
                    }
                } catch (NumberFormatException | NullPointerException e) {

                    display(e.toString(), "Error");

                }
            }

        }
    }

    // To display Errors
    static void display(String s, String err) {
        JOptionPane.showMessageDialog(null, s, err, JOptionPane.ERROR_MESSAGE);
    }

    static void display(String s) {
        JTextArea myTextArea = new JTextArea(s, 50, 50);
        JScrollPane myScrollPane = new JScrollPane(myTextArea);
        JOptionPane.showMessageDialog(null, s, "Content", JOptionPane.INFORMATION_MESSAGE);
    }

    static void display(BasicFile f) {
        String s = f.getFileSize() + " bytes" + "\n" + f.getPath();
        String fn = f.getName();
        JOptionPane.showMessageDialog(null, s, "Filename: " + fn, JOptionPane.INFORMATION_MESSAGE);
    }
    
    // To display File Size ONLY
    static void displayFileSize(BasicFile f) {
        String s = "File Size: " + f.getFileSize() + " bytes";
        String fn = f.getName();
        JOptionPane.showMessageDialog(null, s, "Filename: " + fn, JOptionPane.INFORMATION_MESSAGE);
    }
    
    // To display File Path ONLY
    static void displayFilePath(BasicFile f) {
        String s = "File Path: " + f.getPath();
        String fn = f.getName();
        JOptionPane.showMessageDialog(null, s, "Filename: " + fn, JOptionPane.INFORMATION_MESSAGE);
    }
    
    

    
    
}
