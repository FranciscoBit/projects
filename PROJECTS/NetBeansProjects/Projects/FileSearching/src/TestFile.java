
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.io.IOException;

class TestFile {

    public static void main(String[] arg) {
        boolean done = false;
        BasicFile f = new BasicFile();
        try {
            String menu = "Enter option\n1. Display contents\n2. List file characteristics\n3. Unbuffered read\n4. Buffered read\n5. Tokenize stream\n6. Write results\n7. Quit";
            while (!done) {
                String s = JOptionPane.showInputDialog(menu);
                try {
                    int i = Integer.parseInt(s);
                    switch (i) {
                        case 1:
                            display(f.getContents());
                            break;
                        case 2:
                            display(f);
                            break;
                        case 3:
                            f.unbufferedTime();
                            break;
                        case 4:
                            f.bufferedTime();
                            break;
                        case 5:
                            display(f.tokenizeStream()); //, f.getName());
                            break;

                        case 6:
                            f.writeResult();
                            break;

                        case 7:
                            done = true;
                            break;
                        default:
                            display("This option is underfined", "Error");
                            break;
                    }
                } catch (NumberFormatException e) {
                    display(e.toString(), "Error");
                } catch (NullPointerException e) {
                    display(e.toString(), "Error");
                } catch (IOException e) {
                    display(e.toString(), "Error");
                }
            }
        } catch (NullPointerException e) {
        }
    }

    static void display(String s, String err) {

        JOptionPane.showMessageDialog(null, s, err, JOptionPane.ERROR_MESSAGE);
    }

    static void display(String s) {
        JTextArea t = new JTextArea(s, 25, 40);
        JScrollPane p = new JScrollPane(t);
        JOptionPane.showMessageDialog(null, p, "Content", JOptionPane.INFORMATION_MESSAGE);
    }

    static void display(BasicFile f) {
        String s = f.getFileSize() + " bytes" + "\n" + f.getPath();
        String fn = f.getName();
        JOptionPane.showMessageDialog(null, s, "Filename: " + fn, JOptionPane.INFORMATION_MESSAGE);
    }
}
