
import javax.swing.JFileChooser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileInputStream;
import javax.swing.JOptionPane;
import java.io.BufferedInputStream;
import java.io.StreamTokenizer;
import java.io.BufferedWriter;
import java.io.FileWriter;

public class BasicFile {

    File f;

    public BasicFile() {
        JFileChooser choose = new JFileChooser(".");
        int status = choose.showOpenDialog(null);

        try {
            if (status != JFileChooser.APPROVE_OPTION) {
                throw new IOException();
            }

            f = choose.getSelectedFile();

            if (!f.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            display(e.toString(), "File not found ....");
        } catch (IOException e) {
            display(e.toString(), "Approve option was not selected");
        }
    }

    String getContents() throws IOException {
        String s = "";
        String tmp = "";
        BufferedReader bf = new BufferedReader(new FileReader(f));

        while ((tmp = bf.readLine()) != null) {
            s = s + tmp + "\n";
        }

        return s;
    }

    void display(String msg, String s) {
        JOptionPane.showMessageDialog(null, msg, s, JOptionPane.ERROR_MESSAGE);
    }

    String getName() {
        return f.getName();
    }

    String getPath() {
        return f.getAbsolutePath();
    }

    long getFileSize() {
        return f.length();
    }

    boolean exists() {
        return f.exists();
    }

    void unbufferedTime() throws IOException {
        FileInputStream fis = new FileInputStream(f);
        System.out.println("Size of file " + f.length() + " bytes");
        long startTime, endTime;

        startTime = System.nanoTime();

        while ((fis.read()) != -1) {

        }

        endTime = System.nanoTime();

        long timeElapse = endTime - startTime;

        System.out.println("Time elapse when unbuffered " + timeElapse / 1000000.0 + " msec");
    }

    void bufferedTime() throws IOException {
        FileInputStream fis = new FileInputStream(f);

        byte b[] = new byte[4096];
        BufferedInputStream bis = new BufferedInputStream(fis);

        System.out.println("Size of file " + f.length() + " bytes");
        long startTime, endTime;

        startTime = System.nanoTime();

        while ((bis.read(b)) != -1) {

        }
        endTime = System.nanoTime();

        long timeElapse = endTime - startTime;

        System.out.println("Time elapse when buffered " + timeElapse / 1000000.0 + " msec");

    }

    public String tokenizeStream() {
        String s = "";
        try {
            StreamTokenizer token = new StreamTokenizer(new FileReader(f));

//			token.eolIsSignificant(true);
            int tokval;
            while ((tokval = token.nextToken()) != StreamTokenizer.TT_EOF) {
                switch (tokval) {
                    case StreamTokenizer.TT_WORD:
                        s = s + "String token: " + token.sval + "\n";
                        break;
                    case StreamTokenizer.TT_NUMBER:
                        s = s + "Number token: " + token.nval + "\n";
                        break;
                    case StreamTokenizer.TT_EOL:
                        s = s + "End of line token: " + token.ttype + "\n";
                        break;
                    default:
                        s = s + "Special characters: " + (char) tokval + "\n";
                        break;
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        return s;
    }

    public void writeResult() //throws IOException
    {
        BufferedWriter bw = null;
        try {

            String content = tokenizeStream();

            File file = new File("friday.txt");

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(content);
//			bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bw.close();
            } catch (IOException e) {

            }
        }
    }

}
