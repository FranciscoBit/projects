/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

public interface Constants {
    
    public static final char LEFT_NORMAL = '(';
    public static final char RIGHT_NORMAL = ')';
    
    public static final char PLUS = '+';
    public static final char MINUS = '-';
    public static final char DIVIDE = '/';
    public static final char MULTIPLY = '*';
    public static final char MODULUS = '%';
    
    public static final char L_CURLY = '{';
    public static final char R_CURLY = '}';
    
    public static final char L_BRACKET = '[';
    public static final char R_BRACKET  = ']';
     
}
