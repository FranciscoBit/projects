
/**
 * @author Francisco Munoz ID: 2419358 Programming II
 */

class RPN {

    public static void main(String[] arg) {
        String s[] = {"10 + 2 - 8 + 3",
            "2 3 5 8 + *",
            "5 3 2 * + 4 - 5 +",
            "2 3 5 8 + *",
            "5 + ) * ( 2",
            " 2 + ( - 3 * 5 ) ",
            "(( 2 + 3 ) * 5 ) * 8 ",
            "5 * 10 + ( 15 - 20 ) ) - 25",
            " 5 + ( 5 *  10 + ( 15 - 20 ) - 25 ) * 9",
            "[ 5 + 3 + 2 ]",
            "2 + { 2 * ( 10 – 4 ) / [ { 4 * 2 / ( 3 + 4) } + 2 ] – 9 }"
        };
        for (int i = 0; i < s.length; i++) {

            Arithmetic a = new Arithmetic(s[i]);
            if (a.isBalance()) {
                System.out.println("Expression " + s[i] + " is balanced\n");
                a.postfixExpression();
                System.out.println("The post fixed expression is " + a.getPostfix());
                a.evaluateRPN();
                System.out.println("The answer is: " + a.getAnswer());
                // Supply the necessary codes to determine the validity of your answers
                System.out.println("-----------------------------------------------------------------");
            } else {
                System.out.println("Expression " + s[i] + " is not balanced");
                System.out.println("-----------------------------------------------------------------");
            }
        }
    }
}
