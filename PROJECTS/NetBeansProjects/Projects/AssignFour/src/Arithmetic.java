/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

import java.util.Stack;
import java.util.EmptyStackException;
import java.util.Scanner;

class Arithmetic {

    Stack stk;
    String expression, postfix;
    int length;

    Arithmetic(String s) {
        expression = s;
        postfix = "";
        length = expression.length();
        stk = new Stack();
    }

    /**
     * STEP 1: Validate the expression - make sure parentheses are balanced
     * @return boolean if its balance (true or false) 
     */
    boolean isBalance() {
        // Already defined
        boolean balanceNotFound = false;
        int counter = 0;
        
        try
        {
            
            while( (counter < length) && (balanceNotFound == false) )
            {
                char c = expression.charAt(counter);
                
                if( (c == Constants.LEFT_NORMAL) || (c == Constants.L_CURLY) || 
                        (c == Constants.L_BRACKET) )
                {
                    stk.push(new Character(c));
                }
                else if ( (c == Constants.RIGHT_NORMAL) || (c == Constants.R_CURLY) || 
                        (c == Constants.R_BRACKET) )
                {
                    stk.pop();
                }
                
                counter++;
                
            }

        } // END TRY
        catch(EmptyStackException e)
        {
            System.out.println("ERROR: NOT BALANCE");
            System.out.println("ERROR TYPE: " + e.toString());
            balanceNotFound = true;
        }
        
        
        if ( (stk.empty()) && (balanceNotFound == false) ) {
            return true;
        } 
        else 
        {
            return false;
        }

    }

    /**
     * STEP 2: Convert expression to postfix notation
     */
    void postfixExpression() {

        stk.clear(); // Re-using the stack object
        Scanner scan = new Scanner(expression);
        char current;

        // The algorithm for doing the conversion.... Follow the bullets
        while (scan.hasNext()) {
            String token = scan.next();

            // Bullet # 1
            if (isNumber(token)) {
                System.out.println("Read Operand (" + token + "), Push(" + token + ")");    // NEW
                postfix = postfix + token + " ";
            } 
            else 
            {
                current = token.charAt(0);

                // Bullet # 2 begins
                if (isParentheses(current)) 
                {
                    if (stk.empty() || current == Constants.LEFT_NORMAL) {
                        // push this element on the stack;
                        System.out.println("Read Left Parentheses ('" + current + "')");
                        stk.push(current);
                    } else if (current == Constants.RIGHT_NORMAL) {
                        try {
                            /* Some details ... whatever is popped from the
                             * stack is an object, hence you must cast this
                             * object to its proper type, then extract its
                             * primitive data (type) value.
                             */
                            Character ch = (Character) stk.pop();
                            char top = ch.charValue();

                            while (top != Constants.LEFT_NORMAL) {
                                /*
                                 * Append each token popped onto the output string
                                 * Make sure to place at least one blank space between
                                 * each token
                                 */

                                postfix = postfix + top + " ";
                                ch = (Character) stk.pop();
                                top = top = ch.charValue();

                            }
                        } catch (EmptyStackException e) {

                        }
                    }
                }// Bullet # 2 ends
                // Bullet # 3 begins
                else if (isOperator(current)) {
                    if (stk.empty()) {
                        stk.push(new Character(current));
                    } else {
                        try {
                            // Remember the method peek simply looks at the top
                            // element on the stack, but does not remove it.

                            char top = (Character) stk.peek();
                            boolean higher = hasHigherPrecedence(top, current);

                            System.out.println("Read Operator ('" + top + "')");            //NEW
                            System.out.println("----Stack is not empty");                   //NEW
                            System.out.println("----T1 = pop = <T1 Number>");                          //NEW
                            System.out.println("----T2 = pop = <T2 Number>");                          //NEW
                            System.out.println("----T3 = T2 opt T1 = <T3 Total>");          //NEW
                            System.out.println("----Push(<T3 Total>)");                //NEW
                            
                            while (top != Constants.LEFT_NORMAL && higher) {
                                postfix = postfix + stk.pop() + " ";
                                top = (Character) stk.peek();
                                
                            }
                            
                            System.out.println("Read Operator ('" + current + "')");        //NEW
                            System.out.println("----Stack is not empty");                   //NEW
                            System.out.println("----T1 = pop = <T1 Number>");                          //NEW
                            System.out.println("----T2 = pop = <T2 Number>");                          //NEW
                            System.out.println("----T3 = T2 opt T1 = <T3 Total>");          //NEW
                            System.out.println("----Push(<T3 Total>)");                //NEW
                            
                            stk.push(new Character(current));
                        } catch (EmptyStackException e) {
                            stk.push(new Character(current));
                        }
                    }
                }// Bullet # 3 ends
  
            }
        } // Outer loop ends

        try {
            // Bullet # 4
            while (!stk.empty()) {
                // complete this
                postfix = postfix + stk.pop() + " ";
            }
        } catch (EmptyStackException e) {
            System.out.println("ERROR TYPE: " + e.toString());

        }
    }

    /**
     * Tells us if its a number or not
     * @param str string being tested
     * @return boolean if its a number (true or false)
     */
    boolean isNumber(String str) {

        //define this method
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }

    }

    /**
     * Tells us if it is a parentheses
     * @param current character being tested
     * @return boolean if its a parentheses (true or false)
     */
    boolean isParentheses(char current) {

        // define this method;
        boolean parenthesisFound;

        if ((current == Constants.LEFT_NORMAL)
                || (current == Constants.RIGHT_NORMAL)) {
            parenthesisFound = true;
        } else {
            parenthesisFound = false;
        }

        return parenthesisFound;

    }

    /**
     * Tells us if its an operator
     * @param ch character being tested
     * @return boolean if its operator (true or false)
     */
    boolean isOperator(char ch) {

        // define this method
        boolean operatorFound;

        if ((ch == Constants.PLUS) || (ch == Constants.MINUS)
                || (ch == Constants.DIVIDE) || (ch == Constants.MULTIPLY)
                || (ch == Constants.MODULUS)) {
            operatorFound = true;
        } else {
            operatorFound = false;
        }

        return operatorFound;

    }

    /**
     * Compares characters and tell us higher precedence
     * @param top character being tested
     * @param current character being tested
     * @return boolean if its higher precedence (true or false)
     */
    boolean hasHigherPrecedence(char top, char current) {

        boolean foundHigherPrecedence;
        int topValue = 0;
        int currentValue = 0;
               
        if( (top == Constants.PLUS) || (top == Constants.MINUS) )
        {
            topValue = 1;
        }
        else if((top == Constants.MULTIPLY) || (top == Constants.DIVIDE) ||
                (top == Constants.MODULUS) )
        {
            topValue = 2;
        }
        
        if( (current == Constants.PLUS) || (current == Constants.MINUS) )
        {
            currentValue = 1;
        }
        else if((current == Constants.MULTIPLY) || (current == Constants.DIVIDE) ||
                (current == Constants.MODULUS) )
        {
            currentValue = 2;
        }
        
        if (topValue >= currentValue) 
        {
            foundHigherPrecedence = true;
        } 
        else 
        {
            foundHigherPrecedence = false;

        }

        return foundHigherPrecedence;

    }

    /**
     * Gets the post fix as string
     * @return postfix
     */
    String getPostfix() {

        // define method
        return this.postfix;
    }

    /**
     * STEP 3: Evaluate postfix expression
     */
    void evaluateRPN() {

        Scanner myScannner = new Scanner(getPostfix());
        stk.clear();
        int t;  // TOTAL

        while (myScannner.hasNext()) 
        {

            String s = myScannner.next();

            // If Operands AKA Numbers 
            if (isNumber(s)) {
                
                // Convert to int and push to stack
                int number = Integer.parseInt(s);
                stk.push(number);
                
            } 
            // Everything else (Operators)
            else {
                
                try
                {
                    int t1 = (Integer) stk.pop();       // RIGHT
                    int t2 = (Integer) stk.pop();       // LEFT

                    char x = s.charAt(0);
                    
                    if (x == Constants.PLUS) {
                        stk.push(t2 + t1);
                    } else if (x == Constants.MINUS) {
                        stk.push(t2 - t1);
                    } else if (x == Constants.DIVIDE) {
                        stk.push(t2 / t1);
                    } else if (x == Constants.MULTIPLY) {
                        stk.push(t2 * t1);
                    } else if (x == Constants.MODULUS) {
                        stk.push(t2 % t1);
                    }
   
                }
                catch (EmptyStackException e) {
                    System.out.println("ERROR TYPE: " + e.toString());
                }


            } // END ELSE

        } // END WHILE LOOP

    }
    
    /**
     * Gets the final answer
     * @return final answer
     */
    int getAnswer() {
        try 
        {
            return (Integer) stk.pop();
        } 
        catch (EmptyStackException e) 
        {
            System.out.println("ERROR: ANSWER IS UNRELIABLE");
            System.out.println("ERROR TYPE: " + e.toString());
            return -1;
        }
        
    }
    


}
