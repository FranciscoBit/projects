
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */
public class Graphical {
    
    
    public void Menu ()
    {
        // Create a dictionary list object.
        WordList myDictionary = new WordList();
        
        // MAIN OPTIONS
        String inputMenu = JOptionPane.showInputDialog(
                " Press(1) to Add Word and Meaning"
            + "\n Press(2) to Delete Word" 
            + "\n Press(3) to Show Output ~Added and Deleted~ (Split Pane)"
            + "\n Press(4) to Show Added words (Scroll Pane)"
            + "\n Press(5) to Show Deleted words (Scroll Pane)"
            + "\n Press(Cancel) to Exit");
        
        // WHILE MAIN OPTIONS IS NOT NULL
        while (inputMenu != null)
        {
            int introMenuInt = Integer.parseInt(inputMenu);
            
            // OPTION 1: Add Word and Meaning
            if (introMenuInt == 1) 
            {

                String inputWord = JOptionPane.showInputDialog("Enter Word:");
                String inputMeaning = JOptionPane.showInputDialog("Enter Meaning:");
                
                if ( (inputWord != null) && (inputMeaning != null) )
                {
                    // Create booknodes and add them to the list.
                    myDictionary.addToCurrentList(new WordMeaning(inputWord, inputMeaning));
                    
                    JOptionPane.showMessageDialog(null, "Word and Meaning Added");
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "ERROR. Try Again!");
                }
   
                
            }
            
            // OPTION 2: To Delete Word
            else if (introMenuInt == 2)
            {
                
                String inputWord = JOptionPane.showInputDialog("Enter Word to be Deleted:");
                boolean check = myDictionary.SearchandRemove(inputWord);
                
                if (check == true)
                {
                    JOptionPane.showMessageDialog(null, "Word Deleted.");
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "Word Not Deleted.");
                }
 
                
                
            }
            
            // OPTION 3: Show Output ~Added and Deleted~ (SplitPane)
            else if (introMenuInt == 3)
            {

                SplitPane(myDictionary.SPLITgetAnswerCurrent(), myDictionary.SPLITGetAnswerDeleted());
                break;
                
            }
            
            // OPTION 4: Show Added words (Scroll Pane)
            else if (introMenuInt == 4)
            {
                String myInformation = "";
                myInformation = myDictionary.SCROLLgetAnswerCurrent();
                JTextArea text = new JTextArea(myInformation, 10, 10);
                JScrollPane pane = new JScrollPane(text);
                JOptionPane.showMessageDialog(null, pane, "Added Words", JOptionPane.INFORMATION_MESSAGE);

                
            }
            // OPTION 5: Show Deleted words (Scroll Pane)
            else if (introMenuInt == 5)
            {

                String myInformation = "";
                myInformation = myDictionary.SCROLLGetAnswerDeleted();
                JTextArea text = new JTextArea(myInformation, 10, 10);
                JScrollPane pane = new JScrollPane(text);
                JOptionPane.showMessageDialog(null, pane, "Added Words", JOptionPane.INFORMATION_MESSAGE);

                
            }
            
            
            
            inputMenu = JOptionPane.showInputDialog(
                " Press(1) to Add Word and Meaning"
            + "\n Press(2) to Delete Word" 
            + "\n Press(3) to Show Output ~Added and Deleted~ (Split Pane)"
            + "\n Press(4) to Show Added words (Scroll Pane)"
            + "\n Press(5) to Show Deleted words (Scroll Pane)"
            + "\n Press(Cancel) to Exit");
            
        }
        
    }
    
    
    public void SplitPane(String left, String right) {
        
        JFrame j = new JFrame ();
        
        j.setTitle("Example of Split Pane");
        j.setSize(450, 450);
        
        
        JPanel jp_Left = new JPanel(); JPanel jp_Right = new JPanel();
        
        // We use html and br to create newlines
        JLabel label_Left = new JLabel("<html>" + "CURRENT LIST: <BR>" + left + "</html>");
        JLabel label_Right = new JLabel("<html>" + "DELETED LIST: <BR>" + right + "</html>");
        
        jp_Left.add(label_Left);
        jp_Right.add(label_Right);
        
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, 
                true, jp_Left, jp_Right);
        
        splitPane.setOneTouchExpandable(true);
        j.getContentPane().add(splitPane);
        j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        j.setVisible(true);
        
    }
    
    
}

