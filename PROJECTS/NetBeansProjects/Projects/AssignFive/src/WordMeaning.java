/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

public class WordMeaning {

    String wordName, wordMeaning;

    WordMeaning(String name, String meaning) 
    {
        this.wordName = name;
        this.wordMeaning = meaning;
    }
    
    public String getName()
    {
        return wordName;
    }
    
    public String getMeaning ()
    {
        return wordMeaning;
    }
    

}
