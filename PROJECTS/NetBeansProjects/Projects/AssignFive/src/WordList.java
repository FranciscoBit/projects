/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

public class WordList {
    
    private WordMeaningNode list;
    private WordMeaningNode deletedList;
    
    WordList ()
    {
        list = null;
        deletedList = null;
    }
    
    
    /**
     * We add the word and meaning into the list in alphabetical order
     * @param w WordMeaning
     */
    public void addToCurrentList(WordMeaning w)
    {
        WordMeaningNode tempNode = new WordMeaningNode(w);
        try 
        {
            
            // If nothing we add
            if (list == null) 
            {
                list = tempNode;
            } 
            else 
            {
                WordMeaningNode regularNode = list;
                WordMeaningNode backNode = null;
                boolean wordFound = false;

                while ( (regularNode != null) && (!wordFound) ) 
                {
                    
                    if (tempNode.wordMeaning.getName().compareTo(regularNode.wordMeaning.getName()) < 0) {
                        wordFound = true;
                    } 
                    else 
                    {
                        backNode = regularNode;
                        regularNode = regularNode.next;
                    }
                }

                tempNode.next = regularNode;
                
                if (backNode == null) 
                {
                    list = tempNode;
                } 
                else 
                {
                    backNode.next = tempNode;
                }
            }
        } 
        catch (NullPointerException e) 
        {

        }      
        
    }
    
    /**
     * We add the word into the deleted list
     * @param w WordMeaning
     */
    public void addToDeletedList (WordMeaning w)
    {

        WordMeaningNode tempNode = new WordMeaningNode(w);

        if (deletedList == null) {
            deletedList = tempNode;
        } 
        else {
            WordMeaningNode regularNode = deletedList;

            while (regularNode.next != null) {
                regularNode = regularNode.next;
            }

            regularNode.next = tempNode;
        }
 
    }
    
    
    /**
     * Searches and removes words from the lists and returns a boolean
     * @param w word to be search
     * @return if word was found(then we add it to our deleted list) or not
     */
    boolean SearchandRemove(String w) {
        String word = w;

        WordMeaningNode tempNode = list;
        WordMeaningNode prevNode = null;
        boolean found = false;

        while (tempNode != null) 
        {
            // We search for the word 
            if (word.equalsIgnoreCase(tempNode.wordMeaning.getName())) 
            {
                if (tempNode == list) 
                {
                    list = list.next;
                } else 
                { 
                    prevNode.next = tempNode.next;
                }
                // Found is true and then we add it to our deleted list
                found = true;
                addToDeletedList(tempNode.wordMeaning);
            } 
            else 
            {
                //advance the prevNode when no match is found
                prevNode = tempNode;
            }
           tempNode = tempNode.next;
        }
        return found;
        
        
    }

    /**
     * Gets the string in the current list (SPLIT)
     * @return string of current list for SPLIT PANE
     */
    public String SPLITgetAnswerCurrent ()
    {
        String myAnswer = "";

        
        WordMeaningNode previous = list;
        WordMeaningNode current = previous.next;
        

        while (current != null) {
            
            myAnswer = myAnswer + (current.wordMeaning.getName() + " - " 
                    + current.wordMeaning.getMeaning()+ "<BR>");
            current = current.next;
        }
        return myAnswer;
    }
    
    /**
     * Gets the string in the deleted list (SPLIT)
     * @return string of deleted list for SPLIT PANE
     */
    public String SPLITGetAnswerDeleted() {
        
        String myAnswer = "";
        WordMeaningNode deleatedList = deletedList;
 
        while (deleatedList != null) {
            myAnswer = myAnswer + (deleatedList.wordMeaning.getName() + "<BR>");
            deleatedList = deleatedList.next;
        }
 
        return myAnswer;
    }
    
    
    /**
     * Gets the string in the current list (SCROLL)
     * @return string of deleted list for SCROLL PANE
     */
    public String SCROLLgetAnswerCurrent ()
    {
        String myAnswer = "";

        
        WordMeaningNode previous = list;
        WordMeaningNode current = previous.next;
        

        while (current != null) {
            
            myAnswer = myAnswer + (current.wordMeaning.getName() + " - " 
                    + current.wordMeaning.getMeaning()+ "\n");
            current = current.next;
        }
        return myAnswer;
    }
    
    /**
     * Gets the string in the deleted list (SCROLL)
     * @return string of deleted list for SCROLL PANE
     */
    public String SCROLLGetAnswerDeleted() {
        
        String myAnswer = "";
        WordMeaningNode deleatedList = deletedList;
 
        while (deleatedList != null) {
            myAnswer = myAnswer + (deleatedList.wordMeaning.getName() + "\n");
            deleatedList = deleatedList.next;
        }
 
        return myAnswer;
    }

    
    
}
