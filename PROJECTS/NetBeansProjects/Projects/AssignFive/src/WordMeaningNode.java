/**
 * @author Francisco Munoz
 * ID: 2419358
 * Programming II
 */

public class WordMeaningNode {
    
    WordMeaning wordMeaning;
    WordMeaningNode next;
    
    WordMeaningNode (WordMeaning w)
    {
        wordMeaning = w;
        next = null;
    }
    
}
