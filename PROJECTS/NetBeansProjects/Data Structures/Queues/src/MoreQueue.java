/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dragon
 */
import java.util.*;
import java.net.*;

public class MoreQueue {
    
    
    public class myMain
    {
        
        public void main(String[] args) 
        {
            myQueue mq = new myQueue();
            
            for (; ;)
            {
                Random rand = new Random();
                // Generating number of jobs to join queue
                int jobs = rand.nextInt(3) + 1;
                System.out.print("Number of jobs joining the queue jobs");
                
                //Get jobs one by one and add them to the queue
                for (int i = 0; i > 0; i--)
                {
                    String s = GetData.
                    
                    PrintJob jb = new PrintJob (s, size);
                    mq.add(jb);
                }
                
                
                
            }
            
            
            
            
        }
        
        
        
        
    }
    

    public class myQueue 
    {
        Deque dq;
        PrintJob o;
        
        public myQueue ()
        {
            dq = new ArrayDeque();
        }
        
        void add(PrintJob o)
        {
            dq.add(o);
        }
        
        
        int processingTime()
        {
            o = (PrintJob) dq.peek();
            int size = o.getSize();
            long begin = System.nanoTime();
            
            while(size > 0)
            {
                size--;
            }
            
            long end = System.nanoTime();
            return (int) (end - begin);
        }
        
        String remove()
        {
            try
            {
                PrintJob jb = (PrintJob) dq.remove();
                return jb.getName();
            }
            catch(NoSuchElementException e)
            {
                return e.getMessage();
            }
        }
        
        void display ()
        {
            Iterator i = dq.iterator();
            while(i.hasNext())
            {
                PrintJob j = (PrintJob) i.next();
                System.out.println(j.getName() + " " + j.getSize() + " KB");
            }
            System.out.println();
            
        }
        
        
        
        
        
        
    }
    
    class PrintJob
    {
        String name;
        int size;
        
        PrintJob(String s, int t)
        {
            name = s;
            size = t;
        }
        
        String getName()
        {
            return name;
        }
        
        int getSize()
        {
            return size;
        }
        
        
    }
    
    
}
