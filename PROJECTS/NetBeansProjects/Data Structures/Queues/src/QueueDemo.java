// File: QueueDemo.java

// Test class for CircularArrayQueue.java

import java.util.Random ;

public class QueueDemo 
{
    public static void main(String[] args) 
    {
        CircularArrayQueue<Integer> q = new CircularArrayQueue<Integer>() ;	
        Random r = new Random();

        // append 5 random positive 2-digit ints to the queue
        for (int i = 1; i <= 5; i++) 
        {
            int next = (r.nextInt(90) + 10);
            System.out.print("Now Appending " + next);
            q.append(next);
            System.out.println("\tUpdated queue : " + q);
        }

        // serve the queue 3 times
        for (int i = 1; i <= 3; i++) 
        {
            System.out.print("Item served: " + q.serve());
            System.out.println("\t\tUpdated queue : " + q);
        }

        // append 5 more random ints
        for (int i = 1; i <= 5; i++) 
        {
            int next = r.nextInt(90) + 10 ;	// 10 to 99, inclusive
            System.out.print("Now Appending " + next);
            q.append(next);
            System.out.println("\tUpdated queue: " + q);
        }

        // "safe" serve the queue 10 times
        for (int i = 1; i <= 10; i++) 
        {
            if (!q.isEmpty()) 
            {
                System.out.print("Item served: " + q.serve());
                System.out.println("\t\tUpdated queue: " + q);
            } 
            else 
            {
                System.out.println("ERROR! Attempt to serve empty queue!");
            }
        }
    }
}

/*  Sample output

Now Appending 11        Updated queue:   11
Now Appending 92        Updated queue:   11  92
Now Appending 44        Updated queue:   11  92  44
Item served: 11         Updated queue:   92  44
Item served: 92         Updated queue:   44
Now Appending 37        Updated queue:   44  37
Now Appending 51        Updated queue:   44  37  51
Now Appending 36        Updated queue:   44  37  51  36
Item served: 44         Updated queue:   37  51  36
Item served: 37         Updated queue:   51  36
Item served: 51         Updated queue:   36
Item served: 36         Updated queue:   queue is currently empty!
ERROR! Attempt to serve empty queue!
ERROR! Attempt to serve empty queue!

*/