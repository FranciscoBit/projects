// 	File:  Queue2.java		(Dedicated to John DeLancey)

//  A generic Queue class with linked implementation.

//  Warning: throws NullPointerException if an attempt is made to serve an
//           empty queue


/**
 * A class that implements a queue of Objects.
 */
public class Queue2<E>
{
	// instance vars
	private Node<E> front ;		// pointer to front of queue
	private Node<E> rear ;		// pointer to rear of queue

	/**
	 * Creates an empty queue.
	 */
	public Queue2()  
	{
		front = rear = null ;	// (not necessary, done in declaration)
	}
	
	/**
	 * Is the queue empty?
	 * @return true if the queue is empty, false if nonempty
	 */
	public boolean isEmpty()
	{
		return front == null ;
		// when front is null, rear is also null (but needn't test both)		
	}
	
	/**
	 * Append an object to the end of the queue.
	 * @param x the object to be appended
	 */
	public void append(E x)
	{
		Node<E> temp = new Node<E>(x) ;
		if ( isEmpty() )				// if queue is empty...
		{
			front = rear = temp ;		// ...x becomes only object in queue
		}
		else							// if queue nonempty...
		{
			rear.next = temp ;		// ...append object to end of queue
			rear = rear.next ;		// ...and update rear pointer
		}
	}

	/**
	 * Remove the object at the front of the queue and return it
	 * @return the object at the front of the queue
	 *
	 * Precondition:  the queue is NOT empty
	 **/
	public E serve()
	{
		E saved = front.info ;		// save object at the front of queue
		front = front.next ;		// delete it from queue
		return saved ;				// return it
	}
	
	public String toString()
	{
		if ( isEmpty() )
		  return "queue is currently empty!" ;
		String out = "" ;
		Node<E> temp = front ;
		while (temp != null)
		{
			out = out + temp.info + "  " ;
			temp = temp.next ;
		}
		return out ;
	}
}