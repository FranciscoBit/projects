class Node<E> 		// note: "package" access (no specifier)
	{					
		E info ;		// each node stores an object of a class to be
						// determined later...
		Node next ;		// ...and a pointer to another node
		
		// Create a Node object 
		Node (E x)		// constructor takes one param of class E
		{			
		  info = x ;	// set info portion to parameter passed
		  next = null ;	// not necessary, null is default value
		}
	} // end of Node class definition ======================================