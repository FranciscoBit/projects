class Library
{
	public static void main(String[] arg)
	{
		// Create a book list object.
		BookList books = new BookList();

		// Create booknodes and add them to the list.
		books.add(new Book("Danger on the Waters"));
		books.add( new Book("Paradise Lost"));
		books.add( new Book("Building Bridges"));
		books.add( new Book("Hall Mark of Fame"));

		System.out.print(books);
	}
}