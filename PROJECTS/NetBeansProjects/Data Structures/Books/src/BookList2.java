import java.util.LinkedList;

// AAPEND USING IF/ELSE
class BookList2
{
	private BookNode list, rear;

	BookList2() // This constructor creates an empty list.
	{
		list = null;
	}

	void append_2_Refs(Book b)// appending
	{
		BookNode temp = new BookNode(b);

		if (list == null)
		{
			list = temp;
			rear = temp;
		}
		else
		{
			rear.next = temp;
			rear = temp;
		}
	}
	void append_1_Ref(Book b)// appending
	{
		BookNode temp = new BookNode(b);

		if (list == null)
			list = temp;
		else
		{
			BookNode aux = list;

			while (aux.next != null)
				aux = aux.next;

			aux.next = temp;
		}
	}
	public String toString()
	{
		String result = "";
		BookNode current = list;

		while (current != null)
		{
			result += current.book.getTitle() + "\n";
			current = current.next;
		}
		return result;
	}
}