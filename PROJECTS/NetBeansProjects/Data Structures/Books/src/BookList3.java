// INSERT IN ORDER
class BookList3 // Create an ordered list of books
{
	private BookNode list;

	// This constructor creates an empty list.
	BookList3()
	{
		list = null;
	}

	void add(Book b)// In alphabetical order
	{
		BookNode temp = new BookNode(b);

		if (list == null)
			list = temp;
		else
		{
			BookNode aux = list;
			BookNode back = null;
			boolean found = false;

			while(aux != null && !found)
				if( temp.book.getTitle().compareTo(aux.book.getTitle()) < 0 )
					found = true;
				else
				{
					back = aux;
					aux = aux.next;
				}

		 	temp.next = aux;
		 	if (back == null)
		 		list = temp;
		 	else
		 		back.next = temp;
		}
	}
	public String toString()
	{
		String result = "";

		BookNode current = list;

		while (current != null)
		{
			result += current.book.getTitle() + "\n";
			current = current.next;
		}

		return result;
	}
}