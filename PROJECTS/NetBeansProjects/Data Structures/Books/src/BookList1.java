// AAPEND USING LOOP
class BookList1
{
	private BookNode list;

	// This constructor creates an empty list.

	BookList1()
	{
		list = null;
	}

	void add(Book b) // Appending to the list
	{
		BookNode temp = new BookNode(b);

		if (list == null)
			list = temp;

		else
		{
			BookNode aux = list;

			while(aux.next != null)
					aux = aux.next;

		 	aux.next = temp;

		}// End of the else

	}

	public String toString()
	{
		String result = "";
		int count = 0;
		BookNode current = list;

		while (current != null)
		{
			count++;
			result += current.getBook().getTitle() + "\n";
			current = current.next;
		}

		return result + "\nThe number of books is : " + count;
	}
}