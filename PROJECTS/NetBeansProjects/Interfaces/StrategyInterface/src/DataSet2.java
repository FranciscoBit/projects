//	New version of DataSet class depends on the Measurer interface.

/**
   Computes the average and maximum of a set of data values.
*/
public class DataSet2
{
   private double sum ;		// sum of a set of objects
   private Object maximum ;	// points to largest object
   private int count ;		// number of objects measured
   private Measurer measurer ;	// an object of a class that implements Measurer
   												// type
   /**
      Constructs an empty data set with a given measurer
      @param aMeasurer the measurer that is used to measure data values
   */
   public DataSet2(Measurer aMeasurer)
   {
      sum = 0;
      count = 0;
      maximum = null;
      measurer = aMeasurer;
   }

   /**
      Adds a data value to the set
      @param x object with data to be added
   */
   public void add(Object x)
   {
      sum = sum + measurer.measure(x);
      if ( count == 0 || 
      	  measurer.measure(maximum) < measurer.measure(x) )
         maximum = x;
      count++;
   }

   /**
      Gets the average of the added data.
      @return the average or 0 if no data has been added
   */
   public double getAverage()
   {
      if (count == 0) return 0;
      else return sum / count;
   }

   /**
      Gets the largest of the added data.
      @return the maximum or 0 if no data has been added
   */
   public Object getMaximum()
   {
      return maximum;
   }
}