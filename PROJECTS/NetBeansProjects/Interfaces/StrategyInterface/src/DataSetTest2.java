import java.awt.Rectangle;

/**
   This program demonstrates the use of a Measurer.
*/
public class DataSetTest2
{
   public static void main(String[] args)
   {
      class RectangleMeasurer implements Measurer
      {
         public double measure(Object anObject)
         {
            Rectangle aRectangle = (Rectangle)anObject;
            double area = aRectangle.getWidth() * aRectangle.getHeight();
            return area;
         }
      }

      Measurer m = new RectangleMeasurer();

      DataSet2 data = new DataSet2(m);

      data.add(new Rectangle(5, 10, 20, 30));
      data.add(new Rectangle(10, 20, 30, 40));
      data.add(new Rectangle(20, 30, 5, 10));

      System.out.println("Average area = " + data.getAverage());
      Rectangle max = (Rectangle)data.getMaximum();
      System.out.println("Maximum area = " + max);
   
      class RationalMeasurer implements Measurer
      {
         public double measure(Object anObject)
         {
            Rational aRat = (Rational)anObject;
            double realValue 
               = aRat.toDecimal() ;
            return realValue ;
         }
      }

      Measurer r = new RationalMeasurer() ;

      data = new DataSet2(r) ;

      data.add(new Rational(3,7)) ;
      data.add(new Rational(-4,9)) ;
      data.add(new Rational(2,3)) ;

      System.out.println("\nAverage of the rationals = " + data.getAverage()) ;
      Rational ratMax = (Rational)data.getMaximum() ;
      System.out.println("Largest rational = " + ratMax) ;
   }
}

/* program output
Average area = 616.6666666666666
Maximum area = java.awt.Rectangle[x=10,y=20,width=30,height=40]

Average of the rationals = 0.2169312169312169
Largest rational = 2/3

*/