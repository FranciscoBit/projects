public interface Constants
{
    public static final double MANAGER_TAX      = 1.0/5;
    static final double REGULAR_TAX             = 1.0/6;
    final double RATE                           = 25;
	double RATE2                           		= 25;

    double OVERTIME_RATE  = 1.5;
    static final double OVERTIME_TAX            = 1.0/8;
    static final double FORTY                   = 40;

    public abstract void calculate();
    void  display();
}