public class RegularPay extends Wages
{
    RegularPay(double hours)
    {
        super(hours);
    }

    public void calculate()
    {
        gross = amount * Constants.RATE;
        deduction = gross * Constants.REGULAR_TAX;
        netPay = gross - deduction;
    }
}