class Quadratic{
	double a, b, c,	discriminant, root1, root2;
	String s;

	Quadratic(double x[]){
		s = "";
		a = x[0];
		b = x[1];
		c = x[2];
	}
	boolean isZero() // If a = 0
	{
		try{
			if ( a == 0.0)
			 	throw new QuadraticException("Coefficient of x^2 is zero, not a quadratic");
			return false;
		}
		catch(QuadraticException e)
		{
			s = s + e.toString();
			return true;
		}
	}
	void calculateDiscriminant()
	{
		    discriminant = Math.pow(b, 2) - 4 * a * c;
			try{
				if (discriminant < 0)
					throw new QuadraticException("No real root", a, b, discriminant);
				calculateDoubleRoots();
				s = s + "Real roots are " + root1 + "\tand " + root2;
			}
			catch(QuadraticException e)
			{
				s = e.toString() + ": " + e.imaginaryRoots();
			}
	}
	void calculateSingleRoot()
	{
		s = "Single root " +(-b / (2 * a));
	}
	void calculateDoubleRoots()
	{
		root1 = (-b + Math.sqrt(discriminant))/(2 * a);
		root2 = (-b - Math.sqrt(discriminant))/(2 * a);
	}
	boolean singleRoot(){
		return discriminant == 0;
	}
	public String toString(){
		return s;
	}
}