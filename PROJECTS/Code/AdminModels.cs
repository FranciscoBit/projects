﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectParkE.Models
{
    public class ViewModel
    {

    }



    public class Admin_Model_Sensor
    {
        public string Device_ID { get; set; }
        public string Device_Key { get; set; }
        public string Company_Name { get; set; }
        public string Company_UniqueToken { get; set; }
        public string Device_State { get; set; }
        public string Device_Time { get; set; }
        public string Device_Date { get; set; }
    }
}