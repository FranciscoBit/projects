/*  Improved version of the Measurable interface is an example of a "strategy
 	interface."  The classes being measured now do not have to be modified in 
 	any way, so we can measure classes beyond our control (e.g., classes from
 	Java library or any other classes where we do not have access to the code).
 	
 	This is done by having a "3rd party" class (declared as an inner class)
 	realize the interface, instead of the actual class being measured.  In the
 	realizing class' implementation of method "measure", the Object parameter
 	is "downcast" to the actual class of the objects being measured, and (as
 	before) the kind of measurement used varies from class to class.
 	
 	See class DataSetTest which measures Rectangle objects by area and Rational
 	objects by their decimal value.

/**
   Describes any class whose objects can measure other objects.
*/
public interface Measurer
{
   /**
      Computes the measure of an object.
      @param anObject the object to be measured
      @return the measure
   */
   double measure(Object anObject) ;
}