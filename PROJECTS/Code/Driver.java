import java.util.ArrayList;
/**
   This class tests primarily the Person, Undergraduate, Graduate, and Instructor classes
*/
public class Driver
{
   public static void main(String[] args)
   {
      ArrayList <Person>list = new ArrayList<Person>();
     
       Person p = new Undergraduate("Perry", 1959, "CS");
      /*
      list.add(new Person("Perry", 1959));
      list.add(new Undergraduate("Sylvia", 1979, "Computer Science"));
      list.add(new Graduate("James", 2003, "Computer Science", "Graphical User Interface")); 
      list.add(new Instructor("Edgar", 1969, 65000));
       
       */
      //print(list);
   }
        public static void print( ArrayList list)
   {
	int length = list.size();
		
	for (int i = 0; i < length; i++)
	{
	   Person o = (Person)list.get(i);
           //  if (o instanceof Undergraduate)
          //     System.out.println(o);
          
        //}
        
	if ( o instanceof Graduate)
           System.out.println("The individual is a " + o + "\n");
         else if (o instanceof Undergraduate)
            System.out.println("The individual is a   " + o + "\n");
	else if (o instanceof Person)
            System.out.println("The individual is a   " + o + "\n");
	else if (o instanceof Instructor)
		System.out.println("The individual is a   " + o + "\n");
	else
            System.out.println("Unknown individual.. .  .   .    .\n");
        }
	
   }
}