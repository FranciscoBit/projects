// File: ListIteratorDemo.java

// Shows List method listIterator(), and ListIterator methods hasNext(), 
// next(), hasPrevious(), previous(), and remove()

// Also shows generic "linked list of integer" and "list iterator of integer"   

import java.util.* ;			// for LinkedList and Random

public class ListIteratorDemo 
{
  public static void printList( List<Integer> list ) 
  {
	  for (int i = 0 ; i < list.size() ; i ++)     // for each item on list...
	  {   	
         System.out.print (list.get(i) + "  ") ;   // ...print item
      }
      System.out.println('\n') ; 
   }

   public static void removeOdds( List<Integer> list ) 
   {
   	  // get iterator pointing just before start of list
	  ListIterator<Integer> it = list.listIterator() ; 
        
      System.out.println("removing odd integers...") ;
 
      // iterate through list...
      while ( it.hasNext() ) 			// while more items on list...
      {
         int number = it.next() ;       // get next item
         if ( number % 2 == 1 )  		// ...if it's odd...
           it.remove() ;				// ......delete it
  	  }
   }

   // in this method, linked list and iterator are NOT generic Integer class
   public static void printReversed( List list ) 
   {
   	  // get iterator pointing just past end of list
	  ListIterator it = list.listIterator( list.size() ) ; 
        
      System.out.println("printing list in reverse order...") ;
 
      // move backwards to start of list
      while ( it.hasPrevious() ) 
      {
         System.out.print( it.previous() + "  " ) ;
  	  }
  	  System.out.println() ;
   }
   
   public static void main( String args[] ) 
   {
      LinkedList<Integer> list = new LinkedList<Integer>() ; 		
	  
	  Random r = new Random() ;
	  
	  // add 10 positive 2-digit random ints to list
	  for (int i = 1 ; i <= 10 ; i++) 
	  {
	  	int num = r.nextInt(90) + 10 ;
	  	list.add(num) ;							
	  }
	  
	  System.out.println("printing original list...") ;
      printList(list) ;
 	  
      // use Iterator to remove all odd ints
      removeOdds(list) ;
	  
	  System.out.println("\nprinting updated list...") ;
      printList(list) ;
	  
	  // use Iterator to print list in reverse order
      printReversed(list) ;
   }                                           
}

/*  program output

printing original list...
59  33  98  34  82  75  35  96  69  15

removing odd integers...

printing updated list...
98  34  82  96

printing list in reverse order...
96  82  34  98

*/