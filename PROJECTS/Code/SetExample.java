/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dragon
 */


import java.util.*;


public class SetExample {
    
    public static void main(String[] args)
   {
       String[] group1 = {"a", "b"};
       String[] group2 = {"c", "d"};
       String[] group3 = {"e", "f"};
       
       
       String str = "";
       
       // Create 1st set and add elements
       Set set1 = new HashSet();
       add(group1, set1);
       Iterator i = set1.iterator();
       System.out.println("HashSet set1: " + display(i));
       
       // Create 2nd set and add elements
       Set set2 = new HashSet();
       add(group2, set2);
       i = set2.iterator();
       System.out.println("HashSet sets: " + display(i));
       
       
       // Declare 3rd set
       Set set3 = set1;
       i = set3.iterator();
       System.out.println("Assign set3 to adress of set1: " + display(i));
       
       
       set2.retainAll(set3);    //intersection
       i = set2.iterator();
       System.out.println("set2 = set2 intersect set3: " + display(i));
       
       // Create 4rd set and add elements
       Set set4 = new HashSet();
       add(group3, set4);
       i = set4.iterator();
       System.out.println("Add elements to set4: " + display(i));
       
       set4.addAll(set3);    //union
       i = set4.iterator();
       System.out.println("set4 = set4 union set3: " + display(i));
       
       
       // Create a sorted set and sort set4
       Set sortedSet = new TreeSet(set4);
       i = sortedSet.iterator();
       System.out.println("set4 sorted: " + display(i));
       
       set4.removeAll(set2);    //find difference
       i = set4.iterator();
       System.out.println("set4 - set2: " + display(i));
       
       
       // Create a vector and add elements
       Vector v = new Vector ();
       add(group1, v);
       i = v.iterator();
       System.out.println("Vector: " + display(i));
       
       
       // Create stack and add elements
       Stack s = new Stack();
       for(int j = 0; j < group1.length; j++)
       {
           s.push(group1[j]);
       }
       
       
       // Display elements in the stack
       while(!s.empty())
       {
           str = str + s.pop() + "\n";
       }
       System.out.println("Stack:\n " + str);
       
   }
    
    static void add(Object[] names, Collection s)
    {
        for (int i = 0; i < names.length; i++)
        {
            s.add(names[i]);
        }
    }
    
    
    
    
    static String display (Iterator i)
    {
        if(!i.hasNext())
        {
            return "";
        }
        else
        {
            return i.next() + ", " + display(i);
        }
    }
    
    
    
}
