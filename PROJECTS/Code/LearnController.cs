﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using ProjectParkE.Global;

namespace ProjectParkE.Controllers
{
    public class LearnController : Controller
    {

        // GET: Learn/About
        public ActionResult Overview()
        {
            ViewBag.PageTitle = "Overview" + " - ParkE";

            return View();
        }

        // GET: Learn/About
        public ActionResult About()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_About() + " - ParkE";

            return View();
        }

        // GET: Learn/Services
        public ActionResult Services()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Services() + " - ParkE";

            return View();
        }

        // GET: Learn/Help
        public ActionResult Help()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Help() + " - ParkE";

            return View();
        }

        // GET: Learn/Help
        public ActionResult Download()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Download() + " - ParkE";

            return View();
        }

        // GET: Learn/Services_Sensors
        public ActionResult Services_Sensors()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Services_Sensors() + " - ParkE";

            return View();
        }

        // GET: Learn/Services_Management
        public ActionResult Services_Management()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Services_Management() + " - ParkE";

            return View();
        }

        // GET: Learn/Services_Spaces
        public ActionResult Services_Spaces()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Services_Spaces() + " - ParkE";

            return View();
        }



        // GET: Learn/Privacy
        public ActionResult Privacy()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Privacy() + " - ParkE";

            return View();
        }

        // GET: Learn/Terms
        public ActionResult Terms()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Terms() + " - ParkE";

            return View();
        }

        // GET: Learn/Blog
        public ActionResult Blog()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Blog() + " - ParkE";

            return View();
        }

        // GET: Learn/Businesses
        public ActionResult Businesses()
        {
            ViewBag.PageTitle = Global.Constants.LandingPage.OtherPages_Businesses() + " - ParkE";

            return View();
        }

    }
}