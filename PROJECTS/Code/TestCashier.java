import java.text.NumberFormat;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.util.Date;
import java.text.DateFormat;

class TestCashier
{
	public static void main(String[] arg)
	{
        Cashier c = new Cashier();
		Date d = new Date();
		DateFormat df = DateFormat.getDateInstance();
		String s = df.format(d);
		
       String name = GetData.getWord("Enter name of item");
        double price = GetData.getDouble("Enter price of item");
		c.add(name, price);

//        name = GetData.getWord("Enter name of item");
//        price = GetData.getDouble("Enter price of item");
//		c.add(name, price);
//
//        name = GetData.getWord("Enter name of item");
//        price = GetData.getDouble("Enter price of item");
//		c.add(name, price);
//
//        name = GetData.getWord("Enter name of item");
//        price = GetData.getDouble("Enter price of item");
//		c.add(name, price);

        c.average();


        double amount = GetData.getDouble("Enter money used for Payment");

        c.tendered(amount);
		c.makeChange();

        generateReceipt(c, s);
	}
       static void generateReceipt(Cashier c, String s)
       {
                NumberFormat f = NumberFormat.getCurrencyInstance();
                String g = "\tJava Foundation Store\n\tToday is: " + s + "\n";
                g = g + c.getList() + "__________\nTotal........." +
                f.format(c.getTotal())+"\n\nThe number of" +
                " items purchased is " + c.getItemNumber() + " items\nThe average " +
                "price per item is " + f.format(c.getAverage()) + "\n\nAmount tendered is "
                + f.format(c.getTender()) + "\nThe change is " + f.format(c.getChange()) +
                "\n\nThe change includes:\n" + c.getDollar() + " Dollar(s)\n" +
                c.getQuarter() + " Quarter(s)\n" + c.getDime() + " Dime(s)\n" +
                c.getNickel() + " Nickel(s)\n" + c.getPenny() + " Penn(ies)";
				
				JTextArea text= new JTextArea(g, 20, 25);
                JScrollPane pane= new JScrollPane(text);
                JOptionPane.showMessageDialog(null, pane, "Receipt",
                JOptionPane.INFORMATION_MESSAGE);
       }
}