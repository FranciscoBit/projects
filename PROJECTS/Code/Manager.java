public class Manager extends Wages
{
    public Manager(double amount)
    {
      super(amount);
    }

   public void calculate()
    {
         gross = amount;
         deduction = gross * Constants.MANAGER_TAX;
         netPay = gross - deduction;
    }
}