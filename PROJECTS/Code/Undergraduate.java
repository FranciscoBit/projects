/**
   A student is represented by the name, birth year, and major
*/
public class Undergraduate extends Person
{
   private String major;
   /**
      Construct a Student object
      @param n the name of the student
      @param byear the birth year
      @param m the major
   */
   public Undergraduate(String n, int byear, String m)
   {
      super(n, byear);
      major = m;
   }

   /**
      Print the string representation of the object
      @return a string representation of the object
   */
   public String toString()
   {
      return "Undergraduate[super=" + super.toString() + "\tmajor=" + major + "]";
   }
}