// File: QueueDemo2.java

// Test class for Queue2.java 

import java.util.Random ;

public class QueueDemo2
{
	public static void main(String [] args)
	{
		Queue2<Integer> q = new Queue2<Integer>() ;		// a queue of ints
		
		Random r = new Random() ;

		// append 4 random positive 2-digit ints to the queue
		for (int i = 1 ; i <= 4 ; i++)	
		{
			int next = r.nextInt(90) + 10 ;
			System.out.print("Now Appending " + next) ;
			q.append ( next ) ;	
			System.out.println("\tUpdated queue: " + q) ;
		}

		// serve the queue 2 times
		for (int i = 1 ; i <= 2 ; i++)	
		{
			System.out.print("Item served: " + q.serve() ) ;
		    System.out.println("\t\tUpdated queue: " + q) ;
		}

		// append another int
		int next = r.nextInt(90) + 10 ;
		System.out.print("Now appending " + next) ;
		q.append ( next ) ;
		System.out.println("\tUpdated queue: " + q) ;
		
		// attempt to serve the queue 5 times
		for (int i = 1 ; i <= 5 ; i++)	
		{
			if ( ! q.isEmpty() )
			{
				System.out.print("Item served: " + q.serve() ) ;
		    	System.out.println("\t\tUpdated queue: " + q) ;
		    }
		    else
		    {
		    	System.out.println("ERROR! Attempt to serve empty queue!") ;
		    }
		}
	}
}

/*  Sample output

Now Appending 65    Updated queue: 65  
Now Appending 95    Updated queue: 65  95  
Now Appending 41    Updated queue: 65  95  41  
Now Appending 84    Updated queue: 65  95  41  84  
Item served: 65     Updated queue: 95  41  84  
Item served: 95     Updated queue: 41  84  
Now appending 35    Updated queue: 41  84  35  
Item served: 41     Updated queue: 84  35  
Item served: 84     Updated queue: 35  
Item served: 35     Updated queue: queue is currently empty!
ERROR! Attempt to serve empty queue!
ERROR! Attempt to serve empty queue!

*/