// File: CollectionDemo.java

// Shows List methods add(), size(), get(), and set(), and Collection
// methods iterator(), hasNext(), next(), and remove()

import java.util.*;

public class CollectionDemo 
{
   public static double addAndDelete( Collection c ) 
   {
      // return sum of all doubles in a collection and remove them
      
   	  double sum = 0 ;
   	  
      Iterator i = c.iterator() ;    		// get iterator for collection c

      while ( i.hasNext() ) 				// more items in the collection...
      {       				
         Object temp = i.next() ;			// ... get next object
         if ( temp instanceof Double ) 		// ... if it's a Double...
         {			
         	sum += (Double)temp ;			// ...downcast and add to sum
            i.remove() ;            		// ...and delete it 
         }
      } 
      
      return sum ;        
   }

   public static void main( String args[] ) 
   {
      String colors[] = { "yellow", "cyan", "magenta" } ;
                  
      ArrayList aList = new ArrayList() ; 		// creates an empty ArrayList

      aList.add( 1.23 ) ;     					// add a double to end of aList

      // add contents of array colors[] to end of aList
      for (int k = 0 ; k < colors.length ; k++)	
         aList.add( colors[ k ] ) ;         

      // add another double as new fourth item
      aList.add( 3, 2.34 ) ;        		

      System.out.println( "\nprinting aList:\n" ) ;	 
      for ( int k = 0 ; k < aList.size() ; k++ )	
         System.out.print( aList.get( k ) + " " ) ;	

      double sum = addAndDelete( aList ) ;
      
      System.out.println( "\n\nThe sum of all doubles in aList = " + sum ) ;

      System.out.println( "\nprinting aList after addAndDelete:\n" ) ;
      for ( int k = 0 ; k < aList.size() ; k++ )
         System.out.print( aList.get( k ) + " " ) ;
      System.out.println() ;
   }                                           
}

/*  program output:

printing aList:

1.23 yellow cyan 2.34 magenta

The sum of all doubles in aList = 3.57

printing aList after addAndDelete:

yellow cyan magenta

*/