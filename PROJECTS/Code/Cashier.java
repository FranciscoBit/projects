class Cashier
{
    private String name, s;
    private int itemCount, dollar, quarter, dime, nickel, penny, change;
    private double totalPrice, average, tendered,  y;

    private static final int DOLLAR = 100, QUARTER = 25, DIME = 10, NICKEL = 5;

    Cashier()
    {
        name = "";
        s = "";
    }

    void add(String m, double p)
    {
        s = s + m + "........" + p + "\n";
        totalPrice = totalPrice + p;
        itemCount = itemCount + 1;
    }

    void average()
    {
        average = totalPrice / itemCount;
    }

    void tendered(double a)
    {
        tendered = a;
    }

    void makeChange()
    {
        change = (int)Math.round((tendered - totalPrice)*DOLLAR);

        dollar = (change/DOLLAR);
        change = change%DOLLAR;

        quarter = change/QUARTER;
        change = change%QUARTER;

        dime = change/DIME;
        change = change%DIME;

        nickel = change/NICKEL;

        penny =  change%NICKEL;
    }

    double getTender()
    {
        return tendered;
    }

    double getChange()
    {
        return change;
    }

    double getAverage()
    {
        return average;
    }

    int getDollar()
    {
        return dollar;
    }

    int getQuarter()
    {
        return quarter;
    }

    int getDime()
    {
        return dime;
    }

    int getNickel()
    {
        return nickel;
    }

    int getPenny()
    {
        return penny;
    }

    double getTotal()
    {
        return totalPrice;
    }

    int getItemNumber()
    {
        return itemCount;
    }
    String getList()
    {
        return s;
    }
}