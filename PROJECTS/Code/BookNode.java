/*
 *The book node class - used to make the list.

*/

class BookNode
{
	Book book; // Information/data
	BookNode next; // Link field

	BookNode(Book b)
	{
		book = b;
		next = null;
	}

}