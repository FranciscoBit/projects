import java.util.Iterator;

/**
   A test class for the HashSet class.
*/
public class HashSetTest
{
   public static void main(String[] args)
   {
      HashSet names = new HashSet(101);  // 101 is a prime

      names.add("Sue");
      names.add("Harry");
      names.add("Sue");         // not added since already in the set
      names.add("Nina");
      names.add("Susannah");
      names.add("Larry");
      names.add("Eve");
      names.add("Sarah");
      names.add("Adam");
      names.add("Tony");
      names.add("Katherine");
      names.add("Juliet");
      names.add("Romeo");
      names.remove("Romeo");
      names.remove("George");   // not in set so ignored
      
      // print the set using the iterator
      System.out.println("The set:");
      Iterator iter = names.iterator();
      while (iter.hasNext())
         System.out.println(iter.next());
      System.out.println("\nThere are " + names.size() + 
                         " objects in the set\n");
      
      // test the contains() method
      if (names.contains("Juliet"))
         System.out.println("Juliet is in the set");
      else
         System.out.println("Juliet is not in the set");

      if (names.contains("Romeo"))
         System.out.println("Romeo is in the set");
      else
         System.out.println("Romeo is not in the set");
   }
}

/*    Program Output:
The set:
Harry
Sue
Nina
Susannah
Larry
Eve
Sarah
Adam
Juliet
Katherine
Tony

There are 11 objects in the set

Juliet is in the set
Romeo is not in the set
*/