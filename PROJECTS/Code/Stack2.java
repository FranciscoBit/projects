//  File: Stack2.java		

//  Version 2 of the generic Stack class.  Implementation has been changed
//  to a linked list. The Node class is defined in Node.java

//  WARNING: an attempt to pop or to peek an empty stack will throw a 
//           NullPointerException.


/**
 * A generic stack class
 */
public class Stack2<E>   
{
	private Node<E> top ;	// points to top of stack

	/**
	 * Create an empty stack (not really necessary)
	 */
	public Stack2() 
	{					
		top = null ;
	}
	
	/**
	 * Is the stack empty?
	 * @return true if stack is empty, false if non-empty
	 */
	public boolean isEmpty()
	{
		return top == null ;
	}
	
	/**
	 * Place an item on top of the stack.
	 * @param x the object to be pushed
	 */
	public void push (E x)
	{
		Node<E> newNode = new Node<E>(x) ;
		newNode.next = top ;
		top = newNode ;
	}
	
	/**
	 * Pop (i.e., remove) the item at the top of the stack and return it.
	 * @return a reference to the object on top of the stack.
	 * Precondition: the stack is NOT empty.
	 */
	public E pop()
	{
		E saved = top.info ;
		top = top.next ;
		return saved ;
	}

	/**
	 * Return the item on top of the stack but do not pop it.
	 * @return a reference to the object on top of the stack.
	 * Precondition: the stack is NOT empty.
	 */
	public E peek()
	{
		return top.info ;
	}
}	