// File: RecursionExamples.java

// Class contains a number of unrelated reecursive methods and a main method
// that tests them all.

import java.util.Scanner ;

public class RecursionExamples 
{
  public static void PrintReversed (int n) 
  {
	if (n < 0)		// error
	  System.out.println("Sorry, number must be non-negative!") ;
	else if (n <= 9)				// single-digit integer
	  System.out.print(n) ;
	else
	{
	  System.out.print( n % 10 ) ;	// print last digit
	  PrintReversed ( n/10 ) ;		// remove last digit and print what's
	}								// left, recursively
  }

  public static int Fibonacci (int n) 
  {
	if (n <= 0)		// error, so return bogus value
	{					
	  System.out.println("Sorry, number must be positive!") ;
	  return -999 ;
	}		
	else if (n == 1 || n ==2)		// first 2 terms are 1's
	  return 1 ;
	else							// other terms = sum of 2 previous terms
	  return Fibonacci(n-2) + Fibonacci(n-1) ;
  }

  public static void PrintReversed ( int a[], int count ) 
  {
	if (count == 1)								// single-element array
	  System.out.print( a[0] ) ;				// ...print it
	else
	{
	  System.out.print( a[count-1] + "  " ) ;	// print last element...
	  PrintReversed (a, count-1) ;				// ...print remaining elements
	}
  }

  public static void PrintInOrder ( int a[], int count ) 
  {
	if (count == 1)								// single-element array
	  System.out.print( a[0] + "  " ) ;			// ...print it
	else
	{
	  PrintInOrder(a, count-1);					// stack "shortened" array
	  System.out.print( a[count-1] + "  " ) ;
	}
  }

  
  public static boolean Equal ( int a[], int b[], int count ) 
  {
	if (count == 1)				// single-element arrays
	  return a[0] == b[0] ;
	else
	  return (a[count-1] == b[count-1] && Equal(a,b,count-1)) ;
	  //  arrays are equal if last elements are equal and corresponding
	  //  elements 0 thru next-to-last are equal
  }

  public static void main(String args[]) 
  {
	//  Tests the recursive methods
	
	Scanner input = new Scanner(System.in) ;

	int number ;
	System.out.print( "Enter a positive integer: " ) ;
	number = input.nextInt() ;
	System.out.print( "The number " + number + " reversed is: " ) ;
	PrintReversed (number) ;
	System.out.println('\n') ;

	int nthTerm ;
	System.out.print( "Which term in fibonacci sequence? " ) ;
	nthTerm = input.nextInt() ;
	System.out.println( "The " + nthTerm + "th term is: " 
	                    + Fibonacci(nthTerm) + '\n' ) ;

	int a[] = { 2,5,8,10,41 } ;
	int b[] = { 2,5,8,10,41 } ;
	int count = a.length ;			// number of elements

	System.out.print( "Here's the array in reverse order: " ) ;
	PrintReversed(a,count) ;
	System.out.println( '\n' ) ;

	System.out.print( "Here's the array in original order: " ) ;
	PrintInOrder(a,count) ;
	System.out.println( '\n' ) ;

	if (Equal(a,b,count))
	  System.out.println( "The arrays are equal!\n" ) ;
	else
	  System.out.println( "The arrays are NOT equal!\n" ) ;
  }
}

/*  sample output

Enter a positive integer: 67890
The number 67890 reversed is: 09876

Which term in fibonacci sequence? 11
The 11th term is: 89

Here's the array in reverse order: 41  10  8  5  2

Here's the array in original order: 2  5  8  10  41

The arrays are equal!

*/