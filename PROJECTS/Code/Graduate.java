/**
   A graduate student is represented by the name, birth year, major, and thesis option
*/
public class Graduate extends Undergraduate
{
   private String thesis;
   /**
      Construct a Student object
      @param n the name of the student
      @param byear the birth year
      @param m the major
      @param thesis
   */
   public Graduate(String n, int byear, String m, String thesis)
   {
      super(n, byear, m);
      this.thesis = thesis;
   }

   /**
      Print the string representation of the object
      @return a string representation of the object
   */
   public String toString()
   {
      return "Graduate[super=" + super.toString() + ", Thesis = " + thesis + "]";
   }
}