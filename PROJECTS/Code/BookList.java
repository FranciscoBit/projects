class BookList
{
	private BookNode list;

	BookList() // This constructor creates an empty list.
	{
		list = null;
	}
	void add(Book b)// pre-pending
	{
		BookNode temp = new BookNode(b);
		try
		{
			temp.next = list;
			list = temp;
		}
		catch(NullPointerException e)
		{

		}
	}

	public String toString()
	{
		String result = "";
		BookNode current = list;

		while (current != null)
		{
			result += current.book.getTitle() + " \n";
			current = current.next;
		}
		return result;
	}
}