public class OvertimePay extends RegularPay
{
    double overtimeHours;

    public OvertimePay(double amount)
    {
        super(Constants.FORTY);
        overtimeHours = amount -  40;
    }
    public OvertimePay(double amount, String s)
    {
        this(amount);
        //super(Constants.FORTY);
        
    }
    public  void calculate()
    {
        super.calculate();
        gross = getGross() + overtimePay();
        deduction = getDeduction() + overtimePay() *  Constants.OVERTIME_TAX;
        netPay = gross - deduction;

        System.out.println("Gross overtime: " + super.getGross()  );
    }

     double overtimePay()
    {
        return overtimeHours *  Constants.OVERTIME_RATE *  Constants.RATE;
    }

    public String toString()
    {
    	return "";
    }
}