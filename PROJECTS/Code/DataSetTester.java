import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class DataSetTester
{
   public static void main(String[] args)
   {
      Scanner in = new Scanner(System.in);
      DataSetReader reader = new DataSetReader();
      
      boolean done = false;
      while (!done) 
      {
         try 
         {
            System.out.print("\nPlease enter the file name: ") ;
            String filename = in.next();
            
            double[] data = reader.readFile(filename);
            double sum = 0;
            for (double d : data) sum = sum + d; 
            System.out.println("The sum is " + sum);
            done = true;
         }
         catch (FileNotFoundException exception)
         {
            System.out.println("File not found.");
         }
         catch (BadDataException exception)
         {
            System.out.println("Bad data: " + exception.getMessage());
         }
         catch (IOException exception)
         {
            exception.printStackTrace();
         }
      }
   }
}

/*  sample output

Please enter the file name: bad1.dat
Bad data: Data value expected

Please enter the file name: bad2.dat
Bad data: Length expected

Please enter the file name: bad3.dat
Bad data: Data value expected

Please enter the file name: bad4.dat
Bad data: End of file expected

Please enter the file name: nonesuch.dat
File not found.

Please enter the file name: good.dat
The sum is 55.0

*/
