﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectParkE.Controllers
{
    public class AdminsController : Controller
    {
        // GET: Admins/Index
        public ActionResult Index()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_DASHBOARD() + " - ParkE";

            return View();
        }

        // GET: Admins/Company
        public ActionResult Company()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_COMPANY_PROFILE() + " - ParkE";

            return View();
        }

        // GET: Admins/Admin
        public ActionResult Admin()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_MY_PROFILE() + " - ParkE";

            return View();
        }

        // GET: Admins/Mod
        public ActionResult Mod()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_COMPANY_MODS() + " - ParkE";

            return View();
        }

        // GET: Admins/Sensor
        public ActionResult Sensor()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_COMPANY_SENSORS() + " - ParkE";

            return View();
        }

        // GET: Admins/Analytic
        public ActionResult Analytic()
        {
            ViewBag.PageTitle = Global.Constants.Menus_Sidebar_Admins.GLOBAL_SENSOR_ANALYTICS() + " - ParkE";

            return View();
        }

    }
}